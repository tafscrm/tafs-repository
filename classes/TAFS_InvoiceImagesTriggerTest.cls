@isTest
private class TAFS_InvoiceImagesTriggerTest{

    /************************************************************************************
    * Method       :    testAutopopulateInvoiceOnInvoiceImage
    * Description  :    Scenario to cover the updation of Invoice Id on the Invoice Images object
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testAutopopulateInvoiceOnInvoiceImage() {
       
     
       Test.startTest();
       
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
         
       TAFS_Invoice_Batch__c invBatch = new TAFS_Invoice_Batch__c(Name = 'Invoice Batch 143',TAFS_Client_ID__c = lstAcct[0].Id);
       insert invBatch;
       
       TAFS_Invoice__c inv = new TAFS_Invoice__c(Name = 'Invoice 123',TAFS_Invoice_Batch__c = invBatch.id ,TAFS_Invoice_key__c = '77445566');
       insert inv;
           
       TAFS_Invoice_Images__c invImage = new TAFS_Invoice_Images__c(Name = 'Inv Img 567',TAFS_Invoice_Key__c = 77445566);   
       insert invImage;
       
       Test.stopTest();
       
      
               
    }
}