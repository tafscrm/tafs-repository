public with sharing class TAFS_DocuSignRenegotiationUtility {
    
    public string agreementId{get;set;}
    private string receipientList = '';
    private decimal routingNo = 0;
    public General_Manager__c gm = General_Manager__c.getInstance('General Manager');
    
    public TAFS_DocuSignRenegotiationUtility(){
        agreementId = ApexPages.currentPage().getParameters().get('Id');
    }
    public PageReference sendToDocuSign(){
        List<Contact> contacts = new List<Contact>();
        TAFS_Agreement__c agreement;
        if(agreementId!=null){
            agreement = [Select TAFS_Client_Name__r.Name, TAFS_Client_Name__r.id, Owner.Name, Owner.Email from TAFS_Agreement__c where id =: agreementId];
            contacts = [Select Id,FirstName,LastName, Email from Contact WHERE TAFS_Contact_Type__c = 'Owner' AND Account.id =: agreement.TAFS_Client_Name__r.id];
        }
        string RC = '';string RSL='';string RSRO='';string RROS='';string CCRM='';string CCTM='';string CCNM='';string CRCL=''; string CRL='';string OCO='';string DST='';string LA='';string CEM='';string CES='';string STB='';string SSB='';string SES='';string SEM='';string SRS='';string SCS ='';string RES=''; 
        //DST='ab8269a7-474a-458d-8797-118ca95d8bc1'; 
        DST=''; 
        //Adding Notes & Attachments 
        LA='0'; 
        //Custom Recipient List 
        for(Contact con : contacts){
            routingNo = routingNo +1;
            receipientList = receipientList +'Email~'+con.Email+';FirstName~'+con.FirstName+';LastName~'+con.LastName+';Role~Signer'+routingNo+';RoutingOrder~1,';
        }
        routingNo = routingNo +1;
        receipientList = receipientList +'Email~'+gm.Email__c+';FirstName~'+gm.First_Name__c+';LastName~'+gm.Last_Name__c+';Role~Signer'+routingNo+';RoutingOrder~2,';
        
        CRL= receipientList + 'LoadDefaultContacts~0'; 
        //Custom Email Subject 
        CES='Please eSign Agreement for -'+agreement.TAFS_Client_Name__r.Name; 
        //Custom Email Message 
        CEM= 'Envelope sent by '+ agreement.Owner.Name+'(' + agreement.Owner.Email +')'+ ' for ' +agreement.TAFS_Client_Name__r.Name; 
        // Show Email Subject (default in config) 
        SES = '1'; //Ex: '1' 
        // Show Email Message (default in config) 
        SEM = '1'; //Ex: '1' 
        // Show Tag Button (default in config) 
        STB = '1'; //Ex: '1' 
        // Show Chatter (default in config) 
        SCS = '0'; //Ex: '1' 
        OCO = ''; 
        PageReference pageRef = null;
        pageRef = new PageReference('/apex/dsfs__DocuSign_CreateEnvelope?DSEID=0&SourceID='+agreementId+'&RC='+RC+'&RSL='+RSL+'&RSRO='+RSRO+'&RROS='+RROS+'&CCRM='+CCRM+'&CCTM='+CCTM+'&CRCL='+CRCL+'&CRL='+CRL+'&OCO='+OCO+'&DST='+DST+'&CCNM='+CCNM+'&LA='+LA+'&CEM='+CEM+'&CES='+CES+'&SRS='+SRS+'&STB='+STB+'&SSB='+SSB+'&SES='+SES+'&SEM='+SEM+'&SRS='+SRS+'&SCS='+SCS+'&RES='+RES);
        pageRef.setRedirect(true);    
        return pageRef;
   }
}