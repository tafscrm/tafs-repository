/* CLass Name : TAFS_Invoice_Trigger_Handler
 * Description  : Trigger methods for TAFS_Invoice_Trigger 
 * Created By   : Raushan Anand
 * Created On   : 26-Apr-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                26-Apr-2015              Initial version.
 *
 *****************************************************************************************/
public class TAFS_Transaction_Trigger_Handler {
    
    /****************************************************************************************
    * Method Name      :  updateStatusTypeOnInvoice
    * Input Parameter  :  List of Transaction (List<TAFS_Transaction__c>)
    * Return Type      :  Void
    * Description      :  This method converts the Type and Status value from Custom Settings.
                          Custom settings stores Key Value pair for each object.
    ***************************************************************************************/
    
    public static void updateStatusTypeOnInvoice(List<TAFS_Invoice_Transaction__c> transactionList){
        
        Map<String,String> typeKeyVal = new Map<String,String>();
        Map<String,String> statusKeyVal = new Map<String,String>();
        
        List<TAFS_Invoic_Key_Value_Mapping__c> keyValList = TAFS_Invoic_Key_Value_Mapping__c.getall().values();
        for(TAFS_Invoic_Key_Value_Mapping__c obj : keyValList){
            /*if(obj.TAFS_Object_Name__c == 'Invoice Transactions' && obj.TAFS_Field_Name__c == 'Type'){
                typeKeyVal.put(obj.TAFS_Cadence_Key__c,obj.TAFS_SFDC_Value__c);
            }*/
            if(obj.TAFS_Object_Name__c == 'Invoice Transactions' && obj.TAFS_Field_Name__c == 'Status'){
                statusKeyVal.put(obj.TAFS_Cadence_Key__c,obj.TAFS_SFDC_Value__c);
            }   
        }
        for(TAFS_Invoice_Transaction__c transObj : transactionList){
            if(transObj.TAFS_Type__c != null){
                if(typeKeyVal.containsKey(transObj.TAFS_Type__c)){
                    transObj.TAFS_Type__c = typeKeyVal.get(transObj.TAFS_Type__c);
                }
                /*else{
                    transObj.TAFS_Type__c = '';
                }*/
           }
           if(transObj.TAFS_Status__c != null){
                if(statusKeyVal.containsKey(transObj.TAFS_Status__c)){
                    transObj.TAFS_Status__c = statusKeyVal.get(transObj.TAFS_Status__c);
                }
                /*else{
                    transObj.TAFS_Status__c = '';
                }*/
           }
        }
   }
    
    /****************************************************************************************
    * Method Name      :  updateBatchIdOnInvoice
    * Input Parameter  :  List of Transaction (List<TAFS_Transaction__c>)
    * Return Type      :  Void
    * Description      :  This method populates Invoice Id based upon Invoice No.
    ***************************************************************************************/
    
    public static void updateInvoiceIdOnTransaction(List<TAFS_Invoice_Transaction__c> transactionList){
        List<String> invoiceKeyList = new List<String>();
        Map<String,String> invoiceNoIdMap = new Map<String,String>();
        
        for(TAFS_Invoice_Transaction__c transObj : transactionList){
            if(transObj.TAFS_Invoice_key__c!= null){
                invoiceKeyList.add(transObj.TAFS_Invoice_key__c);
            }
        }
        List<TAFS_Invoice__c> invoiceList = [SELECT Id,Name,TAFS_Invoice_key__c FROM TAFS_Invoice__c WHERE TAFS_Invoice_key__c IN: invoiceKeyList];
        
        for(TAFS_Invoice__c invObj : invoiceList){
            invoiceNoIdMap.put(invObj.TAFS_Invoice_key__c,invObj.Id);
        }
        for(TAFS_Invoice_Transaction__c transObj : transactionList){
            if(invoiceNoIdMap.containsKey(transObj.TAFS_Invoice_key__c)){
                transObj.TAFS_Invoice__c = invoiceNoIdMap.get(transObj.TAFS_Invoice_key__c);
            }
        }
    }

}