/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  11/04/2016
* Description     :  Service class for the fuel consumption page
* Modification Log:  Initial version.
***************************************************************************************/
public class TAFS_FuelConsumptionService{

    public class FuelConsumptionServiceException extends Exception {}
    
    public class FCStub
    {
        
        public Account accountObj {get;set;}                    //this is the parent client Id
        
        public FCStub(Account accountObj){            
            
            this.accountObj = accountObj;
        }
    }
    
    /************************************************************************************
    *�Method�������: ���updateAccount
    *�Description��:��� Method to update the account record based on the child FC records.
    *�Parameter��� :��� List<fcStub>����
    * Return�Type��:��� void
    *************************************************************************************/
    public static void updateAccount(List<FCStub> fcStubList){
        
        List<TAFS_Fuel_Consumptions__c> fcList =  new List<TAFS_Fuel_Consumptions__c>();
        List<Account> accList =  new List<Account>();
        set<Id> accIdset =  new set<Id>();
        Map<Id,List<TAFS_Fuel_Consumptions__c>> accountFCMap =  new Map<Id,List<TAFS_Fuel_Consumptions__c>>();
        Map<Id,List<TAFS_Fuel_Consumptions__c>> accountFCLMMap =  new Map<Id,List<TAFS_Fuel_Consumptions__c>>();
        Map<Id,List<Fuel_Discount__c>> accountFDLiterMap = new Map<Id,List<Fuel_Discount__c>>();
        Map<Id,List<Fuel_Discount__c>> accountFDGallonMap = new Map<Id,List<Fuel_Discount__c>>();
        Map<Id,List<TAFS_Fuel_Commitment__c>> accFuelCommitmentMap =  new Map<Id,List<TAFS_Fuel_Commitment__c>>();
        //added by arpitha
        Map<Id,List<TAFS_Fuel_Consumptions__c>> accountFCLMForLifetimeMap =  new Map<Id,List<TAFS_Fuel_Consumptions__c>>();
        Map<Id,List<Fuel_Discount__c>> accountFDGallonForLifetimeMap = new Map<Id,List<Fuel_Discount__c>>();
        
        for(FCStub fcstub : fcStubList){
        
            if(fcstub.accountObj.Id !=null)
                accIdset.add(fcstub.accountObj.Id);
        }
        if(!accIdset.isEmpty()){
            List<account> literAccountList = [Select Id,Name,TAFS_Committed_Fuel_Units__c,TAFS_In_Network_Usage_MTD__c,TAFS_Days_In_Current_Month__c,TAFS_Committed_Fuel_Quantity__c, TAFS_Last_Successful_Check_In_Date__c,TAFS_Prorated_Committment_MTD__c,
                                          	  TAFS_Difference_Usage_vs_Committed__c,(SELECT Id,TAFS_Client_ID__c,TAFS_In_Network_Fuel_Stop__c,TAFS_Number_of_Reefer_Gallons__c,TAFS_Number_of_Reefer_Liters__c,
                                          	  TAFS_Number_of_Tractor_Gallons__c, TAFS_Number_of_Tractor_Liters__c,TAFS_Total_Spent_on_Fuel_CAD__c,TAFS_Total_Spent_on_Fuel_USD__c,TAFS_Transaction_Date__c,
                                              TAFS_Transaction_Number__c from Fuel_Consumptions__r WHERE CALENDAR_MONTH(TAFS_Transaction_Date__c) =: Date.Today().Month()),(select Id, TAFS_Discount_Per_Gallon__c,TAFS_Discount_Per_Liter__c,
                                              TAFS_Effective_Date_Gallon__c,TAFS_Effective_Date_Liter__c FROM Fuel_Discounts__r WHERE TAFS_Effective_Date_Liter__c<Today ORDER BY TAFS_Effective_Date_Liter__c DESC),(select Id, TAFS_Client_Name__c,TAFS_Client_Number__c,
                                              TAFS_Effective_Date__c,TAFS_Committed_Fuel_Quantity__c FROM Fuel_Commitments__r WHERE TAFS_Effective_Date__c<Today ORDER BY TAFS_Effective_Date__c DESC) FROM Account WHERE ID IN: accIdset];
            for(Account a : literAccountList){
                   
                   if(a.Fuel_Consumptions__r!=null)       
                       accountFCMap.put(a.Id, a.Fuel_Consumptions__r);
                   if(a.Fuel_Discounts__r!=null)
                       accountFDLiterMap.put(a.Id,a.Fuel_Discounts__r) ;
                   if(a.Fuel_Commitments__r!=null)
                   	   accFuelCommitmentMap.put(a.Id,a.Fuel_Commitments__r); 
            }
            List<account> gallonAccountList = [Select Id,Name,TAFS_Committed_Fuel_Units__c,TAFS_In_Network_Usage_MTD__c,TAFS_Days_In_Current_Month__c,TAFS_Committed_Fuel_Quantity__c, TAFS_Last_Successful_Check_In_Date__c,TAFS_Prorated_Committment_MTD__c,
                                               TAFS_Difference_Usage_vs_Committed__c,(SELECT Id,TAFS_Client_ID__c,TAFS_In_Network_Fuel_Stop__c,TAFS_Number_of_Reefer_Gallons__c,TAFS_Number_of_Reefer_Liters__c,
                                               TAFS_Number_of_Tractor_Gallons__c, TAFS_Number_of_Tractor_Liters__c,TAFS_Total_Spent_on_Fuel_CAD__c,TAFS_Total_Spent_on_Fuel_USD__c,TAFS_Transaction_Date__c,
                                               TAFS_Transaction_Number__c from Fuel_Consumptions__r WHERE TAFS_Transaction_Date__c =LAST_MONTH),(select Id, TAFS_Discount_Per_Gallon__c,TAFS_Discount_Per_Liter__c,
                                               TAFS_Effective_Date_Gallon__c,TAFS_Effective_Date_Liter__c FROM Fuel_Discounts__r WHERE TAFS_Effective_Date_Gallon__c<Today ORDER BY TAFS_Effective_Date_Gallon__c DESC) FROM Account WHERE ID IN: accIdset];
            
            //start : US-3497 added by arpitha 
            List<account> gallonAccountListForLifetime = [Select Id,Name,TAFS_Committed_Fuel_Units__c,TAFS_In_Network_Usage_MTD__c,TAFS_Days_In_Current_Month__c,TAFS_Committed_Fuel_Quantity__c, TAFS_Last_Successful_Check_In_Date__c,TAFS_Prorated_Committment_MTD__c,
                                               TAFS_Difference_Usage_vs_Committed__c,(SELECT Id,TAFS_Client_ID__c,TAFS_In_Network_Fuel_Stop__c,TAFS_Number_of_Reefer_Gallons__c,TAFS_Number_of_Reefer_Liters__c,
                                               TAFS_Number_of_Tractor_Gallons__c, TAFS_Number_of_Tractor_Liters__c,TAFS_Total_Spent_on_Fuel_CAD__c,TAFS_Total_Spent_on_Fuel_USD__c,TAFS_Transaction_Date__c,
                                               TAFS_Transaction_Number__c from Fuel_Consumptions__r WHERE TAFS_In_Network_Fuel_Stop__c = TRUE),(select Id, TAFS_Discount_Per_Gallon__c,TAFS_Discount_Per_Liter__c,
                                               TAFS_Effective_Date_Gallon__c,TAFS_Effective_Date_Liter__c FROM Fuel_Discounts__r WHERE TAFS_Effective_Date_Gallon__c<Today ORDER BY TAFS_Effective_Date_Gallon__c DESC) FROM Account WHERE ID IN: accIdset];
            for(Account a : gallonAccountListForLifetime){
                   
                   if(a.Fuel_Consumptions__r!=null)        
                       accountFCLMForLifetimeMap.put(a.Id, a.Fuel_Consumptions__r);
                   if(a.Fuel_Discounts__r!=null)
                       accountFDGallonForLifetimeMap.put(a.Id,a.Fuel_Discounts__r) ;  
            }
            //end : US-3497
            for(Account a : gallonAccountList){
                   
                   if(a.Fuel_Consumptions__r!=null)        
                       accountFCLMMap.put(a.Id, a.Fuel_Consumptions__r);
                   if(a.Fuel_Discounts__r!=null)
                       accountFDGallonMap.put(a.Id,a.Fuel_Discounts__r) ;  
            }
        }
        
        for(FCStub fcstub : fcStubList){
            
            fcstub.accountObj.TAFS_In_Network_Usage_MTD__c = 0;
            fcstub.accountObj.TAFS_Fuel_Savings_Previous_Month__c = 0;
            fcstub.accountObj.TAFS_Fuel_Savings_Lifetime__c = 0;
            fcstub.accountObj.TAFS_Prorated_Committment_MTD__c = 0;
            
            if(accFuelCommitmentMap.containsKey(fcstub.accountObj.Id) && accFuelCommitmentMap.get(fcstub.accountObj.Id).size()>0 && accFuelCommitmentMap.get(fcstub.accountObj.Id)[0].TAFS_Committed_Fuel_Quantity__c!=null)
                fcstub.accountObj.TAFS_Prorated_Committment_MTD__c = accFuelCommitmentMap.get(fcstub.accountObj.Id)[0].TAFS_Committed_Fuel_Quantity__c/(fcstub.accountObj.TAFS_Days_In_Current_Month__c)*Date.Today().Day();
            
            for(TAFS_Fuel_Consumptions__c fc : accountFCLMMap.get(fcstub.accountObj.Id)){
            	
                if(accountFDGallonMap.get(fcstub.accountObj.Id).size()>0 && accountFDLiterMap.get(fcstub.accountObj.Id).size()>0 && fc.TAFS_In_Network_Fuel_Stop__c){
                    fcstub.accountObj.TAFS_Fuel_Savings_Previous_Month__c = fcstub.accountObj.TAFS_Fuel_Savings_Previous_Month__c + 
                                                                        (((fc.TAFS_Number_of_Reefer_Gallons__c + fc.TAFS_Number_of_Tractor_Gallons__c)*accountFDGallonMap.get(fcstub.accountObj.Id)[0].TAFS_Discount_Per_Gallon__c)
                                                                        + ((fc.TAFS_Number_of_Tractor_Liters__c+ fc.TAFS_Number_of_Reefer_Liters__c)*accountFDLiterMap.get(fcstub.accountObj.Id)[0].TAFS_Discount_Per_Liter__c));
                }
            }
            //start : US-3497 added by arpitha
            for(TAFS_Fuel_Consumptions__c fc : accountFCLMForLifetimeMap.get(fcstub.accountObj.Id)){
                if(accountFDGallonForLifetimeMap.get(fcstub.accountObj.Id).size()>0 && accountFDLiterMap.get(fcstub.accountObj.Id).size()>0){
                    fcstub.accountObj.TAFS_Fuel_Savings_Lifetime__c = fcstub.accountObj.TAFS_Fuel_Savings_Lifetime__c + 
                                                                        (((fc.TAFS_Number_of_Reefer_Gallons__c + fc.TAFS_Number_of_Tractor_Gallons__c)*accountFDGallonForLifetimeMap.get(fcstub.accountObj.Id)[0].TAFS_Discount_Per_Gallon__c)
                                                                        + ((fc.TAFS_Number_of_Tractor_Liters__c+ fc.TAFS_Number_of_Reefer_Liters__c)*accountFDLiterMap.get(fcstub.accountObj.Id)[0].TAFS_Discount_Per_Liter__c));
                }
            }
            //end : US-3497
            for(TAFS_Fuel_Consumptions__c fc : accountFCMap.get(fcstub.accountObj.Id)){
                if(fc.TAFS_In_Network_Fuel_Stop__c == true){        
                        fcstub.accountObj.TAFS_In_Network_Usage_MTD__c = fcstub.accountObj.TAFS_In_Network_Usage_MTD__c + (fc.TAFS_Number_of_Reefer_Gallons__c + fc.TAFS_Number_of_Tractor_Gallons__c) +((fc.TAFS_Number_of_Tractor_Liters__c + fc.TAFS_Number_of_Reefer_Liters__c)*0.264172);
                }                
            }
            if(fcstub.accountObj.TAFS_Prorated_Committment_MTD__c!=null && fcstub.accountObj.TAFS_In_Network_Usage_MTD__c!=null){
                fcstub.accountObj.TAFS_Difference_Usage_vs_Committed__c =  fcstub.accountObj.TAFS_In_Network_Usage_MTD__c - fcstub.accountObj.TAFS_Prorated_Committment_MTD__c;
            }
            accList.add(fcstub.accountObj);
        }
        if(!accList.isEmpty()){
            try{
                update accList;
            }
            catch(Exception e){
                throw new FuelConsumptionServiceException('Invalid Data Supplied - Unable to insert client record due to invalid data');
            }
        }
    }
}