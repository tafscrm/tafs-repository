/* CLass Name   : TAFS_Transaction_Trigger_HandlerTest
 * Description  : Test class for TAFS_Transaction_Trigger_Handler
 * Created By   : Arpitha Sudhakar
 * Created On   : 29-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar           29-Aug-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_Transaction_Trigger_HandlerTest {

    static testmethod void test() {
       List<TAFS_Invoice_Transaction__c> invTranList = DataUtilTest_TAFS.createInvoiceTransactions(1,'1024');
       TAFS_Invoic_Key_Value_Mapping__c custSett = new TAFS_Invoic_Key_Value_Mapping__c();
       custSett.TAFS_Object_Name__c ='Invoice Transactions';
       custSett.name = 'test';
       custSett.TAFS_Field_Name__c = 'Status';
       custSett.TAFS_Cadence_Key__c = '0';
       custSett.TAFS_SFDC_Value__c = '0';
       insert custSett;
       
       //system.assert(invTranList[0].Id != null);
       
       Test.startTest();
       TAFS_Transaction_Trigger_Handler.updateStatusTypeOnInvoice(invTranList);
       TAFS_Transaction_Trigger_Handler.updateInvoiceIdOnTransaction(invTranList);
       Test.stopTest();
    }
    
        
}