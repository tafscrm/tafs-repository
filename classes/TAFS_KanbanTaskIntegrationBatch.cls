/* CLass Name   : TAFS_KanbanTaskIntegrationBatch
 * Description  : Batch class to update Kanban Task everyday
 * Created By   : Raushan Anand
 * Created On   : 25-Oct-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand               25-Oct-2016            Initial version.
 *  * Pankaj Singh                16-Nov-2016            Added logic for multiple page processing
 *
 *****************************************************************************************/
global class TAFS_KanbanTaskIntegrationBatch implements Database.Batchable<string>, Database.AllowsCallouts,Database.Stateful {
    List<TAFS_KanbanResponseParser.WrapResponse> batchWrapper = new List<TAFS_KanbanResponseParser.WrapResponse>();
    
    global Iterable<string> start(Database.BatchableContext BC) 
    {        
        list<string> pageNumberList = new list<string>();
        Date dt = Date.today()+1;
        String  todate = dt.Year()+'-'+dt.Month()+'-'+dt.Day();
        String fromdate = Date.today().Year()+'-'+Date.today().Month()+'-'+Date.today().Day();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        req.setEndpoint(Label.TAFS_Kanban_Get_All_Task_URI+'boardid/'+Label.TAFS_Kanban_Board_Id+'/container/archive/fromdate/'+fromDate+'/todate/'+toDate);
        //req.setEndpoint(Label.TAFS_Kanban_Get_All_Task_URI+'boardid/'+Label.TAFS_Kanban_Board_Id+'/container/archive/fromdate/2016-11-09/todate/2016-11-10');
        req.setMethod('POST');
        req.setBody('');
        req.setHeader('APIKey', Label.TAFS_Kanban_API_Key);
        req.setHeader('Accept', 'application/json');
        req.setCompressed(true);        
        try {
            res = http.send(req);
            system.debug('printing response>>>>>'+res);
            if(res.getStatusCode() == 200 && res.getBody() != '[]'){
                batchWrapper = TAFS_KanbanResponseParser.responseParse(res);
                do {
                    pageNumberList.add(String.valueOf(pageNumberList.size()+1));
                } while (pageNumberList.size()< TAFS_KanbanResponseParser.numberOftasksVar);                                      
            }
            else{
                TAFS_Error_Logger.insertErrorLog(res,req);
            }
            
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            
        }
        return pageNumberList;
    }
    global void execute(Database.BatchableContext info, List<string> pageNoList) {
        
        if(pageNoList.size()>0){
            
            List<String> batchNumberList = new List<String>();
            List<String> clientNumberList = new List<String>();
            List<TAFS_Invoice_Batch__c> batchList = new List<TAFS_Invoice_Batch__c>();
            Date dt = Date.today()+1;
            String  todate = dt.Year()+'-'+dt.Month()+'-'+dt.Day();
            String fromdate = Date.today().Year()+'-'+Date.today().Month()+'-'+Date.today().Day();
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setEndpoint(Label.TAFS_Kanban_Get_All_Task_URI+'boardid/'+Label.TAFS_Kanban_Board_Id+'/container/archive/fromdate/'+fromDate+'/todate/'+toDate+'/page/'+pageNoList[0]);
            //req.setEndpoint(Label.TAFS_Kanban_Get_All_Task_URI+'boardid/'+Label.TAFS_Kanban_Board_Id+'/container/archive/fromdate/2016-11-09/todate/2016-11-10/page/'+pageNoList[0]);
            req.setMethod('POST');
            req.setBody('');
            req.setHeader('APIKey', Label.TAFS_Kanban_API_Key);
            req.setHeader('Accept', 'application/json');
            req.setCompressed(true);        
            try {
                res = http.send(req);
                if(res.getStatusCode() == 200 && res.getBody() != '[]'){
                    batchWrapper = TAFS_KanbanResponseParser.responseParse(res); 
                    for(TAFS_KanbanResponseParser.WrapResponse  wrapR : batchWrapper){
                        if(String.isNotBlank(wrapR.clientNumber))
                            clientNumberList.add(wrapR.clientNumber);
                        if(String.isNotBlank(wrapR.batchNumber))
                            batchNumberList.add(wrapR.batchNumber);
                    }                                             
                }
                else{
                    TAFS_Error_Logger.insertErrorLog(res,req);
                }
                for(TAFS_Invoice_Batch__c batchObj : [Select Id,TAFS_Client_Number__c,Name, TAFS_Buy_Specialist__c,TAFS_Buy_Approval_Specialist__c,
                                                      TAFS_Collection_Specialist__c,TAFS_Funding_Specialist__c,TAFS_Verification_Specialist__c FROM 
                                                      TAFS_Invoice_Batch__c WHERE TAFS_Client_Number__c IN: clientNumberList AND Name IN: batchNumberList]){
                    for(TAFS_KanbanResponseParser.WrapResponse tWrap : batchWrapper){
                        System.debug(tWrap.batchNumber +'    '+ tWrap.clientNumber +'   '+batchObj.TAFS_Client_Number__c+'   '+ batchObj.Name);
                        if(tWrap.batchNumber!=null  && tWrap.batchNumber!= '0' && tWrap.batchNumber.equalsIgnoreCase(batchObj.Name) && 
                            tWrap.clientNumber!=null && tWrap.clientNumber!='0' && tWrap.clientNumber!='' && tWrap.clientNumber.equalsIgnoreCase(batchObj.TAFS_Client_Number__c)){
                            batchObj.TAFS_Buy_Specialist__c = tWrap.bs;
                            batchObj.TAFS_Buy_Approval_Specialist__c = tWrap.bas;
                            batchObj.TAFS_Collection_Specialist__c = tWrap.cs;
                            batchObj.TAFS_Funding_Specialist__c = tWrap.fs;
                            batchObj.TAFS_Verification_Specialist__c = tWrap.vs;
                            batchObj.TAFS_Funding_Type__c = tWrap.lanename;
                            batchObj.TAFS_Tags__c = tWrap.tags;
                            batchObj.TAFS_Color__c = tWrap.color;
                            batchList.add(batchObj);
                        }
                    }
                }
                if(!batchList.isEmpty()){
                    Database.update(batchList);
                }
            }catch(Exception excep){
                system.debug(excep.getMessage());
            }
        }
    }
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post the emails are sent.
    **********************************************************************************************************************/    
    global void finish(Database.BatchableContext info) { }
}