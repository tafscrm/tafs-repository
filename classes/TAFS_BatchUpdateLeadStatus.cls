/****************************************************************************************
* Created By      :  Vootla Manoj
* Create Date     :  10/06/2016
* Description     :  Batch class to Update the Lead Status
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_BatchUpdateLeadStatus implements Database.Batchable<sObject> {
    
    global List<Lead> start(Database.BatchableContext BC) {
        //variables
        Set<Id> leadSetId  = new Set<Id>();
        List<Task> taskList = new List<Task>();
        Map<Id, List<Task>> leadIdTaskMap =  new Map<Id, List<Task>>();
        List<Lead> LeadListUpdate = new List<Lead>();
        List<Task> t = new List<Task>();
        for(Lead l : [SELECT Id,name,LastModifiedDate,status,(select id,createddate from tasks order by createddate desc limit 1) FROM Lead where Status IN('No Contact')])
        {
            t.clear();
            leadIdTaskMap.put(l.id,l.tasks);
            t = l.tasks;
            if(!t.isEmpty() && t[0] != null )
            {
                Datetime dt =  t[0].createddate ; 
                dt = dt.addDays( 30) ; 
                if(system.now() > dt )
                {
                    leadSetId.add(l.id);
                } 
            }     
        }
        
         if(!leadSetId.isEmpty() || leadSetId != null){
           LeadListUpdate = [SELECT Id,name,LastModifiedDate,status from Lead where id IN :leadSetId];
         }
        
        return LeadListUpdate;
               
    }
   
    global void execute(Database.BatchableContext BC, List<Lead> scope) {
        
        if(scope.size() > 0){
            for(Lead leadObject : scope)
            {
               leadObject.status = Label.TAFS_LeadStatus_Nurture ;   
            }
        }
        try {
               Database.update(scope);
            }
        catch(DmlException e) {
               System.debug('The following exception has occurred: ' + e.getMessage());
            }
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}