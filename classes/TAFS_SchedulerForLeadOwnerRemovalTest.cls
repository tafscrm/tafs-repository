/* CLass Name   : TAFS_SchedulerForLeadOwnerRemovalTest
 * Description  : Test class for TAFS_SchedulerForLeadOwnerRemoval
 * Created By   : Arpitha Sudhakar
 * Created On   : 21-June-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar                21-June-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_SchedulerForLeadOwnerRemovalTest {

    static testmethod void test() {
       Test.startTest();
       TAFS_SchedulerForLeadOwnerRemoval c = new TAFS_SchedulerForLeadOwnerRemoval();
       String scheduleTime = '0 0 23 * * ?';
       system.schedule('Test schedule', scheduleTime, c);
       Test.stopTest();
    }
}