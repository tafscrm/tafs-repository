/* Class Name   : TAFS_Kanban_Integration_Handler_Test
 * Description  : This class performs unit tests of TAFS_Kanban_Integratioon_Handler
 * Created By   : Raushan Anand
 * Created On   : 6-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                6-May-2015              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_Kanban_Task_Email_Handler_Test {
    
    /* Method Name : testSetupData
     * Description : This method Insert/update the test data for all relevant object.
     * Return Type : void
     * Input Parameter : None
     */
    @testSetup 
    private static void testSetupData() {
        Test.StartTest();
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAccount = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
        TAFS_Invoice_Batch__c invoiceBatch = new TAFS_Invoice_Batch__c();
        invoiceBatch.Name = '123456';
        invoiceBatch.TAFS_Client_ID__c=lstAccount[0].Id;
        ID rectypeid = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Kanban Task').getRecordTypeId();
        INSERT invoiceBatch;
        Task demoTask = new Task(RecordTypeId = rectypeid,OwnerId = UserInfo.getUserId(),Priority = 'Normal',Status='Open',WhatId=invoiceBatch.Id,TAFS_Kanban_Card_Id__c = '23456',TAFS_Blocked_State__c=true);
        INSERT demoTask;
        Test.StopTest();
    }
    
    private static testmethod void testEmailHandlerScenario1(){
        // Create a new email, envelope object and Attachment
        TAFS_Kanban_Task_Email_Handler objEmailHandler = new TAFS_Kanban_Task_Email_Handler();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.subject = 'This is the Email Subject';
        email.htmlBody = '<html><br/>Task ID 12345 : Test Title <br/>Assignee: Raushan <br/></html>';
        envelope.fromAddress = 'test.testing@sometest.com';
        Test.StartTest();
        Map<String,String> responseHeader = new Map<String,String>();
        responseHeader.put('Content-Type', 'application/json');
        String responseBody = '{"taskid":"12345","boardid":"30","title":"Test Title","description":"Test Description","type":"None","assignee":"Raushan","subtasks":"0","subtaskscomplete":"0","color":"#34a97b","priority":"Average","size":null,"deadline":null,"deadlineoriginalformat":null,"extlink":"","tags":"","leadtime":0,"blocked":"0","blockedreason":null,"columnname":"Invoice Backlog","lanename":"Standard EFS","subtaskdetails":[],"columnid":"requested_335","laneid":"343","position":"0","columnpath":"Invoice Backlog","loggedtime":0,"customfields":[{"fieldid":"19","name":"Batch Number","type":"number","value":"123456","mandatory":true},{"fieldid":"23","name":"Client Number","type":"number","value":"123451","mandatory":true}],"comments":[{"author":"Raushan","event":"Comment added","text":"Unblocked 3","date":"2016-05-05 20:32:17","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:31:05","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:25:44","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Kanban Comment","date":"2016-05-05 20:19:21","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 2","date":"2016-05-05 20:14:29","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 1","date":"2016-05-05 20:14:25","taskid":"49136"}]}';
        Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(200,'OK',responseBody,responseHeader));
        Messaging.InboundEmailResult result = objEmailHandler.handleInboundEmail(email, envelope);
        Test.StopTest();
        System.assertEquals( result.success  ,true);

    }
    
    private static testmethod void testEmailHandlerScenario2(){
        // Create a new email, envelope object and Attachment
        TAFS_Kanban_Task_Email_Handler objEmailHandler = new TAFS_Kanban_Task_Email_Handler();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.subject = 'This is the Email Subject';
        email.htmlBody = '<html><br/>Task ID 12345 : Test Title <br/>Assignee: Raushan <br/></html>';
        envelope.fromAddress = 'test.testing@sometest.com';
        Test.StartTest();
        Map<String,String> responseHeader = new Map<String,String>();
        responseHeader.put('Content-Type', 'application/json');
        String responseBody = '{"taskid":"12345","boardid":"30","title":"Test Title","description":"Test Description","type":"None","assignee":"Raushan","subtasks":"0","subtaskscomplete":"0","color":"#34a97b","priority":"Average","size":null,"deadline":null,"deadlineoriginalformat":null,"extlink":"","tags":"","leadtime":0,"blocked":"1","blockedreason":"Blocked - RS Input Required","columnname":"Invoice Backlog","lanename":"Standard EFS","subtaskdetails":[],"columnid":"requested_335","laneid":"343","position":"0","columnpath":"Invoice Backlog","loggedtime":0,"customfields":[{"fieldid":"19","name":"Batch Number","type":"number","value":"123456","mandatory":true},{"fieldid":"23","name":"Client Number","type":"number","value":"123451","mandatory":true}],"comments":[{"author":"Raushan","event":"Comment added","text":"Unblocked 3","date":"2016-05-05 20:32:17","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:31:05","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:25:44","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Kanban Comment","date":"2016-05-05 20:19:21","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 2","date":"2016-05-05 20:14:29","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 1","date":"2016-05-05 20:14:25","taskid":"49136"}]}';
        Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(200,'OK',responseBody,responseHeader));
        Messaging.InboundEmailResult result = objEmailHandler.handleInboundEmail(email, envelope);
        Test.StopTest();
        System.assertEquals( result.success  ,true);

    }
    private static testmethod void testEmailHandlerScenario3(){
        // Create a new email, envelope object and Attachment
        TAFS_Kanban_Task_Email_Handler objEmailHandler = new TAFS_Kanban_Task_Email_Handler();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.subject = 'This is the Email Subject';
        email.htmlBody = '<html><br/>Task ID 23456 : Test Title <br/>Assignee: Raushan <br/></html>';
        envelope.fromAddress = 'test.testing@sometest.com';
        Test.StartTest();
        Map<String,String> responseHeader = new Map<String,String>();
        responseHeader.put('Content-Type', 'application/json');
        String responseBody = '{"taskid":"23456","boardid":"30","title":"Test Title","description":"Test Description","type":"None","assignee":"Raushan","subtasks":"0","subtaskscomplete":"0","color":"#34a97b","priority":"Average","size":null,"deadline":null,"deadlineoriginalformat":null,"extlink":"","tags":"","leadtime":0,"blocked":"1","blockedreason":"Blocked - RS Input Required","columnname":"Invoice Backlog","lanename":"Standard EFS","subtaskdetails":[],"columnid":"requested_335","laneid":"343","position":"0","columnpath":"Invoice Backlog","loggedtime":0,"customfields":[{"fieldid":"19","name":"Batch Number","type":"number","value":"123456","mandatory":true},{"fieldid":"23","name":"Client Number","type":"number","value":"123451","mandatory":true}],"comments":[{"author":"Raushan","event":"Comment added","text":"Unblocked 3","date":"2016-05-05 20:32:17","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:31:05","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:25:44","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Kanban Comment","date":"2016-05-05 20:19:21","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 2","date":"2016-05-05 20:14:29","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 1","date":"2016-05-05 20:14:25","taskid":"49136"}]}';
        Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(200,'OK',responseBody,responseHeader));
        Messaging.InboundEmailResult result = objEmailHandler.handleInboundEmail(email, envelope);
        Test.StopTest();
        System.assertEquals( result.success  ,true);

    }
    private static testmethod void testEmailHandlerScenario4(){
        // Create a new email, envelope object and Attachment
        TAFS_Kanban_Task_Email_Handler objEmailHandler = new TAFS_Kanban_Task_Email_Handler();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.subject = 'This is the Email Subject';
        email.htmlBody = '<html><br/>Task ID 23456 : Test Title <br/>Assignee: Raushan <br/></html>';
        envelope.fromAddress = 'test.testing@sometest.com';
        Test.StartTest();
        Map<String,String> responseHeader = new Map<String,String>();
        responseHeader.put('Content-Type', 'application/json');
        String responseBody = '{"taskid":"23456","boardid":"30","title":"Test Title","description":"Test Description","type":"None","assignee":"Raushan","subtasks":"0","subtaskscomplete":"0","color":"#34a97b","priority":"Average","size":null,"deadline":null,"deadlineoriginalformat":null,"extlink":"","tags":"","leadtime":0,"blocked":"1","blockedreason":"Test Reason","columnname":"Invoice Backlog","lanename":"Standard EFS","subtaskdetails":[],"columnid":"requested_335","laneid":"343","position":"0","columnpath":"Invoice Backlog","loggedtime":0,"customfields":[{"fieldid":"19","name":"Batch Number","type":"number","value":"123456","mandatory":true},{"fieldid":"23","name":"Client Number","type":"number","value":"123451","mandatory":true}],"comments":[{"author":"Raushan","event":"Comment added","text":"Unblocked 3","date":"2016-05-05 20:32:17","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:31:05","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:25:44","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Kanban Comment","date":"2016-05-05 20:19:21","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 2","date":"2016-05-05 20:14:29","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 1","date":"2016-05-05 20:14:25","taskid":"49136"}]}';
        Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(200,'OK',responseBody,responseHeader));
        Messaging.InboundEmailResult result = objEmailHandler.handleInboundEmail(email, envelope);
        Test.StopTest();
        System.assertEquals( result.success  ,true);
    }
    
    private static testmethod void testEmailHandlerScenario5(){
        Test.startTest();
        List<Task> existingTask = [SELECT Id, OwnerId,Subject,Description,Priority,ActivityDate,WhatId,TAFS_Blocked_Reason__c,TAFS_Is_Future_Context__c,
                               TAFS_Blocked_State__c,TAFS_Kanban_Column__c,TAFS_Kanban_Swimlane__c,TAFS_New_Comment__c,TAFS_Kanban_Card_Id__c
                               FROM Task WHERE TAFS_Kanban_Card_Id__c =: '23456'];
        existingTask[0].TAFS_Blocked_Reason__c = 'Blocked - RS Input Required';
        existingTask[0].TAFS_Blocked_State__c = false;
        existingTask[0].TAFS_New_Comment__c ='UNBLOCK COMMENT';
        Map<String,String> responseHeader = new Map<String,String>();
        responseHeader.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(200,'OK','true',responseHeader));
        UPDATE existingTask;
        Test.stopTest();
        List<Task> updatedTask = [SELECT Id, OwnerId,Subject,Description,Priority,ActivityDate,WhatId,TAFS_Blocked_Reason__c,TAFS_Is_Future_Context__c,
                               TAFS_Blocked_State__c,TAFS_Kanban_Column__c,TAFS_Kanban_Swimlane__c,TAFS_New_Comment__c,TAFS_Kanban_Card_Id__c
                               FROM Task WHERE TAFS_Kanban_Card_Id__c =: '23456'];
        System.assertEquals( updatedTask[0].TAFS_New_Comment__c  , null);
    }
    private static testmethod void testEmailHandlerScenario6(){
        Test.startTest();
        List<Task> existingTask = [SELECT Id, OwnerId,Subject,Description,Priority,ActivityDate,WhatId,TAFS_Blocked_Reason__c,TAFS_Is_Future_Context__c,
                               TAFS_Blocked_State__c,TAFS_Kanban_Column__c,TAFS_Kanban_Swimlane__c,TAFS_New_Comment__c,TAFS_Kanban_Card_Id__c
                               FROM Task WHERE TAFS_Kanban_Card_Id__c =: '23456'];
        existingTask[0].TAFS_Blocked_Reason__c = 'Blocked - RS Input Required';
        existingTask[0].TAFS_Blocked_State__c = false;
        existingTask[0].TAFS_New_Comment__c ='UNBLOCK COMMENT';
        Map<String,String> responseHeader = new Map<String,String>();
        responseHeader.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(400,'BAD REQUEST','true',responseHeader));
        UPDATE existingTask;
        Test.stopTest();
        List<Task> updatedTask = [SELECT Id, OwnerId,Subject,Description,Priority,ActivityDate,WhatId,TAFS_Blocked_Reason__c,TAFS_Is_Future_Context__c,
                               TAFS_Blocked_State__c,TAFS_Kanban_Column__c,TAFS_Kanban_Swimlane__c,TAFS_New_Comment__c,TAFS_Kanban_Card_Id__c
                               FROM Task WHERE TAFS_Kanban_Card_Id__c =: '23456'];
        System.assertEquals( updatedTask[0].TAFS_New_Comment__c  , null);
    }
    private static testmethod void testEmailHandlerScenario7(){
        // Create a new email, envelope object and Attachment
        TAFS_Kanban_Task_Email_Handler objEmailHandler = new TAFS_Kanban_Task_Email_Handler();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.subject = 'This is the Email Subject';
        email.htmlBody = '<html><br/>Task ID 23456 : Test Title <br/>Assignee: Raushan <br/></html>';
        envelope.fromAddress = 'test.testing@sometest.com';
        Test.StartTest();
        Map<String,String> responseHeader = new Map<String,String>();
        responseHeader.put('Content-Type', 'application/json');
        String responseBody = '{"taskid":"23456","boardid":"30","title":"Test Title","description":"Test Description","type":"None","assignee":"Raushan","subtasks":"0","subtaskscomplete":"0","color":"#34a97b","priority":"Average","size":null,"deadline":null,"deadlineoriginalformat":null,"extlink":"","tags":"","leadtime":0,"blocked":"1","blockedreason":"Blocked - RS Input Required","columnname":"Invoice Backlog","lanename":"Standard EFS","subtaskdetails":[],"columnid":"requested_335","laneid":"343","position":"0","columnpath":"Invoice Backlog","loggedtime":0,"customfields":[{"fieldid":"19","name":"Batch Number","type":"number","value":"654321","mandatory":true},{"fieldid":"23","name":"Client Number","type":"number","value":"123451","mandatory":true}],"comments":[{"author":"Raushan","event":"Comment added","text":"Unblocked 3","date":"2016-05-05 20:32:17","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:31:05","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:25:44","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Kanban Comment","date":"2016-05-05 20:19:21","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 2","date":"2016-05-05 20:14:29","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 1","date":"2016-05-05 20:14:25","taskid":"49136"}]}';
        Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(200,'OK',responseBody,responseHeader));
        Messaging.InboundEmailResult result = objEmailHandler.handleInboundEmail(email, envelope);
        Test.StopTest();
        List<Account> accList = [SELECT Id FROM Account LIMIT 1];
        TAFS_Invoice_Batch__c invoiceBatch = new TAFS_Invoice_Batch__c();
        invoiceBatch.Name = '654321';
        invoiceBatch.TAFS_Client_Number__c = '123451';
        invoiceBatch.TAFS_Client_ID__c=accList[0].Id;
        INSERT invoiceBatch;
        System.assertEquals( result.success  ,true);

    }
}