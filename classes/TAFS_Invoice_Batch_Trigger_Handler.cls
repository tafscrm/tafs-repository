/* CLass Name : TAFS_Invoice_Batch_Trigger_Handler
 * Description  : Trigger methods for TAFS_Invoice_Batch_Trigger 
 * Created By   : Raushan Anand
 * Created On   : 26-Apr-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                26-Apr-2015              Initial version.
 *
 *****************************************************************************************/
public class TAFS_Invoice_Batch_Trigger_Handler {
    
/****************************************************************************************
* Method Name      :  updateStatusTypeOnInvoice
* Input Parameter  :  List of Invoice Batch (List<TAFS_Invoice_Batch__c>)
* Return Type      :  Void
* Description      :  This method converts the Type and Status value from Custom Settings.
                      Custom settings stores Key Value pair for each object.
***************************************************************************************/
    
    public static void updateStatusTypeOnInvoice(List<TAFS_Invoice_Batch__c> invoiceBatchList){
        
        Map<String,String> typeKeyVal = new Map<String,String>();
        Map<String,String> statusKeyVal = new Map<String,String>();
        List<String> clientNumberList = new List<String>();
        Map<String, String> clientNumberIdMap = new Map<String,String>();
        
        List<TAFS_Invoic_Key_Value_Mapping__c> keyValList = TAFS_Invoic_Key_Value_Mapping__c.getall().values();
        for(TAFS_Invoic_Key_Value_Mapping__c obj : keyValList){
            if(obj.TAFS_Object_Name__c == 'Invoice Transactions' && obj.TAFS_Field_Name__c == 'Type'){
                typeKeyVal.put(obj.TAFS_Cadence_Key__c,obj.TAFS_SFDC_Value__c);
            }
            else if(obj.TAFS_Object_Name__c == 'Invoice Transactions' && obj.TAFS_Field_Name__c == 'Status'){
                statusKeyVal.put(obj.TAFS_Cadence_Key__c,obj.TAFS_SFDC_Value__c);
            }   
        }
        for(TAFS_Invoice_Batch__c invoiceObj : invoiceBatchList){
            if(invoiceObj.TAFS_Client_Number__c != null){
                clientNumberList.add(invoiceObj.TAFS_Client_Number__c);
            }
            if(invoiceObj.TAFS_Type__c != null){
                if(typeKeyVal.containsKey(invoiceObj.TAFS_Type__c)){
                    invoiceObj.TAFS_Type__c = typeKeyVal.get(invoiceObj.TAFS_Type__c);
                }
                /*else{
                    invoiceObj.TAFS_Type__c = '';
                }*/
            }
            if(invoiceObj.TAFS_Status__c != null){
                if(statusKeyVal.containsKey(invoiceObj.TAFS_Status__c)){
                    invoiceObj.TAFS_Status__c = statusKeyVal.get(invoiceObj.TAFS_Status__c);
                }
                /*else{
                    invoiceObj.TAFS_Status__c = '';
                }*/
            }
        }
        if(!clientNumberList.isEmpty()){
            
            List<Account> accList = [SELECT Id, TAFS_Client_Number__c FROM Account WHERE TAFS_Client_Number__c IN: clientNumberList 
                                     AND RecordTypeId =:Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()];
            for(Account acc : accList){
                clientNumberIdMap.put(acc.TAFS_Client_Number__c,acc.Id);
            }
        }
        for(TAFS_Invoice_Batch__c invoiceObj : invoiceBatchList){
            if(invoiceObj.TAFS_Client_Number__c != null){
                if(clientNumberIdMap.containsKey(invoiceObj.TAFS_Client_Number__c)){
                    if(Trigger.IsInsert){
                        invoiceObj.TAFS_Client_ID__c = clientNumberIdMap.get(invoiceObj.TAFS_Client_Number__c);}
                }
            }
        }
    }
/****************************************************************************************
* Method Name      :  updateMonthToDate
* Input Parameter  :  List of Invoice Batch (List<TAFS_Invoice_Batch__c>)
* Return Type      :  Void
* Description      :  Update month to date factored volume data on Client with sum of related AR amount 
                        on Batches.
***************************************************************************************/
    
    public static void updateMonthToDate(List<TAFS_Invoice_Batch__c> invoiceBatchList){
        
        List<String> clientIdList = new List<String>();
        Map<Id,Double> clientFactorMap = new Map<Id,double>();
        for(TAFS_Invoice_Batch__c invoiceObj : invoiceBatchList){
            if(invoiceObj.TAFS_Client_ID__c != null){
                clientIdList.add(invoiceObj.TAFS_Client_ID__c);
            }
        }
        Integer currentMonth = DateTime.now().month();
        Integer currentYear = DateTime.now().year();
        List<TAFS_Invoice_Batch__c> objBatchList = [SELECT Id, Name, TAFS_A_R_Amount__c,TAFS_Client_Id__c,TAFS_Posted_Date__c FROM TAFS_Invoice_Batch__c where TAFS_Client_Id__c IN: clientIdList AND TAFS_Posted_Date__c = THIS_MONTH AND TAFS_Status__c = 'Processed'];
        for(TAFS_Invoice_Batch__c objInvoiceBatch : objBatchList){
            if(objInvoiceBatch.TAFS_Posted_Date__c != null && objInvoiceBatch.TAFS_Posted_Date__c.month() == currentMonth && objInvoiceBatch.TAFS_Posted_Date__c.year() == currentYear && objInvoiceBatch.TAFS_Posted_Date__c <= DateTime.now()){
                if(objInvoiceBatch.TAFS_A_R_Amount__c != null){
                    if(clientFactorMap.containsKey(objInvoiceBatch.TAFS_Client_Id__c)){
                        clientFactorMap.put(objInvoiceBatch.TAFS_Client_Id__c,clientFactorMap.get(objInvoiceBatch.TAFS_Client_Id__c) + objInvoiceBatch.TAFS_A_R_Amount__c);
                    }
                    else{
                        clientFactorMap.put(objInvoiceBatch.TAFS_Client_Id__c,objInvoiceBatch.TAFS_A_R_Amount__c);
                    }
                }  
            }
            
        }
        List<Account> accList = [SELECT Id, TAFS_Client_Number__c,TAFS_Month_To_Date_Factored_Volume_New__c FROM Account WHERE Id IN: clientIdList];
        for(Account objAcc : accList){
            if(clientFactorMap.containsKey(objAcc.Id)){
                objAcc.TAFS_Month_To_Date_Factored_Volume_New__c = clientFactorMap.get(objAcc.Id);
            }
            else{
                objAcc.TAFS_Month_To_Date_Factored_Volume_New__c = 0;
            }
        }
        UPDATE accList;
    }
    
/****************************************************************************************
* Method Name      :  associateKanbanTask
* Input Parameter  :  List of Invoice Batch (List<TAFS_Invoice_Batch__c>)
* Return Type      :  Void
* Description      :  associate existing Task to the Batches
***************************************************************************************/
    public static void associateKanbanTask(List<TAFS_Invoice_Batch__c> invoiceBatchList){
        Map<String,Id> batchMap = new Map<String,Id>();
        Map<String,String> batchStatusMap = new Map<String,String>();
        Set<String> clientList = new Set<STring>();
        Set<String> batchList = new Set<STring>();
        if(invoiceBatchList != null){
            for(TAFS_Invoice_Batch__c objBatch : invoiceBatchList){
                if(objBatch.Name != null){
                    batchList.add(objBatch.Name);
                    clientList.add(objBatch.TAFS_Client_Number__c);
                    batchMap.put(objBatch.Name+'_'+objBatch.TAFS_Client_Number__c ,objBatch.Id);
                    batchMap.put(objBatch.Name+'_'+objBatch.TAFS_Client_Number__c ,objBatch.Id);
                    if(objBatch.TAFS_Status__c == 'Processed' || objBatch.TAFS_Status__c == 'Void')
                        batchStatusMap.put(objBatch.Name+'_'+objBatch.TAFS_Client_Number__c ,objBatch.TAFS_Status__c);
                }                
            }
            List<Task> taskList = [SELECT Id, TAFS_Batch_Number__c,TAFS_Client_Number__c,WhatId FROM Task WHERE TAFS_Batch_Number__c IN: batchList AND TAFS_Client_Number__c IN : clientList];                      
            if(!taskList.isEmpty()){
                for(Task objTask : taskList){
                    if(batchMap.containsKey(objTask.TAFS_Batch_Number__c+'_'+objTask.TAFS_Client_Number__c)){
                        objTask.WhatId = batchMap.get(objTask.TAFS_Batch_Number__c +'_'+ objTask.TAFS_Client_Number__c);
                        //objTask.TAFS_Is_Future_Context__c = false;
                        if(batchStatusMap.containsKey(objTask.TAFS_Batch_Number__c+'_'+objTask.TAFS_Client_Number__c)){
                            objTask.TAFS_Blocked_State__c = false;
                            objTask.TAFS_Blocked_Reason__c = '';
                        }
                    }
                }
                UPDATE taskList;
            } 
        }
    }
}