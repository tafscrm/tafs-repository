/****************************************************************************************
* Created By      :  Pankaj Singh
* Created Date    :  09/07/2016
* Description     :  Batch class for calculating and updating business days for the current month on Account.
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_UpdateCurrentBusinessDaysBatch implements Database.Batchable<sObject> {
    
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<Account>
    * Description      :  This method is used to find all the clients in the system.
    **********************************************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        String query = 'SELECT Id FROM Account WHERE RecordTypeId =:recTypeId';
        return Database.getQueryLocator(query);
    }
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<Account>
    * Return Type      :  Void
    * Description      :  This method is used to update the current working days for all accounts.
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<Account> scope) {
    
        if(scope.size()>0){
            list<Account> accountList = new list<Account>();
            list<BusinessHours> busHoursList = new list<BusinessHours>();
            busHoursList  = [SELECT ID FROM BusinessHours WHERE isActive = TRUE ORDER BY LastModifiedDate DESC LIMIT 1];
            if(busHoursList.size()>0){
                try{                
                    Date prevBusinessDate; 
                    DateTime currentTime = System.now();
                    DateTime startTime = DateTime.newInstance(currentTime.year(), currentTime.month(), 1);
                    DateTime endTime = DateTime.newInstance(currentTime.year(), currentTime.month(), currentTime.day());
                    Decimal noOfWorkingDays = BusinessHours.diff(busHoursList[0].Id, startTime, endTime)/(3600*1000*24);
                    if(BusinessHours.isWithin(busHoursList[0].Id,Date.Today())){
                        prevBusinessDate = Date.Today()-1;
                    }
                    else if(BusinessHours.isWithin(busHoursList[0].Id,Date.Today()-1)){
                        prevBusinessDate = Date.Today()-2;
                    }
                    else if(BusinessHours.isWithin(busHoursList[0].Id,Date.Today()-2)){
                        prevBusinessDate = Date.Today()-3;
                    }
                    else if(BusinessHours.isWithin(busHoursList[0].Id,Date.Today()-3)){
                        prevBusinessDate = Date.Today()-4;
                    }
                    else if(BusinessHours.isWithin(busHoursList[0].Id,Date.Today()-4)){
                        prevBusinessDate = Date.Today()-5;
                    }
                    else if(BusinessHours.isWithin(busHoursList[0].Id,Date.Today()-5)){
                        prevBusinessDate = Date.Today()-6;
                    }
                    else if(BusinessHours.isWithin(busHoursList[0].Id,Date.Today()-6)){
                        prevBusinessDate = Date.Today()-7;
                    }
                    for(Account accObj : scope){
                        accObj.TAFS_Current_Business_Days__c = noOfWorkingDays;
                        accObj.TAFS_Previous_Business_Day__c = prevBusinessDate;
                        accountList.add(accObj);
                    }
                    if(!accountList.isEmpty()){
                        update accountList;
                    }                    
                }catch(DmlException dmlExcep){
                    //Do Nothing
                }
                catch(Exception excep){
                    //Do Nothing
                }           
            }               
        }
    }
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post the emails are sent.
    **********************************************************************************************************************/    
    global void finish(Database.BatchableContext BC) {
    
    }
}