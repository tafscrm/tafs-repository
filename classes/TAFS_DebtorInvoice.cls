/* Class Name   : TAFS_DebtorInvoice
 * Description  : Populate Invoice in email being sent to Debtor Contact
 * Created By   : Raushan Anand
 * Created On   : 9-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                9-Aug-2016              Initial version.
 *
 *****************************************************************************************/
public class TAFS_DebtorInvoice{

    public List<TAFS_Invoice__c> invoiceList{set;}  
    public ID accountID {get;set;}
    Set<Id> invoiceIdSet = new set<Id>();
    
    public TAFS_DebtorInvoice(){
    }
    public List<TAFS_Invoice__c> getInvoiceList(){
        string queryStr = 'Select Id,Name,TAFS_Client_Name__c,Debtor__c,TAFS_Balance__c,TAFS_PO_No__c,TAFS_Client_Name__r.Name,TAFS_Invoice_Amount__c,(SELECT ID FROM Verificatons_Collections__r) '+
                          'FROM TAFS_Invoice__c WHERE (TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO :' +Label.TAFS_VerificationEmailLabel_10+') '+
                          'AND TAFS_Invoice_Batch__r.TAFS_Status__c = \'Processed\' AND (Debtor__r.TAFS_No_Verification_Email__c = false) AND (Debtor__r.TAFS_Gallium_Paying_Agent__c=null OR '+
                          'Debtor__r.TAFS_Gallium_Paying_Agent__c=\'NULL\' OR Debtor__r.TAFS_Gallium_Paying_Agent__c=\'\') AND ((TAFS_Debtor_first_association_withClient__c = TRUE AND TAFS_Balance__c > 0) OR (TAFS_Balance__c > 2000) OR '+
                          '(TAFS_Client_Name__r.TAFS_First_Funding_Date__c > N_DAYS_AGO:'+Label.TAFS_VerificationEmailLabel_90+' AND TAFS_Balance__c > 0)) AND Debtor__c=:accountId';
        
        for(TAFS_Invoice__c invObj : DataBase.query(queryStr)){
            invoiceIdSet.add(invObj.Id);
        }
        string queryStrVer = 'SELECT Id,TAFS_Invoice__c,TAFS_Invoice__r.Debtor__c FROM TAFS_Verificatons_Collections__c Where TAFS_Response__c IN (\'Post Verification Required\')'+
                             ' AND (TAFS_Invoice__r.TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO : '+Label.TAFS_VerificationEmailLabel_10+') AND '+
                             '(TAFS_Invoice__r.TAFS_Invoice_Batch__r.TAFS_Status__c = \'Processed\') AND (TAFS_Invoice__r.TAFS_Balance__c > 0) AND (TAFS_Invoice__r.Debtor__r.TAFS_No_Verification_Email__c = false)'+
                             ' AND (TAFS_Invoice__r.Debtor__r.TAFS_Gallium_Paying_Agent__c=null OR TAFS_Invoice__r.Debtor__r.TAFS_Gallium_Paying_Agent__c=\'NULL\' OR TAFS_Invoice__r.Debtor__r.TAFS_Gallium_Paying_Agent__c=\'\') AND TAFS_Invoice__r.Debtor__c =:accountId';
        
        for(TAFS_Verificatons_Collections__c verObj : Database.query(queryStrVer)){
            if(!invoiceIdSet.contains(verObj.TAFS_Invoice__c))
                invoiceIdSet.add(verObj.TAFS_Invoice__c);
        }
        return ([Select Id,Name,TAFS_Balance__c,Debtor__c,TAFS_PO_No__c,TAFS_Client_Name__c,TAFS_Client_Name__r.Name,TAFS_Invoice_Amount__c FROM TAFS_Invoice__c WHERE ID IN : invoiceIdSet]);        
    }
}