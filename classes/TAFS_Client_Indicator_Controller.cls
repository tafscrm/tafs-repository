/* CLass Name   : TAFS_Client_Indicator_Controller
 * Description  : Class to update indicator fields on Client
 * Created By   : Raushan Anand
 * Created On   : 16-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              16-Aug-2016              Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_Client_Indicator_Controller {
    public Account accountObj{get;set;}
    public    TAFS_Client_Indicator_Controller(ApexPages.StandardController controller){
        
            this.accountObj = (Account)controller.getRecord();     
    } 
    public void populateIndicator(){
        try{
            if(accountObj.Id !=null){
                accountObj = [Select TAFS_Batches_Blocked_Value__c,TAFS_Fuel_vs_Commitment__c,TAFS_Recent_Roadside_Claims__c,TAFS_Client_Status__c,TAFS_Last_Website_Login_Date__c 
                              ,TAFS_Factored_Volume_vs_Average__c, TAFS_Fuel_Volume_vs_Average__c,TAFS_Difference_Usage_vs_Committed__c,TAFS_Prorated_Committment_MTD__c,TAFS_Notice_Of_Termination_Count__c
                              ,Web_Activity_Value__c,Client_Status_Health_Value__c,TAFS_Overall_Client_Health__c,Notice_of_Termination_Value__c FROM Account WHERE ID=: accountObj.Id];    
                accountObj.TAFS_Batches_Blocked_Value__c = TAFS_Client_Indicator_Service.getBatchProcessedDetails(accountObj.Id);
                accountObj.TAFS_Recent_Roadside_Claims__c = TAFS_Client_Indicator_Service.getRoadsideAssistanceClaims(accountObj.Id);
                accountObj.TAFS_Factored_Volume_vs_Average__c = TAFS_Client_Indicator_Service.getFactoredVolumevsAverage(accountObj.Id);
                accountObj.TAFS_Fuel_Volume_vs_Average__c = TAFS_Client_Indicator_Service.getFuelVolumevsAverage(accountObj.Id);
                accountObj.TAFS_Overall_Client_Health__c= TAFS_Client_Indicator_Service.getOverallClientHealth(accountObj);
                accountObj.Client_Status_Health_Value__c= TAFS_Client_Indicator_Service.getClientStatusHealthDetails(accountObj);
                accountObj.Web_Activity_Value__c= TAFS_Client_Indicator_Service.getWebActivityDetails(accountObj);
                accountObj.Notice_of_Termination_Value__c= TAFS_Client_Indicator_Service.getNoticeOfTerminationDetails(accountObj);
                if(Date.Today().Day()>=10)
                    accountObj.TAFS_Fuel_vs_Commitment__c = TAFS_Client_Indicator_Service.getFuelvsCommitmentDetails(accountObj);
                Database.Update(accountObj);
                
            }      
        }
        catch(Exception ex){
        }
    }
}