/****************************************************************************************
* Created By      :  Karthik Gulla
* Create Date     :  06/01/2017
* Description     :  Batch class for lead status update to 'No Contact' if there is no
*                     Activity from past 7 days on lead with status -'Actively Working'
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_BatchLeadStatusUpdateIfNoActivity implements Database.Batchable<sObject> {
    
    global List<Lead> start(Database.BatchableContext BC) {
        //variables
        List<Lead> leadListForLastNdays = new List<Lead>();       
        Set<Id> leadSetIdsForLastNdays  = new Set<Id>();       
        List<Event> eventListForNdays   = new List<Event>();
        List<Task> taskListForNdays     = new List<Task>();
        Set<Id> allLeadIdsSet           = new Set<Id>();
        List<Lead> leadList             = new List<Lead>();
        Set<Id> groupSetId              = new Set<Id>();
          
        List<Group> groupLst = [SELECT Id, Name from Group WHERE Type =: Label.TAFS_Queue_Value AND (Name =: Label.TAFS_National_Accounts OR Name =: Label.TAFS_Direct_Sales)];
        for(Group groupObj : groupLst){
            groupSetId.add(groupObj.Id);
        }

        leadListForLastNdays = [SELECT Id, Name, LastModifiedDate, TAFS_Lead_Type__c FROM Lead WHERE OwnerId IN :groupSetId  
                                AND Status =: Label.TAFS_LeadStatus_Actively_Working];
        
        for(Lead leadObj : leadListForLastNdays){
            leadSetIdsForLastNdays.add(leadObj.Id);
        }
        if(!leadSetIdsForLastNdays.isEmpty() || leadSetIdsForLastNdays != null){
            taskListForNdays  = [SELECT Id, WhoId, CreatedDate FROM Task WHERE WhoId IN :leadSetIdsForLastNdays and CreatedDate >=: System.now().AddDays(Integer.valueOf(Label.TAFS_LeadStatus_Update_To_NoContact_Days)) and CreatedDate <=: System.now()];
            eventListForNdays  = [SELECT Id, WhoId, CreatedDate FROM Event WHERE WhoId IN :leadSetIdsForLastNdays and CreatedDate >=: System.now().AddDays(Integer.valueOf(Label.TAFS_LeadStatus_Update_To_NoContact_Days)) and CreatedDate <=: System.now()];
        }
        if(taskListForNdays.size() != 0){
            for(Task taskObject : taskListForNdays){
                leadSetIdsForLastNdays.remove(taskObject.WhoId); 
            }   
        }
        if(eventListForNdays.size() != 0){
            for(Event eventObject : eventListForNdays){
                leadSetIdsForLastNdays.remove(eventObject.WhoId); 
            }
        }
        allLeadIdsSet.addAll(leadSetIdsForLastNdays);
         
        if(!allLeadIdsSet.isEmpty() || allLeadIdsSet != null){
            leadList = [SELECT Id, Name, Status, TAFS_Lead_Type__c, OwnerId, LastModifiedDate FROM Lead WHERE Id IN :allLeadIdsSet];
        }
        return leadList;
    }
   
    global void execute(Database.BatchableContext BC, List<Lead> scope) {        
        if(scope.size() > 0){
            for(Lead leadObject : scope){
                leadObject.Status = Label.TAFS_LeadStatus_No_Contact;          
            }
        }
        Database.update(scope);
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}