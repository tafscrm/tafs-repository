/* CLass Name   : TAFS_SendVerificationEmailBatchTest
 * Description  : Test class for TAFS_SendVerificationEmailBatch
 * Created By   : Arpitha Sudhakar
 * Created On   : 23-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar           23-Aug-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_SendVerificationEmailBatchTest {

    static testmethod void test() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       List<Lead> leadList = [select id,name from Lead];
       
       Test.startTest();
       TAFS_SendVerificationEmailBatch c = new TAFS_SendVerificationEmailBatch();
       Database.executeBatch(c);
       Test.stopTest();
    }
    
    static testmethod void testpositive() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       DataUtilTest_TAFS.createQueueForLeadRemoval();  
       List<Lead> leadList = [select id,name from Lead];
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       List<Contact> contactLst = DataUtilTest_TAFS.createContactsForLead(leadList.size(),leadList[0].Id);     
       
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        
        TAFS_SendVerificationEmailBatch AU = new TAFS_SendVerificationEmailBatch();
        
        AU.execute(BC, contactLst);
        AU.finish(BC);        
    }
}