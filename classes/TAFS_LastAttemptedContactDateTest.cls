@isTest(seeAllData=True)
private class TAFS_LastAttemptedContactDateTest{


    public static testmethod void testClientInteractionHandler() {
       
     
       Test.startTest();
       
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
         
       TAFS_Client_Interaction__c ci = new TAFS_Client_Interaction__c(name = 'CI 1',TAFS_Client_Name__c = lstAcct[0].id,TAFS_Contact_Date_Time__c = system.now()-1);
       insert ci;
       
       Test.stopTest();
       
      
               
    }
    
    
    public static testmethod void testTaskTriggerHandler() {
       
              
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
     
       Test.startTest();
 
       Task t = new Task(subject = 'Test',priority ='Normal',status = 'open',whatid = lstAcct[0].id,type = 'Call No Contact');
       insert t;
       
       TAFS_Client_Interaction__c ci1 = new TAFS_Client_Interaction__c(name = 'CI 2',TAFS_Client_Name__c = lstAcct[0].id,TAFS_Contact_Date_Time__c = system.now());
       insert ci1;
       
       Test.stopTest();
       
      
               
    }
}