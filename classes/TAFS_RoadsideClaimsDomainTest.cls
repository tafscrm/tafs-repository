@isTest
private class TAFS_RoadsideClaimsDomainTest{

    /************************************************************************************
    * Method       :    testRSClientNumberUpdate
    * Description  :    Tests whether client id is populated properly on Roadside subscription or not
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testRCClientNumberUpdate() {
       
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
       
       List<TAFS_Roadside_Assistance_Claims__c> roadsideClaimsList = DataUtilTest_TAFS.createRoadsideClaims(1,lstAcct[0].Id);
       
       Test.startTest();
           insert roadsideClaimsList;
       Test.stopTest();
       
       system.assertEquals(1,roadsideClaimsList.size());
       system.assertEquals(roadsideClaimsList[0].TAFS_Client_Number__c , lstAcct[0].TAFS_Client_Number__c);
    }
}