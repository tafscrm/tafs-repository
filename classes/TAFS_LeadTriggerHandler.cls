/* CLass Name   : TAFS_LeadTriggerHandler
 * Description  : Trigger methods for Lead
 * Created By   : Manoj M Vootla
 * Created On   : 07/06/2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla              07/06/2016              Initial version.
 *
 *****************************************************************************************/

public with sharing class TAFS_LeadTriggerHandler{

        private static boolean prohibitAfterInsertTrigger = false;

/****************************************************************************************
* Created By      :  Manoj Vootla
* Create Date     :  07/06/2016
* Description     :  Preventing Lead assignment method based on status and owner
* Modification Log:  Initial version.
***************************************************************************************/
    public static void preventLeadAssignment(List<Lead> leadList){
        
         Map<Id, Lead> ownerNewLeadMap =  new Map<Id, Lead>();
         Map<Id, Lead> ownerNoContactLeadMap =  new Map<Id, Lead>();
         set<Id> leadOwnerIdList = new set<Id>();
         set<Id> leadId = new set<Id>();
         List<Group> queueIdList = [select Id,name from Group where Type = 'Queue' and name IN ('Direct Sales','National Accounts')];
        
        //getting list of all the Lead ids
         for(Lead ld : leadList){
            if(ld.ownerId!= queueIdList[0].Id && ld.ownerId!= queueIdList[1].Id){
                leadOwnerIdList.add(ld.ownerid);
                leadId.add(ld.id);
            }
        }
        //getting all the existing leads for the Owner
        if(!leadOwnerIdList.isEmpty() && !leadId.isEmpty()){        
            //query for queue id
            //use that id in the ownerid field                                                      
             for(Lead ldObj : [SELECT Id,status,owner.id FROM Lead WHERE ownerId !=: queueIdList[0].id AND ownerId !=: queueIdList[1].Id AND ownerId IN:leadOwnerIdList AND status IN( 'New','No Contact') AND Id NOT IN :leadId])            
             {
                 if(ldObj.status == Label.TAFS_LeadStatus_New)
                     ownerNewLeadMap.put(ldObj.owner.id,ldobj);
                  if(ldObj.status == Label.TAFS_LeadStatus_No_Contact)    
                     ownerNoContactLeadMap.put(ldObj.owner.id,ldobj);
             }
        }
        
        //logic to prevent lead assignment based on status and owner
        for(Lead ld : leadList){
            if(ld.status == Label.TAFS_LeadStatus_New) {
                if(ownerNewLeadMap.containsKey(ld.ownerid)){           
                    ld.addError(Label.TAFS_Lead_Owner_New_Status_Err_Msg);             
                }
            } 
           
            if(ld.status == Label.TAFS_LeadStatus_No_Contact){
                if(ownerNoContactLeadMap .containsKey(ld.ownerid)){               
                    ld.addError(Label.TAFS_Lead_Owner_NoContact_Status_Err_Msg);             
                }
            }
        }       
  
    } 
    
    /****************************************************
        Pardot
    ****************************************************/
    private Map<Id, Lead> newLeadMap;


    public static void afterUpdate (){
        isEmailAdded();
    }


    public static void isEmailAdded (){
        List<Lead> prospectsList = new List<Lead>();
        for(Lead l : (List<Lead>) Trigger.new){
            Lead old = (Lead) Trigger.oldMap.get(l.Id);
            /* If Email was empty */
            if ( old.status !=  Label.TAFS_LeadStatus_Nurture && l.status == Label.TAFS_LeadStatus_Nurture && l.Email != null ){
                /* Add Lead Prospect */
                prospectsList.add(l);
                System.debug('::LEAD SENT TO PARDOT::');
            }
        }        
        Pardot.createProspectsByLead(prospectsList);
    }
    
/****************************************************************************************
* Created By      :  Manoj Vootla
* Create Date     :  07/06/2016
* Description     :  Post Lead Conversion Operations
* Modification Log:  Initial version.
***************************************************************************************/
    public static void postLeadConversionOperations(List<Lead> leadList){
        
         set<Id> leadId = new set<Id>();
         List<Contact> conDel = new List<Contact>();
         map<Id,Id> leadOwnerIdMap = new map<Id,Id>();
         Set<String> leadName = new set<String>();
         List<contact> conListUpdate = new List<contact>();
         map<Id,Id> leadConvertedAccMap = new map<Id,Id>();
         map<Id,List<Contact>> leadContactListMap = new map<Id,List<Contact>>();
         map<Id,List<TAFS_Insurance_Policies__c>> leadInsuranceListMap = new map<Id,List<TAFS_Insurance_Policies__c>>();
         //Attachment varables
         set<Id> contactIdSet =  new set<Id>();
         list<Attachment> attachList =  new list<Attachment>();
         list<Note> noteList = new list<Note>();
         map<Id,List<Attachment>> conAttachmentMap = new map<Id,List<Attachment>>();
         map<Id,List<Note>> conNoteMap = new map<Id,List<Note>>();
         try{
            //getting list of all the Lead ids
            for(Lead ld : leadList)
            {
                leadId.add(ld.id);
                if(ld.firstname != null){
                    leadName.add(ld.firstname+' '+ld.lastname);
                }
                else{
                    leadName.add(ld.lastname); 
                }
                leadOwnerIdMap.put(ld.id,ld.ownerid);           
            }
            //Fetch all the contacts tied to a lead and the corresponding converted accounts and create a map
            for(lead ld :[select id,name,convertedaccountid,(select id,name,accountid from contacts__r),(select id,name,TAFS_Client__c from Insurance_Policies__r) from lead where id in: leadId])
            {
                 leadConvertedAccMap.put(ld.id,ld.convertedaccountid);
                 leadContactListMap.put(ld.id,ld.contacts__r);       
                 leadInsuranceListMap.put(ld.id,ld.Insurance_Policies__r);
            }
            
            //Update all the client status field of all the convertedaccounts to 'Client Created'
            if(leadConvertedAccMap.values().size() > 0)
            {
                List<account> accList = [select id,TAFS_Client_Status__c from account where id in: leadConvertedAccMap.values()];
                for(account a : accList)
                {
                    a.TAFS_Client_Status__c  = Label.TAFS_ClientStatus_Created;
                }
                if(!accList.isEmpty()){               
                    Database.update(accList);
                }
            }
            
            //Dis-assossiate the additonal contact from account
            if(leadConvertedAccMap.values().size() > 0)
            {
               for(Contact c : [select id,name,AccountId ,(select id,whoid,whatid,subject from tasks where createddate = today) from contact where accountid in: leadConvertedAccMap.values() and name in:leadName and createddate = today])
               {
                     conDel.add(c);
                     contactIdSet.add(c.Id);                 
               }                   
            }
            /*****************code to copy notes attchments from lead to account--Added by Pankaj********************************/
                                  
            for(Attachment a:[select Name ,Id,ParentID, Body from Attachment where ParentID IN : contactIdSet]){
                if(conAttachmentMap.containskey(a.ParentID)){
                    List<Attachment> lstTemp = conAttachmentMap.get(a.parentid);
                    lstTemp.add(a);
                    conAttachmentMap.put(a.ParentID,lstTemp);
                }
                else{
                    List<Attachment> lstTemp = new List<Attachment>();
                    lstTemp.add(a);
                    conAttachmentMap.put(a.ParentID,lstTemp);
                }
            }
            for(Note a:[select Id,Title,ParentID, Body from Note where ParentID IN : contactIdSet]){
                if(conNoteMap.containskey(a.ParentID)){
                    List<Note> lstTemp = conNoteMap.get(a.parentid);
                    lstTemp.add(a);
                    conNoteMap.put(a.ParentID,lstTemp);
                }
                else{
                    List<Note> lstTemp = new List<Note>();
                    lstTemp.add(a);
                    conNoteMap.put(a.ParentID,lstTemp);
                }
            }
            for(Contact conObj : conDel){
                if(conAttachmentMap.containsKey(conObj.Id)){
                    for(Attachment attachmntObj : conAttachmentMap.get(conObj.Id)){
                        Attachment attacbj = New Attachment(Name = attachmntObj.name, body = attachmntObj.body, parentID = conObj.AccountId);
                        attachList.add(attacbj);
                    }
                }
                if(conNoteMap.containsKey(conObj.Id)){
                    for(Note noteObj : conNoteMap.get(conObj.Id)){
                        Note noteRec = New Note(Title = noteObj.Title, body = noteObj.body, parentID = conObj.AccountId);
                        noteList.add(noteRec);
                    }
                }
            }
            for(Contact conObj : conDel){
                conObj.accountid = null;
            }
            if(!conDel.isEmpty()){
                update conDel;
            }        
            if(!attachList.isEmpty()){
                insert attachList;
            } 
            if(!noteList.isEmpty()){
                insert noteList;
            }                
                    
            /*****************attachment code ends here*********************************/
            //Transfer the contacts to the convertedaccounts
            for(lead ld : leadList)
            {        
                for(contact c : leadContactListMap.get(ld.id))
                {
                    c.accountid = leadConvertedAccMap.get(ld.id);
                    conListUpdate.add(c);
                }
            }
            
            if(conListUpdate.size() > 0)
            {
              Database.update(conListUpdate);
            }
                        
            //Transfer the Insurances to the convertedaccounts
            List<TAFS_Insurance_Policies__c> insListUpdate = new List<TAFS_Insurance_Policies__c>();
            for(lead ld : leadList)
            {        
                for(TAFS_Insurance_Policies__c ins : leadInsuranceListMap.get(ld.id))
                {
                    ins.TAFS_Client__c = leadConvertedAccMap.get(ld.id);
                    insListUpdate.add(ins);
                }
            }
            
            if(insListUpdate.size() > 0)
            {
              Database.update(insListUpdate);
            }
            
            //Create Lead Owner as Account Team Member on the converted account
            list<AccountTeamMember> newmembers = new list<AccountTeamMember>();
            for(lead ld : leadList)
            {        
                if(leadConvertedAccMap.containsKey(ld.id) && leadOwnerIdMap.containsKey(ld.id)){
                    AccountTeamMember teammember=new AccountTeamMember();
                    teammember.AccountId=leadConvertedAccMap.get(ld.id);
                    teammember.UserId=leadOwnerIdMap.get(ld.id);
                    teammember.TeamMemberRole = 'Sales Account Manager';
                    newmembers.add(teammember);
                }
            }
             if(newmembers.size() > 0)
            {
              Database.insert(newmembers);
            }
         }catch(Exception excep){
             //Do nothing
         }
    }       
   
/****************************************************************************************
* Created By      :  Raushan Anand
* Create Date     :  15/06/2016
* Description     :  Callout to FCF System
* Modification Log:  Initial version.
***************************************************************************************/
    @future(Callout=true)
    public static void freightConnectUpdate(List<String> referenceIdList){
        for(String referenceId : referenceIdList){
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setEndpoint(Label.TAFS_FC_LEAD_CONVERSION_URL+referenceId);
            req.setMethod('POST');
            req.setBody('');
            req.setHeader('Accept', 'application/json');
            req.setCompressed(true); // otherwise we hit a limit of 32000
            
            try {
                res = http.send(req);
                if(res.getStatusCode() != 204){
                    TAFS_Error_Logger.insertErrorLogForFC(res,req);
                }
            }
            catch(System.CalloutException ex){
                System.debug('Callout error: '+ ex);
            }
        }
    }
/****************************************************************************************
* Created By      :  Raushan Anand
* Create Date     :  15/06/2016
* Description     :  Associate Insurance policy to Account
* Modification Log:  Initial version.
***************************************************************************************
    public static void associateInsurancePolicyToAccount(List<Lead> leadList){
        List<String> referenceList = new List<String>();
        for(Lead objLead : leadList){
            if(objLead.IsConverted && String.IsnotBlank(objLead.TAFS_Reference_ID__c)){
                referenceList.add(objLead.TAFS_Reference_ID__c);
            }
        }
        if(!referenceList.isEmpty()){
            List<TAFS_Insurance_Policies__c> insPolicyList = [SELECT ID,TAFS_Client__c FROM TAFS_Insurance_Policies__c WHERE TAFS_Reference_ID__c IN: referenceList];
            UPDATE insPolicyList;
        }
    }*/
/****************************************************************************************
* Method Name      :  validateFCBeforeInsert
* Input Parameter  :  List of Lead (List<Lead>)
* Return Type      :  Void
* Description      :  This method copies the value from Dup fields to actual fields while creation of Lead.
***************************************************************************************/
    public static void validateFCBeforeInsert(List<Lead> leadList){
        for(Lead objLead : leadList)
        {
            if(String.IsNotBlank(objLead.TAFS_Street_Dup__c)){
                objLead.Street = objLead.TAFS_Street_Dup__c;
            }
            if(String.IsNotBlank(objLead.TAFS_City_Dup__c)){
                objLead.City = objLead.TAFS_City_Dup__c;
            }
            if(String.IsNotBlank(objLead.TAFS_State_Dup__c)){
                objLead.State = objLead.TAFS_State_Dup__c;
            }
            if(String.IsNotBlank(objLead.TAFS_Phone_Dup__c)){
                objLead.Phone = objLead.TAFS_Phone_Dup__c;
            }
            if(String.IsNotBlank(objLead.TAFS_Postal_Code_Dup__c)){
                objLead.PostalCode = objLead.TAFS_Postal_Code_Dup__c;
            }
            if(String.IsNotBlank(objLead.TAFS_Company_Dup__c)){
                objLead.Company = objLead.TAFS_Company_Dup__c;
            }
            if(String.IsNotBlank(objLead.TAFS_MC_Number_Dup__c)){
                objLead.TAFS_MC_Number__c = objLead.TAFS_MC_Number_Dup__c;
            }
            if(String.IsNotBlank(objLead.TAFS_DOT_Number_Dup__c)){
                objLead.TAFS_DOT_Number__c = objLead.TAFS_DOT_Number_Dup__c;
            }
            if(String.IsNotBlank(objLead.DBA_Name_Dup__c)){
                objLead.TAFS_DBA_Name__c = objLead.DBA_Name_Dup__c;
            }
            if(objLead.TAFS_Number_of_Active_Vehicles_Dup__c != NULL){
                objLead.TAFS_Number_of_Active_Vehicles__c = Integer.ValueOf(objLead.TAFS_Number_of_Active_Vehicles_Dup__c);
            }
            if(String.IsNotBlank(objLead.TAFS_Last_Name_Dup__c)){
                objLead.LastName = objLead.TAFS_Last_Name_Dup__c;
            }
            if(String.IsNotBlank(objLead.TAFS_Email_Dup__c)){
                objLead.Email = objLead.TAFS_Email_Dup__c;
            }
        }
    }

/****************************************************************************************
* Method Name      :  validateFCBeforeUpdate
* Input Parameter  :  List of Lead (List<Lead>)
* Return Type      :  Void
* Description      :  Validation check after update
***************************************************************************************/
    public static void validateFCBeforeUpdate(List<Lead> leadList , Map<Id,Lead> oldListMap){
        Map<String,String> profileMap = new Map<String,String>();
        List<Id> lastModifiedUser = new List<Id>();
        for(Lead objL : oldListMap.Values()){
            lastModifiedUser.add(ObjL.LastModifiedById);
        }
        List<User> userList = [SELECT Id, Profile.Name FROM User WHERE ID IN : lastModifiedUser];
        for(User objUser : userList){
            profileMap.put(objUser.Id,objUser.Profile.Name);
        }
        for(Lead objLead : leadList)
        {
            if(profileMap.get(objLead.LastModifiedById) == 'TAFS_Integration_User'){
                
                if(objLead.Status == Label.TAFS_LeadStatus_New || objLead.Status == Label.TAFS_LeadStatus_No_Contact || objLead.Status == Label.TAFS_LeadStatus_Nurture || objLead.Status == Label.TAFS_LeadStatus_Incorrect_Info || objLead.Status == Label.TAFS_LeadStatus_Not_Eligible){
                    
                    if(String.IsNotBlank(objLead.TAFS_Street_Dup__c)){
                        objLead.Street = objLead.TAFS_Street_Dup__c;
                    }
                    if(String.IsNotBlank(objLead.TAFS_City_Dup__c)){
                        objLead.City = objLead.TAFS_City_Dup__c;
                    }
                    if(String.IsNotBlank(objLead.TAFS_State_Dup__c)){
                        objLead.State = objLead.TAFS_State_Dup__c;
                    }
                    if(String.IsNotBlank(objLead.TAFS_Phone_Dup__c)){
                        objLead.Phone = objLead.TAFS_Phone_Dup__c;
                    }
                    if(String.IsNotBlank(objLead.TAFS_Postal_Code_Dup__c)){
                        objLead.PostalCode = objLead.TAFS_Postal_Code_Dup__c;
                    }
                    if(String.IsNotBlank(objLead.TAFS_Company_Dup__c)){
                        objLead.Company = objLead.TAFS_Company_Dup__c;
                    }
                    if(String.IsNotBlank(objLead.TAFS_MC_Number_Dup__c)){
                        objLead.TAFS_MC_Number__c = objLead.TAFS_MC_Number_Dup__c;
                    }
                    if(String.IsNotBlank(objLead.TAFS_DOT_Number_Dup__c)){
                        objLead.TAFS_DOT_Number__c = objLead.TAFS_DOT_Number_Dup__c;
                    }
                    if(String.IsNotBlank(objLead.DBA_Name_Dup__c)){
                        objLead.TAFS_DBA_Name__c = objLead.DBA_Name_Dup__c;
                    }
                    if(objLead.TAFS_Number_of_Active_Vehicles_Dup__c != null){
                        objLead.TAFS_Number_of_Active_Vehicles__c = Integer.ValueOf(objLead.TAFS_Number_of_Active_Vehicles_Dup__c);
                    }
                    if(String.IsNotBlank(objLead.TAFS_Last_Name_Dup__c)){
                        objLead.LastName = objLead.TAFS_Last_Name_Dup__c;
                    }
                    if(String.IsNotBlank(objLead.TAFS_Email_Dup__c)){
                        objLead.Email = objLead.TAFS_Email_Dup__c;
                    }
                }
            }
        }
    }
    
    
    
    /****************************************************************************************
    * Created By      :  Arpitha Sudhakar
    * Create Date     :  19/07/2016
    * Description     :  When lead is converted, map lead fields to Agreement fields and create a new Agreement record
    * Modification Log:  Initial version.
***************************************************************************************/
    public static void mapLeadtoContractAfterConversion(List<Id> convertedLeadList) {
    
        if(prohibitAfterInsertTrigger){
            return;        
        }
        //variables
        String qry = '';
        List<TAFS_Agreement__c> agreementList =  new List<TAFS_Agreement__c>();
        TAFS_Agreement__c AgreementObj = new TAFS_Agreement__c();
        Map<string, string> MapMappingTable=new map<string,string>();
        Map<Id,TAFS_Agreement__c> leadAccMap = new Map<Id,TAFS_Agreement__c>(); 
        //fetching values from custom settings
        
        try{
            for (TAFS_Lead_Agreement_Conversion__c mappingTableRec : TAFS_Lead_Agreement_Conversion__c.getall().Values())
             {
                if (mappingTableRec.Name != null && mappingTableRec.TAFS_Agreement_Field__c != Null )
                {
                    MapMappingTable.put(mappingTableRec.TAFS_Agreement_Field__c, mappingTableRec.TAFS_Agreement_Field__c);
                    qry += mappingTableRec.TAFS_Agreement_Field__c + ',';
                }
             }
           qry = 'select ' + qry + 'id,convertedaccountid FROM Lead where id IN : convertedLeadList';
           List<Lead> MyLead = Database.query(qry);
           
           for(Lead leadObj : MyLead){
               for(String sMyLeadField: MapMappingTable.keySet())
               {
                   String sLeadField = MapMappingTable.get(sMyLeadField);
                   AgreementObj.put(sLeadField, leadObj.get(sMyLeadField));
               }
               AgreementObj.TAFS_Status__c = 'Active';
               AgreementObj.TAFS_Client_Name__c = leadObj.convertedaccountid;
               agreementList.add(agreementObj);
               leadAccMap.put(leadObj.convertedaccountid,agreementObj);
           }
           if(agreementList.size() > 0){
               Database.insert(agreementList);
               List<dsfs__DocuSign_Recipient_Status__c> docusignRecStatusList = [SELECT Id, TAFS_Agreement__c,dsfs__Lead__c,dsfs__Lead__r.TAFS_Reference_ID__c FROM dsfs__DocuSign_Recipient_Status__c WHERE dsfs__Lead__c IN : convertedLeadList];
               List<dsfs__DocuSign_Status__c> docusignStatusList = [SELECT Id, TAFS_Agreement__c,dsfs__Lead__c,dsfs__Lead__r.TAFS_Reference_ID__c FROM dsfs__DocuSign_Status__c WHERE dsfs__Lead__c IN : convertedLeadList];
               for(TAFS_Agreement__c objAgreement : agreementList){
                   for(dsfs__DocuSign_Recipient_Status__c recStatus : docusignRecStatusList){
                       if(recStatus.dsfs__Lead__r.TAFS_Reference_ID__c == objAgreement.TAFS_Reference_ID__c){
                           recStatus.TAFS_Agreement__c = objAgreement.Id;
                       }
                   }
                   for(dsfs__DocuSign_Status__c docStatus : docusignStatusList){
                       if(docStatus.dsfs__Lead__r.TAFS_Reference_ID__c == objAgreement.TAFS_Reference_ID__c){
                           docStatus.TAFS_Agreement__c = objAgreement.Id;
                       }
                   }
               }
               Database.Update(docusignRecStatusList);
               Database.Update(docusignStatusList);
               prohibitAfterInsertTrigger = true;
           } 
            System.debug('###RA-->' + leadAccMap);
        }catch(Exception ex){
            TAFS_Error_Log__c errLog = new TAFS_Error_Log__c();
            errLog.TAFS_Status__c = JSON.serializePretty(ex.getCause());
            errLog.TAFS_Status_Code__c = String.valueOf(ex.getLineNumber());
            errLog.TAFS_Request__c = ex.getTypeName();
            errLog.TAFS_Response_Body__c = ex.getMessage();
            errLog.TAFS_Endpoint__c = 'TAFS_LeadTriggerHandler.mapLeadtoContractAfterConversion';
            errLog.TAFS_Type__c = 'Salesforce Apex Exception';
            insert errLog;
        }
    }
    public static void formatphonenum (List<Lead> leadList) {
        
        for(Lead objLead : leadList){
            if(String.isNotBlank(objLead.Phone) && objLead.Phone.length() == 10){
                objLead.Phone = objLead.Phone.remove(' ');
                objLead.Phone = objLead.Phone.remove(')');
                objLead.Phone = objLead.Phone.remove('(');
                objLead.Phone = objLead.Phone.remove('-');
                objLead.Phone = formatUSFormat(objLead.Phone);
            }
            if(String.isNotBlank(objLead.TAFS_Alternate_Phone__c) && objLead.TAFS_Alternate_Phone__c.length() == 10){
                objLead.TAFS_Alternate_Phone__c = objLead.TAFS_Alternate_Phone__c.remove(' ');
                objLead.TAFS_Alternate_Phone__c = objLead.TAFS_Alternate_Phone__c.remove(')');
                objLead.TAFS_Alternate_Phone__c = objLead.TAFS_Alternate_Phone__c.remove('(');
                objLead.TAFS_Alternate_Phone__c = objLead.TAFS_Alternate_Phone__c.remove('-');
                objLead.TAFS_Alternate_Phone__c = formatUSFormat(objLead.TAFS_Alternate_Phone__c);
            }
            if(String.isNotBlank(objLead.Fax) && objLead.Fax.length() == 10){
                objLead.Fax = objLead.Fax.remove(' ');
                objLead.Fax = objLead.Fax.remove(')');
                objLead.Fax = objLead.Fax.remove('(');
                objLead.Fax = objLead.Fax.remove('-');
                objLead.Fax = formatUSFormat(objLead.Fax);
            }
        }
    }
    static String formatUSFormat(String phone){
        phone = '(' + phone.substring(0, 3) + ') ' + phone.substring(3, 6) + '-' + phone.substring(6);
        return phone;
    }
    /****************************************************************************************
    * Method Name      :  updateLeadObjectionOnCon
    * Input Parameter  :  List<Lead>
    * Return Type      :  Void
    * Description      :  Updates lead objection values on contact
    ***************************************************************************************/
    public static void updateLeadObjectionOnCon(List<Lead> leadList){
        set<ID> leadIdSet = new set<ID>();
        list<Contact> contactList =  new list<Contact>();
        map<Id, List<Contact>> leadConMap =  new map<Id, List<Contact>>();
        for(Lead objLead : leadList){
            if(objLead.Id!=null){
                leadIdSet.add(objLead.id);
            }
        }
        if(!leadIdSet.isEmpty()){        
            for(Lead leadObj : [SELECT ID,TAFS_Not_Interested_Reason__c,(Select Id from Contacts__r) FROM Lead WHERE ID IN : leadIdSet]){
                leadConMap.put(leadObj.Id,leadObj.Contacts__r) ;               
            }
        }
        for(Lead leadObj : leadList){
            if(leadConMap.containsKey(leadObj.Id)){
                for(Contact con : leadConMap.get(leadObj.Id)){
                    con.TAFS_Lead_Not_Interested_Reason__c = leadObj.TAFS_Not_Interested_Reason__c;
                    con.TAFS_Is_Lead_Trigger_Context__c = true;
                    contactList.add(con);
                }
            }        
        }
        if(!contactList.isEmpty()){
            Database.update(contactList);
        }    
    }
    /****************************************************************************************
    * Method Name      :  updateLeadStatusToIncorrectInfo
    * Input Parameter  :  List<Lead>
    * Return Type      :  Void
    * Description      :  Updates lead Status to Incorrect Info
    ***************************************************************************************/
    public static void updateLeadStatusToIncorrectInfo(List<Lead> leadList){
        Set<String> invalidPhoneSet = new Set<String>();
        String phoneStr,alternateStr;
        List<TAFS_Invalid_Phone_Number__c> phoneList = TAFS_Invalid_Phone_Number__c.getAll().Values();
        if(phoneList.size()>0){
            for(TAFS_Invalid_Phone_Number__c obj : phoneList){
                invalidPhoneSet.add(obj.Name);
            }
        }
        if(invalidPhoneSet.size()>0){
            for(Lead objLead: leadList){
                if(objLead.Phone !=null){
                    phoneStr = objLead.Phone;
                    phoneStr = phoneStr.remove(' ');
                    phoneStr = phoneStr.remove(')');
                    phoneStr = phoneStr.remove('(');
                    phoneStr = phoneStr.remove('-');
                }
                
                if(objLead.TAFS_Alternate_Phone__c !=null){
                    alternateStr = objLead.TAFS_Alternate_Phone__c;
                    alternateStr = alternateStr.remove(' ');
                    alternateStr = alternateStr.remove(')');
                    alternateStr = alternateStr.remove('(');
                    alternateStr = alternateStr.remove('-');
                }
                if((String.ISNotBlank(objLead.TAFS_Reference_Id__c) && (objLead.TAFS_Reference_Id__c.containsIgnoreCase('MX') || objLead.TAFS_Reference_Id__c.containsIgnoreCase('FF')))
                ||(String.ISNotBlank(objLead.State) && (objLead.State == 'QC' || objLead.State == 'PQ' ||objLead.State == 'Quebec' || objLead.State == 'Qu�bec'))){
                    objLead.Status = Label.TAFS_LeadStatus_Not_Eligible;
                }
                else if(invalidPhoneSet.contains(phoneStr) && invalidPhoneSet.contains(alternateStr) || (String.IsBlank(phoneStr) && invalidPhoneSet.contains(alternateStr))
                    || (String.IsBlank(alternateStr) && invalidPhoneSet.contains(phoneStr))){
                    objLead.Status = Label.TAFS_LeadStatus_Incorrect_Info;
                }
                
                /*else if(((!invalidPhoneSet.contains(phoneStr)) || (!invalidPhoneSet.contains(alternateStr))) && 
                (objLead.Status == Label.TAFS_LeadStatus_Not_Eligible || objLead.Status == Label.TAFS_LeadStatus_Incorrect_Info)){
                    objLead.Status = Label.TAFS_LeadStatus_New;
                }*/
                
            }
        }
    }
    /****************************************************************************************
    * Method Name      :  updateLeadStatuswhileLeadUpdate
    * Input Parameter  :  Map<Id,Lead>, Map<Id,Lead>
    * Return Type      :  Void
    * Description      :  Updates lead Status to Incorrect Info
    ***************************************************************************************/
    public static void updateLeadStatuswhileLeadUpdate(Map<Id,Lead> newLeadMap,Map<Id,Lead> oldLeadMap){
        Set<String> invalidPhoneSet = new Set<String>();
        String phoneStr,alternateStr;
        List<TAFS_Invalid_Phone_Number__c> phoneList = TAFS_Invalid_Phone_Number__c.getAll().Values();
        if(phoneList.size()>0){
            for(TAFS_Invalid_Phone_Number__c obj : phoneList){
                invalidPhoneSet.add(obj.Name);
            }
        }
        for(Id leadId : newLeadMap.keyset()){
            if((oldLeadMap.get(leadId).Phone != newLeadMap.get(leadId).Phone) || (oldLeadMap.get(leadId).TAFS_Alternate_Phone__c!= newLeadMap.get(leadId).TAFS_Alternate_Phone__c)){
                if(invalidPhoneSet.size()>0){
                    if(newLeadMap.get(leadId).Phone != null){
                        phoneStr = newLeadMap.get(leadId).Phone;
                        phoneStr = phoneStr.remove(' ');
                        phoneStr = phoneStr.remove(')');
                        phoneStr = phoneStr.remove('(');
                        phoneStr = phoneStr.remove('-');
                    }
                    if(newLeadMap.get(leadId).TAFS_Alternate_Phone__c !=null){
                        alternateStr = newLeadMap.get(leadId).TAFS_Alternate_Phone__c;
                        alternateStr = alternateStr.remove(' ');
                        alternateStr = alternateStr.remove(')');
                        alternateStr = alternateStr.remove('(');
                        alternateStr = alternateStr.remove('-');
                    }
                    if(invalidPhoneSet.contains(phoneStr) && invalidPhoneSet.contains(alternateStr) || (String.IsBlank(phoneStr) && invalidPhoneSet.contains(alternateStr))
                    || (String.IsBlank(alternateStr) && invalidPhoneSet.contains(phoneStr))){
                        newLeadMap.get(leadId).Status = Label.TAFS_LeadStatus_Incorrect_Info;
                    }
                    else if(newLeadMap.get(leadId).Status==Label.TAFS_LeadStatus_Incorrect_Info){
                        newLeadMap.get(leadId).Status = Label.TAFS_LeadStatus_New;
                    }
                }
            }
        }
        
    }

    /****************************************************************************************
    * Method Name      :  customizeLeadConversion
    * Input Parameter  :  List<Id>
    * Return Type      :  Void
    * Description      :  map Open Activities and Activity History from Lead to Account 
                          on Coversion
    ***************************************************************************************/
    public static void customizeLeadConversion(List<Lead> lstLeads){
        List<Task> lstTasks = new List<Task>();
        List<Id> lstConvertedContactIds = new List<Id>();
        Map<Id,Id> mapConvertedAccountByLead = new Map<Id,Id>();

        for(Lead l:lstLeads){
            lstConvertedContactIds.add(l.ConvertedContactId);
            mapConvertedAccountByLead.put(l.ConvertedContactId,l.ConvertedAccountId);
        }
        
        if(lstLeads.size()>0)
            lstTasks = [SELECT Id, WhoId, WhatId, Status FROM Task WHERE WhoId In :lstConvertedContactIds];
       
        for(Task t:lstTasks){
            t.WhatId = mapConvertedAccountByLead.get(t.WhoId);
            t.WhoId = null;
        }
        
        if(lstTasks.size() > 0)
            update lstTasks;
    }
}