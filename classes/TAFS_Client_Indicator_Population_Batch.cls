/* Class Name   : TAFS_Client_Indicator_Population_Batch
 * Description  : Batch to run everyday and create task based upon Cliennt Interaction
 * Created By   : Raushan Anand
 * Created On   : 9-Sep-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand               9-Sep-2016               Initial version.
 *
 *****************************************************************************************/
global class TAFS_Client_Indicator_Population_Batch implements Database.Batchable<sObject>{

    global List<Account> start(Database.BatchableContext BC){
        List<Account> accList = [Select Id from Account where recordtype.name='Client' AND TAFS_Client_Status__c != 'Terminated'];
        return accList;
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        for(Account a : scope){
            PageReference tpageRef = Page.TAFS_Client_Indicator;
            tpageRef.getParameters().put('Id', a.Id);
            Apexpages.StandardController stdController = new Apexpages.StandardController(a);
            TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
            controller.populateIndicator();
		}
    }
    global void finish(Database.BatchableContext BC){
        Database.executeBatch(new TAFS_Task_Prioritization_Batch(),100);
    }
}