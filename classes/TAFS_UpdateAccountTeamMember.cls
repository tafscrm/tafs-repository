/****************************************************************************************
* Created By      :  Naveenkanth B
* Create Date     :  06/07/2016
* Description     :  Batch Class to Update the Fields(RS,SAM,TOS) on Account
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_UpdateAccountTeamMember implements Database.Batchable < sObject > {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id FROM Account';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List < Account > scope) {
        
        List<Account> lstAccount = new List<Account>();
        set<String> roleList =  new set<String>();
        list<Account> updatedAccList = new list<Account>();
        List<AccountTeamMember> tempATMList = new List<AccountTeamMember>();
        Integer rsCount; 
        Integer samCount;
        Integer osCount;
        Integer uCount;
        Integer samrefCount;
        Integer samnaCount;
        Integer vsmCount;
        Integer vsmrefCount;
        Integer vsmretCount;
        Integer vsmnaCount;
        Integer vsmnarefCount;
        Integer namCount;
        Integer namrefCount;
        Map <ID, List<AccountTeamMember>> accountTeamMemberMap = new Map < ID, List<AccountTeamMember>>();
        try{            
            if(scope.size() > 0){
                //creating a map of account ids and accountteammember
                for(Account accObj : [SELECT ID, (SELECT Id ,userid, User.Name, TeamMemberRole FROM AccountTeamMembers WHERE TeamMemberRole IN('Relationship Specialist' ,'Sales Account Manager','Onboarding Specialist','Underwriter','Sales Account Manager – Referred','VP of Sales and Marketing','VP of Sales and Marketing – Referred','VP of Sales and Marketing – Retained','National Account Manager','National Account Manager – Referred','Sales Account Manager – NA','VP of Sales and Marketing – NA','VP of Sales and Marketing – NA Referred') AND AccountId IN: scope ORDER BY CreatedDate DESC) FROM Account]){                         
                    
                    rsCount = 0;
                    samCount = 0;
                    osCount = 0;
                    uCount = 0;
                    samrefCount = 0;
                    samnaCount = 0;
                    vsmCount = 0;
                    vsmrefCount = 0;
                    vsmretCount = 0;
                    vsmnaCount = 0;
                    vsmnarefCount = 0;
                    namCount = 0;
                    namrefCount = 0;
                    
                    tempATMList =  new List<AccountTeamMember>();
                    lstAccount.add(accObj);
                    
                    for(AccountTeamMember atm : accObj.AccountTeamMembers){                     
                        if(atm.TeamMemberRole == Label.TAFS_SAM && rsCount == 0){
                            rsCount++;
                            tempATMList.add(atm);                           
                        }
                        if(atm.TeamMemberRole == Label.TAFS_RS && samCount == 0){
                            samCount++;
                            tempATMList.add(atm);                           
                        }
                        if(atm.TeamMemberRole == Label.TAFS_TOS && osCount == 0){
                            osCount++;
                            tempATMList.add(atm);                           
                        }
                        if(atm.TeamMemberRole == Label.TAFS_UW && uCount == 0){
                            uCount++;
                            tempATMList.add(atm);                           
                        }
                        if(atm.TeamMemberRole == Label.TAFS_SAM_REF && samrefCount == 0){
                            samrefCount++;
                            tempATMList.add(atm);                           
                        }
                        if(atm.TeamMemberRole == Label.TAFS_SAM_NA && samnaCount == 0){
                            samnaCount++;
                            tempATMList.add(atm);
                        }
                        if(atm.TeamMemberRole == Label.TAFS_VSM && vsmCount == 0){
                            vsmCount++;
                            tempATMList.add(atm);                           
                        }
                        if(atm.TeamMemberRole == Label.TAFS_VSM_REF && vsmrefCount == 0){
                            vsmrefCount++;
                            tempATMList.add(atm);                           
                        }
                        if(atm.TeamMemberRole == Label.TAFS_VSM_RET && vsmretCount == 0){
                            vsmretCount++;
                            tempATMList.add(atm);                           
                        }
                        if(atm.TeamMemberRole == Label.TAFS_VSM_NA && vsmnaCount == 0){
                            vsmnaCount++;
                            tempATMList.add(atm);                           
                        }
                         if(atm.TeamMemberRole == Label.TAFS_VSM_NA_REF&& vsmnarefCount == 0){
                            vsmnarefCount++;
                            tempATMList.add(atm);                           
                        }
                        if(atm.TeamMemberRole == Label.TAFS_NAM && namCount == 0){
                            namCount++;
                            tempATMList.add(atm);
                        }
                        if(atm.TeamMemberRole == Label.TAFS_NAM_REF && namrefCount == 0){
                            namrefCount++;
                            tempATMList.add(atm);                           
                            
                    }   
                    accountTeamMemberMap.put(accObj.Id,tempATMList);                 
                } 
            }
            if(!lstAccount.isEmpty()){
                //Assigning the account id to the accObj record to form a set of account ids 
                for(Account accObj : lstAccount){                    
                    
                    roleList = new set<String>();
                    
                    if(accountTeamMemberMap.containskey(accObj.Id)){
                        //Assigning the TeamMemberRole to the roleList based on the account ids
                        for(AccountTeamMember teamMember : accountTeamMemberMap.get(accObj.Id)){
                            roleList.add(teamMember.TeamMemberRole);
                        }
                        
                        //Iterating through accountTeamMemberMap and checking for the teammember role 
                        for(AccountTeamMember teamMember : accountTeamMemberMap.get(accObj.Id)){                            
                            
                            if(teamMember.TeamMemberRole == Label.TAFS_RS){                             
                                accObj.TAFS_Relationship_Specialist__c = teamMember.User.Name;
                                accObj.TAFS_Relationship_Specialist_UserId__c = teamMember.userid;                             
                            }
                            
                            else if(teamMember.TeamMemberRole == Label.TAFS_SAM){                            
                                accObj.TAFS_Sales_Account_Manager__c = teamMember.User.Name;
                                accObj.TAFS_Sales_Account_Manager_Lookup__c = teamMember.UserId;
                            }
                            
                            else if(teamMember.TeamMemberRole == Label.TAFS_TOS){                             
                                accObj.TAFS_TOS__c = teamMember.User.Name;
                            }
                            
                            else if(teamMember.TeamMemberRole == Label.TAFS_UW){                             
                                accObj.TAFS_Underwriter__c = teamMember.User.Name;
                            }
                            
                            else if(teamMember.TeamMemberRole == Label.TAFS_SAM_REF){                             
                                accObj.TAFS_Sales_Account_Manager_Referred__c = teamMember.User.Name;
                            }
                            
                            else if(teamMember.TeamMemberRole == Label.TAFS_SAM_NA){                             
                                accObj.TAFS_Sales_Account_Manager_NA__c = teamMember.User.Name;
                            }
                            
                            else if(teamMember.TeamMemberRole == Label.TAFS_VSM){                             
                                accObj.TAFS_VP_of_Sales_and_Marketing__c = teamMember.User.Name;
                            }
                            
                            else if(teamMember.TeamMemberRole == Label.TAFS_VSM_REF){                             
                                accObj.TAFS_VP_of_Sales_and_Marketing_Referred__c = teamMember.User.Name;
                            }
                            
                            else if(teamMember.TeamMemberRole == Label.TAFS_VSM_RET){                             
                                accObj.TAFS_VP_of_Sales_and_Marketing_Retained__c = teamMember.User.Name;
                            }
                            
                            else if(teamMember.TeamMemberRole == Label.TAFS_VSM_NA){                             
                                accObj.TAFS_VP_of_Sales_and_Marketing_NA__c= teamMember.User.Name;
                            }
                            
                             else if(teamMember.TeamMemberRole == Label.TAFS_VSM_NA_REF){                             
                                accObj.TAFS_VP_of_Sales_Marketing_NA_Referred__c= teamMember.User.Name;
                            }
                            
                            else if(teamMember.TeamMemberRole == Label.TAFS_NAM){                             
                                accObj.TAFS_National_Account_Manager__c = teamMember.User.Name;
                            }
                            
                            else if(teamMember.TeamMemberRole == Label.TAFS_NAM_REF){                             
                                accObj.TAFS_National_Account_Manager_Referred__c = teamMember.User.Name;
                            }
                                                                          
                        }
                        if(roleList!=null){ 
                            if(!roleList.contains(Label.TAFS_RS)){
                                accObj.TAFS_Relationship_Specialist__c = '';                       
                            }
                            if(!roleList.contains(Label.TAFS_SAM)){
                                accObj.TAFS_Sales_Account_Manager__c = '';
                            }
                            if(!roleList.contains(Label.TAFS_TOS)){
                                accObj.TAFS_TOS__c = '';
                            }
                            if(!roleList.contains(Label.TAFS_UW)){
                                accObj.TAFS_Underwriter__c = '';
                            }
                            if(!roleList.contains(Label.TAFS_SAM_REF)){
                                accObj.TAFS_Sales_Account_Manager_Referred__c = '';
                            }
                            if(!roleList.contains(Label.TAFS_SAM_NA)){
                                accObj.TAFS_Sales_Account_Manager_NA__c = '';
                            }
                            if(!roleList.contains(Label.TAFS_VSM)){
                                accObj.TAFS_VP_of_Sales_and_Marketing__c = '';
                            }
                            if(!roleList.contains(Label.TAFS_VSM_REF)){
                                accObj.TAFS_VP_of_Sales_and_Marketing_Referred__c = '';
                            }
                            if(!roleList.contains(Label.TAFS_VSM_RET)){
                                accObj.TAFS_VP_of_Sales_and_Marketing_Retained__c = '';
                            }
                            if(!roleList.contains(Label.TAFS_VSM_NA)){
                                accObj.TAFS_VP_of_Sales_and_Marketing_NA__c= '';
                            }
                            if(!roleList.contains(Label.TAFS_VSM_NA_REF)){
                                accObj.TAFS_VP_of_Sales_Marketing_NA_Referred__c= '';
                            }
                            if(!roleList.contains(Label.TAFS_NAM)){
                                accObj.TAFS_National_Account_Manager__c = '';
                            }
                            if(!roleList.contains(Label.TAFS_NAM_REF)){
                                accObj.TAFS_National_Account_Manager_Referred__c = '';
                            }
                        }     
                        /*if(accountTeamMemberMap.get(accObj.Id)==null){
                            
                            accObj.TAFS_Relationship_Specialist__c   = '';
                            accObj.TAFS_Sales_Account_Manager__c     = '';
                        }*/
                        updatedAccList.add(accObj);
                        }
                    
                    }                                     
                    if(!updatedAccList.isEmpty())
                       update updatedAccList;
               }
            }
            }catch(Exception e){
                system.debug('inside eception>>'+e.getMessage());
            }
    }
    
    global void finish(Database.BatchableContext BC) {

    }
}