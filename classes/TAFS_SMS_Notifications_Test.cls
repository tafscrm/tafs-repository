/* CLass Name   : TAFS_SMS_Notifications_Test
 * Description  : Test class for to SMS Notifications code
 * Created By   : Manoj Vootla
 * Created On   : 08-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj Vootla               08-Aug-2016              Initial version.
 *
 *****************************************************************************************/
 
 @isTest(seeAllData = True)
private class TAFS_SMS_Notifications_Test{

    /************************************************************************************
    * Method       :    testSendSMSForClients
    * Description  :    Scenario to cover the code on Invoice Batch to send SMS to Client Contacts
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testSendSMSForClients() {
       
     
       Test.startTest();
       
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);  
       lstAcct[0].TAFS_Office__c  = 'USA';    
       update lstAcct[0];
       list<Contact> lstCnt = DataUtilTest_TAFS.createContacts(1,lstAcct[0].id);
       lstCnt[0].TAFS_Preferred_Communication_Channel__c = 'Text';
       update lstCnt[0];
       list<TAFS_Invoice_Batch__c> lstIB = DataUtilTest_TAFS.createIvoiceBatches(1,lstAcct[0].id);       
       lstIB[0].TAFS_Status__c = 'Processed';
       lstIB[0].TAFS_Paid_to_Client__c = 78000;
       update lstIB[0];
       TAFS_Invoice_Batch_Contact_Relation__c ibC = [select id,name from TAFS_Invoice_Batch_Contact_Relation__c where TAFS_Invoice_Batch__c = :lstIB[0].id];
       
       
       smagicinteract__smsMagic__c sh = new smagicinteract__smsMagic__c (TAFS_Invoice_Batch_Contact_Relation__c = ibC.id,
       smagicinteract__Name__c = 'Invoice Batch 123',smagicinteract__ObjectType__c = 'TAFS_Invoice_Batch_Contact_Relation__c',
       smagicinteract__SMSText__c = 'Hi There',smagicinteract__Type__c = 'Outgoing',
       smagicinteract__external_field__c = 'kjdf8uu0980',smagicinteract__PhoneNumber__c = '9944879277',
       smagicinteract__SenderId__c = '17162710020'       
       );
       
       insert sh;
       
       smagicinteract__Incoming_SMS__c is = new smagicinteract__Incoming_SMS__c(
        smagicinteract__external_field__c = 'sdfjuwe000989',
        smagicinteract__Inbound_Number__c = '17162710020',smagicinteract__Mobile_Number__c = '9944879277',
        smagicinteract__SMS_Text__c = 'How are you doing',TAFS_Invoice_Batch_Contact_Relation__c = ibC.id,
        TAFS_Relationship_Specialist_Email__c = 'vmanoj@deloitte.com'
       );
       
       insert is;
       
       Test.stopTest();
                     
    }
    
    /************************************************************************************
    * Method       :    testSendSMSForLeads
    * Description  :    Scenario to cover the code for sending SMS for Lead's contacts
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testSendSMSForLeads() {
       
     
       Test.startTest();
       
       list<Lead> leasList = DataUtilTest_TAFS.createleads(1); 
       leasList[0].status = 'Agreement Out';
       update leasList[0];
       list<Contact> lstCnt = DataUtilTest_TAFS.createContactsForLead(1,leasList[0].id);
       lstCnt[0].title = 'Owner';
       update lstCnt[0];
       Task t = new Task( subject = 'Call No Contact Task',OwnerId = UserInfo.getUserId(),WhoId  = leasList[0].id,type = 'Call No Contact'); 
       insert t;      
       
       Test.stopTest();
                     
    }
}