/****************************************************************************************
* Created By      :  Pankaj Singh
* Created Date    :  10/24/2016
* Description     :  Batch class for updating fuel consumption data on Account
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_UpdateFuelConsumptionDataOnAccount implements Database.Batchable<sObject> {
    
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<Contact>
    * Description      :  This method is used to find all the debtor contacts to whom the emails need to be sent.
    **********************************************************************************************************************/
    global List<Account> start(Database.BatchableContext BC) {
        //variable declaration
        List<Account> accountlist = new List<Account>();
        accountlist = [SELECT ID,Name,TAFS_Committed_Fuel_Units__c,TAFS_In_Network_Usage_MTD__c,TAFS_Days_In_Current_Month__c,TAFS_Last_Successful_Check_In_Date__c,
                       TAFS_Prorated_Committment_MTD__c,TAFS_Difference_Usage_vs_Committed__c FROM Account WHERE 
                       RecordTypeId =: Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()]; 
                              
        return accountlist;
    }
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<Account>
    * Return Type      :  Void
    * Description      :  This method is used to update fuel consumption data on Account
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<Account> scope) {
    
        if(scope.size()>0){       
            try{                
                List<Account> accList =  new List<Account>();
                set<Id> accIdset =  new set<Id>();
                Map<Id,List<TAFS_Fuel_Consumptions__c>> accountFCMap =  new Map<Id,List<TAFS_Fuel_Consumptions__c>>();
                Map<Id,List<TAFS_Fuel_Consumptions__c>> accountFCLMMap =  new Map<Id,List<TAFS_Fuel_Consumptions__c>>();
                Map<Id,Fuel_Discount__c> accountFDLiterMap = new Map<Id,Fuel_Discount__c>();
                Map<Id,Fuel_Discount__c> accountFDGallonMap = new Map<Id,Fuel_Discount__c>();
                Map<Id,TAFS_Fuel_Commitment__c> accFuelCommitmentMap =  new Map<Id,TAFS_Fuel_Commitment__c>();
                //added by arpitha
                Map<Id,List<TAFS_Fuel_Consumptions__c>> accountFCLMForLifetimeMap =  new Map<Id,List<TAFS_Fuel_Consumptions__c>>();
                for(Account acc : scope){
                    accIdset.add(acc.Id);
                }
                if(!accIdset.isEmpty()){
                    //populate fuel consumption map for current month
                    for(TAFS_Fuel_Consumptions__c fc : [SELECT Id,TAFS_Client_ID__c,TAFS_In_Network_Fuel_Stop__c,TAFS_Number_of_Reefer_Gallons__c,TAFS_Number_of_Reefer_Liters__c,TAFS_Number_of_Tractor_Gallons__c,
                                                        TAFS_Number_of_Tractor_Liters__c,TAFS_Total_Spent_on_Fuel_CAD__c,TAFS_Total_Spent_on_Fuel_USD__c,TAFS_Transaction_Date__c,TAFS_Transaction_Number__c
                                                        FROM TAFS_Fuel_Consumptions__c WHERE CALENDAR_MONTH(TAFS_Transaction_Date__c) =: Date.Today().Month() AND TAFS_Client_ID__c IN : accIdset]){
                        if(accountFCMap.containskey(fc.TAFS_Client_ID__c)){
                            List<TAFS_Fuel_Consumptions__c> lstTemp = accountFCMap.get(fc.TAFS_Client_ID__c);
                            lstTemp.add(fc);
                            accountFCMap.put(fc.TAFS_Client_ID__c,lstTemp);
                        }
                        else{
                            List<TAFS_Fuel_Consumptions__c> lstTemp = new List<TAFS_Fuel_Consumptions__c>();
                            lstTemp.add(fc);
                            accountFCMap.put(fc.TAFS_Client_ID__c,lstTemp);
                        }                                    
                    }
                    for(Fuel_Discount__c fd : [select Id, TAFS_Client_ID__c,TAFS_Discount_Per_Gallon__c,TAFS_Discount_Per_Liter__c,TAFS_Effective_Date_Gallon__c,TAFS_Effective_Date_Liter__c FROM Fuel_Discount__c 
                                               WHERE TAFS_Effective_Date_Liter__c<Today AND TAFS_Client_ID__c IN :accIdset]){
                        
                        if(accountFDLiterMap.containskey(fd.TAFS_Client_ID__c)){
                            if(accountFDLiterMap.get(fd.TAFS_Client_ID__c).TAFS_Effective_Date_Liter__c<fd.TAFS_Effective_Date_Liter__c){                   
                                accountFDLiterMap.put(fd.TAFS_Client_ID__c,fd);
                            }
                        }
                        else{
                            accountFDLiterMap.put(fd.TAFS_Client_ID__c,fd);
                        }                              
                    }
                    for(TAFS_Fuel_Commitment__c fuelCommitment : [SELECT Id, TAFS_Client_Name__c,TAFS_Client_Number__c,TAFS_Effective_Date__c,TAFS_Committed_Fuel_Quantity__c
                                                                  FROM TAFS_Fuel_Commitment__c WHERE TAFS_Effective_Date__c<Today AND TAFS_Client_Name__c IN :accIdset]){
                        if(accFuelCommitmentMap.containskey(fuelCommitment.TAFS_Client_Name__c)){ 
                            if(accFuelCommitmentMap.get(fuelCommitment.TAFS_Client_Name__c).TAFS_Effective_Date__c<fuelCommitment.TAFS_Effective_Date__c){                  
                                accFuelCommitmentMap.put(fuelCommitment.TAFS_Client_Name__c,fuelCommitment);
                            }
                        }
                        else{
                            accFuelCommitmentMap.put(fuelCommitment.TAFS_Client_Name__c,fuelCommitment);
                        }                                                     
                    }
                    //populate fuel consumption map for last month
                    for(TAFS_Fuel_Consumptions__c fc : [SELECT Id,TAFS_Client_ID__c,TAFS_In_Network_Fuel_Stop__c,TAFS_Number_of_Reefer_Gallons__c,TAFS_Number_of_Reefer_Liters__c,TAFS_Number_of_Tractor_Gallons__c,
                                                        TAFS_Number_of_Tractor_Liters__c,TAFS_Total_Spent_on_Fuel_CAD__c,TAFS_Total_Spent_on_Fuel_USD__c,TAFS_Transaction_Date__c,TAFS_Transaction_Number__c
                                                        FROM TAFS_Fuel_Consumptions__c WHERE TAFS_Transaction_Date__c =LAST_MONTH AND TAFS_Client_ID__c IN : accIdset]){
                        if(accountFCLMMap.containskey(fc.TAFS_Client_ID__c)){
                            List<TAFS_Fuel_Consumptions__c> lstTemp = accountFCLMMap.get(fc.TAFS_Client_ID__c);
                            lstTemp.add(fc);
                            accountFCLMMap.put(fc.TAFS_Client_ID__c,lstTemp);
                        }
                        else{
                            List<TAFS_Fuel_Consumptions__c> lstTemp = new List<TAFS_Fuel_Consumptions__c>();
                            lstTemp.add(fc);
                            accountFCLMMap.put(fc.TAFS_Client_ID__c,lstTemp);
                        }                                    
                    }
                    //populating discount map for gallon                                                                                        
                    for(Fuel_Discount__c fd : [select Id, TAFS_Client_ID__c,TAFS_Discount_Per_Gallon__c,TAFS_Discount_Per_Liter__c,TAFS_Effective_Date_Gallon__c,TAFS_Effective_Date_Liter__c FROM Fuel_Discount__c 
                                               WHERE TAFS_Effective_Date_Gallon__c<Today AND TAFS_Client_ID__c IN :accIdset]){
                        
                        if(accountFDGallonMap.containskey(fd.TAFS_Client_ID__c)){
                            if(accountFDGallonMap.get(fd.TAFS_Client_ID__c).TAFS_Effective_Date_Gallon__c<fd.TAFS_Effective_Date_Gallon__c){                   
                                accountFDGallonMap.put(fd.TAFS_Client_ID__c,fd);
                            }
                        }
                        else{
                            accountFDGallonMap.put(fd.TAFS_Client_ID__c,fd);
                        }                              
                    }
                    for(TAFS_Fuel_Consumptions__c fc : [SELECT Id,TAFS_Client_ID__c,TAFS_In_Network_Fuel_Stop__c,TAFS_Number_of_Reefer_Gallons__c,TAFS_Number_of_Reefer_Liters__c,TAFS_Number_of_Tractor_Gallons__c,
                                                        TAFS_Number_of_Tractor_Liters__c,TAFS_Total_Spent_on_Fuel_CAD__c,TAFS_Total_Spent_on_Fuel_USD__c,TAFS_Transaction_Date__c,TAFS_Transaction_Number__c
                                                        FROM TAFS_Fuel_Consumptions__c WHERE TAFS_In_Network_Fuel_Stop__c = TRUE AND TAFS_Client_ID__c IN : accIdset]){
                        if(accountFCLMForLifetimeMap.containskey(fc.TAFS_Client_ID__c)){
                            List<TAFS_Fuel_Consumptions__c> lstTemp = accountFCLMForLifetimeMap.get(fc.TAFS_Client_ID__c);
                            lstTemp.add(fc);
                            accountFCLMForLifetimeMap.put(fc.TAFS_Client_ID__c,lstTemp);
                        }
                        else{
                            List<TAFS_Fuel_Consumptions__c> lstTemp = new List<TAFS_Fuel_Consumptions__c>();
                            lstTemp.add(fc);
                            accountFCLMForLifetimeMap.put(fc.TAFS_Client_ID__c,lstTemp);
                        }                                    
                    }
            
                    for(Account accObj : scope){
                    
                        accObj.TAFS_In_Network_Usage_MTD__c = 0;
                        accObj.TAFS_Fuel_Savings_Previous_Month__c = 0;
                        accObj.TAFS_Fuel_Savings_Lifetime__c = 0;
                        accObj.TAFS_Prorated_Committment_MTD__c = 0;
                        
                        if(accFuelCommitmentMap.containsKey(accObj.Id)){                            
                            if(accFuelCommitmentMap.get(accObj.Id)!=null && accFuelCommitmentMap.get(accObj.Id).TAFS_Committed_Fuel_Quantity__c!=null)
                                
                                accObj.TAFS_Prorated_Committment_MTD__c = accFuelCommitmentMap.get(accObj.Id).TAFS_Committed_Fuel_Quantity__c/(accObj.TAFS_Days_In_Current_Month__c)*Date.Today().Day();
                        }
                        if(accountFCLMMap.containsKey(accObj.Id)){
                            for(TAFS_Fuel_Consumptions__c fc : accountFCLMMap.get(accObj.Id)){
                            
                                if(accountFDGallonMap.containsKey(accObj.Id) && accountFDGallonMap.get(accObj.Id)!=null && accountFDLiterMap.containsKey(accObj.Id) && accountFDLiterMap.get(accObj.Id)!=null && fc.TAFS_In_Network_Fuel_Stop__c){
                                    accObj.TAFS_Fuel_Savings_Previous_Month__c = accObj.TAFS_Fuel_Savings_Previous_Month__c +  (((fc.TAFS_Number_of_Reefer_Gallons__c + fc.TAFS_Number_of_Tractor_Gallons__c)*accountFDGallonMap.get(accObj.Id).TAFS_Discount_Per_Gallon__c)                                                                            
                                                                                 + ((fc.TAFS_Number_of_Tractor_Liters__c+ fc.TAFS_Number_of_Reefer_Liters__c)*accountFDLiterMap.get(accObj.Id).TAFS_Discount_Per_Liter__c));
                                }
                            }
                        }
                        //start : US-3497 added by arpitha
                        if(accountFCLMForLifetimeMap.containsKey(accObj.Id)){
                            for(TAFS_Fuel_Consumptions__c fc : accountFCLMForLifetimeMap.get(accObj.Id)){
                                if(accountFDGallonMap.containsKey(accObj.Id) && accountFDGallonMap.get(accObj.Id)!=null && accountFDLiterMap.containsKey(accObj.Id) && accountFDLiterMap.get(accObj.Id)!=null){
                                    accObj.TAFS_Fuel_Savings_Lifetime__c = accObj.TAFS_Fuel_Savings_Lifetime__c + (((fc.TAFS_Number_of_Reefer_Gallons__c + fc.TAFS_Number_of_Tractor_Gallons__c)*accountFDGallonMap.get(accObj.Id).TAFS_Discount_Per_Gallon__c) +                                                                                    
                                                                           ((fc.TAFS_Number_of_Tractor_Liters__c+ fc.TAFS_Number_of_Reefer_Liters__c)*accountFDLiterMap.get(accObj.Id).TAFS_Discount_Per_Liter__c));
                                }
                            }
                        }
                        //end : US-3497
                        if(accountFCMap.containsKey(accObj.Id)){
                            for(TAFS_Fuel_Consumptions__c fc : accountFCMap.get(accObj.Id)){
                                if(fc.TAFS_In_Network_Fuel_Stop__c){        
                                     accObj.TAFS_In_Network_Usage_MTD__c = accObj.TAFS_In_Network_Usage_MTD__c + (fc.TAFS_Number_of_Reefer_Gallons__c + fc.TAFS_Number_of_Tractor_Gallons__c) +((fc.TAFS_Number_of_Tractor_Liters__c + fc.TAFS_Number_of_Reefer_Liters__c)*0.264172);
                                }                
                            }
                        }
                        if(accObj.TAFS_Prorated_Committment_MTD__c!=null && accObj.TAFS_In_Network_Usage_MTD__c!=null){
                            accObj.TAFS_Difference_Usage_vs_Committed__c =  accObj.TAFS_In_Network_Usage_MTD__c - accObj.TAFS_Prorated_Committment_MTD__c;
                        }
                        accList.add(accObj);
                    }
                    if(!accList.isEmpty()){
                        try{
                            update accList;
                        }
                        catch(Exception e){
                            //Do Nothing;
                        }
                    }
                }          
            }catch(Exception excep){
                 //Do Nothing
            }          
        }               
    }
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post the emails are sent.
    **********************************************************************************************************************/    
    global void finish(Database.BatchableContext BC) {
    
    }
}