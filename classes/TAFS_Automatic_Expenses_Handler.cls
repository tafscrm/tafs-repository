/* CLass Name   : TAFS_Automatic_Expenses_Handler
 * Description  : Trigger methods for TAFS_Automatic_Expenses__c 
 * Created By   : Manoj M Vootla
 * Created On   : 27-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla              27-May-2015              Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_Automatic_Expenses_Handler {

/****************************************************************************************
* Method Name      :  updateClientIdOnAutomaticExpense
* Input Parameter  :  List of Driver Lists (List<TAFS_Automatic_Expenses__c>)
* Return Type      :  Void
* Description      :  This method populates CLient Id based upon Client key
***************************************************************************************/
    public static void updateClientIdOnAutomaticExpense(List<TAFS_Automatic_Expenses__c> automaticExpenseList){
    
        List<String> clientNoList = new List<String>();
        Map<String,String> clientNoIdMap = new Map<String,String>();
        for(TAFS_Automatic_Expenses__c aexpense : automaticExpenseList){
            if(aexpense.TAFS_Client_Number__c != null){
                clientNoList.add(aexpense.TAFS_Client_Number__c);
            }
        }

        for(Account accObj : [SELECT Id,TAFS_Client_Number__c,Name FROM account WHERE TAFS_Client_Number__c IN: clientNoList]){
            clientNoIdMap.put(accObj.TAFS_Client_Number__c,accObj.Id);
        }
        for(TAFS_Automatic_Expenses__c aexpense : automaticExpenseList){
            if(clientNoIdMap.containsKey(aexpense.TAFS_Client_Number__c)){
                aexpense.TAFS_Client_Name__c = clientNoIdMap.get(aexpense.TAFS_Client_Number__c);
            }
        } 
    }
}