/* CLass Name : TAFS_Invoice_Batch_SMS_Trigger_Helper 
 * Description  : SMS Notificatoin Trigger methods for TAFS_Invoice_Batch_Trigger 
 * Created By   : Manoj Vootla
 * Created On   : 7/27/2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj Vootla               7/27/2016               Initial version.
 *
 *****************************************************************************************/

public class TAFS_Invoice_Batch_SMS_Trigger_Helper {
      
    public static void createInvoiceBatchContactRecords(List<TAFS_Invoice_Batch__c> invoiceBatchList)
    {
      List<Id> accouontIdList = new List<Id>();
      Map<Id,List<Contact>> accContactsMap = new Map<Id,List<Contact>>();
      List<TAFS_Invoice_Batch_Contact_Relation__c> upsertInvoiceBatchContacts = new List<TAFS_Invoice_Batch_Contact_Relation__c>();
      String phone = '';
      for(TAFS_Invoice_Batch__c ib : invoiceBatchList)
      {
       accouontIdList.add(ib.TAFS_Client_ID__c);
      }
           
      for(Account a:[select id,name,TAFS_Relationship_Specialist_UserId__r.email,(select id,name,accountid,TAFS_Preferred_Communication_Channel__c,MobilePhone,Phone from contacts) from account where id in :accouontIdList])
      {
       accContactsMap.put(a.id,a.contacts);
      }
      
      for(TAFS_Invoice_Batch__c ib : invoiceBatchList)
      { 
       for(contact c : accContactsMap.get(ib.TAFS_Client_ID__c))
       {
        TAFS_Invoice_Batch_Contact_Relation__c invoiceBatchCont = new TAFS_Invoice_Batch_Contact_Relation__c();
        invoiceBatchCont.TAFS_External_Id__c = String.valueof(ib.id) + String.valueof(c.id);
        invoiceBatchCont.TAFS_Invoice_Batch__c = ib.id;
        invoiceBatchCont.Contact__c = c.id;
        if(c.MobilePhone != null) {
            phone = string.valueof(c.MobilePhone).replaceAll('[^a-zA-Z0-9]', ''); 
            invoiceBatchCont.TAFS_Contact_Phone__c = '1'+ phone;
        }
        else if(c.Phone != null){
            phone = string.valueof(c.Phone).replaceAll('[^a-zA-Z0-9]', ''); 
            invoiceBatchCont.TAFS_Contact_Phone__c = '1'+ phone;
        }
        invoiceBatchCont.TAFS_Blocked_Reason__c = ib.TAFS_Blocked_Reason__c;
        invoiceBatchCont.TAFS_Blocked_State__c = ib.TAFS_Blocked_State__c;
        invoiceBatchCont.TAFS_Paid_to_Client__c = ib.TAFS_Paid_to_Client__c;
        invoiceBatchCont.TAFS_Preferred_Communication_Channel__c = c.TAFS_Preferred_Communication_Channel__c ;
        if(Trigger.isInsert) invoiceBatchCont.TAFS_Invoice_Batch_Created__c = true;
        else invoiceBatchCont.TAFS_Invoice_Batch_Created__c = false;
        invoiceBatchCont.TAFS_Invoice_Batch_Status__c = ib.TAFS_Status__c;
        upsertInvoiceBatchContacts.add(invoiceBatchCont);
        system.debug('Hello');
       }
      }
       if(!upsertInvoiceBatchContacts.isempty() && upsertInvoiceBatchContacts.size() >0)
       upsert upsertInvoiceBatchContacts TAFS_External_Id__c ;
         
    }   
         
  }