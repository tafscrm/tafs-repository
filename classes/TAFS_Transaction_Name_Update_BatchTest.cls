/* CLass Name   : TAFS_Transaction_Name_Update_BatchTest
 * Description  : Test class for TAFS_Transaction_Name_Update_Batch
 * Created By   : Arpitha Sudhakar
 * Created On   : 29-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar           29-Aug-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_Transaction_Name_Update_BatchTest {

    static testmethod void test() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       List<TAFS_Invoice_Transaction__c> invTranList = DataUtilTest_TAFS.createInvoiceTransactionsType(1,'1024','FUND');     
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       List<Lead> leadList = [select id,name from Lead];
       
       Test.startTest();
       TAFS_Transaction_Name_Update_Batch c = new TAFS_Transaction_Name_Update_Batch();
       Database.executeBatch(c);
       Test.stopTest();
    }
    
    static testmethod void testtwo() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       List<TAFS_Invoice_Transaction__c> invTranList = DataUtilTest_TAFS.createInvoiceTransactionsType(1,'1024','BUY');     
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       List<Lead> leadList = [select id,name from Lead];
       
       Test.startTest();
       TAFS_Transaction_Name_Update_Batch c = new TAFS_Transaction_Name_Update_Batch();
       Database.executeBatch(c);
       Test.stopTest();
    }
    
    static testmethod void testthree() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       List<TAFS_Invoice_Transaction__c> invTranList = DataUtilTest_TAFS.createInvoiceTransactionsType(1,'1024','PMT');     
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       List<Lead> leadList = [select id,name from Lead];
       
       Test.startTest();
       TAFS_Transaction_Name_Update_Batch c = new TAFS_Transaction_Name_Update_Batch();
       Database.executeBatch(c);
       Test.stopTest();
    }
    
    static testmethod void testfour() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       List<TAFS_Invoice_Transaction__c> invTranList = DataUtilTest_TAFS.createInvoiceTransactionsType(1,'1024','PMT NSF');     
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       List<Lead> leadList = [select id,name from Lead];
       
       Test.startTest();
       TAFS_Transaction_Name_Update_Batch c = new TAFS_Transaction_Name_Update_Batch();
       Database.executeBatch(c);
       Test.stopTest();
    }
    
    static testmethod void testfive() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       List<TAFS_Invoice_Transaction__c> invTranList = DataUtilTest_TAFS.createInvoiceTransactionsType(1,'1024','*C/B');     
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       List<Lead> leadList = [select id,name from Lead];
       
       Test.startTest();
       TAFS_Transaction_Name_Update_Batch c = new TAFS_Transaction_Name_Update_Batch();
       Database.executeBatch(c);
       Test.stopTest();
    }
    
    static testmethod void testsix() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       List<TAFS_Invoice_Transaction__c> invTranList = DataUtilTest_TAFS.createInvoiceTransactionsType(1,'1024','C/B');     
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       List<Lead> leadList = [select id,name from Lead];
       
       Test.startTest();
       TAFS_Transaction_Name_Update_Batch c = new TAFS_Transaction_Name_Update_Batch();
       Database.executeBatch(c);
       Test.stopTest();
    }
    
    static testmethod void testpositive() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       DataUtilTest_TAFS.createContactsForLead(1,leadLst[0].id);
       List<Lead> leadList = [select id,name from Lead];
       List<TAFS_Invoice_Transaction__c> invTranList = DataUtilTest_TAFS.createInvoiceTransactions(1,'1024');
       
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       List<Contact> contactLst = DataUtilTest_TAFS.createContactsForLead(leadList.size(),leadList[0].Id);     
       
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        
        TAFS_Transaction_Name_Update_Batch AU = new TAFS_Transaction_Name_Update_Batch();
        
        AU.execute(BC, invTranList);
        AU.finish(BC);        
    }
    
    
}