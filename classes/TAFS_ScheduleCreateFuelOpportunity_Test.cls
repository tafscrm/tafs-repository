/* CLass Name   : TAFS_ScheduleCreateFuelOpportunity_Test
 * Description  : Test class for TAFS_ScheduleCreateFuelOpportunityForRS
 * Created By   : Karthik Gulla
 * Created On   : 27-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla          27-Feb-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_ScheduleCreateFuelOpportunity_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(2,clientTermList[0].Id);
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[0].Id, UserInfo.getUserId(), 'Relationship Specialist');
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[1].Id, UserInfo.getUserId(), 'Relationship Specialist');
        DataUtilTest_TAFS.createOpportunities(1, lstAcct[1].Id, 'Fuel');
    } 

    /************************************************************************************
    * Method       :    testScheduleCreateNewFuelOpportunity
    * Description  :    Test Method to test the Creation of Fuel Opportunity when a 
                        Relationship Specialist is added on the client
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testScheduleCreateNewFuelOpportunity() {
        Test.startTest();
        TAFS_ScheduleCreateFuelOpportunityForRS tafsInvSummaryLoad = new TAFS_ScheduleCreateFuelOpportunityForRS();
        String scheduleTime = '0 0 23 * * ?';
        System.schedule('TAFS_ScheduleCreateFuelOpportunityRS_Test', scheduleTime, tafsInvSummaryLoad);
        Test.stopTest();
    }
}