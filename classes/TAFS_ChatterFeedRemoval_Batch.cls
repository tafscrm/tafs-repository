/****************************************************************************************
* Created By      :  Karthik Gulla
* Created Date    :  11/10/2016
* Description     :  Batch class to hard delete the chatter feeds that are created 31 days 
                     before or beyond
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_ChatterFeedRemoval_Batch implements Database.Batchable<sObject> {
    
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<Contact>
    * Description      :  This method is used to find all the chatter feeds for all objects that are 31 days or before
    **********************************************************************************************************************/
    global List<FeedItem> start(Database.BatchableContext BC) {
       List<FeedItem> lstChatterFeedsForRemoval = new List<FeedItem>();
       String chatterFeedRemovalQuery = 'SELECT Id, CreatedDate FROM FeedItem WHERE CreatedDate <= N_DAYS_AGO :'+Label.TAFS_ChatterFeedRemoval_Days;
       lstChatterFeedsForRemoval = Database.query(chatterFeedRemovalQuery);
       return lstChatterFeedsForRemoval;
    }
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<FeedItem>
    * Return Type      :  Void
    * Description      :  This method is used to hard delete chatter feeds on all objects
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<FeedItem> lstFeedItems) {
        if(lstFeedItems.size()>0){       
            try{                
                delete lstFeedItems;
            }catch(Exception excep){
                 System.debug('### Exception: TAFS_ChatterFeedRemoval_Batch.execute ###'+excep.getMessage());
            }          
        }               
    }
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post hard delete of chatter feeds on all objects.
    **********************************************************************************************************************/    
    global void finish(Database.BatchableContext BC) {
    
    }
}