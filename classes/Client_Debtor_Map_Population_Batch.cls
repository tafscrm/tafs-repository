/* Class Name   : TAFS_Client_Indicator_Population_Batch
 * Description  : Class to be delted after one execution
 * Created By   : Raushan Anand
 * Created On   : 6-Oct-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand               6-Oct-2016               Initial version.
 *
 *****************************************************************************************/
global class Client_Debtor_Map_Population_Batch implements Database.Batchable<sObject>{

    global List<TAFS_Invoice__c> start(Database.BatchableContext BC){
        List<TAFS_Invoice__c> invList = [SELECT Id,TAFS_Client_Debtor_Map__c,TAFS_Client_Name__c,Debtor__c FROM TAFS_Invoice__c WHERE TAFS_Client_Name__c != null AND Debtor__c != null];
        return invList;
    }
    global void execute(Database.BatchableContext BC, List<TAFS_Invoice__c> scope){
        for(TAFS_Invoice__c inv : scope){
            if(String.isNotBlank(inv.Debtor__c) && STring.isNotBlank(inv.TAFS_Client_Name__c))
    			inv.TAFS_Client_Debtor_Map__c = inv.Debtor__c+'_'+inv.TAFS_Client_Name__c;
		}
		UPDATE scope;
    }
    global void finish(Database.BatchableContext BC){
        
    }
}