/* Class Name   : TAFS_Update_Fuel_Vs_Commitment_Batch
 * Description  : This class updates all the value in
 * Created By   : Raushan Anand
 * Created On   : 18-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                18-Aug-2016              Initial version.
 *
 *****************************************************************************************/
global class TAFS_Update_Fuel_Vs_Commitment_Batch implements Database.Batchable<sObject>{
    global List<Account> start(Database.BatchableContext BC){
        List<Account> log = [SELECT Id,TAFS_Fuel_vs_Commitment__c,TAFS_Difference_Usage_vs_Committed__c,TAFS_Prorated_Committment_MTD__c  FROM Account where RecordType.Name='Client' AND TAFS_Prorated_Committment_MTD__c  != null AND TAFS_Difference_Usage_vs_Committed__c != null 
                            AND TAFS_Prorated_Committment_MTD__c  != 0 AND TAFS_Difference_Usage_vs_Committed__c != 0];
        return log;
    }
   
    global void execute(Database.BatchableContext BC, List<Account> scope){
        Double netValue=0;
        for(Account objAccount : scope){
             netValue = objAccount.TAFS_Difference_Usage_vs_Committed__c / objAccount.TAFS_Prorated_Committment_MTD__c;
                if(netValue >=0.2)
                    objAccount.TAFS_Fuel_vs_Commitment__c='Positive';
                else if(netValue >= -0.05 && netValue < 0.2)
                    objAccount.TAFS_Fuel_vs_Commitment__c='Neutral';
                else if(netValue < -0.05 && netValue > -0.2)
                    objAccount.TAFS_Fuel_vs_Commitment__c='Warning';
                else
                    objAccount.TAFS_Fuel_vs_Commitment__c='Negative';   
        }
        Database.update(scope);
        System.Debug('###RA'+scope);
    }   
    
    global void finish(Database.BatchableContext BC){
        
    }
}