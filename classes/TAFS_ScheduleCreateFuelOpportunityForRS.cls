/* CLass Name   : TAFS_ScheduleCreateFuelOpportunityForRS
 * Description  : Create a Fuel Opportunity When a User of Role - 'Relationship Specialist' is added to Account
 * Created By   : Karthik Gulla
 * Created On   : 22-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla              22-Feb-2017              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleCreateFuelOpportunityForRS implements Schedulable {   
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_CreateFuelOpportunityForRS_Batch());
   }
}