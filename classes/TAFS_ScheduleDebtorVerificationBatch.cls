/* CLass Name   : TAFS_ScheduleDebtorVerificationBatch
 * Description  : Scheduler class for TAFS_DeleteDebtorVerificationsBatch class
 * Created By   : Pankaj Singh
 * Created On   : 10-Oct-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                10-Oct-2016              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleDebtorVerificationBatch implements Schedulable {
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_DeleteDebtorVerificationsBatch());
   }
}