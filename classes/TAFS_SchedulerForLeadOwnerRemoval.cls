global class TAFS_SchedulerForLeadOwnerRemoval implements Schedulable{ 

     global void execute(SchedulableContext sc) {
        TAFS_BatchLeadOwnerRemoval b = new TAFS_BatchLeadOwnerRemoval(); 
        database.executebatch(b);
     }
  }
/*
public static String CRON_7AM = '0 0 7 * * ?';
public static String CRON_12PM = '0 0 12 * * ?';
public static String CRON_6PM = '0 0 18 * * ?';
System.schedule('LeadOwnerBatch 7AM', CRON_7AM, new TAFS_SchedulerForLeadOwnerRemoval());
System.schedule('LeadOwnerBatch 12PM', CRON_12PM, new TAFS_SchedulerForLeadOwnerRemoval());
System.schedule('LeadOwnerBatch 6PM', CRON_6PM, new TAFS_SchedulerForLeadOwnerRemoval());
*/