/* CLass Name   : TAFS_ScheduleFuelVsCommitmentBatchTest 
 * Description  : Update Fuel Vs Commitment Values
 * Created By   : Raushan Anand
 * Created On   : 29-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                29-Aug-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_ScheduleFuelVsCommitmentBatchTest {
    
    @testSetup 
	static void setup() {
		list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
		list<Account> lstAcct = DataUtilTest_TAFS.createClients(1,clientTermList[0].Id);	    
    }  
	static testmethod void testFuelCommitmentBatch1(){     
        Test.startTest();    
        TAFS_ScheduleFuelVsCommitmentBatch sh1 = new TAFS_ScheduleFuelVsCommitmentBatch();
		String sch = '0 0 23 * * ?';
        List<Account> accList = [Select Id from Account LIMIT 1];
        accList[0].TAFS_Difference_Usage_vs_Committed__c = 50;
        accList[0].TAFS_Prorated_Committment_MTD__c = 100;
        UPDATE accList[0];
        system.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest();    
    }
    static testmethod void testFuelCommitmentBatch2(){     
        Test.startTest();    
        TAFS_ScheduleFuelVsCommitmentBatch sh1 = new TAFS_ScheduleFuelVsCommitmentBatch();
		String sch = '0 0 23 * * ?';
        List<Account> accList = [Select Id from Account LIMIT 1];
        accList[0].TAFS_Difference_Usage_vs_Committed__c = -50;
        accList[0].TAFS_Prorated_Committment_MTD__c = 100;
        UPDATE accList[0];
        system.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest();    
    }
    static testmethod void testFuelCommitmentBatch3(){     
        Test.startTest();    
        TAFS_ScheduleFuelVsCommitmentBatch sh1 = new TAFS_ScheduleFuelVsCommitmentBatch();
		String sch = '0 0 23 * * ?';
        List<Account> accList = [Select Id from Account LIMIT 1];
        accList[0].TAFS_Difference_Usage_vs_Committed__c = 10;
        accList[0].TAFS_Prorated_Committment_MTD__c = 100;
        UPDATE accList[0];
        system.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest();    
    }
    static testmethod void testFuelCommitmentBatch4(){     
        Test.startTest();    
        TAFS_ScheduleFuelVsCommitmentBatch sh1 = new TAFS_ScheduleFuelVsCommitmentBatch();
		String sch = '0 0 23 * * ?';
        List<Account> accList = [Select Id from Account LIMIT 1];
        accList[0].TAFS_Difference_Usage_vs_Committed__c = -10;
        accList[0].TAFS_Prorated_Committment_MTD__c = 100;
        UPDATE accList[0];
        system.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest();    
    }
}