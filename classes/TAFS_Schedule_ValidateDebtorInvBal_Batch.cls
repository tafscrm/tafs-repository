/* CLass Name   : TAFS_Schedule_ValidateDebtorInvBal_Batch
 * Description  : Schedulable class for the batch to create a class
                  when a particular Client/Debtor balance exceeds business limits
 * Created By   : Karthik Gulla
 * Created On   : 16-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla              08-Mar-2017              Initial version.
 *
 *****************************************************************************************/
global class TAFS_Schedule_ValidateDebtorInvBal_Batch implements Schedulable {   
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_Validate_DebtorInvoiceBalance_Batch());
   }
}