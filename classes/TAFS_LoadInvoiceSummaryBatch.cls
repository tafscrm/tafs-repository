/****************************************************************************************
* Created By      :  Karthik Gulla
* Create Date     :  09/02/2017
* Description     :  Batch class to load Invoice Summary by Month and Year
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_LoadInvoiceSummaryBatch implements Database.Batchable<sObject> {
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<AggregateResult>
    * Description      :  This method is used to find the aggregate results of InvoiceBatch, Invoices
    **********************************************************************************************************************/
    global List<AggregateResult> start(Database.BatchableContext BC) {
        String aggregateQueryStr = 'SELECT Calendar_Year(TAFS_Posted_Date__c) Year, Calendar_Month(TAFS_Posted_Date__c) Month,'+
                                   'COUNT(Name) InvoiceBatches FROM TAFS_Invoice_Batch__c WHERE TAFS_Posted_Date__c = LAST_N_MONTHS: '+Label.TAFS_InvoiceSummary_Calculations_NumberOfMonths+
                                   ' AND TAFS_Status__c = \''+Label.TAFS_Pick_list_Value_For_Invoice_Batch_Processed+'\' Group By Calendar_Year(TAFS_Posted_Date__c), Calendar_Month(TAFS_Posted_Date__c)';
       List<AggregateResult> lstInvoiceBatches = new List<AggregateResult>();
       for(AggregateResult aggInvBatchRes:Database.query(aggregateQueryStr)){
            lstInvoiceBatches.add(aggInvBatchRes);
       }
       return lstInvoiceBatches;
    }
    
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<AggregateResult>
    * Return Type      :  Void
    * Description      :  This method is used update/insert Invoice summary calculations to Invoice Summary Table
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<AggregateResult> scope) {
        Map<String,String> mapFieldMappings = new Map<String,String>();
        mapFieldMappings.put('ARAmountUSD','TAFS_A_R_Amount_USD__c');
        mapFieldMappings.put('ARAmountCAD','TAFS_A_R_Amount_CAD__c');
        mapFieldMappings.put('FeesEarnedUSD','TAFS_Fees_Earned_USD__c');
        mapFieldMappings.put('FeesEarnedCAD','TAFS_Fees_Earned_CAD__c');
        mapFieldMappings.put('Invoices','TAFS_Number_of_Invoices__c');
        mapFieldMappings.put('InvoiceBatches','TAFS_Number_of_InvoiceBatches__c');
        mapFieldMappings.put('TotalGallons','TAFS_Total_Fuel_Gallons__c');
        mapFieldMappings.put('TotalLitres','TAFS_Total_Fuel_Litres__c');

        String usdCurrencyType = 'USD';
        String cadCurrencyType = 'CAD';

        Map<String,TAFS_Invoice_Summary__c> mapInvoiceSummary = new Map<String,TAFS_Invoice_Summary__c>();
        List<TAFS_Invoice_Summary__c> lstInsertInvSummary = new List<TAFS_Invoice_Summary__c>();
        List<TAFS_Invoice_Summary__c> lstUpdateInvSummary = new List<TAFS_Invoice_Summary__c>();
        Map<String,AggregateResult> mapCalendarFuelConsumption = new Map<String,AggregateResult>();

        String fcAggregateQueryStr = 'SELECT Calendar_Year(TAFS_Transaction_Date__c) Year, Calendar_Month(TAFS_Transaction_Date__c) Month, SUM(TAFS_Total_Gallons__c) TotalGallons,'+
                                     'SUM(TAFS_Total_Litres__c) TotalLitres FROM TAFS_Fuel_Consumptions__c WHERE TAFS_Transaction_Date__c = LAST_N_MONTHS: '+Label.TAFS_InvoiceSummary_Calculations_NumberOfMonths+
                                     ' AND TAFS_In_Network_Fuel_Stop__c = TRUE Group By Calendar_Year(TAFS_Transaction_Date__c), Calendar_Month(TAFS_Transaction_Date__c)';

        for(AggregateResult fcAggRes: Database.query(fcAggregateQueryStr)){
            mapCalendarFuelConsumption.put(getMonthName(Integer.valueOf(fcAggRes.get('Month')),'MMM')+' '+String.valueOf(fcAggRes.get('Year')),fcAggRes);
        }

        Map<String,AggregateResult> mapCalendarFuelConsumptionTemp = mapCalendarFuelConsumption.clone();
        for(AggregateResult existInv:scope){
            String invSummName = getMonthName(Integer.valueOf(existInv.get('Month')),'MMM')+' '+String.valueOf(existInv.get('Year'));
            if(mapCalendarFuelConsumptionTemp.containsKey(invSummName)){
                mapCalendarFuelConsumptionTemp.remove(invSummName);
            }
        }
        
        for(String mapFuelKey:mapCalendarFuelConsumptionTemp.keySet()){
            String invSummName = getMonthName(Integer.valueOf(mapCalendarFuelConsumptionTemp.get(mapFuelKey).get('Month')),'MMM')+' '+String.valueOf(mapCalendarFuelConsumptionTemp.get(mapFuelKey).get('Year'));
            TAFS_Invoice_Summary__c tafsInvSumm = new TAFS_Invoice_Summary__c();
            tafsInvSumm.Name = invSummName;
            tafsInvSumm.TAFS_Year__c = String.valueOf(mapCalendarFuelConsumptionTemp.get(mapFuelKey).get('Year'));
            tafsInvSumm.TAFS_Month__c = getMonthName(Integer.valueOf(mapCalendarFuelConsumptionTemp.get(mapFuelKey).get('Month')),'MMMM');
            if(mapCalendarFuelConsumptionTemp.containsKey(invSummName)){
                tafsInvSumm.TAFS_Total_Fuel_Gallons__c = Decimal.valueOf(String.valueOf(mapCalendarFuelConsumptionTemp.get(invSummName).get('TotalGallons')));
                tafsInvSumm.TAFS_Total_Fuel_Litres__c = Decimal.valueOf(String.valueOf(mapCalendarFuelConsumptionTemp.get(invSummName).get('TotalLitres')));

                Map<String,Decimal> mapDiffGallons = calculatePercentageWithDifference('TotalGallons', mapCalendarFuelConsumptionTemp.get(invSummName), mapInvoiceSummary, mapFieldMappings);
                tafsInvSumm.put('TAFS_Fuel_Difference_Gallons__c', mapDiffGallons.get('difference'));
                tafsInvSumm.put('TAFS_Percent_Difference_of_Fuel_Gallons__c', mapDiffGallons.get('percent'));

                Map<String,Decimal> mapDiffLitres = calculatePercentageWithDifference('TotalLitres', mapCalendarFuelConsumptionTemp.get(invSummName), mapInvoiceSummary, mapFieldMappings);
                tafsInvSumm.put('TAFS_Fuel_Difference_Litres__c', mapDiffLitres.get('difference'));
                tafsInvSumm.put('TAFS_Percent_Difference_of_Fuel_Litres__c', mapDiffLitres.get('percent'));
            }
            lstInsertInvSummary.add(tafsInvSumm);
        }

        String existInvSummaryQueryStr = 'SELECT Id, Name, TAFS_Month__c, TAFS_Fees_Earned_USD__c, TAFS_Fees_Earned_Difference_USD__c,'+ 
                                         'TAFS_Percent_Difference_Fees_Earned_USD__c, TAFS_A_R_Amount_USD__c, TAFS_A_R_Amount_Difference_USD__c,'+
                                         'TAFS_Percent_Difference_A_R_Amount_USD__c, TAFS_Total_Fuel_Gallons__c, TAFS_Total_Fuel_Litres__c,'+ 
                                         'TAFS_Fuel_Difference_Gallons__c, TAFS_Fuel_Difference_Litres__c, TAFS_Percent_Difference_of_Fuel_Gallons__c,'+
                                         'TAFS_Percent_Difference_of_Fuel_Litres__c, TAFS_Number_of_Invoices__c, TAFS_Number_of_Invoices_Difference__c,'+
                                         'TAFS_Percent_Difference_NumberOfInvoices__c, TAFS_Number_of_InvoiceBatches__c, TAFS_Number_of_InvoiceBatches_Difference__c,'+
                                         'TAFS_Percent_Difference_InvoiceBatches__c, TAFS_Year__c, TAFS_Fees_Earned_CAD__c, TAFS_A_R_Amount_CAD__c, TAFS_Fees_Earned_Difference_CAD__c,'+
                                         'TAFS_A_R_Amount_Difference_CAD__c, TAFS_Percent_Difference_Fees_Earned_CAD__c, TAFS_Percent_Difference_A_R_Amount_CAD__c'+
                                         ' FROM TAFS_Invoice_Summary__c WHERE (CreatedDate = LAST_N_MONTHS: '+Label.TAFS_InvoiceSummary_Calculations_NumberOfMonths+
                                         'OR LastModifiedDate = LAST_N_MONTHS: '+Label.TAFS_InvoiceSummary_Calculations_NumberOfMonths+')';
        for(TAFS_Invoice_Summary__c existInvSummary:Database.query(existInvSummaryQueryStr)){
            mapInvoiceSummary.put(existInvSummary.Name,existInvSummary);
        }

        for(AggregateResult invBatchAggRes:scope){
            String invSummaryName = getMonthName(Integer.valueOf(invBatchAggRes.get('Month')),'MMM')+' '+String.valueOf(invBatchAggRes.get('Year'));
            TAFS_Invoice_Summary__c invSummary;
            if(mapInvoiceSummary.containsKey(invSummaryName)){
                invSummary = mapInvoiceSummary.get(invSummaryName);
            }
            else{
                invSummary = new TAFS_Invoice_Summary__c();
                invSummary.Name = invSummaryName;
                invSummary.TAFS_Year__c = String.valueOf(invBatchAggRes.get('Year'));
                invSummary.TAFS_Month__c = getMonthName(Integer.valueOf(invBatchAggRes.get('Month')),'MMMM');
                invSummary.TAFS_SummaryDate__c = Date.newInstance(Integer.valueOf(invBatchAggRes.get('Year')), Integer.valueOf(invBatchAggRes.get('Month')), 1);
            }

            invSummary.TAFS_Number_of_InvoiceBatches__c = Decimal.valueOf(String.valueOf(invBatchAggRes.get('InvoiceBatches')));

            Map<String,Decimal> mapDiffInvBatchesVals = calculatePercentageWithDifference('InvoiceBatches', invBatchAggRes, mapInvoiceSummary, mapFieldMappings);
            invSummary.put('TAFS_Number_of_InvoiceBatches_Difference__c', mapDiffInvBatchesVals.get('difference'));
            invSummary.put('TAFS_Percent_Difference_InvoiceBatches__c', mapDiffInvBatchesVals.get('percent'));

            //Number of Invoices Calculation
            String invoicesQueryStr = 'SELECT Calendar_Year(TAFS_Invoice_Batch__r.TAFS_Posted_Date__c) Year, Calendar_Month(TAFS_Invoice_Batch__r.TAFS_Posted_Date__c) Month, COUNT(Name) Invoices FROM'+
                                      ' TAFS_Invoice__c WHERE TAFS_Invoice_Batch__r.TAFS_Status__c =\''+Label.TAFS_Pick_list_Value_For_Invoice_Batch_Processed+'\' AND TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = LAST_N_MONTHS: '+Label.TAFS_InvoiceSummary_Calculations_NumberOfMonths+
                                      ' GROUP BY Calendar_Year(TAFS_Invoice_Batch__r.TAFS_Posted_Date__c), Calendar_Month(TAFS_Invoice_Batch__r.TAFS_Posted_Date__c)';

            for(AggregateResult agResInvoices:Database.query(invoicesQueryStr)){
                if(agResInvoices.get('Month')!= null && agResInvoices.get('Year')!=null && invSummaryName == getMonthName(Integer.valueOf(agResInvoices.get('Month')),'MMM')+' '+String.valueOf(agResInvoices.get('Year'))){
                    invSummary.TAFS_Number_of_Invoices__c = Decimal.valueOf(String.valueOf(agResInvoices.get('Invoices')));

                    Map<String,Decimal> mapDiffInvVals = calculatePercentageWithDifference('Invoices', agResInvoices, mapInvoiceSummary, mapFieldMappings);
                    invSummary.put('TAFS_Number_of_Invoices_Difference__c', mapDiffInvVals.get('difference'));
                    invSummary.put('TAFS_Percent_Difference_NumberOfInvoices__c', mapDiffInvVals.get('percent'));
                }
            }

            //Fuel Calculations
            if(mapCalendarFuelConsumption.containsKey(invSummaryName)){
                invSummary.TAFS_Total_Fuel_Gallons__c = Decimal.valueOf(String.valueOf(mapCalendarFuelConsumption.get(invSummaryName).get('TotalGallons')));
                invSummary.TAFS_Total_Fuel_Litres__c = Decimal.valueOf(String.valueOf(mapCalendarFuelConsumption.get(invSummaryName).get('TotalLitres')));

                Map<String,Decimal> mapDiffGallons = calculatePercentageWithDifference('TotalGallons', mapCalendarFuelConsumption.get(invSummaryName), mapInvoiceSummary, mapFieldMappings);
                invSummary.put('TAFS_Fuel_Difference_Gallons__c', mapDiffGallons.get('difference'));
                invSummary.put('TAFS_Percent_Difference_of_Fuel_Gallons__c', mapDiffGallons.get('percent'));

                Map<String,Decimal> mapDiffLitres = calculatePercentageWithDifference('TotalLitres', mapCalendarFuelConsumption.get(invSummaryName), mapInvoiceSummary, mapFieldMappings);
                invSummary.put('TAFS_Fuel_Difference_Litres__c', mapDiffLitres.get('difference'));
                invSummary.put('TAFS_Percent_Difference_of_Fuel_Litres__c', mapDiffLitres.get('percent'));
            }

            // A/R Amount Calculations for CAD CurrencyType
            String aggregateCADQueryStr = 'SELECT Calendar_Month(TAFS_Posted_Date__c) Month,Calendar_Year(TAFS_Posted_Date__c) Year,SUM(TAFS_A_R_Amount__c) ARAmountCAD,'+
                                          'SUM(TAFS_Fee_Earned__c) FeesEarnedCAD FROM TAFS_Invoice_Batch__c WHERE TAFS_Posted_Date__c = LAST_N_MONTHS: '+Label.TAFS_InvoiceSummary_Calculations_NumberOfMonths+
                                          ' AND TAFS_Status__c = \''+Label.TAFS_Pick_list_Value_For_Invoice_Batch_Processed+'\' AND TAFS_Client_ID__r.TAFS_Currency_Type__c = \''+cadCurrencyType+
                                          '\' Group By Calendar_Year(TAFS_Posted_Date__c), Calendar_Month(TAFS_Posted_Date__c)';

            for(AggregateResult agResCAD:Database.query(aggregateCADQueryStr)){
                if(agResCAD.get('Month')!= null && agResCAD.get('Year')!=null && invSummaryName == getMonthName(Integer.valueOf(agResCAD.get('Month')),'MMM')+' '+String.valueOf(agResCAD.get('Year'))){
                    invSummary.TAFS_A_R_Amount_CAD__c = Decimal.valueOf(String.valueOf(agResCAD.get('ARAmountCAD')));

                    Map<String,Decimal> mapDiffARAmountCAD = calculatePercentageWithDifference('ARAmountCAD', agResCAD, mapInvoiceSummary, mapFieldMappings);
                    invSummary.put('TAFS_A_R_Amount_Difference_CAD__c', mapDiffARAmountCAD.get('difference'));
                    invSummary.put('TAFS_Percent_Difference_A_R_Amount_CAD__c', mapDiffARAmountCAD.get('percent'));
                }
            }

            // A/R Amount Calculations for USD CurrencyType
            String aggregateUSDQueryStr = 'SELECT Calendar_Month(TAFS_Posted_Date__c) Month,Calendar_Year(TAFS_Posted_Date__c) Year,SUM(TAFS_A_R_Amount__c) ARAmountUSD,'+
                                          'SUM(TAFS_Fee_Earned__c) FeesEarnedUSD FROM TAFS_Invoice_Batch__c WHERE TAFS_Posted_Date__c = LAST_N_MONTHS: '+Label.TAFS_InvoiceSummary_Calculations_NumberOfMonths+
                                          ' AND TAFS_Status__c = \''+Label.TAFS_Pick_list_Value_For_Invoice_Batch_Processed+'\' AND TAFS_Client_ID__r.TAFS_Currency_Type__c = \''+usdCurrencyType+
                                          '\' Group By Calendar_Year(TAFS_Posted_Date__c), Calendar_Month(TAFS_Posted_Date__c)';
            for(AggregateResult agResUSD:Database.query(aggregateUSDQueryStr)){
                if(agResUSD.get('Month')!= null && agResUSD.get('Year')!=null && invSummaryName == getMonthName(Integer.valueOf(agResUSD.get('Month')),'MMM')+' '+String.valueOf(agResUSD.get('Year'))){
                    invSummary.TAFS_A_R_Amount_USD__c = Decimal.valueOf(String.valueOf(agResUSD.get('ARAmountUSD')));

                    Map<String,Decimal> mapDiffARAmountUSD = calculatePercentageWithDifference('ARAmountUSD', agResUSD, mapInvoiceSummary, mapFieldMappings);
                    invSummary.put('TAFS_A_R_Amount_Difference_USD__c', mapDiffARAmountUSD.get('difference'));
                    invSummary.put('TAFS_Percent_Difference_A_R_Amount_USD__c', mapDiffARAmountUSD.get('percent'));
                }
            }

            // Fees Earned Calculations for USD CurrencyType
            String aggregateUSDFeesEarnedQueryStr = 'SELECT Calendar_Month(TAFS_Date__c) Month,Calendar_Year(TAFS_Date__c) Year,SUM(TAFS_Fee_Earned__c) FeesEarnedUSD'+
                                                    ' FROM TAFS_Invoice_Transaction__c WHERE TAFS_Date__c = LAST_N_MONTHS: '+Label.TAFS_InvoiceSummary_Calculations_NumberOfMonths+
                                                    ' AND TAFS_Status__c = \''+Label.TAFS_Pick_list_Value_For_Invoice_Batch_Processed+'\' AND TAFS_Invoice_ClientCurrencyType__c = \''+usdCurrencyType+
                                                    '\' Group By Calendar_Year(TAFS_Date__c), Calendar_Month(TAFS_Date__c)';
            for(AggregateResult agResFeesEarnedUSD:Database.query(aggregateUSDFeesEarnedQueryStr)){
                if(agResFeesEarnedUSD.get('Month')!= null && agResFeesEarnedUSD.get('Year')!=null && invSummaryName == getMonthName(Integer.valueOf(agResFeesEarnedUSD.get('Month')),'MMM')+' '+String.valueOf(agResFeesEarnedUSD.get('Year'))){
                    invSummary.TAFS_Fees_Earned_USD__c = Decimal.valueOf(String.valueOf(agResFeesEarnedUSD.get('FeesEarnedUSD')));

                    Map<String,Decimal> mapDiffFeesEarnedUSD = calculatePercentageWithDifference('FeesEarnedUSD', agResFeesEarnedUSD, mapInvoiceSummary, mapFieldMappings);
                    invSummary.put('TAFS_Fees_Earned_Difference_USD__c', mapDiffFeesEarnedUSD.get('difference'));
                    invSummary.put('TAFS_Percent_Difference_Fees_Earned_USD__c', mapDiffFeesEarnedUSD.get('percent'));
                }
            }

            // Fees Earned Calculations for CAD CurrencyType
            String aggregateCADFeesEarnedQueryStr = 'SELECT Calendar_Month(TAFS_Date__c) Month,Calendar_Year(TAFS_Date__c) Year,SUM(TAFS_Fee_Earned__c) FeesEarnedCAD'+
                                                    ' FROM TAFS_Invoice_Transaction__c WHERE TAFS_Date__c = LAST_N_MONTHS: '+Label.TAFS_InvoiceSummary_Calculations_NumberOfMonths+
                                                    ' AND TAFS_Status__c = \''+Label.TAFS_Pick_list_Value_For_Invoice_Batch_Processed+'\' AND TAFS_Invoice_ClientCurrencyType__c = \''+cadCurrencyType+
                                                    '\' Group By Calendar_Year(TAFS_Date__c), Calendar_Month(TAFS_Date__c)';
            for(AggregateResult agResFeesEarnedCAD:Database.query(aggregateCADFeesEarnedQueryStr)){
                if(agResFeesEarnedCAD.get('Month')!= null && agResFeesEarnedCAD.get('Year')!=null && invSummaryName == getMonthName(Integer.valueOf(agResFeesEarnedCAD.get('Month')),'MMM')+' '+String.valueOf(agResFeesEarnedCAD.get('Year'))){
                    invSummary.TAFS_Fees_Earned_CAD__c = Decimal.valueOf(String.valueOf(agResFeesEarnedCAD.get('FeesEarnedCAD')));

                    Map<String,Decimal> mapDiffFeesEarnedCAD = calculatePercentageWithDifference('FeesEarnedCAD', agResFeesEarnedCAD, mapInvoiceSummary, mapFieldMappings);
                    invSummary.put('TAFS_Fees_Earned_Difference_CAD__c', mapDiffFeesEarnedCAD.get('difference'));
                    invSummary.put('TAFS_Percent_Difference_Fees_Earned_CAD__c', mapDiffFeesEarnedCAD.get('percent'));
                }
            }

            if(mapInvoiceSummary.containsKey(invSummaryName)){
                lstUpdateInvSummary.add(invSummary);
            }
            else{
                lstInsertInvSummary.add(invSummary);
            }
        }

        if(lstUpdateInvSummary.size()>0)
            Database.update(lstUpdateInvSummary);

        if(lstInsertInvSummary.size()>0)
            Database.insert(lstInsertInvSummary);
    }   
    
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post loading the Invoice Summary Table.
    **********************************************************************************************************************/  
    global void finish(Database.BatchableContext BC) {
        System.debug('### TAFS_LoadInvoiceSummaryBatch: Successfully Completed Loading Invoice Summary Table ###');
    }

    /**********************************************************************************************************************
    * Method Name      :  getMonthName
    * Input Parameter  :  Integer MonthNumber, String monthFormat
    * Return Type      :  String
    * Description      :  This method is used to get the month name either in Short Notation(MMM - i.e Jan) or full name of
                          the month(MMMM - i.e January)
    **********************************************************************************************************************/
    global static String getMonthName(Integer MonthNumber, String monthFormat){
        DateTime currentDateTime = DateTime.newInstance(System.Today().year(), MonthNumber, 1);
        return currentDateTime.format(monthFormat);
    }

    /**********************************************************************************************************************
    * Method Name      :  calculatePercentageWithDifference
    * Input Parameter  :  String property, 
                          AggregateResult aggResult, 
                          Map<String,TAFS_Invoice_Summary__c> mapInvoiceSummary, 
                          Map<String,String> mappings
    * Return Type      :  Map<String,Decimal>
    * Description      :  This method is used calculate the percentage difference, difference of current month values with 
                          the values of same month of the previous year
    **********************************************************************************************************************/
    global static Map<String,Decimal> calculatePercentageWithDifference(String property, AggregateResult aggResult, Map<String,TAFS_Invoice_Summary__c> mapInvoiceSummary, Map<String,String> mappings){
        Map<String,Decimal> mapDifferenceValues = new Map<String,Decimal>();
        Decimal newValue = Decimal.valueOf(String.valueOf(aggResult.get(property)));
        Decimal originalValue = getOriginalValue(property, aggResult, mapInvoiceSummary, mappings);

        if(originalValue != null && originalValue != 0){
            Decimal diff = newValue - originalValue;
            mapDifferenceValues.put('difference', diff);
            mapDifferenceValues.put('percent',(diff/originalValue)*100);
        }
        return mapDifferenceValues;
    }

    /**********************************************************************************************************************
    * Method Name      :  getOriginalValue
    * Input Parameter  :  String property, 
                          AggregateResult aggResult, 
                          Map<String,TAFS_Invoice_Summary__c> mapInvoiceSummary, 
                          Map<String,String> mappings
    * Return Type      :  Decimal
    * Description      :  This method is used to get the aggregate value for the same month of Previous year
    **********************************************************************************************************************/
    global static Decimal getOriginalValue(String property, AggregateResult aggResult, Map<String,TAFS_Invoice_Summary__c> mapInvoiceSummary, Map<String,String> mappings){
        Decimal year = Decimal.valueOf(String.valueOf(aggResult.get('Year')));
        Decimal prevYear = year - 1;
        Integer month = Integer.valueOf(aggResult.get('Month'));
        String priorKey = getMonthName(month, 'MMM')+' '+String.valueOf(prevYear);
        Decimal newValue = Decimal.valueOf(String.valueOf(aggResult.get(property)));
        if(mapInvoiceSummary.containsKey(priorKey)){
            TAFS_Invoice_Summary__c tafsInvSummary = mapInvoiceSummary.get(priorKey);
            if(String.valueOf(tafsInvSummary.get(mappings.get(property)))!= null){
                Decimal originalValue = Decimal.valueOf(String.valueOf(tafsInvSummary.get(mappings.get(property))));
                return originalValue;
            }
            else{
                return 0;
            }
        }
        else
            return null;
    }
}