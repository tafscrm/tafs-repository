/* CLass Name   : TAFS_SendCollectionEmailBatchTest
 * Description  : Test class for TAFS_SendCollectionEmailBatch
 * Created By   : Arpitha Sudhakar
 * Created On   : 29-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar           29-Aug-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_SendCollectionEmailBatchTest {

    static testmethod void test() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       EmailTemplate e = new EmailTemplate (developerName = 'TAFS_Collection_Batch_Template', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test', body ='test', subject = 'test'); 
       //insert e;
       
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       List<Lead> leadList = [select id,name from Lead];
       
       Test.startTest();
       TAFS_SendCollectionEmailBatch c = new TAFS_SendCollectionEmailBatch();
       Database.executeBatch(c);
       Test.stopTest();
    }
    
    static testmethod void testpositive() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       DataUtilTest_TAFS.createContactsForLead(1,leadLst[0].id);
       List<Lead> leadList = [select id,name from Lead];
       EmailTemplate e = new EmailTemplate (developerName = 'TAFS_Collection_Batch_Template', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test', body ='test', subject = 'test'); 
       //insert e;
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       List<Contact> contactLst = DataUtilTest_TAFS.createContactsForLead(leadList.size(),leadList[0].Id);     
       
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        
        TAFS_SendCollectionEmailBatch AU = new TAFS_SendCollectionEmailBatch();
        
        AU.execute(BC, contactLst);
        AU.finish(BC);        
    }
    
    static testmethod void testpositivetwo() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       DataUtilTest_TAFS.createContactsForLead(1,leadLst[0].id);
       List<Lead> leadList = [select id,name from Lead];
       EmailTemplate e = new EmailTemplate (developerName = 'TAFS_Collection_Batch_Template', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test', body ='test', subject = 'test'); 
       //insert e;
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       List<Contact> contactLst = new List<Contact>();
       
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        
        TAFS_SendCollectionEmailBatch AU = new TAFS_SendCollectionEmailBatch();
        
        AU.execute(BC, contactLst);
        AU.finish(BC);        
    }
}