/* Class Name   : TAFS_PushNotificationHandler
 * Description  : Handler class to send push notifications
 * Created By   : Pankaj Singh
 * Created On   : 29-JUly-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh              29-July-2016             Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_PushNotificationHandler{
    
    private static boolean prohibitAfterInsertTrigger = false;
    private static boolean prohibitAfterUpdateTrigger = false;
        
    public static void onBatchInsert(set<Id> invoiceBatchIdList){
        
        if(prohibitAfterInsertTrigger)
        {
            return;
        }
        sendPushNotifications(invoiceBatchIdList);
        prohibitAfterInsertTrigger = true;        
    }
    
    public static void onBatchUpdate(set<Id> invoiceBatchIdList){
        if(prohibitAfterUpdateTrigger)
        {
            return;
        }
        sendPushNotifications(invoiceBatchIdList);
        prohibitAfterUpdateTrigger = true;        
    }
    /* Method Name     : sendPushNotifications
     * Description     : Performs the callout to the Push Notificatiosn API
     * Return Type     : void
     * Input Parameter : set<Id>,string
     */
    @future(callout=true)
    public static void sendPushNotifications(set<Id> batchIdList){
        
        List<TAFS_Notification__c> notificationList =  new List<TAFS_Notification__c>();
        try{
            String token = '';
            String validity = '';
            String endpoint = ''; 
            String pushMessage = ''; 
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            HttpResponse response =  new HttpResponse();
            
            request.setHeader('Accept','application/json');
            request.setBody('application/json'); 
            
            TAFS_Manage_Report_Ids__c pushToken = TAFS_Manage_Report_Ids__c.getOrgDefaults(); 
            
            if(pushToken.TAFS_Push_Notification_Token__c  == null || pushToken.TAFS_PushNotificationTokenExpiryDate__c <= Date.Today()){
                
                system.debug(pushToken.TAFS_PushNotificationTokenExpiryDate__c < Date.Today());       
                system.debug('inside first if????'+pushToken.TAFS_PushNotificationTokenExpiryDate__c);                      
                endpoint = Label.TAFS_Push_Notification_URL +'/login?username='+Label.TAFS_Push_Notification_Username+'&password='+Label.TAFS_Push_Notification_Password;
                request.setMethod('POST');
                request.setEndpoint(endpoint);
                request.setTimeout(120000); 
                response = http.send(request); 
                String resp = response.getBody();
                Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(resp);
                token = (String)root.get('token');
                validity = (String)root.get('expirationDate');
                pushToken.TAFS_Push_Notification_Token__c = token.substring(0,254);
                pushToken.TAFS_Push_Notification_Token2__c = token.substring(254,token.length());         
                pushToken.TAFS_PushNotificationTokenExpiryDate__c = Date.valueOf(validity);
                
             }   
             else {     
                token =  pushToken.TAFS_Push_Notification_Token__c + pushToken.TAFS_Push_Notification_Token2__c;                              
             }   
             //Making the call to push notification API
             for(TAFS_Invoice_Batch__c invoiceBatch : [SELECT ID,TAFS_Status__c,TAFS_Blocked_Reason__c,TAFS_Client_ID__r.TAFS_Client_Number__c, TAFS_Blocked_State__c,TAFS_Posted_Date__c,TAFS_Paid_to_Client__c,TAFS_Client_Number__c FROM TAFS_Invoice_Batch__c WHERE ID IN : batchIdList]){
                    pushMessage = '';               
                    if(invoiceBatch.TAFS_Status__c == Label.TAFS_Pick_list_Value_For_Invoice_Batch_Processed && invoiceBatch.TAFS_Paid_to_Client__c > 0){
                        pushMessage = 'TAFS Alert: Client No - '+invoiceBatch.TAFS_Client_ID__r.TAFS_Client_Number__c+' - '+ Label.TAFS_Invoice_Batch_Processed_Notification1 + invoiceBatch.TAFS_Paid_to_Client__c +' '+ Label.TAFS_Invoice_Batch_Processed_Notification2;
                        
                    }else if(invoiceBatch.TAFS_Blocked_State__c == true && invoiceBatch.TAFS_Status__c != Label.TAFS_Pick_list_Value_For_Invoice_Batch_Processed && invoiceBatch.TAFS_Blocked_Reason__c == Label.TAFS_Blocked_Batch_RSInput){
                        pushMessage = 'TAFS Alert: Client No - '+invoiceBatch.TAFS_Client_ID__r.TAFS_Client_Number__c+' - '+ Label.TAFS_Batch_Blocked_Push_Notification;
                    }               
                    else{
                        pushMessage = 'TAFS Alert: Client No - '+invoiceBatch.TAFS_Client_ID__r.TAFS_Client_Number__c+' - '+ Label.TAFS_Batch_Creation_Push_Notification;
                    }
                    if(pushMessage!='' && pushMessage!=null){
                                  
                        endpoint  = Label.TAFS_Push_Notification_URL+ '/push?clientnumber='+invoiceBatch.TAFS_Client_ID__r.TAFS_Client_Number__c + '&message='+EncodingUtil.urlEncode(pushMessage,'UTF-8');         
                        request.setMethod('POST');        
                        request.setEndpoint(endpoint);
                        request.setHeader('Authorization',token);
                        request.setTimeout(120000);       
                        response = (new Http()).send(request);
                       
                        if(response.getStatusCode()==200){
                            TAFS_Notification__c notificationObj =  new TAFS_Notification__c(TAFS_Client_Number__c = invoiceBatch.TAFS_Client_ID__r.TAFS_Client_Number__c,
                                                                                            TAFS_Invoice_Batch__c = invoiceBatch.Id , TAFS_Message__c = pushMessage, TAFS_Message_Sent_Date__c = system.now());
                            notificationList.add(notificationObj);
                        } 
                        else{                            
                            TAFS_Error_Logger.insertErrorLogForPN(response,request);               
                        }
                     }                         
             }
             //updating the custom setting with the lates token and expiry date 
             if(pushToken!=null){           
                 update pushToken;
             }
             //inserting the notification records into SFDC
             if(!notificationList.isEmpty()){
                 insert notificationList;
             }                                       
        }catch(CalloutException e){
            //Do nothing
        }catch(Exception excep){
            //Do Nothing
        }      
    }
}