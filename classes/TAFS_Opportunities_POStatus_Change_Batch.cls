/****************************************************************************************
* Created By      :  Karthik Gulla
* Create Date     :  06/03/2017
* Description     :  Batch class to update 'Pending' PO Opportunities to Cancelled , if its
                     Pending for more than 2 business days
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_Opportunities_POStatus_Change_Batch implements Database.Batchable<sObject> {
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<Opportunity>
    * Description      :  This method is used to find the Pending PO Opportunities
    **********************************************************************************************************************/
    global List<Opportunity> start(Database.BatchableContext BC) {
        List<Opportunity> lstOpportunities = new List<Opportunity>();
        Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Tire Program').getRecordTypeId();
        String pendingOpportunitiesQuery = 'SELECT Id,TAFS_PO_Status_Change_Date__c,TAFS_PO_Status__c FROM Opportunity WHERE TAFS_PO_Status__c = \'Pending\' AND IsClosed = false AND RecordTypeId=\''+recTypeId+'\'';
        for(Opportunity oppty:Database.query(pendingOpportunitiesQuery)){
            lstOpportunities.add(oppty);
        }
        return lstOpportunities;
    }
    
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<Opportunity>
    * Return Type      :  Void
    * Description      :  This method is used to update the Pending PO Opportunities to Cancelled, if its pending for 
                          more than two Business days
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        List<Opportunity> lstUpdateOpportunities = new List<Opportunity>();
        if(scope.size()>0){
            list<BusinessHours> busHoursList = new list<BusinessHours>();
            busHoursList  = [SELECT Id FROM BusinessHours WHERE isActive = TRUE ORDER BY LastModifiedDate DESC LIMIT 1];
            if(busHoursList.size()>0){
                DateTime currentTime = System.now();
                DateTime endTime = DateTime.newInstance(currentTime.year(), currentTime.month(), currentTime.day());
                for(Opportunity opp:scope){
                    if(opp.TAFS_PO_Status_Change_Date__c != null){
                        Date startDate = opp.TAFS_PO_Status_Change_Date__c;
                        DateTime startTime = DateTime.newInstance(startDate.year(), startDate.month(), startDate.day());
                        Decimal noOfWorkingDays = BusinessHours.diff(busHoursList[0].Id, startTime, endTime)/(3600*1000*24);
                        if(noOfWorkingDays >= 2){
                            opp.TAFS_PO_Status__c = 'Canceled';
                            lstUpdateOpportunities.add(opp);
                        }
                    }
                }
            }
        }
        if(lstUpdateOpportunities.size() > 0)
            Database.update(lstUpdateOpportunities);
    }   
    
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post 'PO Status' Update
    **********************************************************************************************************************/  
    global void finish(Database.BatchableContext BC) {
        System.debug('### PO Status is successfully updated to Cancelled ###');
    }
}