/* CLass Name   : TAFS_ScheduleKanbanTaskIntegrationTest
 * Description  : Test class for TAFS_ScheduleKanbanTaskIntegration
 * Created By   : Karthik Gulla
 * Created On   : 09-Nov-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla				  09-Nov-2016				Initial Version
 *
 *****************************************************************************************/
@isTest
public class TAFS_ScheduleKanbanTaskIntegrationTest {
	/************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {     
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
        list<TAFS_Invoice_Batch__c> lstInvoiceBatches = DataUtilTest_TAFS.createIvoiceBatches(1,lstAcct[0].Id);        
    }

    /************************************************************************************
    * Method       : 	testScheduleKanbanTaskIntegration
    * Description  :    Test Method to test Kanban Task Integration
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    /*static testmethod void testScheduleKanbanTaskIntegration() {
       Test.startTest();
       Map<String,String> responseHeader = new Map<String,String>();
       responseHeader.put('Content-Type', 'application/json');
       //String responseBody = '{"test":"test"}';
       /*String responseBody = '{"numberoftasks":"2",';
       responseBody = responseBody + '"task":["customfields":[{"fieldid":"19","name":"Batch Number","type":"number","value":"123456","mandatory":true},';
       responseBody = responseBody + '{"fieldid":"23","name":"Client Number","type":"number","value":"123451","mandatory":true}]]}';*/

/*
       Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(200,'OK',createSampleResponse(),responseHeader));

       TAFS_ScheduleKanbanTaskIntegration c = new TAFS_ScheduleKanbanTaskIntegration();
       String scheduleTime = '0 0 23 * * ?';
       system.schedule('Test schedule', scheduleTime, c);
       Test.stopTest();
    }*/

    static String createSampleResponse(){
    	TAFS_KanbanResponseParser.cls_customfields clsCustFieldOne = new TAFS_KanbanResponseParser.cls_customfields();
    	clsCustFieldOne.fieldid = '20';
    	clsCustFieldOne.name = 'Batch Number';
    	clsCustFieldOne.type = 'number';
    	clsCustFieldOne.value = 'Batch1';
    	clsCustFieldOne.mandatory = true;

    	TAFS_KanbanResponseParser.cls_customfields clsCustFieldTwo = new TAFS_KanbanResponseParser.cls_customfields();
    	clsCustFieldTwo.fieldid = '21';
    	clsCustFieldTwo.name = 'Client Number';
    	clsCustFieldTwo.type = 'number';
    	clsCustFieldTwo.value = '123451';
    	clsCustFieldTwo.mandatory = true;

    	TAFS_KanbanResponseParser.cls_customfields clsCustFieldThree = new TAFS_KanbanResponseParser.cls_customfields();
    	clsCustFieldThree.fieldid = '22';
    	clsCustFieldThree.name = 'BS';
    	clsCustFieldThree.type = 'number';
    	clsCustFieldThree.value = '11236';
    	clsCustFieldThree.mandatory = true;

    	TAFS_KanbanResponseParser.cls_customfields clsCustFieldFour = new TAFS_KanbanResponseParser.cls_customfields();
    	clsCustFieldFour.fieldid = '23';
    	clsCustFieldFour.name = 'BAS';
    	clsCustFieldFour.type = 'number';
    	clsCustFieldFour.value = '11237';
    	clsCustFieldFour.mandatory = true;

    	TAFS_KanbanResponseParser.cls_customfields clsCustFieldFive = new TAFS_KanbanResponseParser.cls_customfields();
    	clsCustFieldFive.fieldid = '24';
    	clsCustFieldFive.name = 'FS';
    	clsCustFieldFive.type = 'number';
    	clsCustFieldFive.value = '11238';
    	clsCustFieldFive.mandatory = true;

    	TAFS_KanbanResponseParser.cls_customfields clsCustFieldSix = new TAFS_KanbanResponseParser.cls_customfields();
    	clsCustFieldSix.fieldid = '25';
    	clsCustFieldSix.name = 'CS';
    	clsCustFieldSix.type = 'number';
    	clsCustFieldSix.value = '11239';
    	clsCustFieldSix.mandatory = true;

    	TAFS_KanbanResponseParser.cls_customfields clsCustFieldSeven = new TAFS_KanbanResponseParser.cls_customfields();
    	clsCustFieldSeven.fieldid = '26';
    	clsCustFieldSeven.name = 'VS';
    	clsCustFieldSeven.type = 'number';
    	clsCustFieldSeven.value = '11240';
    	clsCustFieldSeven.mandatory = true;

    	List<TAFS_KanbanResponseParser.cls_customfields> lstCustFields = new List<TAFS_KanbanResponseParser.cls_customfields>();
    	lstCustFields.add(clsCustFieldOne);
    	lstCustFields.add(clsCustFieldTwo);
    	lstCustFields.add(clsCustFieldThree);
    	lstCustFields.add(clsCustFieldFour);
    	lstCustFields.add(clsCustFieldFive);
    	lstCustFields.add(clsCustFieldSix);
    	lstCustFields.add(clsCustFieldSeven);

    	TAFS_KanbanResponseParser.Task t = new TAFS_KanbanResponseParser.Task();
    	t.customfields = lstCustFields;

    	List<TAFS_KanbanResponseParser.Task> lstResponseTasks = new List<TAFS_KanbanResponseParser.Task>();
    	lstResponseTasks.add(t);

    	Map<String,List<TAFS_KanbanResponseParser.Task>> mapTaskWrappers = new Map<String,List<TAFS_KanbanResponseParser.Task>>();
    	mapTaskWrappers.put('task',lstResponseTasks);
    	return JSON.serializePretty(mapTaskWrappers);
    }
}