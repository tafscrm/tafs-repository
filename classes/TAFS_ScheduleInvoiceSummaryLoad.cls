/* CLass Name   : TAFS_ScheduleInvoiceSummaryLoad
 * Description  : Load the Invoice Summary Table by doing monthly calculations
 * Created By   : Karthik Gulla
 * Created On   : 15-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla              15-Feb-2017              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleInvoiceSummaryLoad implements Schedulable {   
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_LoadInvoiceSummaryBatch());
   }
}