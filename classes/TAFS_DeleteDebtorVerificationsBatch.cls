/****************************************************************************************
* Created By      :  Pankaj Singh
* Created Date    :  08/05/2016
* Description     :  Batch class for deleting old Debtor Collection/Verification object records
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_DeleteDebtorVerificationsBatch implements Database.Batchable<sObject> {
    
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<TAFS_Debtor_Collection_Verification__c>
    * Description      :  This method is used to find all the Debtor Collection/Verification where invoice balance is 0 or null.
    **********************************************************************************************************************/
    global List<TAFS_Debtor_Collection_Verification__c> start(Database.BatchableContext BC) {
        //variable declaration
        List<TAFS_Debtor_Collection_Verification__c> debtorVClist = new List<TAFS_Debtor_Collection_Verification__c>();
        debtorVClist = [SELECT ID,TAFS_Invoice__r.TAFS_Balance__c FROM TAFS_Debtor_Collection_Verification__c WHERE TAFS_Invoice__r.TAFS_Balance__c = 0 OR TAFS_Invoice__r.TAFS_Balance__c = null];         
        return debtorVClist;
    }
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<Contact>
    * Return Type      :  Void
    * Description      :  This method is used to delete the Debtor Collection/Verification records
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<TAFS_Debtor_Collection_Verification__c> scope) {
    
        if(scope.size()>0){        
            try{
                delete scope;   
            }catch(Exception excep){
                //Do Nothing
            }          
        }               
    }
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post the emails are sent.
    **********************************************************************************************************************/    
    global void finish(Database.BatchableContext BC) {
    
    }
}