/****************************************************************************************
* Create By     :   Pankaj Singh
* Create Date   :   04/07/2016
* Description   :   Test class for TAFS_Event_Trigger_Handler
* Modification Log:
*-----------------------------------------------------------------------------
** Developer                    Date          Description
** ----------------------------------------------------------------------------
** Pankaj Singh                 08/24/2016    Initial version.

*****************************************************************************************/
@isTest
private class TAFS_Event_Trigger_Handler_Test {

    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        
         list<Lead> leadList =  DataUtilTest_TAFS.createleads(1);
         list<Account> lstAcct = DataUtilTest_TAFS.createAccountsForAgreements(1);
         
    } 
    
    private static list<Event> createEvents(integer total, Id leadId) 
    {
        list<Event> events = new list<Event>();
        for(integer i = 0; i < total; i++)
        {
            events.add(new Event(Subject              = 'Test',
                                 StartDateTime        = System.now(),
                                 EndDateTime          = System.now()+1,
                                 OwnerId              = UserInfo.getUserId(),
                                 WhoId                = leadId,
                                 Type                 = 'Other'
                               ));
        }
        return events;
    }   
     private static testmethod void testStatusNew(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='New',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
     
         list<Event> eventList = createEvents(1,leadObj.Id); 
         
         Test.startTest();
             insert eventList;
         Test.stopTest();       
                 
     }
     
     private static testmethod void testActivWork(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='Actively Working',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'Actively Working';
         update leadObj;
         
         list<Event> eventList = createEvents(1,leadObj.Id); 
         
         Test.startTest();
             insert eventList;
         Test.stopTest();       
                 
     }
     
     private static testmethod void testAgreeOut(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='Agreement Out',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'Agreement Out';
         update leadObj;
         
         list<Event> eventList = createEvents(1,leadObj.Id); 
         
         Test.startTest();
             insert eventList;
         Test.stopTest();       
                 
     }
     
     private static testmethod void testAgreeReady(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='Agreement Ready',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'Agreement Ready';
         update leadObj;
         
         list<Event> eventList = createEvents(1,leadObj.Id); 
         
         Test.startTest();
             insert eventList;
         Test.stopTest();       
                 
     }
     
     private static testmethod void testApplicationOut(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='Application Out',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'Application Out';
         update leadObj;
         
         list<Event> eventList = createEvents(1,leadObj.Id); 
         
         Test.startTest();
             insert eventList;
         Test.stopTest();       
                 
     }
     
     private static testmethod void testAwaitDoc(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='Awaiting Documentation',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'Awaiting Documentation';
         update leadObj;
         
         list<Event> eventList = createEvents(1,leadObj.Id); 
         
         Test.startTest();
             insert eventList;
         Test.stopTest();       
                 
     }
     
     private static testmethod void testNoContact(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='No Contact',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'No Contact';
         update leadObj;
         
         list<Event> eventList = createEvents(1,leadObj.Id); 
         
         TAFS_Error_Logger el = new TAFS_Error_Logger();
         
         HttpRequest request = new HttpRequest();
         HttpResponse response =  new HttpResponse();
                 
         Test.startTest();
             TAFS_Error_Logger.insertErrorLog(response,request);
             TAFS_Error_Logger.insertErrorLogForFC(response,request);
             TAFS_Error_Logger.insertErrorLogForPN(response,request);
         
             insert eventList;
         Test.stopTest();       
                 
     }
}