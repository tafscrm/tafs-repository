/* Class Name   : TAFS_Task_Prioritization_Batch
 * Description  : Batch to run everyday and create task
 * Created By   : Raushan Anand
 * Created On   : 7-Sep-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand               7-Sep-2016               Initial version.
 *
 *****************************************************************************************/
global class TAFS_Task_Prioritization_Batch implements Database.Batchable<sObject>{
    global List<Account> start(Database.BatchableContext BC){
        TAFS_Task_Batch_Execution__c cs = TAFS_Task_Batch_Execution__c.getInstance('Last_Execution_Time');
        //List<Account> accList = [SELECT Id FROM Account Where TAFS_Client_Status__c != 'Terminated' AND RecordType.Name = 'Client' AND (lastModifieddate = Yesterday OR lastModifieddate = Today)];
        List<Account> accList = new List<Account>();
        if(cs !=null){
            accList = [SELECT Id FROM Account Where TAFS_Client_Status__c != 'Terminated' AND RecordType.Name = 'Client' AND (lastModifieddate >=: cs.Timestamp__c)];  
        }
        else
            accList = [SELECT Id FROM Account Where TAFS_Client_Status__c != 'Terminated' AND RecordType.Name = 'Client' AND (lastModifieddate >= Yesterday)];  
        return accList;
    }
    
    global void execute(Database.BatchableContext BC, List<Account> accountList){
        TAFS_Task_Batch_Execution__c cs1 = TAFS_Task_Batch_Execution__c.getInstance('Last_Execution_Time');
        if(cs1 != null){
            List<AccountHistory> scope = [SELECT AccountId,oldvalue,newvalue,Isdeleted,Id,field,createddate,createdbyId,Account.TAFS_Month_To_Date_Factored_Volume_New__c,
                                               Account.TAFS_Previous_Month_Factored_Volume__c,Account.TAFS_Relationship_Specialist_UserId__c FROM AccountHistory where
                                              (field='TAFS_Batches_Blocked_Value__c' OR field='Notice_of_Termination_Value__c' OR field='Client_Status_Health_Value__c'
                                               OR field='TAFS_Factored_Volume_vs_Average__c' OR field='TAFS_Fuel_Volume_vs_Average__c' OR field='TAFS_Fuel_vs_Commitment__c' OR
                                               field='Web_Activity_Value__c' OR field='TAFS_Recent_Roadside_Claims__c') AND AccountId IN: accountList AND
                                                (createdDate >=: cs1.Timestamp__c) Order By CreatedDate DESC];

            Map<Id,List<Task>> mapExistTasks = new Map<Id,List<Task>>();
            for(Task tsk:[Select Id, Subject,Priority,WhatId,TAFS_Followup_Reason__c,What.Type,ActivityDate,OwnerId,Status 
                            FROM Task 
                            WHERE WhatId IN: accountList 
                            AND RecordType.Name = 'Client Follow-up'
                            AND Subject = 'Factored Volume' 
                            AND Status='Open' Order By CreatedDate desc]){
                if(mapExistTasks.containsKey(tsk.whatId))
                    mapExistTasks.get(tsk.WhatId).add(tsk);
                else{
                    List<Task> lstTasks = new List<Task>();
                    lstTasks.add(tsk);
                    mapExistTasks.put(tsk.WhatId,lstTasks);
                }
            }
            
            //List<Task> taskList = new List<Task>();
            Set<Task> taskList = new Set<Task>();
            //List<Task> updatetaskList = new List<Task>();
            Set<Task> updatetaskList = new Set<Task>();
            Map<Id,Double> accountVolMap = new Map<Id,Double>();
            Map<Id,Id> accountRSMap = new Map<Id,Id>();
            Map<Id,Set<String>> accFieldMap = new Map<Id,Set<String>>();
            Set<String> processsedField = new Set<String>();
            Set<String> greenValSet = new Set<String>();
            Id recordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Client Follow-up').getRecordTypeId();
            List<TAFS_Task_Prioritization_Matrix__c> priorityMatrix = TAFS_Task_Prioritization_Matrix__c.getAll().values();
            BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true];
            Datetime nextStart = BusinessHours.nextStartDate(bh.id, DateTime.now());
            for(AccountHistory objAccHistory : scope){
                if(objAccHistory.Account.TAFS_Relationship_Specialist_UserId__c != null)
                    accountRSMap.put(objAccHistory.AccountId,objAccHistory.Account.TAFS_Relationship_Specialist_UserId__c);
                if(objAccHistory.NewValue == 'Negative' && objAccHistory.OldValue != 'Negative' && (!processsedField.contains(objAccHistory.AccountId+'_'+objAccHistory.Field))){
                    processsedField.add(objAccHistory.AccountId+'_'+objAccHistory.Field);
                    accountVolMap.put(objAccHistory.AccountId,objAccHistory.Account.TAFS_Previous_Month_Factored_Volume__c);
                    if(accFieldMap.containsKey(objAccHistory.AccountId)){
                        Set<String> temp = accFieldMap.get(objAccHistory.AccountId);
                        temp.add(objAccHistory.Field);
                        accFieldMap.put(objAccHistory.AccountId,temp);
                    }
                    else{
                        Set<String> temp = new Set<String>();
                        temp.add(objAccHistory.Field);
                        accFieldMap.put(objAccHistory.AccountId,temp);
                    }
                    System.debug('###RA processsedField1 ==>'+processsedField);
                    System.debug('###RA objAccHistory ==>'+objAccHistory.Field);
                }
                else if(objAccHistory.NewValue == 'Positive' && objAccHistory.OldValue != 'Positive' && 
                        (objAccHistory.Field =='TAFS_Factored_Volume_vs_Average__c' || objAccHistory.Field =='TAFS_Fuel_Volume_vs_Average__c') &&
                        (!processsedField.contains(objAccHistory.AccountId+'_'+objAccHistory.Field))){
                            
                    processsedField.add(objAccHistory.AccountId+'_'+objAccHistory.Field);
                    greenValSet.add(objAccHistory.AccountId +'_'+ objAccHistory.Field);
                    accountVolMap.put(objAccHistory.AccountId,objAccHistory.Account.TAFS_Previous_Month_Factored_Volume__c);
                    if(accFieldMap.containsKey(objAccHistory.AccountId)){
                        Set<String> temp = accFieldMap.get(objAccHistory.AccountId);
                        temp.add(objAccHistory.Field);
                        accFieldMap.put(objAccHistory.AccountId,temp);
                    }
                    else{
                        Set<String> temp = new Set<String>();
                        temp.add(objAccHistory.Field);
                        accFieldMap.put(objAccHistory.AccountId,temp);
                    }
                            System.debug('###RA processsedField2 ==>'+processsedField);
                            System.debug('###RA accFieldMap'+accFieldMap);
                            System.debug('###RA objAccHistory ==>'+objAccHistory.Field);
                }
                else{
                    processsedField.add(objAccHistory.AccountId+'_'+objAccHistory.Field);
                    System.debug('###RA processsedField3 ==>'+processsedField);
                    System.debug('###RA objAccHistory ==>'+objAccHistory.Field);
                }
            }
            System.debug('###RA accFieldMap'+accFieldMap);
            for(Id objId : accountVolMap.keySet()){
                Double priority = 0;
                String subject;
                String reason;
                System.debug('###RA objId ==>'+objId);
                for(TAFS_Task_Prioritization_Matrix__c objPMatrix : priorityMatrix){
                    if(accountVolMap.get(objId) >= objPMatrix.TAFS_Factored_Volume_Min_Cap__c && accountVolMap.get(objId) < objPMatrix.TAFS_Factored_Volume_Max_Cap__c
                      && objPMatrix.TAFS_Color__c == 'Red'){
                        if(accFieldMap.containsKey(objId)){
                            for(String str : accFieldMap.get(objId)){
                                System.debug('###RA str ==>'+str);
                                if(str == 'TAFS_Batches_Blocked_Value__c' && objPMatrix.TAFS_Blocked_Batch__c != null){
                                    if(String.isBlank(reason))
                                        reason = 'Blocked Batch Changed to Red';
                                    else
                                        reason += ';Blocked Batch Changed to Red';
                                    if(objPMatrix.TAFS_Blocked_Batch__c > priority){
                                        priority = objPMatrix.TAFS_Blocked_Batch__c;
                                        subject = 'Blocked Batches';
                                    }
                                }
                                
                                else if(str == 'Notice_of_Termination_Value__c' && objPMatrix.TAFS_Notice_of_Termination__c != null){
                                    if(String.isBlank(reason))
                                        reason = 'Notice of Termination Changed to Red';
                                    else
                                        reason += ';Notice of Termination Changed to Red';
                                    if(objPMatrix.TAFS_Notice_of_Termination__c > priority){
                                        priority = objPMatrix.TAFS_Notice_of_Termination__c;
                                        subject = 'Notice of Termination';
                                    }
                                }
                                else if(str == 'Client_Status_Health_Value__c' && objPMatrix.TAFS_Client_Status__c != null){
                                    if(String.isBlank(reason))
                                        reason = 'Client Status Changed to Red';
                                    else
                                        reason += ';Client Status Changed to Red';
                                    if(objPMatrix.TAFS_Client_Status__c > priority){
                                        priority = objPMatrix.TAFS_Client_Status__c;
                                        subject = 'Client Status';
                                    }
                                }
                                else if(str == 'TAFS_Factored_Volume_vs_Average__c' && objPMatrix.TAFS_Factored_Volume__c != null && (!greenValSet.contains(objId+'_'+str))){
                                    if(String.isBlank(reason))
                                        reason = 'Factored Volume Changed to Red';
                                    else
                                        reason += ';Factored Volume Changed to Red';
                                    if(objPMatrix.TAFS_Factored_Volume__c > priority){
                                        priority = objPMatrix.TAFS_Factored_Volume__c;
                                        subject = 'Factored Volume';
                                    }
                                }
                                else if(str == 'TAFS_Fuel_Volume_vs_Average__c' && objPMatrix.TAFS_Fuel_Volume__c != null && (!greenValSet.contains(objId+'_'+str))){
                                    if(String.isBlank(reason))
                                        reason = 'Fuel Volume Changed to Red';
                                    else
                                        reason += ';Fuel Volume Changed to Red';
                                    if(objPMatrix.TAFS_Fuel_Volume__c > priority){
                                        priority = objPMatrix.TAFS_Fuel_Volume__c;
                                        subject = 'Fuel Volume';
                                    }
                                }
                                else if(str == 'TAFS_Fuel_vs_Commitment__c' && objPMatrix.TAFS_Fuel_Commitment__c != null){
                                    
                                    if(String.isBlank(reason))
                                        reason = 'Fuel Commitment Changed to Red';
                                    else
                                        reason += ';Fuel Commitment Changed to Red';
                                    
                                    if(objPMatrix.TAFS_Fuel_Commitment__c > priority){
                                        priority = objPMatrix.TAFS_Fuel_Commitment__c;
                                        subject = 'Fuel Commitment';
                                    }
                                }
                                else if(str == 'TAFS_Recent_Roadside_Claims__c' && objPMatrix.TAFS_Recent_Roadside_Claims__c != null){
                                    if(String.isBlank(reason))
                                        reason = 'Roadside Claims Changed to Red';
                                    else
                                        reason += ';Roadside Claims Changed to Red';
                                    if(objPMatrix.TAFS_Recent_Roadside_Claims__c > priority){
                                        priority = objPMatrix.TAFS_Recent_Roadside_Claims__c;
                                        subject = 'Roadside Claims';
                                    }
                                }
                                else if(str == 'Web_Activity_Value__c' && objPMatrix.TAFS_Web_Login__c != null){
                                    if(String.isBlank(reason))
                                        reason = 'Web Login Changed to Red';
                                    else
                                        reason += ';Web Login Changed to Red';
                                    System.debug('###RA'+ objPMatrix.TAFS_Web_Login__c + '--->'+priority);
                                    if(objPMatrix.TAFS_Web_Login__c > priority){
                                        priority = objPMatrix.TAFS_Web_Login__c;
                                        subject = 'Web Activity';
                                    }
                                }
                                
                            }
                        }
                       /* Task t = new Task();
                        t.Subject = subject;
                        t.Priority = String.valueOf(Integer.valueOf(priority));
                        t.WhatId = objId;
                        t.TAFS_Followup_Reason__c = reason;
                        t.ActivityDate = nextStart.date();
                        if(accountRSMap.containsKey(objId))
                            t.OwnerId = accountRSMap.get(objId);
                        System.debug('objId====>>'+ t);
                        taskList.add(t);*/
                    }
                    else if (accountVolMap.get(objId) >= objPMatrix.TAFS_Factored_Volume_Min_Cap__c && accountVolMap.get(objId) < objPMatrix.TAFS_Factored_Volume_Max_Cap__c
                      && objPMatrix.TAFS_Color__c == 'Green'){
                          if(accFieldMap.containsKey(objId)){
                              for(String str : accFieldMap.get(objId)){
                                if(str == 'TAFS_Factored_Volume_vs_Average__c' && greenValSet.contains(objId+'_'+str) && objPMatrix.TAFS_Factored_Volume__c != null){
                                    
                                    if(String.isBlank(reason))
                                        reason = 'Factored Volume Changed to Green';
                                    else
                                        reason += ';Factored Volume Changed to Green';
                                    if(objPMatrix.TAFS_Factored_Volume__c > priority){
                                        priority = objPMatrix.TAFS_Factored_Volume__c;
                                        subject = 'Factored Volume';
                                    }
                                    
                                }
                                else if(str == 'TAFS_Fuel_Volume_vs_Average__c' && greenValSet.contains(objId+'_'+str) && objPMatrix.TAFS_Fuel_Volume__c != null){
                                    
                                    if(String.isBlank(reason))
                                        reason = 'Fuel Volume Changed to Green';
                                    else
                                        reason += ';Fuel Volume Changed to Green';
                                    if(objPMatrix.TAFS_Fuel_Volume__c > priority){
                                        priority = objPMatrix.TAFS_Fuel_Volume__c;
                                        subject = 'Fuel Volume';
                                    }
                                }
                              }
                          }
                        /*Task t = new Task();
                        t.Subject = subject;
                        t.Priority = String.valueOf(Integer.valueOf(priority));
                        t.WhatId = objId;
                        t.TAFS_Followup_Reason__c = reason;
                        t.ActivityDate = nextStart.date();
                        if(accountRSMap.containsKey(objId))
                            t.OwnerId = accountRSMap.get(objId);
                        System.debug('objId====>>'+ t);
                        taskList.add(t);*/
                    }
                    
                }

                Task t;
                if(mapExistTasks.containsKey(objId)){
                    if((mapExistTasks.get(objId)[0]).subject.toLowerCase() == 'factored volume')
                        t = mapExistTasks.get(objId)[0];
                    else
                        t = new Task();
                    t.Priority = String.valueOf(Integer.valueOf(priority));
                }
                else{
                    t = new Task();
                }

                if(String.isNotBlank(subject) && String.isNotBlank(reason) && priority != 0)
                {   String tempReason = t.TAFS_Followup_Reason__c != null ? t.TAFS_Followup_Reason__c : '';
                    if(!tempReason.contains(reason))
                        t.TAFS_Followup_Reason__c = tempReason+';'+reason;
                    t.Subject = subject;
                    t.RecordTypeId= recordTypeId;
                    t.WhatId = objId;
                    t.ActivityDate = nextStart.date();
                    if(accountRSMap.containsKey(objId))
                        t.OwnerId = accountRSMap.get(objId);
                    System.debug('objId====>>'+ t);
                    if(mapExistTasks.containsKey(objId) && (mapExistTasks.get(objId)[0]).subject.toLowerCase() == 'factored volume')
                        updatetaskList.add(t);
                    else
                        taskList.add(t);
                }
            }
            if(!taskList.isEmpty()){
                System.debug('taskList ===>'+ taskList);
                Database.insert(new List<Task>(taskList));
            }

            if(!updatetaskList.isEmpty()){
                Database.update(new List<Task>(updatetaskList));
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){
        Database.executeBatch(new TAFS_Past_Due_Client_Batch(), 100);
    }
}