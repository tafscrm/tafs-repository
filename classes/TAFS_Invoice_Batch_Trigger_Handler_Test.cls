/* CLass Name   : TAFS_Invoice_Batch_Trigger_Handler_test
 * Description  : Test class for Trigger methods of TAFS_Invoice_Batch_Trigger 
 * Created By   : Raushan Anand
 * Created On   : 26-Apr-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                26-Apr-2015              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_Invoice_Batch_Trigger_Handler_Test {
    
    @testsetup
    static void setUpInvoiceBatchData(){
        List<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        List<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);        
        DataUtilTest_TAFS.createIvoiceKeyMapping('Test1','Invoice Transactions','Status','0','Processed');
        DataUtilTest_TAFS.createIvoiceKeyMapping('Test2','Invoice Transactions','Type','0','Processed');  
        DataUtilTest_TAFS.createIvoiceKeyMapping('Test3','Invoice','Invoice Delivery Method','0','Processed'); 
        DataUtilTest_TAFS.createIvoiceKeyMapping('Test4','Invoice','Invoice Delivery Status','0','Processed');
        DataUtilTest_TAFS.createIvoiceKeyMapping('Test5','Verification Collection','Action','0','Processed');
        DataUtilTest_TAFS.createIvoiceKeyMapping('Test6','Verification Collection','Type','0','Processed');
    }
    
    private static testmethod void testInvoiceBatchUpdate(){
        
        Account acc = [SELECT Id,TAFS_Client_Number__c FROM Account limit 1];
        List<TAFS_Invoice_Batch__c> invoiceBatchObjList = DataUtilTest_TAFS.createIvoiceBatches(1,acc.Id);
        Test.startTest();
            update invoiceBatchObjList;
        Test.stopTest();        
        system.assert(invoiceBatchObjList[0].TAFS_Client_ID__c!=null);
    }
    /*private static testmethod void testInvoiceUpdate(){
        
        Account acc = [SELECT Id,TAFS_Client_Number__c FROM Account limit 1];
        List<TAFS_Invoice_Batch__c> invoiceBatchObjList = DataUtilTest_TAFS.createIvoiceBatches(1,acc.Id);
        List<TAFS_Invoice__c> invoiceObjList = DataUtilTest_TAFS.createInvoices(1,invoiceBatchObjList[0].Name);
        Test.startTest();
            update invoiceObjList;
        Test.stopTest();

        List<TAFS_Invoice__c> updatedInvoiceObjList = [Select Id, Name , TAFS_Invoice_Batch__c FROM TAFS_Invoice__c limit 1];
        system.assert(updatedInvoiceObjList[0].TAFS_Invoice_Batch__c!=null);
    }
    private static testmethod void testInvoiceTransactionUpdate(){
        
        Account acc = [SELECT Id,TAFS_Client_Number__c FROM Account limit 1];
        List<TAFS_Invoice_Batch__c> invoiceBatchObjList = DataUtilTest_TAFS.createIvoiceBatches(1,acc.Id);
        List<TAFS_Invoice__c> invoiceObjList = DataUtilTest_TAFS.createInvoices(1,invoiceBatchObjList[0].Name);
        List<TAFS_Invoice_Transaction__c> invoiceTransObjList = DataUtilTest_TAFS.createInvoiceTransactions(1,invoiceObjList[0].TAFS_Invoice_Key__c);
        Test.startTest();
            update invoiceTransObjList;
        Test.stopTest();   
        
        List<TAFS_Invoice_Transaction__c> updatedInvoiceTransObjList = [Select Id, Name , TAFS_Invoice__c FROM TAFS_Invoice_Transaction__c limit 1];
        system.assert(updatedInvoiceTransObjList[0].TAFS_Invoice__c!=null);
    }
    private static testmethod void testVCUpdate(){
        
        Account acc = [SELECT Id,TAFS_Client_Number__c FROM Account limit 1];
        List<TAFS_Invoice_Batch__c> invoiceBatchObjList = DataUtilTest_TAFS.createIvoiceBatches(1,acc.Id);
        List<TAFS_Invoice__c> invoiceObjList = DataUtilTest_TAFS.createInvoices(1,invoiceBatchObjList[0].Name);
        List<TAFS_Verificatons_Collections__c> vcObjList = DataUtilTest_TAFS.createVerificationCollection(1,invoiceObjList[0].TAFS_Invoice_Key__c);
        Test.startTest();
            update vcObjList;
        Test.stopTest();   
        
        List<TAFS_Verificatons_Collections__c> updatedvcObjList = [Select Id, Name , TAFS_Invoice__c FROM TAFS_Verificatons_Collections__c limit 1];
        system.assert(updatedvcObjList[0].TAFS_Invoice__c != null);
    }*/
}