@isTest(seeAllData = True)
private class TAFS_PardotTest{

    /************************************************************************************
    * Method       :    testPreventLeadAssignment
    * Description  :    Scenario to cover the prevention of Lead Assignment based on status and owner
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testPreventLeadAssignment() {
       
              User u = [select id,name,profile.name from user where profile.name != 'System Administrator' and profile.name != 'TAFS_Integration_User' and profile.name like '%TAFS_%' and isactive = true limit 1];
       
       system.runas(u){
       Lead l1 = new Lead(LastName = 'L1',Company = 'Lead 1 Company',TAFS_Lead_Type__c = 'Direct Sales',Status = 'New');
       insert l1;
       l1.ownerid = u.id;
       update l1;
       
       Lead l2 = new Lead(LastName = 'L2',Company = 'Lead 2 Company',TAFS_Lead_Type__c = 'Direct Sales',Status = 'No Contact');
       insert l2;
       l2.ownerid = u.id;
       update l2;
     
       Test.startTest();
         
       Lead l3 = new Lead(LastName = 'L3',Company = 'Lead 3 Company',TAFS_Lead_Type__c = 'Direct Sales',Status = 'New');
       insert l3;
       l1.ownerid = u.id;
       update l1;
       
       Lead l4 = new Lead(LastName = 'L4',Company = 'Lead 4 Company',TAFS_Lead_Type__c = 'Direct Sales',Status = 'No Contact');
       insert l4;
       l2.ownerid = u.id;
       update l2;
            
       
       Test.stopTest();
       
      }
               
    }
    
    
    /************************************************************************************
    * Method       :    testpostLeadConversionOperations
    * Description  :    Scenario to cover the post lead conversion operations
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testpostLeadConversionOperations() {
    List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
    List<Contact> conLst = DataUtilTest_TAFS.createContactsForLead(1,leadLst[0].id);
    User u = [select id,name,profile.name from user where profile.name != 'System Administrator' and profile.name != 'TAFS_Integration_User' and profile.name like '%TAFS_%' and isactive = true limit 1];
      Test.startTest();    
      leadLst[0].ownerid = u.id;
      leadLst[0].email = 'vmanoj@xyz.com';
      update leadLst[0]; 
                             
      Test.stopTest();
    }
    
}