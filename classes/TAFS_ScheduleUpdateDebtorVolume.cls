/* CLass Name   : TAFS_ScheduleUpdateDebtorVolume 
 * Description  : Updates the Debtor Volume on all debtors
 * Created By   : Karthik Gulla
 * Created On   : 17-Nov-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla              17-Nov-2016              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleUpdateDebtorVolume implements Schedulable {   
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_UpdateDebtorVolume_Batch(),10);
   }
}