/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  14/04/2016
* Description     :  Domain class for Fuel Committment
* Modification Log:  Initial version.
***************************************************************************************/
public with sharing class TAFS_FuelCommitmentDomain {

    //private static boolean prohibitBeforeInsertTrigger = false;
    
    Map<Id, List<TAFS_Fuel_Commitment__c>> accountFCMap =  new Map<Id, List<TAFS_Fuel_Commitment__c>>();
    set<Id> accoundIdList = new set<Id>();
    
    public void onBeforeInsert(List<TAFS_Fuel_Commitment__c> fuelCommitmentlist){
        
       //if(prohibitBeforeInsertTrigger)
       //{
       //     return;
       //}
        //getting list of all the account ids associated with fuel discount records
        for(TAFS_Fuel_Commitment__c fc : fuelCommitmentlist){
            accoundIdList.add(fc.TAFS_Client_Name__c);
        }
        //getting all the existing fuel committments for the Accounts
        for(Account accObj : [SELECT Id,(SELECT id,TAFS_Client_Name__c,TAFS_Client_Number__c,TAFS_Effective_Date__c,TAFS_Committed_Fuel_Quantity__c FROM Fuel_Commitments__r) FROM Account WHERE ID IN:accoundIdList])
        {
            accountFCMap.put(accObj.Id,accObj.Fuel_Commitments__r);
        }
        //logic to find the duplicate fuel committment record, if any
        for(TAFS_Fuel_Commitment__c fc : fuelCommitmentlist){
            if(accountFCMap.containsKey(fc.TAFS_Client_Name__c)){
                for(TAFS_Fuel_Commitment__c fcOld : accountFCMap.get(fc.TAFS_Client_Name__c)) {
                    if(fcOld.TAFS_Effective_Date__c == fc.TAFS_Effective_Date__c)
                        fc.addError('You alreday have an existing fuel commitment record with the same effective date. Please change the effective date and try again');
                }              
            }
        }       
       // prohibitBeforeInsertTrigger = true;    
    }    
}