/* Class Name   : TAFS_Task_Trigger_Handler
 * Description  : This class handles the trigger operations on Task
 * Created By   : Raushan Anand
 * Created On   : 3-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                3-May-2015              Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_Task_Trigger_Handler {
    
    /* Method Name : updateCardsonKanban
     * Description : This method Insert/update the task from Kanban in SFDC from the Response received.
     * Return Type : void
     * Input Parameter : List of Task - List<Task> 
     */
     @future(callout = true)
     public static void updateCardsonKanban(List<String> cardIdList){
         for(String cardId : cardIdList){
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setEndpoint(Label.TAFS_Kanban_Unblock_URL+'boardid/'+Label.TAFS_Kanban_Board_Id+'/taskid/'+cardId+'/event/unblock');
            req.setMethod('POST');
            req.setBody('');
            req.setHeader('APIKey', Label.TAFS_Kanban_API_Key);
            req.setHeader('Accept', 'application/json');
            req.setCompressed(true); // otherwise we hit a limit of 32000
            
            try {
                res = http.send(req);
                System.Debug('###RA'+res.getBody());
                if(res.getStatusCode() != 200){
                    TAFS_Error_Logger.insertErrorLog(res,req);
                }
            
            } catch(System.CalloutException e) {
                System.debug('Callout error: '+ e);
            
            }
         }
     }
     
    /* Method Name : addComments
     * Description : Future method to insert update comments on Kanban
     * Return Type : void
     * Input Parameter : Map<String,String>
     */
     
     @future(callout = true)
     public static void addComments(Map<String,String> cardIdCommentMap){
         for(String cardId : cardIdCommentMap.keyset()){
            String newComment = cardIdCommentMap.get(cardId);
            newComment = newComment.replace('/',' ');
            newComment = EncodingUtil.urlEncode(newComment,'UTF-8');
            newComment = newComment.replace('+','%20');
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setEndpoint(Label.TAFS_Kanban_Add_Comments_URL+'/taskid/'+cardId+'/comment/'+newComment);
            req.setMethod('POST');
            req.setBody('');
            req.setHeader('APIKey', Label.TAFS_Kanban_API_Key);
            req.setHeader('Accept', 'application/json');
            req.setCompressed(true); // otherwise we hit a limit of 32000
            
            try {
                res = http.send(req);
                System.Debug('###RA'+res.getBody());
                if(res.getStatusCode() != 200){
                    TAFS_Error_Logger.insertErrorLog(res,req);
                }
            
            } catch(System.CalloutException e) {
                System.debug('Callout error: '+ e);
            
            }
        }
    }
    
    /* Method Name : UpdateFieldsOnInvoiceBatch
     * Description : Method to update 
     * Return Type : void
     * Input Parameter : List<Task>
     */
    public static void UpdateFieldsOnInvoiceBatch(List<Task> taskList) {
        if(taskList!=null){
            //List<TAFS_Invoice_Batch__c> invoiceBatchList = new List<TAFS_Invoice_Batch__c>();
            List<String> batchIdList = new List<String>();
            Map<String,TAFS_Invoice_Batch__c> batchMap = new Map<String,TAFS_Invoice_Batch__c>();
            for(Task objTask : taskList){
                if(objTask.WhatId != null)
                    batchIdList.add(objTask.WhatId);
            }
            if(!batchIdList.IsEmpty()){
                List<TAFS_Invoice_Batch__c> batchList = [SELECT Id, TAFS_Relationship_Specialist__c, TAFS_Blocked_Reason__c, TAFS_Blocked_State__c,TAFS_Funding_Type__c FROM TAFS_Invoice_Batch__c WHERE ID IN: batchIdList];   
                for(Task objTask : taskList) {
                    for (TAFS_Invoice_Batch__c batchInvoice : batchList) {
                        if (batchInvoice.Id == objTask.WhatId) {
                            batchInvoice.TAFS_Relationship_Specialist__c = objTask.OwnerId;
                            batchInvoice.TAFS_Blocked_Reason__c = objTask.TAFS_Blocked_Reason__c;
                            batchInvoice.TAFS_Blocked_State__c = objTask.TAFS_Blocked_State__c;
                            batchInvoice.TAFS_Funding_Type__c = objTask.TAFS_Kanban_Swimlane__c;
                            batchMap.put(batchInvoice.Id,batchInvoice);
                            //invoiceBatchList.add(batchInvoice);
                        }
                    }
                }
            }
            if(!batchMap.isEmpty()){
                UPDATE batchMap.values();
            }
        }
    }
   /************************************************************************************
    * Method       :    sortLeadStatus
    * Description  :    Method to sort Lead based on Lead status.
    * Parameter    :    List<Taskb>    
    * Return Type  :    void
    *************************************************************************************/
    public static void sortLeadStatus(List<Task> taskList){
    
        set<Id> leadIdlist = new set<Id>();
        List<Lead> leadsToBeUpdated =  new List<Lead>();
        map<Id, List<Task>> leadTaskMap =  new map<Id, List<Task>>();
        Map<String,Id> queueMap = new Map<String,Id>();
        List<Group> queueList = [Select Id,Name from Group where Type = 'Queue' AND (Name = 'Direct Sales' OR Name ='National Accounts')];
        for(Group gr : queueList){
            queueMap.put(gr.Name, gr.Id);
        }
        TAFS_LeadSortCustomSetting__c lsc = TAFS_LeadSortCustomSetting__c.getOrgDefaults();

        for(Task taskObj : taskList){
            leadIdlist.add(taskObj.whoid);
        }
        if(!leadIdlist.isEmpty()){
            for(Lead leadObj : [SELECT Id,Status,TAFS_Sort_By_Status__c,(SELECT Id,CreatedDate,WhoId,Type FROM Tasks ORDER BY CreatedDate DESC) FROM Lead WHERE ID IN:leadIdlist]){
            
                if(leadObj.Tasks!=null)
                    leadTaskMap.put(leadObj.Id, leadObj.Tasks);
            }
        }
        if(!leadTaskMap.keySet().isEmpty()){
            for(Lead leadObj : [SELECT ID,Status,TAFS_Sort_By_Status__c,TAFS_Lead_Type__c,OwnerId FROM Lead WHERE ID IN : leadTaskMap.keySet()]){
                if(leadObj.Status == 'Awaiting Documentation')
                    leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('Awaiting Documentation').TAFS_Sort_Order__c + 
                    (Math.Floor( Decimal.valueOf(leadTaskMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
                if(leadObj.Status == 'Agreement Out')
                    leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('Agreement Out').TAFS_Sort_Order__c  +  
                    (Math.Floor( Decimal.valueOf(leadTaskMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
                if(leadObj.Status == 'Agreement Ready')
                    leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('Agreement Ready').TAFS_Sort_Order__c +  
                    (Math.Floor( Decimal.valueOf(leadTaskMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
                if(leadObj.Status == 'Application Out')
                    leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('Application Out').TAFS_Sort_Order__c  +  
                    (Math.Floor( Decimal.valueOf(leadTaskMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
                if(leadObj.Status == 'New'){
                    leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('New').TAFS_Sort_Order__c  +  
                    (lsc.TAFS_Reference_Date_Field_Future__c.getTime() - (Math.Floor( Decimal.valueOf(leadTaskMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))));
                    if(leadTaskMap.containsKey(leadObj.Id)){
                        for(Task objTask : leadTaskMap.get(leadObj.Id)){
                            if(objTask.Type=='Call No Contact' && objTask.whoId.getSObjectType().getDescribe().getName()=='Lead'){
                                leadObj.Status ='No Contact';
                                if(leadObj.TAFS_Lead_Type__c =='Direct Sales' && queueMap.containsKey('Direct Sales')){
                                    leadobj.OwnerId=queueMap.get('Direct Sales');
                                }
                                else if(leadObj.TAFS_Lead_Type__c =='National Accounts' && queueMap.containsKey('National Accounts')){
                                    leadobj.OwnerId=queueMap.get('National Accounts');
                                }
                            }
                            
                        }    
                    }
                }
                if(leadObj.Status == 'No Contact')
                    leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('No Contact').TAFS_Sort_Order__c  +  
                    (Math.Floor( Decimal.valueOf(leadTaskMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
                if(leadObj.Status == 'Actively Working')
                    leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('Actively Working').TAFS_Sort_Order__c  +  
                    (Math.Floor( Decimal.valueOf(leadTaskMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
                if(leadObj.Status == 'No Contact' || leadObj.Status == 'New'){
                    if(leadTaskMap.containsKey(leadObj.Id)){
                        for(Task objTask : leadTaskMap.get(leadObj.Id)){
                            if(objTask.Type=='Call' && objTask.whoId.getSObjectType().getDescribe().getName()=='Lead'){
                                leadObj.Status ='Actively Working';
                            }
                            
                        }
                        
                    }
                }
                leadsToBeUpdated.add(leadObj);            
            }
        }
        if(!leadsToBeUpdated.isEmpty()){
            update leadsToBeUpdated;
        }
    
    }

    /* Method Name : sendSMSToLeadContacts
     * Description : Sending SMS to Lead Contacts
     * Return Type : void
     * Input Parameter : List<Task>
     */
    public static void sendSMSToLeadContacts(List<Task> taskList)
     {
        if(taskList!=null)
        {
            List<Id> leadIdList = new List<Id>();         
            List<Contact> contactList = new List<Contact>(); 
            String phone = '';
            for(Task objTask : taskList)
            {
             if(objTask.WhoId != null && objTask.Type=='Call No Contact' && objTask.whoId.getSObjectType().getDescribe().getName()=='Lead') 
              leadIdList.add(objTask.WhoId);
            }
            
            for(Lead l : [select id,name,status,(select id,name,title,TAFS_Send_SMS_Leads__c,TAFS_SMS_Phone__c,phone,MobilePhone from contacts__r where TAFS_Contact_Type__c = 'Owner') from lead where id IN : leadIdList and status IN ( 'Application Out','Agreement Out')])
            {
             contactList.addAll(l.contacts__r);
            }
            
            if(!contactList.isEmpty())
            { 
             for(Contact c : contactList)
             {
              if(c.MobilePhone != null) 
              {
              phone = string.valueof(c.MobilePhone).replaceAll('[^a-zA-Z0-9]', ''); 
              c.TAFS_SMS_Phone__c= '1'+ phone;
              }
              else 
              {
              phone = string.valueof(c.Phone).replaceAll('[^a-zA-Z0-9]', ''); 
              c.TAFS_SMS_Phone__c= '1'+ phone;
              }
              c.TAFS_Send_SMS_Leads__c = true;
             }
             
             update contactList;
            }
                                 
        }
    }
    
    /************************************************************************************
    * Method       :    leadUnassignWhenTaskComplete
    * Description  :    Method to Unassign a lead which is in 'No Contact' status when a Task is completed
    * Parameter    :    List<Task>    
    * Return Type  :    void
    *************************************************************************************/
    public static void leadUnassignWhenTaskComplete(List<Task> taskList){
        Set<Id> allLeadIdsSet           = new Set<Id>();
        List<Lead> leadList             = new List<Lead>();
        Set<Id> groupSetId              = new Set<Id>();
        //List<Lead> leadListForNoContact = new List<Lead>();
        Set<Id> leadSetIdsForNoContact  = new Set<Id>();
        List<Task> taskListForNoContact = new List<Task>();
        Set<Id> taskIdSet               = new Set<Id>();
        Set<Id> taskWhoIdSet            = new Set<Id>();
        
        for(Task taskObj : tasklist){
            if(taskObj.Status == 'Completed' && taskObj.Type == 'Call No Contact' && taskObj.WhoId != null){
                taskIdSet.add(taskObj.Id);
                taskWhoIdSet.add(taskObj.WhoId);
            }
        }
        
        if(taskIdSet.size() > 0){
            List<Group> groupLst = [select Id,name from Group where Type =: Label.TAFS_Queue_Value and (Name =: Label.TAFS_National_Accounts OR Name =: Label.TAFS_Direct_Sales)];
            for(Group groupObj : groupLst){
                groupSetId.add(groupObj.Id);
            }
            //leadListForNoContact = [SELECT Id,name,TAFS_Lead_Type__c,Status FROM Lead where OwnerId NOT IN :groupSetId AND Status =: Label.TAFS_LeadStatus_No_Contact];
            
            for(Lead leadObj : [SELECT Id,name,TAFS_Lead_Type__c,Status FROM Lead where OwnerId NOT IN :groupSetId AND Status =: Label.TAFS_LeadStatus_No_Contact and ID IN :taskWhoIdSet]){
                    leadSetIdsForNoContact.add(leadObj.Id);
            }
            if(!leadSetIdsForNoContact.isEmpty() || leadSetIdsForNoContact != null){
                taskListForNoContact  = [SELECT Id,WhoId,CreatedDate,Status,Type FROM Task WHERE Id IN :taskIdSet AND WhoId IN :leadSetIdsForNoContact];
            }
            for(Task taskObj : taskListForNoContact){
                allLeadIdsSet.add(taskObj.WhoId);
            }
            if(!allLeadIdsSet.isEmpty() || allLeadIdsSet != null){
                leadList = [select id,name,TAFS_Lead_Type__c from lead where id IN :allLeadIdsSet];
            }
            
            Group grpObjectDirSales = [select Id,name from Group where Type =: Label.TAFS_Queue_Value and name =: Label.TAFS_Direct_Sales LIMIT 1];
            Group grpObjectNatAcc   = [select Id,name from Group where Type =: Label.TAFS_Queue_Value and name =: Label.TAFS_National_Accounts LIMIT 1];
            
            for(Lead leadObject : leadList)
            {
                if(leadObject.TAFS_Lead_Type__c == Label.TAFS_Direct_Sales){
                    if(grpObjectDirSales.id != null){
                       leadObject.OwnerId = grpObjectDirSales.Id;
                        }
                }
                else if(leadObject.TAFS_Lead_Type__c == Label.TAFS_National_Accounts){
                    if(grpObjectNatAcc.id != null){
                        leadObject.OwnerId = grpObjectNatAcc.Id;
                    }
                }            
            }
            Database.update(leadList);
        }
    }
    
    /************************************************************************************
    * Method       : updateLeadStatusToIncorrectInfo
    * Description  : Method to update the Lead status to 'Incorrect Information' whenever a activity of type 'Bad Number' is created
    * Parameter    : List<Task>    
    * Return Type  : void
    *************************************************************************************/
    public static void updateLeadStatusToIncorrectInfo(List<Task> taskList){
        
        Set<Id> leadSetIds  = new Set<Id>();
        Set<Id> leadIdlist = new Set<Id>();
        List<Lead> leadListForFinalUpdate  = new List<Lead>();
        
        for(Task taskObj : tasklist){
            if(taskobj.whoId!=null){
                if(taskObj.Type == 'Bad Number' && taskobj.whoId.getSObjectType().getDescribe().getName()=='Lead'){
                    leadIdlist.add(taskObj.whoid);
                }
                else if(taskObj.Type == 'Call' && taskobj.whoId.getSObjectType().getDescribe().getName()=='Lead'){
                    leadSetIds.add(taskObj.whoid);
                }
            }
        }
        if(leadIdlist.size() > 0){
            for(Lead leadObj : [SELECT Id,Status,TAFS_Lead_Type__c,(SELECT Id,CreatedDate,WhoId,Type,Status FROM Tasks) FROM Lead WHERE ID IN:leadIdlist]){
                if(leadObj.Tasks != null){
                    leadObj.Status = 'Incorrect Information';
                    leadListForFinalUpdate.add(leadObj);
                }
            }
        }
        if(leadSetIds.size() > 0){
            for(Lead leadObj : [SELECT Id,Status,TAFS_Lead_Type__c,(SELECT Id,CreatedDate,WhoId,Type,Status FROM Tasks) FROM Lead WHERE Status IN('New','No Contact') AND ID IN:leadSetIds]){
                if(leadObj.Tasks != null){
                    leadObj.Status = 'Actively Working';
                    leadListForFinalUpdate.add(leadObj);
                }
            }
        }
        if(!leadListForFinalUpdate.isEmpty()){
            Database.update(leadListForFinalUpdate);
        }
    }
    
    /************************************************************************************
    * Method       : updateLastAttemptedContactDateOnAccount
    * Description  : Method to update the 'Last Attempted Contact Date' to Today whenever an activity of type 'Call No Contact' is created
    * Parameter    : List<Task>    
    * Return Type  : void
    *************************************************************************************/
    public static void updateLastAttemptedContactDateOnAccount(List<Task> taskList){
        Set<Id> setClientIds = new Set<Id>();
        for(Task t:taskList){
            if(t.Type == Label.TAFS_TaskType_CallNoContact){
                setClientIds.add(t.WhatId);
            }
        }
        List<Account> lstAccounts = new List<Account>();
        for(Account acc:[SELECT Id, TAFS_Last_Attempted_Contact_Date__c FROM Account WHERE Id In :setClientIds]){
            acc.TAFS_Last_Attempted_Contact_Date__c = System.Today();
            lstAccounts.add(acc);
        }
        if(lstAccounts.size()>0)
            update lstAccounts;
    }
}