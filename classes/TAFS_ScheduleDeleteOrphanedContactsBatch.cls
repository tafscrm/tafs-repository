/* CLass Name   : TAFS_ScheduleDeleteOrphanedContactsBatch 
 * Description  : Updates the Debtor Volume on all debtors
 * Created By   : DeleteS, if any Orphaned Contacts available after Lead Conversion process
 * Created On   : 19-JAN-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla              19-JAN-2017             Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleDeleteOrphanedContactsBatch implements Schedulable {   
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_BatchDeleteOrphanedContacts());
   }
}