@isTest
private class TAFS_DriverListTriggerTest{

    /************************************************************************************
    * Method       :    testupdateClientIdOnDriverList
    * Description  :    Scenario to cover the updation of Client Id on the Driver List object
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testupdateClientIdOnDriverList() {
       
     
       Test.startTest();
       
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);        
                 
       TAFS_Driver_List__c dl = new TAFS_Driver_List__c(Name = 'Driver list 567',TAFS_Client_Number__c = '123450',TAFS_Client_Name__c=lstAcct[0].id);   
       insert dl;
       
       Test.stopTest();
                     
    }
}