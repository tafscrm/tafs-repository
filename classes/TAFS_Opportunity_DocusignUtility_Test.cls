/* CLass Name   : TAFS_Opportunity_DocusignUtility_Test
 * Description  : Test class for TAFS_Opportunity_DocusignUtility
 * Created By   : Karthik Gulla
 * Created On   : 14-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla               14-Mar-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private with sharing class TAFS_Opportunity_DocusignUtility_Test {
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(2,clientTermList[0].Id);
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[0].Id, UserInfo.getUserId(), 'Relationship Specialist');
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[1].Id, UserInfo.getUserId(), 'Relationship Specialist');
        DataUtilTest_TAFS.createOpportunities(1, lstAcct[0].Id,'Tire Program');
        DataUtilTest_TAFS.createOpportunities(1, lstAcct[1].Id,'Business Loan');

        General_Manager__c genManager = new General_Manager__c();
        genManager.Name = 'General Manager';
        genManager.Email__c = UserInfo.getUserEmail();
        genManager.First_Name__c = UserInfo.getFirstName();
        genManager.Last_Name__c = UserInfo.getLastName();
        insert genManager;
    } 
    private static testmethod void testOpportunityDocumentSend() {
        List<Opportunity> lstOpportunities = [SELECT Id FROM Opportunity];       
        Test.startTest();
        PageReference pageRef = Page.TAFS_Opportunity_SendDocsWithDocusign;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', lstOpportunities[0].Id);
        TAFS_Opportunity_DocusignUtility tafsOppDocusign = new TAFS_Opportunity_DocusignUtility();
        tafsOppDocusign.sendDocumentsToDocuSign();

        ApexPages.currentPage().getParameters().put('Id', lstOpportunities[1].Id);
        TAFS_Opportunity_DocusignUtility tafsOppDocusignOne = new TAFS_Opportunity_DocusignUtility();
        tafsOppDocusignOne.sendDocumentsToDocuSign();
        Test.stopTest();
    }   
}