@isTest
public class TAFS_Insurance_Policies_Trigger_Test {
/************************************************************************************
* Method       :    testPolicy1
* Description  :    Scenario to cover the Insurance Policies
* Parameter    :    NIL    
* Return Type  :    void
*************************************************************************************/
    public static testmethod void testPolicy1(){
       
        Lead objLead 		= 	 new Lead(
                                 OwnerId              = UserInfo.getUserId(),
                                 Status               = 'New',
                                 LastName             = 'Last Test',
                                 TAFS_Lead_Type__c    = 'Direct Sales',
                                 Company              = 'xyz',
                                 TAFS_Reference_ID__c = '1234567',
                                 FirstName            = 'Test First',
                                 Salutation           = 'Mr',
            					 TAFS_City_Dup__c	  ='Test',
            					 TAFS_Company_Dup__c	  ='Test',
            					 DBA_Name_Dup__c	  ='Test',
            					 TAFS_DOT_Number_Dup__c	  ='Test',
            					 TAFS_Last_Name_Dup__c	  ='Test',
            					 TAFS_MC_Number_Dup__c	  ='Test',
            					 TAFS_Number_of_Active_Vehicles_Dup__c	 =2,
            					 TAFS_Phone_Dup__c	  = '123455678',
            					 TAFS_Postal_Code_Dup__c	  ='12345',
            					 TAFS_State_Dup__c	  ='Test',
            					 TAFS_Street_Dup__c	  ='Test'            					 
                               	 );
        INSERT objLead;
        List<TAFS_Client_Terms__c> cTerms = DataUtilTest_TAFS.createClientTerms(1);
        List<Account> clientObj = DataUtilTest_TAFS.createAccounts(1, cTerms[0].Id);
        clientObj[0].TAFS_Reference_ID__c = '12345678';
        UPDATE clientObj;
        
        List<TAFS_Insurance_Policies__c> incList = new List<TAFS_Insurance_Policies__c>();
        
        TAFS_Insurance_Policies__c objInsurance = new TAFS_Insurance_Policies__c();
        objInsurance.TAFS_Reference_ID__c ='1234567';
        objInsurance.TAFS_Type__c ='Test Type';
        objInsurance.TAFS_Insurer__c='Test Insurer';
        objInsurance.TAFS_Effective_Date__c= Date.Today();
        incList.add(objInsurance);
        
        TAFS_Insurance_Policies__c objInsurance2 = new TAFS_Insurance_Policies__c();
        objInsurance2.TAFS_Reference_ID__c ='12345678';
        objInsurance2.TAFS_Type__c ='Test Type';
        objInsurance2.TAFS_Insurer__c='Test Insurer';
        objInsurance2.TAFS_Effective_Date__c= Date.Today();
        incList.add(objInsurance2);
        
        INSERT incList;
        
        List<TAFS_Insurance_Policies__c> upInsList = new List<TAFS_Insurance_Policies__c>();
        TAFS_Insurance_Policies__c objInsurance3 = new TAFS_Insurance_Policies__c();
        objInsurance3.TAFS_Reference_ID__c ='1234567';
        objInsurance3.TAFS_Type__c ='Test Type';
        objInsurance3.TAFS_Insurer__c='Test Insurer';
        objInsurance3.TAFS_Effective_Date__c= Date.Today();
        upInsList.add(objInsurance3);
        
        TAFS_Insurance_Policies__c objInsurance4 = new TAFS_Insurance_Policies__c();
        objInsurance4.TAFS_Reference_ID__c ='12345678';
        upInsList.add(objInsurance4);
        
        INSERT upInsList;
    }

}