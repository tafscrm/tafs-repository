/* CLass Name   : TAFS_ScheduleFuelConsumptionBatch
 * Description  : Send verification email to Debtor Contacts
 * Created By   : Pankaj Singh
 * Created On   : 25-Oct-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                25-Oct-2016              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleFuelConsumptionBatch implements Schedulable {
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_UpdateFuelConsumptionDataOnAccount(),10);
   }
}