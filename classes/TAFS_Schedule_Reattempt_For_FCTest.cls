/* CLass Name   : TAFS_Schedule_Reattempt_For_FCTest 
 * Description  : Reattempt logic for FC
 * Created By   : Raushan Anand
 * Created On   : 29-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                29-Aug-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_Schedule_Reattempt_For_FCTest {
    static testmethod void testReattemptBatch(){
        TAFS_Error_Log__c errObj = new TAFS_Error_Log__c(TAFS_Request__c = 'System.HttpRequest[Endpoint=http://tafssf.freightconnect.com/leadToClient/12345MC123456, Method=POST]',
                                                        TAFS_Status_Code__c = '200',
                                                        TAFS_Type__c = 'Freight Connect',
                                                        TAFS_Endpoint__c = 'http://tafssf.freightconnect.com/leadToClient/12345MC123456');
		INSERT errObj;
            
        Test.startTest();    
        Schedule_Reattempt_For_FC sh1 = new Schedule_Reattempt_For_FC();
		String sch = '0 0 23 * * ?';
        Map<String,String> responseHeader = new Map<String,String>();
        responseHeader.put('Content-Type', 'application/json');
        String responseBody = '{"taskid":"12345","boardid":"30","title":"Test Title","description":"Test Description","type":"None","assignee":"Raushan","subtasks":"0","subtaskscomplete":"0","color":"#34a97b","priority":"Average","size":null,"deadline":null,"deadlineoriginalformat":null,"extlink":"","tags":"","leadtime":0,"blocked":"1","blockedreason":"Blocked - RS Input Required","columnname":"Invoice Backlog","lanename":"Standard EFS","subtaskdetails":[],"columnid":"requested_335","laneid":"343","position":"0","columnpath":"Invoice Backlog","loggedtime":0,"customfields":[{"fieldid":"19","name":"Batch Number","type":"number","value":"123456","mandatory":true},{"fieldid":"23","name":"Client Number","type":"number","value":"0","mandatory":true}],"comments":[{"author":"Raushan","event":"Comment added","text":"Unblocked 3","date":"2016-05-05 20:32:17","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:31:05","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:25:44","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Kanban Comment","date":"2016-05-05 20:19:21","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 2","date":"2016-05-05 20:14:29","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 1","date":"2016-05-05 20:14:25","taskid":"49136"}]}';
        Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(204,'OK',responseBody,responseHeader));
        system.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest();    
    }
}