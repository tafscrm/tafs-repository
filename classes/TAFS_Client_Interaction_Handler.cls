/* CLass Name   : TAFS_Client_Interaction_Handler
 * Description  : Trigger methods for Client Interaction object
 * Created By   : Manoj M Vootla
 * Created On   : 30-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla              30-May-2015              Initial version.
 *  * Karthik Gulla               22-Dec-2016              LastAttemptedContactDate Changes
 *****************************************************************************************/
public with sharing class TAFS_Client_Interaction_Handler {

/****************************************************************************************
* Method Name      :  updateLastAttemptedContactDateOnClient
* Input Parameter  :  List of Client Interactions (List<TAFS_Client_Interaction__c>)
* Return Type      :  Void
* Description      :  This method populates last attempted contact date field on the Client based on the latest value
***************************************************************************************/
public static void updateLastAttemptedContactDateOnClient(List<TAFS_Client_Interaction__c> clientInteractionList){
    
    Map<Id, List<TAFS_Client_Interaction__c>> accountCIMap =  new Map<Id, List<TAFS_Client_Interaction__c>>();
    Map<Id, List<Task>> accountTaskMap =  new Map<Id, List<Task>>();
    Map<Id, Account> accountIdMap = new Map<Id, Account>();
    set<Id> accounIdSet = new set<Id>();
    String account_prefix = Schema.SObjectType.Account.getKeyPrefix();
    
    for(TAFS_Client_Interaction__c clIntList : clientInteractionList){
        accounIdSet.add(clIntList.TAFS_Client_Name__c);    
    }
     
    try{
        for(Account accObj : [SELECT Id,TAFS_Last_Attempted_Contact_Date__c,(SELECT id,createddate from Tasks where Type = :Label.TAFS_TaskType_CallNoContact order by createddate desc),(SELECT id,TAFS_Contact_Date_Time__c from Client_Interactions__r order by TAFS_Contact_Date_Time__c desc) FROM Account WHERE ID IN:accounIdSet]){
            accountIdMap.put(accObj.Id,accObj);
            if(accObj.Client_Interactions__r!=null)
                accountCIMap.put(accObj.Id,accObj.Client_Interactions__r);
            if(accObj.Tasks!=null)
                accountTaskMap.put(accObj.Id,accObj.Tasks);  
        }
        
     
        List<Account> accUpdate = new List<Account>();
        List<Task> tList = new List<Task>();
        List<TAFS_Client_Interaction__c> clIntList = new List<TAFS_Client_Interaction__c>();
        for(TAFS_Client_Interaction__c cIntList : clientInteractionList){
            tList.clear();
            clIntList.clear();
            if(accountTaskMap.containskey(cIntList.TAFS_Client_Name__c))
                tList = accountTaskMap.get(cIntList.TAFS_Client_Name__c);
            
            if(accountCIMap.containskey(cIntList.TAFS_Client_Name__c))
                clIntList = accountCIMap.get(cIntList.TAFS_Client_Name__c);
          
            Account a = accountIdMap.get(cIntList.TAFS_Client_Name__c);
            
            if(tList == null || tList.size()<1){
                if(clIntList == null || clIntList.size()<1){
                    a.TAFS_Last_Attempted_Contact_Date__c = (cIntList.TAFS_Contact_Date_Time__c).date();
                }
                else{
                    if(cIntList.TAFS_Contact_Date_Time__c > clIntList[0].TAFS_Contact_Date_Time__c)
                        a.TAFS_Last_Attempted_Contact_Date__c = (cIntList.TAFS_Contact_Date_Time__c).date();
                    else
                        a.TAFS_Last_Attempted_Contact_Date__c = (clIntList[0].TAFS_Contact_Date_Time__c).date();
                }
            }
            
            if(tList !=null && tList.size()>0){
                if(clIntList == null || clIntList.size()<1){
                    if(cIntList.TAFS_Contact_Date_Time__c > tList[0].createddate)
                        a.TAFS_Last_Attempted_Contact_Date__c = (cIntList.TAFS_Contact_Date_Time__c).date();
                    else
                        a.TAFS_Last_Attempted_Contact_Date__c = (tList[0].createddate).date();
                }
                else{
                    if(cIntList.TAFS_Contact_Date_Time__c > tList[0].createddate){
                        if(cIntList.TAFS_Contact_Date_Time__c > clIntList[0].TAFS_Contact_Date_Time__c)
                            a.TAFS_Last_Attempted_Contact_Date__c = (cIntList.TAFS_Contact_Date_Time__c).date();  
                        else
                            a.TAFS_Last_Attempted_Contact_Date__c = (clIntList[0].TAFS_Contact_Date_Time__c).date();
                    }
                    else if(tList[0].createddate > clIntList[0].TAFS_Contact_Date_Time__c){
                        a.TAFS_Last_Attempted_Contact_Date__c = (tList[0].createddate).date();
                    }
                    else{
                        a.TAFS_Last_Attempted_Contact_Date__c = (clIntList[0].TAFS_Contact_Date_Time__c).date();
                    }
                }
            }
           
            accUpdate.add(a);
        }
     
        if(accUpdate !=null && !accUpdate.isEmpty())
            update accUpdate;
      }
      catch(Exception e){
         System.debug('The following exception has occurred: ' + e.getMessage());
      }
  }
}