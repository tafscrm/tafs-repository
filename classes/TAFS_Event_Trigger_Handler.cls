/* Class Name   : TAFS_Event_Trigger_Handler
 * Description  : This class handles the trigger operations on Event
 * Created By   : Pankaj Singh
 * Created On   : 8-June-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                3-May-2015              Initial version.
 *
 *****************************************************************************************/
public class TAFS_Event_Trigger_Handler{

 /************************************************************************************
    * Method       :    sortLeadStatus
    * Description  :    Method to sort Lead based on Lead status.
    * Parameter    :    List<Taskb>    
    * Return Type  :    void
    *************************************************************************************/
    public static void sortLeadStatus(List<Event> eventList){
    
        set<Id> leadIdlist = new set<Id>();
        List<Lead> leadsToBeUpdated =  new List<Lead>();
        map<Id, List<Event>> leadEventMap =  new map<Id, List<Event>>();
        
        TAFS_LeadSortCustomSetting__c lsc = TAFS_LeadSortCustomSetting__c.getOrgDefaults();

        for(Event eventObj : eventList){
            leadIdlist.add(eventObj.whoid);
        }
        for(Lead leadObj : [SELECT Id,Status,TAFS_Sort_By_Status__c,(SELECT Id,CreatedDate FROM Events ORDER BY CreatedDate DESC) FROM Lead WHERE ID IN:leadIdlist]){
            
            if(leadObj.Tasks!=null)
                leadEventMap.put(leadObj.Id, leadObj.Events);
        }
        for(Lead leadObj : [SELECT ID,Status,TAFS_Sort_By_Status__c FROM Lead WHERE ID IN : leadEventMap.keySet()]){
            if(leadObj.Status == 'Awaiting Documentation')
                leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('Awaiting Documentation').TAFS_Sort_Order__c + 
                (Math.Floor( Decimal.valueOf(leadEventMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
            if(leadObj.Status == 'Agreement Out')
                leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('Agreement Out').TAFS_Sort_Order__c  +  
                (Math.Floor( Decimal.valueOf(leadEventMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
            if(leadObj.Status == 'Agreement Ready')
                leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('Agreement Ready').TAFS_Sort_Order__c +  
                (Math.Floor( Decimal.valueOf(leadEventMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
            if(leadObj.Status == 'Application Out')
                leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('Application Out').TAFS_Sort_Order__c  +  
                (Math.Floor( Decimal.valueOf(leadEventMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
            if(leadObj.Status == 'New')
                leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('New').TAFS_Sort_Order__c  +  
                 (lsc.TAFS_Reference_Date_Field_Future__c.getTime() - (Math.Floor( Decimal.valueOf(leadEventMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))));
            if(leadObj.Status == 'No Contact')
                leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('No Contact').TAFS_Sort_Order__c  +  
                (Math.Floor( Decimal.valueOf(leadEventMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
            if(leadObj.Status == 'Actively Working')
                leadObj.TAFS_Sort_By_Status__c = TAFS_Lead_Sort_Order__c.getInstance('Actively Working').TAFS_Sort_Order__c  +  
                (Math.Floor( Decimal.valueOf(leadEventMap.get(leadObj.Id)[0].CreatedDate.getTime()/(1000)))-lsc.TAFS_Reference_Date_Field_Past__c.getTime());
            
            leadsToBeUpdated.add(leadObj);            
        }
        if(!leadsToBeUpdated.isEmpty()){
            update leadsToBeUpdated;
        }
    
    }

}