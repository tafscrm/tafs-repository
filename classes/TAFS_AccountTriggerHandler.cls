public class TAFS_AccountTriggerHandler{

/****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  15/06/2016
    * Description     :  Assign task on Lead
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void assignTask(List<Account> accList, map<Id, Account> accOldMap){
    
        list<Task> NewTask = new list<Task>();
        Map<Id, List<AccountTeamMember>> accountTeamMemberMap =  new Map<Id, List<AccountTeamMember>>();
        
        for(Account accObj : [select id, (SELECT AccountId,UserId,TeamMemberRole FROM AccountTeamMembers where TeamMemberRole IN ('Relationship Specialist','Sales Account Manager') ORDER BY CreatedDate DESC) from Account Where Id IN : accList]){
            
            if(accObj.AccountTeamMembers!=null)
                accountTeamMemberMap.put(accObj.id , accObj.AccountTeamMembers);
        }
        for(Account accObj : accList)
        {
            if(accountTeamMemberMap.get(accObj.Id).size()==0){
                accObj.TAFS_Sales_Account_Manager_Lookup__c = null;
            }
            else{
                for(AccountTeamMember m1: accountTeamMemberMap.get(accObj.Id)){
                    if(accObj.TAFS_Sales_Account_Manager_Lookup__c==null && m1.TeamMemberRole == 'Sales Account Manager'){
                        accObj.TAFS_Sales_Account_Manager_Lookup__c = m1.UserId;
                    }            
                }
            }
            if(accOldMap.get( accObj.Id ).TAFS_Authority_Status_Contract__c!= accObj.TAFS_Authority_Status_Contract__c||
            accOldMap.get( accObj.Id ).TAFS_Authority_Status_Common__c != accObj.TAFS_Authority_Status_Common__c ||
            accOldMap.get( accObj.Id ).TAFS_Authority_Status_Broker__c != accObj.TAFS_Authority_Status_Broker__c)
            {
                for(AccountTeamMember m1: accountTeamMemberMap.get(accObj.Id))
                {
                    if(m1.TeamMemberRole== 'Relationship Specialist'){
                        Task objTask= new Task();
                        objTask.OwnerId     = m1.Userid;
                        objTask.Subject     = 'Authority Followup';
                        objTask.WhatId      = accObj.Id;
                        NewTask.add(objTask);
                    }
                }            
            }
        }
        if(!NewTask.isEmpty()){
            Database.insert(NewTask);
        }
    }
}