/* CLass Name   : TAFS_ScheduleVerificationEmailBatch
 * Description  : Send verification email to Debtor Contacts
 * Created By   : Raushan Anand
 * Created On   : 10-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                10-Aug-2016              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleVerificationEmailBatch implements Schedulable {
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_SendVerificationEmailBatch(),1);
   }
}