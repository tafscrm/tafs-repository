@isTest
private class TAFS_AutomaticExpensesTriggerTest{

    /************************************************************************************
    * Method       :    testupdateClientIdOnAutomaticExpense
    * Description  :    Scenario to cover the updation of Client Id on the Automatic Expense object
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testupdateClientIdOnAutomaticExpense() {
       
     
       Test.startTest();
       
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);        
       TAFS_Automatic_Expenses__c atexp = new TAFS_Automatic_Expenses__c(Name = 'Auto Exp 123',TAFS_Client_Number__c = '123450',TAFS_Amount__c=20,TAFS_Client_Name__c=lstAcct[0].id);   
       insert atexp;
       
       Test.stopTest();
                     
    }
}