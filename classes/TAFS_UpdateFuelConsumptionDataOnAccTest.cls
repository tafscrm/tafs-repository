/* CLass Name   : TAFS_UpdateFuelConsumptionDataOnAccTest
 * Description  : Test class for TAFS_UpdateFuelConsumptionDataOnAccount
 * Created By   : Karthik Gulla
 * Created On   : 07-Nov-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla          14-Nov-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_UpdateFuelConsumptionDataOnAccTest{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
     
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
        list<TAFS_Fuel_Consumptions__c> fcList =  DataUtilTest_TAFS.createFuelConsumptions(4,lstAcct[0].Id);

        //insert Fuel Discounts Test data
        list<Fuel_Discount__c> fdList = DataUtilTest_TAFS.createFuelDiscounts(4,lstAcct[0].Id);
        insert fdList;

        //insert Fuel Commitment Test data
        list<TAFS_Fuel_Commitment__c> fcommList = DataUtilTest_TAFS.createFuelCommitments(4,lstAcct[0].Id);
        insert fcommList;

        for(Integer i = 0;i<fcList.size();i++){
            if(i > 1)
                fcList[i].TAFS_Transaction_Date__c = Date.Today().addMonths(-1);
        }
        update fcList;

        for(Fuel_Discount__c fd:fdList){
            fd.TAFS_Effective_Date_Liter__c = Date.Today() - 1;
            fd.TAFS_Effective_Date_Gallon__c = Date.Today() - 1;
        }
        update fdList;

        for(TAFS_Fuel_Commitment__c fcomm:fcommList){
            fcomm.TAFS_Effective_Date__c = Date.Today() - 1;
        }
        update fcommList;
    } 

    /************************************************************************************
    * Method       :    testUpdateFuelConsumptionDataonAccountTest
    * Description  :    Test Method to test Update Fuel Consumption Data on Account
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testUpdateFuelConsumptionDataonAccountTest() { 
        //Start Test
        Test.startTest();
        TAFS_UpdateFuelConsumptionDataOnAccount tafsUpdateFuelConsumptionBatch = new TAFS_UpdateFuelConsumptionDataOnAccount();
        Database.executeBatch(tafsUpdateFuelConsumptionBatch);        
        //Stop Test
        Test.stopTest();

        //Assertion to check the updated Fuel Consumption details on Account
        List<Account> lstAcc = [SELECT TAFS_In_Network_Usage_MTD__c, TAFS_Fuel_Savings_Previous_Month__c, TAFS_Fuel_Savings_Lifetime__c, TAFS_Prorated_Committment_MTD__c 
                        FROM Account];
        for(Account ac:lstAcc){
            System.assertEquals(ac.TAFS_In_Network_Usage_MTD__c <> 0, true);
            System.assertEquals(ac.TAFS_Fuel_Savings_Previous_Month__c <> 0, true);
            System.assertEquals(ac.TAFS_Fuel_Savings_Lifetime__c <> 0, true);
            System.assertEquals(ac.TAFS_Prorated_Committment_MTD__c <> 0, true);
        }
    }
}