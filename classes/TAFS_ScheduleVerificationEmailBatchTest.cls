/* CLass Name   : TAFS_ScheduleVerificationEmailBatchTest
 * Description  : Test class for TAFS_ScheduleVerificationEmailBatch
 * Created By   : Arpitha Sudhakar
 * Created On   : 24-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar            24-Aug-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_ScheduleVerificationEmailBatchTest {

    static testmethod void test() {
       Test.startTest();
       TAFS_ScheduleVerificationEmailBatch c = new TAFS_ScheduleVerificationEmailBatch();
       String scheduleTime = '0 0 23 * * ?';
       system.schedule('Test schedule', scheduleTime, c);
       Test.stopTest();
    }
}