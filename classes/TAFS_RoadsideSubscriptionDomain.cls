/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  13/04/2016
* Description     :  Domain class for Roadside Assistance Subscription
* Modification Log:  Initial version.
***************************************************************************************/
public with sharing class TAFS_RoadsideSubscriptionDomain {

    private static boolean prohibitBeforeInsertTrigger = false;
    
    public void onBeforeInsert(List<TAFS_Roadside_Assistance__c> subscriptionList){
    
        if(prohibitBeforeInsertTrigger)
        {
            return;
        }
        
        Map<String, String> clientNumerIdMap =  new Map<String, String>();
        
        //creating a map of client number and account ids
        for(Account acc : [SELECT Id,TAFS_Client_Number__c FROM ACCOUNT]){
           
            clientNumerIdMap.put(acc.TAFS_Client_Number__c, acc.Id);
        } 
        //Assigning the account id to the roadside subscription record based on the client number
        for(TAFS_Roadside_Assistance__c rs : subscriptionList){
             if(clientNumerIdMap.containsKey(rs.TAFS_Client_Number__c)){
                 rs.TAFS_Client_ID__c = clientNumerIdMap.get(rs.TAFS_Client_Number__c);
             }           
        }
        prohibitBeforeInsertTrigger = true;
    
    }
    
}