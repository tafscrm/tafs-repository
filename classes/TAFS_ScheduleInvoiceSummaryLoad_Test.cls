/* CLass Name   : TAFS_ScheduleInvoiceSummaryLoad_Test
 * Description  : Test class for TAFS_ScheduleInvoiceSummaryLoad
 * Created By   : Karthik Gulla
 * Created On   : 15-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla          15-Feb-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_ScheduleInvoiceSummaryLoad_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[0].Id, UserInfo.getUserId(), 'Relationship Specialist');
        List<TAFS_Invoice_Batch__c> batchLst = DataUtilTest_TAFS.createIvoiceBatches(1,lstAcct[0].id);
        for(TAFS_Invoice_Batch__c tafsInvBatch:batchLst){
            tafsInvBatch.TAFS_Posted_Date__c = dateTime.now().addMonths(-1);
        }
        update batchLst;

        List<TAFS_Invoice__c> invLst = DataUtilTest_TAFS.createInvoices(2,batchLst[0].id);
        for(TAFS_Invoice__c tfInv:invLst){
            tfInv.Debtor__c = lstAcct[0].Id;
        }
        update invLst;

        list<TAFS_Fuel_Consumptions__c> fcList =  DataUtilTest_TAFS.createFuelConsumptions(1,lstAcct[0].Id);
        fcList[0].TAFS_Transaction_Date__c = Date.today().addMonths(-1);
        update fcList[0];

        list<TAFS_Invoice_Transaction__c> lstInvTransactionsUSD = DataUtilTest_TAFS.createInvoiceTransactions(1,invLst[0].TAFS_Invoice_key__c);
        lstInvTransactionsUSD[0].TAFS_Status__c = 'Processed';
        lstInvTransactionsUSD[0].TAFS_Date__c = System.today().addDays(-30);
        update lstInvTransactionsUSD[0];

        //For Canadian Dollars(CAD) Break up data
        Account testAcc = new Account( 
                                    Name                                    = 'Test Account 0012324',
                                    AccountNumber                           =  '0012324',
                                    TAFS_Client_Status__c                   = 'Client Created', 
                                    TAFS_Client_Credit_Check_Password__c    = 'test',
                                    TAFS_Lead_Source__c                     = 'test lead',                                    
                                    TAFS_Company_Type__c                    = 'Carrier',
                                    TAFS_Business_Type__c                   = 'test',
                                    TAFS_Currency_Type__c                   = 'CAD',
                                    TAFS_UCC_Date__c                        =  Date.Today(),
                                    TAFS_Signed_Date__c                     =  Date.Today(),
                                    TAFS_Federal_Tax_ID__c                  = 'fed-001',
                                    TAFS_Default_Remittance_Method__c       = 'default',
                                    TAFS_Max_Invoice_Amount__c              =  200,
                                    Phone                                   = '9663066004',
                                    TAFS_Email__c                           = 'a@b.com',
                                    TAFS_Year_Started__c                    = 'test',
                                    TAFS_Office__c                          = 'CA',
                                    TAFS_Client_Terms__c                    =  clientTermList[0].Id,
                                    BillingStreet                           = 'test',
                                    BillingState                            = 'test',
                                    BillingCity                             = 'test',
                                    BillingCountry                          = 'US',
                                    BillingPostalCode                       ='560100',
                                    TAFS_DOT__c                             = 'dot-001',
                                    TAFS_Client_Number__c                   = '123451'
                                );
        insert testAcc;
        DataUtilTest_TAFS.createAccountTeamMember(testAcc.Id, UserInfo.getUserId(), 'Relationship Specialist');

        TAFS_Invoice_Batch__c tafsInvBatch = new TAFS_Invoice_Batch__c(TAFS_Client_ID__c        = testAcc.Id,
                                                           TAFS_Client_Number__c                = '123452',
                                                           Name                                 = 'Batch2',
                                                           TAFS_Type__c                         = '0',
                                                           TAFS_Status__c                       = 'processed',
                                                           TAFS_A_R_Amount__c                   = 100,
                                                           TAFS_Fee_Earned__c                   = 50,
                                                           TAFS_Posted_Date__c                  = dateTime.now().addMonths(-1),
                                                           TAFS_External_Id__c                  = 'EXT2');
        insert tafsInvBatch;

        TAFS_Invoice__c tafsInvoice = new TAFS_Invoice__c(Name                       = 'INV2',
                                                TAFS_Batch_No__c                     =  tafsInvBatch.Id,
                                                TAFS_Invoice_Batch__c                =  tafsInvBatch.Id,
                                                TAFS_Balance__c                      = 11100.00,
                                                TAFS_Client_Number__c                = '123452',
                                                TAFS_Invoice_key__c                  = 'key2',
                                                TAFS_Invoice_Delivery_Status__c      =  '0',
                                                TAFS_PO_No__c                        = '1122',
                                                TAFS_Purchased__c                    = System.now(),
                                                TAFS_Client_Name__c                  = testAcc.Id,
                                                TAFS_Invoice_Delivery_Method__c      =  '0');
        insert tafsInvoice;

        list<TAFS_Invoice_Transaction__c> lstInvTransactionsCAD = DataUtilTest_TAFS.createInvoiceTransactions(1,tafsInvoice.TAFS_Invoice_key__c);
        lstInvTransactionsCAD[0].TAFS_Status__c = 'Processed';
        lstInvTransactionsCAD[0].TAFS_Date__c = System.today().addDays(-30);
        update lstInvTransactionsCAD[0];

        list<TAFS_Fuel_Consumptions__c> fcListCAD =  DataUtilTest_TAFS.createFuelConsumptions(2,testAcc.Id);
        fcListCAD[0].TAFS_Transaction_Date__c = Date.today().addMonths(-1);
        fcListCAD[1].TAFS_Transaction_Date__c = Date.today().addMonths(-2);
        update fcListCAD;
    } 

    /************************************************************************************
    * Method       :    testScheduleInvoiceSummaryLoad
    * Description  :    Test Method to test the load of Invoice summary with monthly Calculations
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testScheduleInvoiceSummaryLoad() {
        Test.startTest();
        TAFS_ScheduleInvoiceSummaryLoad tafsInvSummaryLoad = new TAFS_ScheduleInvoiceSummaryLoad();
        String scheduleTime = '0 0 23 * * ?';
        System.schedule('TAFS_ScheduleInvoiceSummaryLoad_Test', scheduleTime, tafsInvSummaryLoad);
        Test.stopTest();
    }
}