/* CLass Name : Schedule_MTD_Update
 * Description  : Update Month to date Factored Volume field on Account
 * Created By   : Raushan Anand
 * Created On   : 30-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                30-May-2015              Initial version.
 *
 *****************************************************************************************/
global class Schedule_MTD_Update implements Schedulable {
   	global void execute(SchedulableContext SC) {
		Database.executeBatch(new TAFS_Update_Month_To_Date_Batch(),50);
   }
}