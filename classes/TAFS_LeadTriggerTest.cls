@isTest(SeeAllData=false)
private class TAFS_LeadTriggerTest{


    /************************************************************************************
    * Method       :    testAutopopulateInvoiceOnInvoiceImage
    * Description  :    Scenario to cover the prevention of Lead Assignment based on status and owner
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
 /* public static testmethod void testPreventLeadAssignment() {
       
       User u = [select id,name from user where profile.name = 'System Administrator' and isactive = true limit 1];
       
       Lead l1 = new Lead(LastName = 'L1',Company = 'Lead 1 Company',TAFS_Lead_Type__c = 'Direct Sales',Status = 'New');
       insert l1;
       l1.ownerid = u.id;
       update l1;
       
       Lead l2 = new Lead(LastName = 'L2',Company = 'Lead 2 Company',TAFS_Lead_Type__c = 'Direct Sales',Status = 'No Contact');
       insert l2;
       l2.ownerid = u.id;
       update l2;
       
       list<Lead> leadlst = new list<Lead>();
       leadlst = [SELECT Id,status,owner.id FROM Lead];
       insert leadlst ;
       
       list<User> userlst = new list<User>();
       userlst = [SELECT Id, Profile.Name FROM User];
       insert userlst ;
     
       Test.startTest();
         
       Lead l3 = new Lead(LastName = 'L3',Company = 'Lead 3 Company',TAFS_Lead_Type__c = 'Direct Sales',Status = 'New');
       insert l3;
       l1.ownerid = u.id;
       update l1;
       
       Lead l4 = new Lead(LastName = 'L4',Company = 'Lead 4 Company',TAFS_Lead_Type__c = 'Direct Sales',Status = 'No Contact');
       insert l4;
       l2.ownerid = u.id;
       update l2;
       
       Test.stopTest();
        
    }*/
    public static testmethod void testFCCalloutCheckSuccess() {
        TAFS_Lead_Agreement_Conversion__c custSetting = new TAFS_Lead_Agreement_Conversion__c ();
        custSetting.TAFS_Agreement_Field__c ='TAFS_Account_Setup_Fee__c';
        custSetting.Name = 'TAFS_Account_Setup_Fee';
        insert custSetting;
        List<Lead> objLeadList = DataUtilTest_TAFS.createleads(2);
        System.debug('$$$$RA>>>>>'+objLeadList[0]);
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(objLeadList[0].id);
        lc.setDoNotCreateOpportunity(true);
        lc.setConvertedStatus('Qualified');     
        lc.setOwnerId(UserInfo.getUserId());
        Test.StartTest();
        Map<String,String> responseHeader = new Map<String,String>();
        responseHeader.put('Content-Type', 'application/json');
        String responseBody = '{"taskid":"12345","boardid":"30","title":"Test Title","description":"Test Description","type":"None","assignee":"Raushan","subtasks":"0","subtaskscomplete":"0","color":"#34a97b","priority":"Average","size":null,"deadline":null,"deadlineoriginalformat":null,"extlink":"","tags":"","leadtime":0,"blocked":"1","blockedreason":"Blocked - RS Input Required","columnname":"Invoice Backlog","lanename":"Standard EFS","subtaskdetails":[],"columnid":"requested_335","laneid":"343","position":"0","columnpath":"Invoice Backlog","loggedtime":0,"customfields":[{"fieldid":"19","name":"Batch Number","type":"number","value":"123456","mandatory":true},{"fieldid":"23","name":"Client Number","type":"number","value":"0","mandatory":true}],"comments":[{"author":"Raushan","event":"Comment added","text":"Unblocked 3","date":"2016-05-05 20:32:17","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:31:05","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:25:44","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Kanban Comment","date":"2016-05-05 20:19:21","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 2","date":"2016-05-05 20:14:29","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 1","date":"2016-05-05 20:14:25","taskid":"49136"}]}';
        Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(204,'OK',responseBody,responseHeader));
        Attachment attachObj =  new Attachment(Name='test', ParentID = objLeadList[0].id, Body = Blob.valueOf('Test'));
        Note noteObj = new Note(title='test', ParentID = objLeadList[0].id, Body = 'Test');
        insert attachObj;
        insert noteObj;
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        Test.StopTest();
        
        System.assert(lcr.isSuccess());
    }
    public static testmethod void testFCCalloutCheckFailure() {
        TAFS_Lead_Agreement_Conversion__c custSetting = new TAFS_Lead_Agreement_Conversion__c ();
        custSetting.TAFS_Agreement_Field__c ='TAFS_Account_Setup_Fee__c';
        custSetting.Name = 'TAFS_Account_Setup_Fee';
        insert custSetting;
        
        List<Lead> objLeadList = DataUtilTest_TAFS.createleads(1);
        System.debug('$$$$RA>>>>>'+objLeadList[0]);
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(objLeadList[0].id);
        lc.setDoNotCreateOpportunity(true);
        lc.setConvertedStatus('Qualified');     
        lc.setOwnerId(UserInfo.getUserId());
        Test.StartTest();
            Map<String,String> responseHeader = new Map<String,String>();
            responseHeader.put('Content-Type', 'application/json');
            String responseBody = '{"taskid":"12345","boardid":"30","title":"Test Title","description":"Test Description","type":"None","assignee":"Raushan","subtasks":"0","subtaskscomplete":"0","color":"#34a97b","priority":"Average","size":null,"deadline":null,"deadlineoriginalformat":null,"extlink":"","tags":"","leadtime":0,"blocked":"1","blockedreason":"Blocked - RS Input Required","columnname":"Invoice Backlog","lanename":"Standard EFS","subtaskdetails":[],"columnid":"requested_335","laneid":"343","position":"0","columnpath":"Invoice Backlog","loggedtime":0,"customfields":[{"fieldid":"19","name":"Batch Number","type":"number","value":"123456","mandatory":true},{"fieldid":"23","name":"Client Number","type":"number","value":"0","mandatory":true}],"comments":[{"author":"Raushan","event":"Comment added","text":"Unblocked 3","date":"2016-05-05 20:32:17","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:31:05","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Unblocked","date":"2016-05-05 20:25:44","taskid":"49136"},{"author":"Raushan","event":"Comment added","text":"Kanban Comment","date":"2016-05-05 20:19:21","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 2","date":"2016-05-05 20:14:29","taskid":"49136"},{"author":"Mike M","event":"Comment added","text":"Comment 1","date":"2016-05-05 20:14:25","taskid":"49136"}]}';
            Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(200,'OK',responseBody,responseHeader));
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            String hour = String.valueOf(Datetime.now().hour());
            String min = String.valueOf(Datetime.now().minute());
            String ss = String.valueOf(Datetime.now().second()+5);
            String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
            //Schedule_Reattempt_For_FC s = new Schedule_Reattempt_For_FC();
            //System.schedule('Job Started At ' + String.valueOf(Datetime.now()), nextFireTime, s);
        Test.StopTest();
        
        System.assert(lcr.isSuccess());
    }
    public static testmethod void testFCUPdateCheck() {
        DataUtilTest_TAFS.createInvalidPhoneNumber('2025211484');
        TAFS_Lead_Agreement_Conversion__c custSetting = new TAFS_Lead_Agreement_Conversion__c ();
        custSetting.TAFS_Agreement_Field__c ='TAFS_Account_Setup_Fee__c';
        custSetting.Name = 'TAFS_Account_Setup_Fee';
        insert custSetting;
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='TAFS_Integration_User']; 
        System.Debug(p.Id);
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='tafsIntegrationuser@testorg.com');
        System.runAs(u){
            List<Lead> objLeadList = DataUtilTest_TAFS.createleads(1);
            objLeadList[0].TAFS_City_Dup__c   ='Test';
            objLeadList[0].TAFS_Company_Dup__c    ='Test';
            objLeadList[0].DBA_Name_Dup__c    ='Test';
            objLeadList[0].TAFS_DOT_Number_Dup__c     ='Test';
            objLeadList[0].TAFS_Last_Name_Dup__c      ='Test';
            objLeadList[0].TAFS_MC_Number_Dup__c      ='Test';
            objLeadList[0].TAFS_Number_of_Active_Vehicles_Dup__c=2;
            objLeadList[0].TAFS_Phone_Dup__c      = '123455678';
            objLeadList[0].TAFS_Postal_Code_Dup__c    ='12345';
            objLeadList[0].TAFS_State_Dup__c      ='Test';
            objLeadList[0].TAFS_Street_Dup__c     ='Test' ;
            
            Attachment attachObj =  new Attachment(Name='test', ParentID = objLeadList[0].id, Body = Blob.valueOf('Test'));
            Note noteObj = new Note(title='test', ParentID = objLeadList[0].id, Body = 'Test');
            insert attachObj;
            insert noteObj;
            
            UPDATE objLeadList;
           
        }
        Test.StopTest();
    }
    
    public static testmethod void testLeadTriggerHandlerCheck() {
        DataUtilTest_TAFS.createInvalidPhoneNumber('2025211484');
        TAFS_Lead_Agreement_Conversion__c custSetting = new TAFS_Lead_Agreement_Conversion__c ();
        custSetting.TAFS_Agreement_Field__c ='TAFS_Account_Setup_Fee__c';
        custSetting.Name = 'TAFS_Account_Setup_Fee';
        insert custSetting;
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='TAFS_Integration_User']; 
        System.Debug(p.Id);
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='tafsIntegrationuser@testorg.com');
        System.runAs(u){
            List<Lead> objLeadList = DataUtilTest_TAFS.createleads(1);
            objLeadList[0].TAFS_City_Dup__c   ='Test';
            objLeadList[0].TAFS_Company_Dup__c    ='Test';
            objLeadList[0].DBA_Name_Dup__c    ='Test';
            objLeadList[0].TAFS_DOT_Number_Dup__c     ='Test';
            objLeadList[0].TAFS_Last_Name_Dup__c      ='Test';
            objLeadList[0].TAFS_MC_Number_Dup__c      ='Test';
            objLeadList[0].TAFS_Number_of_Active_Vehicles_Dup__c=2;
            objLeadList[0].TAFS_Phone_Dup__c      = '123455678';
            objLeadList[0].TAFS_Postal_Code_Dup__c    ='12345';
            objLeadList[0].TAFS_State_Dup__c      ='Test';
            objLeadList[0].TAFS_Street_Dup__c     ='Test' ;
            objLeadList[0].Fax    ='1234567890' ;
            
            Attachment attachObj =  new Attachment(Name='test', ParentID = objLeadList[0].id, Body = Blob.valueOf('Test'));
            Note noteObj = new Note(title='test', ParentID = objLeadList[0].id, Body = 'Test');
            insert attachObj;
            insert noteObj;
            
            UPDATE objLeadList;
            
            TAFS_LeadTriggerHandler test = new TAFS_LeadTriggerHandler();
            TAFS_LeadTriggerHandler.preventLeadAssignment(objLeadList );
            TAFS_LeadTriggerHandler.validateFCBeforeInsert(objLeadList);
        }
        Test.StopTest();
    }
    
    private static testmethod void testAttachmentTransfer() {
        
        list<Account> accList = DataUtilTest_TAFS.createAccountsForAgreements(1);
        list<lead> leadlist = DataUtilTest_TAFS.createleads(1);
        //leadlist[0].convertedaccountid = accList[0].Id;
        //update leadlist;
        list<Contact> conList =  DataUtilTest_TAFS.createContactsForLead(1,leadlist[0].id);
        conList[0].AccountId = accList[0].Id;
        update conList;
        Attachment attachObj =  new Attachment(Name='test', ParentID = leadlist[0].id, Body = Blob.valueOf('Test'));
        Note noteObj = new Note(title='test', ParentID = leadlist[0].id, Body = 'Test');
        insert attachObj;
        insert noteObj;
        Test.startTest();
            TAFS_LeadTriggerHandler.postLeadConversionOperations(leadlist);   
        Test.stopTest();
    }
}