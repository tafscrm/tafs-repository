/* CLass Name   : TAFS_ExpenseHandler
 * Description  : Handler class for Expense trigger
 * Created By   : Pankaj Singh
 * Created On   : 28-Oct-2015 
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                28-Oct-2015              Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_ExpenseHandler{
    
    /****************************************************************************************
    * Method Name      :  populateInvoiceBatchonExpense
    * Input Parameter  :  List<TAFS_Expense__c>
    * Return Type      :  Void
    * Description      :  This method populates the invoice batch id on teh expense record.
    ***************************************************************************************/
    public static void populateInvoiceBatchonExpense(List<TAFS_Expense__c> expenseList){
    
        map<String,Id> accountBatchMap = new map<String,Id>();
        set<String> uniqueKeySet = new set<String>();
        
        for(TAFS_Expense__c expense : expenseList){
			if(String.IsNotBlank(expense.TAFS_BatchNbr_ClientNbr__c))
				uniqueKeySet.add(expense.TAFS_BatchNbr_ClientNbr__c);      
        }
        for(TAFS_Invoice_Batch__c ibObj : [SELECT ID,Name,TAFS_External_Id__c FROM TAFS_Invoice_Batch__c WHERE TAFS_External_Id__c IN : uniqueKeySet]){
            accountBatchMap.put(ibObj.TAFS_External_Id__c,ibObj.Id);    
        }
        for(TAFS_Expense__c expense : expenseList){
            if(accountBatchMap.containsKey(expense.TAFS_BatchNbr_ClientNbr__c)){
                expense.TAFS_Invoice_Batch__c = accountBatchMap.get(expense.TAFS_BatchNbr_ClientNbr__c);
            }
        }
    }
}