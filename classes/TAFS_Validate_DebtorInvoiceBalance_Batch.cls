/****************************************************************************************
* Created By      :  Karthik Gulla
* Create Date     :  15/03/2017
* Description     :  Batch class used to create 'Debtor Concentration' Cases Whenever it
                     exceeds the business limits
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_Validate_DebtorInvoiceBalance_Batch implements Database.Batchable<sObject>, Database.Stateful {
    global Map<String,BalanceDetailsWrapper> mapClientDebtorBalances = new Map<String,BalanceDetailsWrapper>();
    global Map<String,BalanceDetailsWrapper> mapClientBalances = new Map<String,BalanceDetailsWrapper>();
    global Map<String,Set<String>> mapClientDebtors = new Map<String,Set<String>>();
    global Map<String,Case> mapExistCases = new Map<String,Case>();
    
    global final String query = 'SELECT Id, TAFS_Client_Name__c, TAFS_Client_Name__r.Name , Debtor__c, Debtor__r.Name, TAFS_Balance__c FROM TAFS_Invoice__c WHERE TAFS_Balance__c > 0';
    
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<Case>
    * Description      :  This method is used to find all Invoices where Balance is greater than zero
    **********************************************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<Case>
    * Return Type      :  Void
    * Description      :  This method is used to cases whenever balance on Invoices for a Client/Debtor exceeds the limits
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<TAFS_Invoice__c> scope) {
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Debtor Concentration').getRecordTypeId();
        for(TAFS_Invoice__c tafsInvoice:scope){
            BalanceDetailsWrapper clientBalWrapper;
            if(mapClientBalances.containsKey(tafsInvoice.TAFS_Client_Name__c)){
                clientBalWrapper = mapClientBalances.get(tafsInvoice.TAFS_Client_Name__c);
                clientBalWrapper.balance += tafsInvoice.TAFS_Balance__c;
            }
            else{
                clientBalWrapper = new BalanceDetailsWrapper();
                clientBalWrapper.objectId = tafsInvoice.TAFS_Client_Name__c;
                clientBalWrapper.name = tafsInvoice.TAFS_Client_Name__r.Name;
                clientBalWrapper.balance = tafsInvoice.TAFS_Balance__c; 
            }
            mapClientBalances.put(tafsInvoice.TAFS_Client_Name__c, clientBalWrapper);

            BalanceDetailsWrapper clientDebtorBalWrapper;
            if(mapClientDebtorBalances.containsKey(tafsInvoice.TAFS_Client_Name__c+'-'+tafsInvoice.Debtor__c)){
                clientDebtorBalWrapper = mapClientDebtorBalances.get(tafsInvoice.TAFS_Client_Name__c+'-'+tafsInvoice.Debtor__c);
                clientDebtorBalWrapper.balance += tafsInvoice.TAFS_Balance__c;
            }
            else{
                clientDebtorBalWrapper = new BalanceDetailsWrapper();
                clientDebtorBalWrapper.objectId = tafsInvoice.TAFS_Client_Name__c+'-'+tafsInvoice.Debtor__c;
                clientDebtorBalWrapper.name = tafsInvoice.Debtor__r.Name;
                clientDebtorBalWrapper.balance = tafsInvoice.TAFS_Balance__c;
            }
            mapClientDebtorBalances.put(tafsInvoice.TAFS_Client_Name__c+'-'+tafsInvoice.Debtor__c, clientDebtorBalWrapper);

            if(mapClientDebtors.containsKey(tafsInvoice.TAFS_Client_Name__c)){
                mapClientDebtors.get(tafsInvoice.TAFS_Client_Name__c).add(tafsInvoice.TAFS_Client_Name__c+'-'+tafsInvoice.Debtor__c);
            }
            else{
                Set<String> setDebtors = new Set<String>();
                setDebtors.add(tafsInvoice.TAFS_Client_Name__c+'-'+tafsInvoice.Debtor__c);
                mapClientDebtors.put(tafsInvoice.TAFS_Client_Name__c, setDebtors);
            }
        }

        for(Case c: [SELECT Id, CaseNumber, AccountId, TAFS_Debtor__c FROM Case WHERE RecordTypeId = :recTypeId AND AccountId <> NULL AND TAFS_Debtor__c <> NULL]){
            mapExistCases.put(c.AccountId+'-'+c.TAFS_Debtor__c, c);
        }
    }   
    
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post collecting the data required 
                            'Debtor Concentration' Case Creation
    **********************************************************************************************************************/  
    global void finish(Database.BatchableContext BC) {  
        List<Case> lstNewCases = new List<Case>();
        List<String> lstDescParameters = new List<String>();
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Debtor Concentration').getRecordTypeId();
        Id debtorConcentrationQueueId = [SELECT Id FROM Group WHERE Name = 'Debtor Concentration' and Type = 'Queue' LIMIT 1].Id;

        for(String clientId:mapClientDebtors.keySet()){
            if(mapClientBalances.containsKey(clientId)){
                Decimal totalClientBalance = mapClientBalances.get(clientId).balance;
                for(String clientDebtorId:mapClientDebtors.get(clientId)){
                    if(mapClientDebtorBalances.containsKey(clientDebtorId)){
                        Decimal debtorInvBalance = mapClientDebtorBalances.get(clientDebtorId).balance;
                        Decimal percentDebtorBal = (debtorInvBalance/totalClientBalance)*100;
                        if(percentDebtorBal > Integer.valueOf(Label.TAFS_DebtorConcentration_InvoiceBalance_Percent_Limit) 
                            && debtorInvBalance > Integer.valueOf(Label.TAFS_DebtorConcentration_InvoiceBalance_Limit)
                            && !mapExistCases.containsKey(clientDebtorId)){
                            //Create a case
                            Case newCase = new Case();
                            newCase.AccountId = clientId;
                            newCase.TAFS_Debtor__c = clientDebtorId.split('-')[1];
                            newCase.RecordTypeId = recTypeId;
                            newCase.Status = 'New';
                            newCase.Subject = 'Debtor Concentration - for Client: '+mapClientBalances.get(clientId).name+' ; Debtor: '+mapClientDebtorBalances.get(clientDebtorId).name;
                            lstDescParameters.add(mapClientBalances.get(clientId).name);
                            lstDescParameters.add(String.valueOf(mapClientDebtorBalances.get(clientDebtorId).balance));
                            lstDescParameters.add(mapClientDebtorBalances.get(clientDebtorId).name);
                            lstDescParameters.add(Label.TAFS_DebtorConcentration_InvoiceBalance_Percent_Limit);
                            lstDescParameters.add(String.valueOf(mapClientBalances.get(clientId).balance));
                            newCase.Description = String.format(Label.TAFS_DebtorConcentration_Case_Description, lstDescParameters);
                            lstDescParameters.clear();
                            newCase.OwnerId = debtorConcentrationQueueId;
                            lstNewCases.add(newCase);
                        }
                    }
                }
            }
        }

        if(lstNewCases.size() > 0)
            Database.insert(lstNewCases);
    }

    /**********************************************************************************************************************
    * Class Name       :  BalanceDetailsWrapper
    * Description      :  Wrapper class to maintain balance details
    **********************************************************************************************************************/ 
    global class BalanceDetailsWrapper{
        public String objectId;
        public String name;
        public Decimal balance;
    }
}