/*************************************************************************************/
/* Class Name   : ReusableMockResponse_Test
 * Description  : This is used in all the test classes that cover the webservice callouts.
                   It sets a mock response, so that the test method do not fail.
 * Created By   : Raushan Anand
 * Created On   : 6-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                6-May-2015             Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_ReusableMockResponse_Test implements HttpCalloutMock{

	protected Integer code;
    protected String status;
    protected String bodyAsString;
    protected Blob bodyAsBlob;
    protected Map < String, String > responseHeaders;
    
    //Constructor for Response Genaration
    public TAFS_ReusableMockResponse_Test(Integer code, String status, String body, Map < String, String > responseHeaders) {

        this.code = code;
        this.status = status;
        this.bodyAsString = body;
        this.bodyAsBlob = null;
        this.responseHeaders = responseHeaders;

    }
    // This is an overriden global interface method, that has to be defined when HttpCalloutMock Interface is implemented.
    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse response = new HttpResponse();
        response.setStatusCode(code);
        response.setStatus(status);
        if (bodyAsBlob != null) {
            response.setBodyAsBlob(bodyAsBlob);
        } else {
            response.setBody(bodyAsString);
        }

        if (responseHeaders != null) {
            for (String key: responseHeaders.keySet()) {
                response.setHeader(key, responseHeaders.get(key));
            }
        }
        system.debug('***Response***'+ response);
        system.debug('Response Body'+ response.getBody());
        return response;

    }
}