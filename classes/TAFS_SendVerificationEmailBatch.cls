/* Class Name   : TAFS_SendVerificationEmailBatch
 * Description  : Class to log error in Error Log object
 * Created By   : Raushan Anand
 * Created On   : 08-Aug-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                08-Aug-2015             Initial version.
 *
 *****************************************************************************************/
global class TAFS_SendVerificationEmailBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<Contact>
    * Description      :  This method is used to find all the debtor contacts to whom the emails need to be sent.
    **********************************************************************************************************************/
    global List<Contact> start(Database.BatchableContext BC) {
        Set<Id> invoiceIdSet = new Set<Id>();
        Set<Id> debtorIdSet = new Set<Id>();
        List<Contact> contactlist = new List<Contact>();
        map<Id,String> debtorEmailMap =  new map<Id,String>();
        list<TAFS_Debtor_Collection_Verification__c> dcList =  new list<TAFS_Debtor_Collection_Verification__c>();
        string queryStr = 'Select Id,Name,TAFS_Client_Name__c,Debtor__c,TAFS_Balance__c,TAFS_PO_No__c,TAFS_Client_Name__r.Name,TAFS_Invoice_Amount__c,(SELECT ID FROM Verificatons_Collections__r) '+
                          'FROM TAFS_Invoice__c WHERE (TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO :' +Label.TAFS_VerificationEmailLabel_10+') '+
                          'AND TAFS_Invoice_Batch__r.TAFS_Status__c = \'Processed\' AND (Debtor__r.TAFS_No_Verification_Email__c = false) AND (Debtor__r.TAFS_Gallium_Paying_Agent__c=null OR '+
                          'Debtor__r.TAFS_Gallium_Paying_Agent__c=\'NULL\' OR Debtor__r.TAFS_Gallium_Paying_Agent__c=\'\') AND ((TAFS_Debtor_first_association_withClient__c = TRUE AND TAFS_Balance__c > 0) OR (TAFS_Balance__c > 2000) OR '+
                          '(TAFS_Client_Name__r.TAFS_First_Funding_Date__c > N_DAYS_AGO:'+Label.TAFS_VerificationEmailLabel_90+' AND TAFS_Balance__c > 0))';
                                     
        List<TAFS_Invoice__c> invoiceList = Database.query(queryStr);
        
        for(TAFS_Invoice__c invObj : invoiceList){
            if(invObj.Debtor__c!=null){
                invoiceIdSet.add(invObj.Id);
            }
        }
        string queryStrVer = 'SELECT Id,TAFS_Invoice__c,TAFS_Invoice__r.Debtor__c FROM TAFS_Verificatons_Collections__c Where TAFS_Response__c IN (\'Post Verification Required\')'+
                             ' AND (TAFS_Invoice__r.TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO : '+Label.TAFS_VerificationEmailLabel_10+') AND '+
                             '(TAFS_Invoice__r.TAFS_Invoice_Batch__r.TAFS_Status__c = \'Processed\') AND (TAFS_Invoice__r.TAFS_Balance__c > 0) AND (TAFS_Invoice__r.Debtor__r.TAFS_No_Verification_Email__c = false)'+
                             ' AND (TAFS_Invoice__r.Debtor__r.TAFS_Gallium_Paying_Agent__c=null OR TAFS_Invoice__r.Debtor__r.TAFS_Gallium_Paying_Agent__c=\'NULL\' OR TAFS_Invoice__r.Debtor__r.TAFS_Gallium_Paying_Agent__c=\'\')';
                                                       
        for(TAFS_Verificatons_Collections__c verObj : Database.query(queryStrVer)){
            if(verObj.TAFS_Invoice__r.Debtor__c!=null){
                if(!invoiceIdSet.contains(verObj.TAFS_Invoice__c)){
                    invoiceIdSet.add(verObj.TAFS_Invoice__c);
                }
            }
        }
        for(TAFS_Invoice__c invObj : [SELECT Id, Debtor__c FROM TAFS_Invoice__c where Id IN : invoiceIdSet]){
            if(String.IsNotBlank(invObj.Debtor__c)){
                debtorIdSet.add(invObj.Debtor__c);
            }
        }
        if(!debtorIdSet.isEmpty()){
            for(Contact con : [SELECT ID,AccountId,Account.Name,Email FROM Contact WHERE AccountId IN:debtorIdSet AND LastName = 'AP Post Verification-SFDC' ]){
                contactlist.add(con);
                debtorEmailMap.put(con.AccountId,con.Email);                    
            }
        }
        for(TAFS_Invoice__c invObj : [SELECT ID,Name,TAFS_PO_No__c,TAFS_Balance__c,TAFS_Client_Name__c,Debtor__c FROM TAFS_Invoice__c WHERE ID IN:invoiceIdSet]){
            TAFS_Debtor_Collection_Verification__c dcObj = new TAFS_Debtor_Collection_Verification__c();
            dcObj.TAFS_Client_Name__c     = invObj.TAFS_Client_Name__c;
            dcObj.TAFS_Debtor__c          = invObj.Debtor__c;
            dcObj.TAFS_Debtor_Email__c    = debtorEmailMap.get(invObj.Debtor__c);
            dcObj.TAFS_Invoice__c         = invObj.Id;
            dcObj.TAFS_Invoice_No__c      = invObj.Name;
            dcObj.TAFS_Invoice_Balance__c = invObj.TAFS_Balance__c;
            dcObj.TAFS_PO_No__c           = invObj.TAFS_PO_No__c;
            dcObj.TAFS_Type__c            = 'Verification';
            dcList.add(dcObj);   
        }
        if(!dcList.isEmpty()){
            Database.insert(dcList);
        }        
        return contactlist;
    }
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<Contact>
    * Return Type      :  Void
    * Description      :  This method is used to send email to debtor contacts for invoices which is more than 46 days old
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<Contact> scope) {
    
        if(scope.size()>0){
            Set<Id> debtorIdSet = new Set<Id>();
            map<Id,List<TAFS_Debtor_Collection_Verification__c>> debtorMap = new map<Id,List<TAFS_Debtor_Collection_Verification__c>>();
            map<Id,String> debtorPoMap = new map<Id,String>();
            list<TAFS_Debtor_Collection_Verification__c> dcList = new list<TAFS_Debtor_Collection_Verification__c>();
            for(Contact con : scope){
                debtorIdSet.add(con.AccountId);
            }
            for(TAFS_Debtor_Collection_Verification__c dcObj : [SELECT ID,TAFS_Created_In_kanbanize__c,TAFS_Debtor__c,TAFS_PO_No__c FROM TAFS_Debtor_Collection_Verification__c WHERE TAFS_Debtor__c IN : debtorIdSet AND CreatedDate = TODAY AND TAFS_Type__c = 'Verification']){
                if(debtorPoMap.containskey(dcObj.TAFS_Debtor__c) && dcObj.TAFS_PO_No__c!=null){
                     String tempStr = debtorPoMap.get(dcObj.TAFS_Debtor__c) + dcObj.TAFS_PO_No__c + ';';
                     debtorPoMap.put(dcObj.TAFS_Debtor__c,tempStr);
                }
                else if(dcObj.TAFS_PO_No__c!=null || dcObj.TAFS_PO_No__c!=''){
                     String tempStr = dcObj.TAFS_PO_No__c + ';';
                     debtorPoMap.put(dcObj.TAFS_Debtor__c,tempStr);
                }        
                if(debtorMap.containskey(dcObj.TAFS_Debtor__c)){
                     List<TAFS_Debtor_Collection_Verification__c> lstTemp = debtorMap.get(dcObj.TAFS_Debtor__c);
                     lstTemp.add(dcObj);
                     debtorMap.put(dcObj.TAFS_Debtor__c,lstTemp);
                }
                else{
                     List<TAFS_Debtor_Collection_Verification__c> lstTemp = new List<TAFS_Debtor_Collection_Verification__c>();
                     lstTemp.add(dcObj);
                     debtorMap.put(dcObj.TAFS_Debtor__c,lstTemp);
                }         
            }
            List<Messaging.SingleEmailMessage> messageList = new List<Messaging.SingleEmailMessage>();
            try{
                //getting the email template Id
                EmailTemplate template = [SELECT ID,Name, Body, subject FROM EmailTemplate WHERE DeveloperName = 'TAFS_Post_Verification_Email_Template'];
                //getting org wide email address id
                OrgWideEmailAddress emailAddress = [SELECT ID FROM OrgWideEmailAddress WHERE DisplayName = 'Debtor Post Verification Email' LIMIT 1];
                for(Contact conObj : scope){
                
                    Http http = new Http();
                    HttpRequest request = new HttpRequest();
                    HttpResponse response =  new HttpResponse();
                    string poString = '';
                    if(debtorPoMap.containsKey(conObj.AccountId)){
                        poString = debtorPoMap.get(conObj.AccountId);
                    }
                    if(poString!=null){
                        poString.removeEnd(';');
                    }
                    else{
                        poString = '';
                    }
                    String endpoint  = Label.TAFS_Kanban_Create_New_Task_URL +'boardid/'+Label.TAFS_Verification_Kanban_Board_Id+'/title/'+EncodingUtil.urlEncode(conObj.Account.Name,'UTF-8').replace('+','%20')+'/description/'+ EncodingUtil.urlEncode(poString,'UTF-8').replace('+','%20')+'/priority/Average/column/Queue/lane/Standard';
                    request.setMethod('POST');
                    request.setHeader('Accept','application/json');
                    request.setBody('application/json'); 
                    request.setEndpoint(endpoint);
                    request.setHeader('APIKey',Label.TAFS_Kanban_API_Key);       
                    response = (new Http()).send(request);                   
                    if(response.getStatusCode() == 200){
                        if(debtorMap.containsKey(conObj.AccountId)){
                            for(TAFS_Debtor_Collection_Verification__c dcObj : debtorMap.get(conObj.AccountId)){
                                dcObj.TAFS_Created_In_kanbanize__c = true;
                                dcList.add(dcObj);
                            }
                        }
                    }                   
                    //setting all the email parameters
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(conObj.Id);
                    mail.setWhatid(conObj.AccountId);
                    mail.setSaveAsActivity(true);
                    mail.setTemplateId(template.Id);
                    mail.setOrgWideEmailAddressId(emailAddress.Id);
                    messageList.add(mail);                        
                }
                //sending the emails and storing the result
                Messaging.SendEmailResult [] resultSet = Messaging.sendEmail(messageList); 
                if(!dcList.isEmpty()){
                    Database.update(dcList);
                }
            }catch(Exception excep){
                //Do Nothing
            }          
        }               
    }
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post the emails are sent.
    **********************************************************************************************************************/    
    global void finish(Database.BatchableContext BC) {
    
    }
}