/* CLass Name   : TAFS_BatchLeadOwnerRemovalTest
 * Description  : Test class for TAFS_BatchLeadOwnerRemoval 
 * Created By   : Arpitha Sudhakar
 * Created On   : 20-June-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar                20-June-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_BatchLeadOwnerRemovalTest {

    static testmethod void test() {
       DataUtilTest_TAFS.createleads(1);
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadList = [select id,name from Lead];
       
       Test.startTest();
       TAFS_BatchLeadOwnerRemoval c = new TAFS_BatchLeadOwnerRemoval();
       Database.executeBatch(c);
       Test.stopTest();
    }
    
    static testmethod void testpositive() {
       DataUtilTest_TAFS.createleads(1);
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       DataUtilTest_TAFS.createQueueForLeadRemoval();       
       
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        
        Group testGroup = new Group(Name = Label.TAFS_Direct_Sales, Type = 'Queue');
        insert testGroup;

        QueueSobject testQueue = new QueueSObject(QueueId = testGroup.Id, SobjectType = 'Lead');
        System.runAs(new User(Id = UserInfo.getUserId())) {   
            insert testQueue;
        }

        Lead l = new Lead(OwnerId = UserInfo.getUserId(), LastName = 'testL1AA', Company = 'test');
        insert l;
        //
        Group testGroup2 = new Group(Name = Label.TAFS_National_Accounts, Type = 'Queue');
        insert testGroup2;

        QueueSobject testQueue2 = new QueueSObject(QueueId = testGroup2.Id, SobjectType = 'Lead');
        System.runAs(new User(Id = UserInfo.getUserId())) {   
            insert testQueue2;
        }

        Lead l2 = new Lead(OwnerId = UserInfo.getUserId(), LastName = 'testL2AA', Company = 'test');
        insert l2;
        
        List<Lead> leadList = [select id,name,TAFS_Lead_Type__c  from Lead where LastName IN('testL1AA','testL2AA')];
        TAFS_BatchLeadOwnerRemoval AU = new TAFS_BatchLeadOwnerRemoval();
        
         
        try{
            AU.execute(BC, leadList);
        }catch(Exception ex){}
        AU.finish(BC);
               
    }
    
    static testmethod void testpositiveNA() {
       DataUtilTest_TAFS.createleadsNA(1);
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       DataUtilTest_TAFS.createQueueForLeadRemoval();       
       
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        Group testGroup2 = new Group(Name = Label.TAFS_National_Accounts, Type = 'Queue');
        insert testGroup2;

        QueueSobject testQueue2 = new QueueSObject(QueueId = testGroup2.Id, SobjectType = 'Lead');
        System.runAs(new User(Id = UserInfo.getUserId())) {   
            insert testQueue2;
        }

        Lead l2 = new Lead(OwnerId = UserInfo.getUserId(), LastName = 'testL2AA', Company = 'test', TAFS_Lead_Type__c   = Label.TAFS_National_Accounts);
        insert l2;
        
        List<Lead> leadList = [select id,name,TAFS_Lead_Type__c  from Lead where LastName IN('testL1AA','testL2AA')];
        TAFS_BatchLeadOwnerRemoval AU = new TAFS_BatchLeadOwnerRemoval();
        try{
            AU.execute(BC, leadList);
        }catch(Exception ex){}
        AU.finish(BC);
               
    }
}