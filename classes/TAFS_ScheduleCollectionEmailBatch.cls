/* CLass Name   : TAFS_ScheduleCollectionEmailBatch
 * Description  : Send verification email to Debtor Contacts
 * Created By   : Pankaj Singh
 * Created On   : 10-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                10-Aug-2016              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleCollectionEmailBatch implements Schedulable {
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_SendCollectionEmailBatch(),1);
   }
}