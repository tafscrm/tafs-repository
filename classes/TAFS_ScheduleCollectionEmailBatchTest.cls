/* CLass Name   : TAFS_ScheduleCollectionEmailBatchTest
 * Description  : Test class for TAFS_ScheduleCollectionEmailBatch
 * Created By   : Arpitha Sudhakar
 * Created On   : 24-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar           24-Aug-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
    public class TAFS_ScheduleCollectionEmailBatchTest {

    static testmethod void test() {
       Test.startTest();
       TAFS_ScheduleCollectionEmailBatch c = new TAFS_ScheduleCollectionEmailBatch();
       String scheduleTime = '0 0 23 * * ?';
       system.schedule('Test schedule', scheduleTime, c);
       Test.stopTest();
    }
}