/****************************************************************************************
* Created By      :  Manoj Vootla
* Create Date     :  10/06/2016
* Description     :  Scheduler class for TAFS_BatchUpdateLeadStatus 
* Modification Log:  Initial version.
***************************************************************************************/

global class TAFS_SchedulerForUpdateLeadStatus implements Schedulable{

     global void execute(SchedulableContext sc) {
        TAFS_BatchUpdateLeadStatus b = new TAFS_BatchUpdateLeadStatus(); 
        database.executebatch(b);
     }
       
  }
/*
public static String CRON_7AM = '0 0 7 * * ?';
public static String CRON_12PM = '0 0 12 * * ?';
public static String CRON_6PM = '0 0 18 * * ?';
System.schedule('UpdateLeadStatusBatch 7AM', CRON_7AM, new TAFS_SchedulerForUpdateLeadStatus());
System.schedule('UpdateLeadStatusBatch 12PM', CRON_12PM, new TAFS_SchedulerForUpdateLeadStatus());
System.schedule('UpdateLeadStatusBatch 6PM', CRON_6PM, new TAFS_SchedulerForUpdateLeadStatus());
*/