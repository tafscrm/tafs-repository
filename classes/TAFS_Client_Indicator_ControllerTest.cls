/* Class Name   : TAFS_Client_Indicator_ControllerTest
 * Description  : Test Class to cover Client Indicator Controller and Service Class
 * Created By   : Raushan Anand
 * Created On   : 29-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                29-Aug-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_Client_Indicator_ControllerTest {

    @testSetup 
	static void setup() {
		list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
		list<Account> lstAcct = DataUtilTest_TAFS.createClients(1,clientTermList[0].Id);
		    
    }  
    static testMethod void testControllerAction() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Account acc = new Account();
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFactoredVolumeNegative() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Account acc = new Account();
        List<TAFS_Invoice_Batch__c> batchList = new List<TAFS_Invoice_Batch__c>();
        TAFS_Invoice_Batch__c batchObj1 = new TAFS_Invoice_Batch__c(TAFS_Client_ID__c					= accList[0].Id,
                                                                   TAFS_Client_Number__c                = '12345',
                                                                   Name							        = 'Batch12',
                                                                   TAFS_Type__c							='0',
                                                                   TAFS_Status__c						='0',
                                                                   TAFS_A_R_Amount__c                   = 900,
                                                                   TAFS_Posted_Date__c                  = dateTime.now().addDays(-20));
        TAFS_Invoice_Batch__c batchObj2 = new TAFS_Invoice_Batch__c(TAFS_Client_ID__c					= accList[0].Id,
                                                                   TAFS_Client_Number__c                = '12345',
                                                                   Name							        = 'Batch12',
                                                                   TAFS_Type__c							='0',
                                                                   TAFS_Status__c						='0',
                                                                   TAFS_A_R_Amount__c                   = 100,
                                                                   TAFS_Posted_Date__c                  = dateTime.now().addDays(-2));
        batchList.add(batchObj1);
        batchList.add(batchObj2);
        INSERT batchList;
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFactoredVolumePositive() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Account acc = new Account();
        List<TAFS_Invoice_Batch__c> batchList = new List<TAFS_Invoice_Batch__c>();
        TAFS_Invoice_Batch__c batchObj1 = new TAFS_Invoice_Batch__c(TAFS_Client_ID__c					= accList[0].Id,
                                                                   TAFS_Client_Number__c                = '12345',
                                                                   Name							        = 'Batch12',
                                                                   TAFS_Type__c							='0',
                                                                   TAFS_Status__c						='0',
                                                                   TAFS_A_R_Amount__c                   = 2000,
                                                                   TAFS_Posted_Date__c                  = dateTime.now().addDays(-20));
        TAFS_Invoice_Batch__c batchObj2 = new TAFS_Invoice_Batch__c(TAFS_Client_ID__c					= accList[0].Id,
                                                                   TAFS_Client_Number__c                = '12345',
                                                                   Name							        = 'Batch12',
                                                                   TAFS_Type__c							='0',
                                                                   TAFS_Status__c						='0',
                                                                   TAFS_A_R_Amount__c                   = 1000,
                                                                   TAFS_Posted_Date__c                  = dateTime.now().addDays(-2));
        batchList.add(batchObj1);
        batchList.add(batchObj2);
        INSERT batchList;
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFactoredVolumeNeutral() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Account acc = new Account();
        List<TAFS_Invoice_Batch__c> batchList = new List<TAFS_Invoice_Batch__c>();
        TAFS_Invoice_Batch__c batchObj1 = new TAFS_Invoice_Batch__c(TAFS_Client_ID__c					= accList[0].Id,
                                                                   TAFS_Client_Number__c                = '12345',
                                                                   Name							        = 'Batch12',
                                                                   TAFS_Type__c							='0',
                                                                   TAFS_Status__c						='0',
                                                                   TAFS_A_R_Amount__c                   = 400,
                                                                   TAFS_Posted_Date__c                  = dateTime.now().addDays(-20));
        TAFS_Invoice_Batch__c batchObj2 = new TAFS_Invoice_Batch__c(TAFS_Client_ID__c					= accList[0].Id,
                                                                   TAFS_Client_Number__c                = '12345',
                                                                   Name							        = 'Batch12',
                                                                   TAFS_Type__c							='0',
                                                                   TAFS_Status__c						='0',
                                                                   TAFS_A_R_Amount__c                   = 100,
                                                                   TAFS_Posted_Date__c                  = dateTime.now().addDays(-2));
        batchList.add(batchObj1);
        batchList.add(batchObj2);
        INSERT batchList;
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFactoredVolumeWarning() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Account acc = new Account();
        List<TAFS_Invoice_Batch__c> batchList = new List<TAFS_Invoice_Batch__c>();
        TAFS_Invoice_Batch__c batchObj1 = new TAFS_Invoice_Batch__c(TAFS_Client_ID__c					= accList[0].Id,
                                                                   TAFS_Client_Number__c                = '12345',
                                                                   Name							        = 'Batch12',
                                                                   TAFS_Type__c							='0',
                                                                   TAFS_Status__c						='0',
                                                                   TAFS_A_R_Amount__c                   = 713,
                                                                   TAFS_Posted_Date__c                  = dateTime.now().addDays(-20));
        TAFS_Invoice_Batch__c batchObj2 = new TAFS_Invoice_Batch__c(TAFS_Client_ID__c					= accList[0].Id,
                                                                   TAFS_Client_Number__c                = '12345',
                                                                   Name							        = 'Batch12',
                                                                   TAFS_Type__c							='0',
                                                                   TAFS_Status__c						='0',
                                                                   TAFS_A_R_Amount__c                   = 100,
                                                                   TAFS_Posted_Date__c                  = dateTime.now().addDays(-2));
        batchList.add(batchObj1);
        batchList.add(batchObj2);
        INSERT batchList;
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFuelConsumptionNegative() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Account acc = new Account();
        List<TAFS_Fuel_Consumptions__c> fuelCList = new List<TAFS_Fuel_Consumptions__c>();
        TAFS_Fuel_Consumptions__c fuelCObj1 = new TAFS_Fuel_Consumptions__c(TAFS_Client_ID__c          = accList[0].Id,
                                                                  TAFS_Client_Number__c                = '12345',
                                                                  TAFS_In_Network_Fuel_Stop__c         = true,
                                                                  TAFS_Transaction_Date__c             = Date.Today().addDays(-20),
                                                                  TAFS_Number_of_Reefer_Gallons__c     = 450,
                                                                  TAFS_Number_of_Tractor_Gallons__c    = 450
                                                                  );
        TAFS_Fuel_Consumptions__c fuelCObj2 = new TAFS_Fuel_Consumptions__c(TAFS_Client_ID__c          = accList[0].Id,
                                                                  TAFS_Client_Number__c                = '12345',
                                                                  TAFS_In_Network_Fuel_Stop__c         = true,
                                                                  TAFS_Transaction_Date__c             = Date.Today().addDays(-2),
                                                                  TAFS_Number_of_Reefer_Gallons__c     = 50,
                                                                  TAFS_Number_of_Tractor_Gallons__c    = 50
                                                                  );
        fuelCList.add(fuelCObj1);
        fuelCList.add(fuelCObj2);
        INSERT fuelCList;
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFuelConsumptionPositive() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Account acc = new Account();
        List<TAFS_Fuel_Consumptions__c> fuelCList = new List<TAFS_Fuel_Consumptions__c>();
        TAFS_Fuel_Consumptions__c fuelCObj1 = new TAFS_Fuel_Consumptions__c(TAFS_Client_ID__c          = accList[0].Id,
                                                                  TAFS_Client_Number__c                = '12345',
                                                                  TAFS_In_Network_Fuel_Stop__c         = true,
                                                                  TAFS_Transaction_Date__c             = Date.Today().addDays(-20),
                                                                  TAFS_Number_of_Reefer_Gallons__c     = 1000,
                                                                  TAFS_Number_of_Tractor_Gallons__c    = 1000
                                                                  );
        TAFS_Fuel_Consumptions__c fuelCObj2 = new TAFS_Fuel_Consumptions__c(TAFS_Client_ID__c          = accList[0].Id,
                                                                  TAFS_Client_Number__c                = '12345',
                                                                  TAFS_In_Network_Fuel_Stop__c         = true,
                                                                  TAFS_Transaction_Date__c             = Date.Today().addDays(-2),
                                                                  TAFS_Number_of_Reefer_Gallons__c     = 500,
                                                                  TAFS_Number_of_Tractor_Gallons__c    = 500
                                                                  );
        fuelCList.add(fuelCObj1);
        fuelCList.add(fuelCObj2);
        INSERT fuelCList;
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFuelConsumptionNeutral() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Account acc = new Account();
        List<TAFS_Fuel_Consumptions__c> fuelCList = new List<TAFS_Fuel_Consumptions__c>();
        TAFS_Fuel_Consumptions__c fuelCObj1 = new TAFS_Fuel_Consumptions__c(TAFS_Client_ID__c          = accList[0].Id,
                                                                  TAFS_Client_Number__c                = '12345',
                                                                  TAFS_In_Network_Fuel_Stop__c         = true,
                                                                  TAFS_Transaction_Date__c             = Date.Today().addDays(-20),
                                                                  TAFS_Number_of_Reefer_Gallons__c     = 200,
                                                                  TAFS_Number_of_Tractor_Gallons__c    = 200
                                                                  );
        TAFS_Fuel_Consumptions__c fuelCObj2 = new TAFS_Fuel_Consumptions__c(TAFS_Client_ID__c          = accList[0].Id,
                                                                  TAFS_Client_Number__c                = '12345',
                                                                  TAFS_In_Network_Fuel_Stop__c         = true,
                                                                  TAFS_Transaction_Date__c             = Date.Today().addDays(-2),
                                                                  TAFS_Number_of_Reefer_Gallons__c     = 50,
                                                                  TAFS_Number_of_Tractor_Gallons__c    = 50
                                                                  );
        fuelCList.add(fuelCObj1);
        fuelCList.add(fuelCObj2);
        INSERT fuelCList;
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFuelConsumptionWarning() {
    	List<Account> accList = [Select Id,TAFS_Difference_Usage_vs_Committed__c,TAFS_Prorated_Committment_MTD__c from Account LIMIT 1];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Account acc = new Account();
        List<TAFS_Fuel_Consumptions__c> fuelCList = new List<TAFS_Fuel_Consumptions__c>();
        TAFS_Fuel_Consumptions__c fuelCObj1 = new TAFS_Fuel_Consumptions__c(TAFS_Client_ID__c          = accList[0].Id,
                                                                  TAFS_Client_Number__c                = '12345',
                                                                  TAFS_In_Network_Fuel_Stop__c         = true,
                                                                  TAFS_Transaction_Date__c             = Date.Today().addDays(-20),
                                                                  TAFS_Number_of_Reefer_Gallons__c     = 400,
                                                                  TAFS_Number_of_Tractor_Gallons__c    = 313
                                                                  );
        TAFS_Fuel_Consumptions__c fuelCObj2 = new TAFS_Fuel_Consumptions__c(TAFS_Client_ID__c          = accList[0].Id,
                                                                  TAFS_Client_Number__c                = '12345',
                                                                  TAFS_In_Network_Fuel_Stop__c         = true,
                                                                  TAFS_Transaction_Date__c             = Date.Today().addDays(-2),
                                                                  TAFS_Number_of_Reefer_Gallons__c     = 50,
                                                                  TAFS_Number_of_Tractor_Gallons__c    = 50
                                                                  );
        fuelCList.add(fuelCObj1);
        fuelCList.add(fuelCObj2);
        INSERT fuelCList;
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFuelCommitmentPositive() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        accList[0].TAFS_Difference_Usage_vs_Committed__c = 50;
        accList[0].TAFS_Prorated_Committment_MTD__c = 100;
        UPDATE accList[0];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFuelCommitmentNegative() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        accList[0].TAFS_Difference_Usage_vs_Committed__c = -50;
        accList[0].TAFS_Prorated_Committment_MTD__c = 100;
        UPDATE accList[0];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFuelCommitmentNeutral() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        accList[0].TAFS_Difference_Usage_vs_Committed__c = 10;
        accList[0].TAFS_Prorated_Committment_MTD__c = 100;
        UPDATE accList[0];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
    static testMethod void testFuelCommitmentWarning() {
    	List<Account> accList = [Select Id from Account LIMIT 1];
        accList[0].TAFS_Difference_Usage_vs_Committed__c = -10;
        accList[0].TAFS_Prorated_Committment_MTD__c = 100;
        UPDATE accList[0];
        PageReference tpageRef = Page.TAFS_Client_Indicator;
  		Test.setCurrentPage(tpageRef);
  		ApexPages.currentPage().getParameters().put('Id', accList[0].Id);
        Apexpages.StandardController stdController = new Apexpages.StandardController(accList[0]);
        TAFS_Client_Indicator_Controller controller = new TAFS_Client_Indicator_Controller(stdController);
        controller.populateIndicator();
    }
}