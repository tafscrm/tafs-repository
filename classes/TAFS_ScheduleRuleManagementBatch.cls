/* CLass Name   : TAFS_ScheduleRuleManagementBatch
 * Description  : Update Task Priority daily
 * Created By   : Pankaj Singh
 * Created On   : 09-Dec-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                09-Dec-2016             Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleRuleManagementBatch implements Schedulable {
    global void execute(SchedulableContext SC) {      
        Id jobId = Database.executeBatch(new TAFS_RiskManagementRuleBatch(),100);
        TAFS_Task_Batch_Execution__c cs = TAFS_Task_Batch_Execution__c.getInstance('Rule Management Batch Time');
        if(jobId != null){
            cs.Job_Id__c = jobId;
            UPDATE cs;
        }
   }
}