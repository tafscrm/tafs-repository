/* Class Name   : TAFS_Kanban_Integration_Service
 * Description  : This class handles the integration Callouts and response from KANBAN.
 * Created By   : Raushan Anand
 * Created On   : 2-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                2-May-2015              Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_Kanban_Integration_Service {
	
	 /* Method Name : getTaskDetails
	 * Description : Method performs a callout to Kanban to get Card Details and create a corresponding task
	 *				 in SFDC
	 * Return Type : void
	 * Input Parameter : Map<String,String>
	 */
	public static void getTaskDetails(Map<String,String> kanbanCardMap){
		
		String taskId;
		String assignee;
		if(!kanbanCardMap.isEmpty()){
			if(kanbanCardMap.containsKey('TaskId'))
				taskId = kanbanCardMap.get('TaskId');
			if(kanbanCardMap.containsKey('Assignee'))
				assignee = kanbanCardMap.get('Assignee');
		}
		HttpRequest req = new HttpRequest();
		HttpResponse res = new HttpResponse();
		Http http = new Http();
		req.setEndpoint(Label.TAFS_Kanban_URL+'boardid/'+Label.TAFS_Kanban_Board_Id+'/taskid/'+taskId+'/comments/yes');
		req.setMethod('POST');
		req.setBody('');
		req.setHeader('APIKey', Label.TAFS_Kanban_API_Key);
		req.setHeader('Accept', 'application/json');
		req.setCompressed(true); // otherwise we hit a limit of 32000
		
		try {
			res = http.send(req);
			if(res.getStatusCode() == 200){
		    	TAFS_Kanban_Integration_Handler.responseParser(res);
			}
			else{
				TAFS_Error_Logger.insertErrorLog(res,req);
			}
		} catch(System.CalloutException e) {
			System.debug('Callout error: '+ e);
		
		}
	}
}