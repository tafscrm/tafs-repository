@isTest
private class TAFS_FuelDiscountDomainTest{

    /************************************************************************************
    * Method       :    testRSClientNumberUpdate
    * Description  :    Tests whether client id is populated properly on Roadside subscription or not
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testRCClientNumberUpdate() {
       
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
       
       List<Fuel_Discount__c> fuelDiscountList = DataUtilTest_TAFS.createFuelDiscounts(2,lstAcct[0].Id);
       
       Test.startTest();
           insert fuelDiscountList;
       Test.stopTest();
       
       system.assertEquals(2,fuelDiscountList.size());
    }
}