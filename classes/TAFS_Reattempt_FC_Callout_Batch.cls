/* Class Name   : TAFS_Reattempt_FC_Callout_Batch
 * Description  : Class handles Lead conversion check to Freight Connect
 * Created By   : Raushan Anand
 * Created On   : 15-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                15-May-2015              Initial version.
 *
 *****************************************************************************************/
global class TAFS_Reattempt_FC_Callout_Batch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    
    global List<TAFS_Error_Log__c> start(Database.BatchableContext BC){
        system.debug('printing data>>>>>>>');
        List<TAFS_Error_Log__c> log = [SELECT Id,TAFS_Request__c,TAFS_Status_Code__c,TAFS_Endpoint__c FROM TAFS_Error_Log__c WHERE TAFS_Status_Code__c !='204' AND TAFS_Type__c ='Freight Connect' AND CreatedDate>=: Date.Today()];
    	system.debug('printing data>>>>>>>'+log);
        return log;
    }
   
	global void execute(Database.BatchableContext BC, List<TAFS_Error_Log__c> scope){
        for(TAFS_Error_Log__c errLog : scope){
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setEndpoint(errLog.TAFS_Endpoint__c);
            req.setMethod('POST');
            req.setBody('');
            req.setHeader('Accept', 'application/json');
            req.setCompressed(true); // otherwise we hit a limit of 32000
            try {
                res = http.send(req);
                System.Debug('###RA'+res.getBody());
                System.Debug('###RACode'+res.getStatusCode());
                errLog.TAFS_Status_Code__c = String.valueOf(res.getStatusCode());
            }
            catch(System.CalloutException e) {
                System.debug('Callout error: '+ e);    
            }  
        }
        Database.update(scope);
    }   
    
    global void finish(Database.BatchableContext BC){
        
    }
}