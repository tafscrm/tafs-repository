/***********************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  24/03/2016
* Description     :  Controller class for creating follow up task from client Interaction page
* Modification Log:  Initial version.
************************************************************************************************
*  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh              18-Aug-2016              Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_FollowupTaskPageController{
    
    public Task followUptask{get;set;}
    public string priorityVar{get;set;}
    public string statusVar{get;set;}
    
    
    public List<SelectOption> getStatusValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Open','Open'));
        options.add(new SelectOption('Completed','Completed'));
        return options;
    }
    
    public List<SelectOption> getPriorityValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('High','High'));
        options.add(new SelectOption('Normal','Normal'));
        return options;
    }
    private ApexPages.StandardController controller;
    
    public TAFS_Client_Interaction__c clientInteractionRec{get;set;}
    
    public List<Schema.FieldSetMember> getInformationFields() {
        return SObjectType.TAFS_Client_Interaction__c.FieldSets.TAFS_Information.getFields();
    }
    
    public List<Schema.FieldSetMember> getItemsDiscussedFields() {
        return SObjectType.TAFS_Client_Interaction__c.FieldSets.TAFS_Items_Discussed.getFields();
    }
    
    public List<Schema.FieldSetMember> getTaskInfoFields() {
        return SObjectType.Task.FieldSets.TAFS_Followup_Task_Information.getFields();
    }
    
    public List<Schema.FieldSetMember> getAdditionalInfoFields() {
        return SObjectType.Task.FieldSets.TAFS_Additional_Information.getFields();
    }
    
    public List<Schema.FieldSetMember> getReminderFields() {
       return SObjectType.Task.FieldSets.TAFS_Reminder.getFields();
    }
    
    public List<Schema.FieldSetMember> getNotesFields() {
       return SObjectType.TAFS_Client_Interaction__c.FieldSets.TAFS_Interaction_Notes.getFields();
    }
    
    public TAFS_FollowupTaskPageController(ApexPages.StandardController controller) {           
       
       this.controller = controller;
       followUptask = new Task();
       followUptask.Type = Label.TAFS_FollowUpTaskType; 
       clientInteractionRec = (TAFS_Client_Interaction__c)controller.getRecord();
       
       
    } 
    /************************************************************************************
    * Method       :    saveRecords
    * Description  :    Method to save the Client Interaction and followup task record
    * Parameter    :    Nil    
    * Return Type  :    Pagereference
    *************************************************************************************/
    public PageReference saveRecords(){
        
        try{
            followUptask.WhatId = clientInteractionRec.TAFS_Client_Name__c;
            
            if((followUptask.subject!=null || followUptask.ActivityDate!=null || followUptask.Description!=null || clientInteractionRec.TAFS_Assigned_To__c!=null) 
                && (followUptask.subject==null || followUptask.ActivityDate==null || followUptask.Description==null || clientInteractionRec.TAFS_Assigned_To__c==null))
            {                 
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill in all the task details if you are trying to create a follow Up task.'));
                    return null;
            }
            else if(followUptask.subject!=null && followUptask.ActivityDate!=null && followUptask.Description!=null && clientInteractionRec.TAFS_Assigned_To__c!=null){ 
                
                followUptask.Status = statusVar;
                followUptask.Priority = priorityVar;
                followUptask.OwnerId = clientInteractionRec.TAFS_Assigned_To__c;                                         
                Database.insert(followUptask);
            }                    
            if(clientInteractionRec.Id==null){ 
                clientInteractionRec.TAFS_Assigned_To__c =null;           
                Database.insert(clientInteractionRec);
            }
            else{
                clientInteractionRec.TAFS_Assigned_To__c =null;          
                Database.update(clientInteractionRec);
            } 
            //return controller.save();
            return (new PageReference('/'+clientInteractionRec.Id));
        }catch(Exception excep){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,excep.getMessage()));
            return null;
        }
       
        
    } 
    /************************************************************************************
    * Method       :    saveAndNew
    * Description  :    Method to save the Client Interaction and followup task record 
                        and open the new page
    * Parameter    :    Nil    
    * Return Type  :    Pagereference
    *************************************************************************************/
    public PageReference saveAndNew(){
        
        try{
            followUptask.WhatId = clientInteractionRec.TAFS_Client_Name__c;
            if((followUptask.subject!=null || followUptask.ActivityDate!=null || followUptask.Description!=null || clientInteractionRec.TAFS_Assigned_To__c!=null) 
                && (followUptask.subject==null || followUptask.ActivityDate==null || followUptask.Description==null || clientInteractionRec.TAFS_Assigned_To__c==null))
            {                 
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill in all the task details if you are trying to create a follow Up task.'));
                    return null;
            }
            else if(followUptask.subject!=null && followUptask.ActivityDate!=null && followUptask.Description!=null && clientInteractionRec.TAFS_Assigned_To__c!=null){ 
                
                followUptask.Status = statusVar;
                followUptask.Priority = priorityVar;
                followUptask.OwnerId = clientInteractionRec.TAFS_Assigned_To__c;                                         
                Database.insert(followUptask);
            }            
            if(clientInteractionRec.Id==null){ 
                clientInteractionRec.TAFS_Assigned_To__c =null;           
                Database.insert(clientInteractionRec);
            }
            else{
                clientInteractionRec.TAFS_Assigned_To__c =null;          
                Database.update(clientInteractionRec);
            }          
            clientInteractionRec = new TAFS_Client_Interaction__c();
            followUptask = new Task();     
            return new PageReference('/apex/TAFS_CreateFollowUpTask');
            
        }catch(Exception excep){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,excep.getMessage()));
            return null;
        }        
    }            
}