/* CLass Name   : TAFS_ScheduleOpportunitiesPOStatus_Test
 * Description  : Test class for TAFS_ScheduleOpportunitiesPOStatusChange
 * Created By   : Karthik Gulla
 * Created On   : 14-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla               14-Mar-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_ScheduleOpportunitiesPOStatus_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(2,clientTermList[0].Id);
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[0].Id, UserInfo.getUserId(), 'Relationship Specialist');
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[1].Id, UserInfo.getUserId(), 'Relationship Specialist');
        DataUtilTest_TAFS.createTireProgramOpportunities(1, lstAcct[1].Id);
    } 

    /************************************************************************************
    * Method       :    testScheduleOpportunitiesPOStatus
    * Description  :    Test Method to test the Opportunities PO Status Change
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testScheduleOpportunitiesPOStatus() {
        Test.startTest();
        TAFS_ScheduleOpportunitiesPOStatusChange tafsPOStatusChange = new TAFS_ScheduleOpportunitiesPOStatusChange();
        String scheduleTime = '0 0 23 * * ?';
        System.schedule('TAFS_ScheduleOpportunitiesPOStatusChange_Test', scheduleTime, tafsPOStatusChange);
        Test.stopTest();
    }
}