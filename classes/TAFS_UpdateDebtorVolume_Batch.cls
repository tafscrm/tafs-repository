/****************************************************************************************
* Created By      :  Karthik Gulla
* Created Date    :  11/17/2016
* Description     :  Batch class to update Debtor Volume of all debtors
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_UpdateDebtorVolume_Batch implements Database.Batchable<sObject> {
    
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<Account>
    * Description      :  This method is used to retrieve all the Debtors available in the system
    **********************************************************************************************************************/
    global List<Account> start(Database.BatchableContext BC) {
       List<Account> lstDebtors = new List<Account>();
       Id debtorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Debtor').getRecordTypeId();
       String debtorsQuery = 'SELECT Id, Name, TAFS_Debtor_Volume__c, Debtor_RiskMgmt_Rule_Status__c, TAFS_Debtor_Code__c FROM Account WHERE RecordTypeId =:debtorRecordTypeId';
       lstDebtors = Database.query(debtorsQuery);
       return lstDebtors;
    }

    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<Account>
    * Return Type      :  Void
    * Description      :  This method is used to calculate the Debtor Volume of all Debtors
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<Account> lstDebtors) {
        
        List<Id> lstAccountIds = new List<Id>();
        List<Case> lstCases = new List<Case>();
        Map<Id,List<TAFS_Invoice__c>> mapDebtorInvoices = new Map<Id,List<TAFS_Invoice__c>>();
        Id level2QueueId = [SELECT Id FROM Group WHERE Name =:Label.TAFS_Level_2_Risk_Management_Queue_Name and Type = 'Queue' limit 1].Id;
        Id caseRiskMgmtRuleRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Risk Management Rule').getRecordTypeId();
        if(lstDebtors.size()>0){                         
            try{                
                for(Account debt:lstDebtors){
                    lstAccountIds.add(debt.Id);
                }

                for(TAFS_Invoice__c tafsInvoice:[SELECT Id, Name, TAFS_Balance__c, Debtor__c  
                                                        FROM TAFS_Invoice__c 
                                                        WHERE Debtor__c In :lstAccountIds
                                                        AND TAFS_Balance__c != null
                                                        AND TAFS_Invoice_Batch__r.TAFS_Status__c = :Label.TAFS_Pick_list_Value_For_Invoice_Batch_Processed]){
                    if(mapDebtorInvoices.containsKey(tafsInvoice.Debtor__c ))
                        mapDebtorInvoices.get(tafsInvoice.Debtor__c).add(tafsInvoice);
                    else{
                        List<TAFS_Invoice__c> lstTafsInvoices = new List<TAFS_Invoice__c>();
                        lstTafsInvoices.add(tafsInvoice);
                        mapDebtorInvoices.put(tafsInvoice.Debtor__c,lstTafsInvoices);
                    }
                }
                for(Account acc:lstDebtors){
                    Double debtorVolume = 0;
                    if(mapDebtorInvoices.containsKey(acc.Id)){
                        for(TAFS_Invoice__c tafInvoiceRec:mapDebtorInvoices.get(acc.Id)){
                            debtorVolume = debtorVolume + tafInvoiceRec.TAFS_Balance__c;
                        }
                    }
                    acc.TAFS_Debtor_Volume__c = debtorVolume;
                }
                if(!lstDebtors.isEmpty()){
                    update lstDebtors;
                }
                for(Account debt:lstDebtors){
                    if(debt.TAFS_Debtor_Volume__c > Integer.valueOf(Label.TAFS_Debtor_volume_for_Risk_Management)
                        && debt.Debtor_RiskMgmt_Rule_Status__c != 'Pending'
                        && debt.Debtor_RiskMgmt_Rule_Status__c != 'Enrolled'){
                        Case newCase = new Case();
                        newCase.AccountId = debt.Id;
                        newCase.RecordTypeId = caseRiskMgmtRuleRecordTypeId;
                        newCase.Status = 'New';
                        newCase.Subject = 'Risk Management Rule Creation for '+debt.Name+'('+debt.TAFS_Debtor_Code__c+')';
                        newCase.OwnerId = level2QueueId;
                        lstCases.add(newCase);
                        if(debt.Debtor_RiskMgmt_Rule_Status__c == 'Default' || debt.Debtor_RiskMgmt_Rule_Status__c == null || debt.Debtor_RiskMgmt_Rule_Status__c == '')
                            debt.Debtor_RiskMgmt_Rule_Status__c = 'Pending';
                    }
                }
                if(!lstCases.isEmpty()){
                    insert lstCases;
                }
                update lstDebtors;
            }catch(Exception excep){
                 System.debug('### Exception: TAFS_UpdateDebtorVolume_Batch.execute ###'+excep.getMessage());
            }          
        }               
    }

    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post update of Debtor Volume on Debtors
    **********************************************************************************************************************/    
    global void finish(Database.BatchableContext BC) {
    
    }
}