/****************************************************************************************
* Created By      :  Arpitha Sudhakar
* Create Date     :  19/08/2016
* Description     :  After insert for Fuel Discount to enter default value for gallon/liter 
* Modification Log:  Initial version.
***************************************************************************************/
public with sharing class TAFS_UpdateLiterGallonDefaultValue {
    
    public static void onAfterInsert(List<Account> acclist){
        List<Fuel_Discount__c> fuelList = new List<Fuel_Discount__c>();
        List<Id> accIdList = new List<Id>();
        try{
            for(Account accObj : acclist){
                accIdList.add(accObj.Id);
            }
            List<Account> accListFinal = [select id,name,recordtype.name from account where Id IN : accIdList];
            for(Account accObj : accListFinal){
                if(accObj.recordtype.name == 'Client'){
                    Fuel_Discount__c fuelDisObj = new Fuel_Discount__c();
                    fuelDisObj.TAFS_Client_ID__c = accObj.Id;
                    fuelDisObj.TAFS_Discount_Per_Gallon__c = 0.075;
                    fuelDisObj.TAFS_Discount_Per_Liter__c = 0.015;
                    fuelDisObj.TAFS_Effective_Date_Gallon__c = Date.today();
                    fuelDisObj.TAFS_Effective_Date_Liter__c = Date.today();
                    fuelList.add(fuelDisObj);
                }
            }
            if(fuelList.size() > 0){
                Database.insert(fuelList);
            }
        }
        catch(Exception ex){
        }
    } 
}