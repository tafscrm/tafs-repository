/****************************************************************************************
* Created By      :  Pankaj Singh
* Created Date    :  09/07/2016
* Description     :  Batch class for calculating and updating business days for the current month on Account.
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_UpdateMonthlyBusinessDaysBatch implements Database.Batchable<sObject> {
    
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<Account>
    * Description      :  This method is used to find all the clients in the system.
    **********************************************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        String query = 'SELECT Id FROM Account WHERE RecordTypeId =:recTypeId';
        return Database.getQueryLocator(query);
    }
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<Contact>
    * Return Type      :  Void
    * Description      :  This method is used to update the number of working days in a month for all accounts.
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<Account> scope) {
    
        if(scope.size()>0){
            list<Account> accountList = new list<Account>();
            list<BusinessHours> busHoursList = new list<BusinessHours>();
            busHoursList  = [SELECT ID FROM BusinessHours WHERE isActive = TRUE ORDER BY LastModifiedDate DESC LIMIT 1];
            if(busHoursList.size()>0){
                try{                
                    Date startDateMonth = Date.Today().toStartofMonth();
                    Date endDateMonth   = startDateMonth.addMonths(1).addDays(-1);
                    DateTime startTime = DateTime.newInstance(startDateMonth.year(), startDateMonth.month(), startDateMonth.day());
                    DateTime endTime = DateTime.newInstance(endDateMonth.year(), endDateMonth.month(), endDateMonth.day());
                    Decimal noOfWorkingDays = BusinessHours.diff(busHoursList[0].Id, startTime, endTime)/(3600*1000*24);
                    for(Account accObj : scope){
                        accObj.TAFS_Total_Business_Days_This_Month__c = noOfWorkingDays+1;
                        accountList.add(accObj);
                    }
                    if(!accountList.isEmpty()){
                        update accountList;
                    }                    
                }catch(DmlException dmlExcep){
                    //Do Nothing
                }
                catch(Exception excep){
                    //Do Nothing
                }           
            }               
        }
    }
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post the emails are sent.
    **********************************************************************************************************************/    
    global void finish(Database.BatchableContext BC) {
    
    }
}