/****************************************************************************************
* Create By     :   Karthik Gulla
* Created Date  :   04/07/2017
* Description   :   Test class for TAFS_Opportunity_Trigger_Handler
* Modification Log:
*-----------------------------------------------------------------------------
** Developer                    Date          Description
** ----------------------------------------------------------------------------
** Karthik Gulla                04/07/2017    Initial version.

*****************************************************************************************/
@isTest
public class TAFS_Opportunity_Trigger_Handler_Test {
    public static List<Opportunity> lstOpportunities;
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(2,clientTermList[0].Id);
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[0].Id, UserInfo.getUserId(), 'Relationship Specialist');
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[0].Id, UserInfo.getUserId(), 'Relationship Specialist');
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[1].Id, UserInfo.getUserId(), 'Relationship Specialist');
        lstOpportunities = DataUtilTest_TAFS.createOpportunities(1, lstAcct[0].Id,'Tire Program');
        Id oppId = lstOpportunities[0].Id;

        dsfs__DocuSign_Status__c d = new dsfs__DocuSign_Status__c();
        d.dsfs__Company__c = lstOpportunities[0].AccountId;
        d.dsfs__Opportunity__c = oppId;
        d.dsfs__Envelope_Status__c = 'Completed';
        d.dsfs__DocuSign_Envelope_ID__c = '1001A123-1234-5678-1D84-F8D44652A382';
        d.dsfs__Subject__c = 'Document for eSignature';
        d.dsfs__Completed_Date_Time__c = System.now();
        insert d;

        Attachment attach = new Attachment();     
        attach.Name = 'Unit Test Attachment';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.body = bodyBlob;
        attach.parentId = d.id;
        insert attach;

        Opportunity opp = [SELECT Id, TAFS_Agreement_Type__c, StageName, TAFS_PO_Status__c FROM Opportunity WHERE Id = :oppId];
        opp.StageName = 'PO Redeemed';
        opp.TAFS_PO_Status__c = 'Redeemed';
        opp.TAFS_Agreement_Type__c = 'Tax Exempt';
        opp.TAFS_PO_Status_Change_Date__c = System.today();
        update opp;
    } 

    public static testmethod void testOpportunityStagePORedeemed(){
        Test.startTest();
        
        Test.stopTest();
    }
}