/* CLass Name : TAFS_Transaction_Name_Update_Batch
 * Description  : Update Name field on Transaction(transKey_InvoiceKey)
 * Created By   : Raushan Anand
 * Created On   : 24-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                24-May-2015              Initial version.
 *
 *****************************************************************************************/
global class TAFS_Transaction_Name_Update_Batch implements Database.batchable<sObject>{ 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        //String str = '%_%';
        return Database.getQueryLocator('SELECT Id,Name,TAFS_Invoice_Key__c,TAFS_Type__c FROM TAFS_Invoice_Transaction__c'); //WHERE Name LIKE \''+str+'\''
    }
    
    global void execute(Database.BatchableContext BC, List<TAFS_Invoice_Transaction__c> scope){
        for(TAFS_Invoice_Transaction__c s : scope){
            String typeStr;
            if(s.TAFS_Type__c =='*C/B'){
                typeStr='7_5';
            }
            else if(s.TAFS_Type__c =='C/B'){
                typeStr='5_5';
            }
            else if(s.TAFS_Type__c =='FUND'){
                typeStr='13_6';
            }
            else if(s.TAFS_Type__c =='PMT NSF'){
                typeStr='2_7';
            }
            else if(s.TAFS_Type__c =='BUY'){
                typeStr='1_1';
            }
            else if(s.TAFS_Type__c =='PMT'){
                typeStr='2_5';
            }
            if(!String.isBlank(typeStr) && (!s.Name.contains('_')) && (!String.isBlank(s.TAFS_Invoice_Key__c)))
            	s.Name= s.Name+'_'+s.TAFS_Invoice_Key__c+'_'+typeStr;
            else if(!String.isBlank(typeStr) && (s.Name.contains('_'))){
                s.Name= s.Name+'_'+typeStr;
            }
        }
        System.debug('###RA'+scope);
        update scope;
    }
    
    global void finish(Database.BatchableContext BC){
    }
}