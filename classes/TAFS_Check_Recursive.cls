/* Class Name   : TAFS_Check_Recursive
 * Description  : Static class to avoid cyclic execution of Trigger
 * Created By   : Raushan Anand
 * Created On   : 1-July-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                1-July-2015              Initial version.
 *
 *****************************************************************************************/
public class TAFS_Check_Recursive {
    private static boolean run = true;
    private static boolean run2 = true;
    private static boolean prohibitAfterUpdateTrigger = true;
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
    public static boolean runAfter(){
        if(run2){
            run2=false;
            return true;
        }else{
            return run2;
        }
    }
    public static boolean runContactAfterUpdate(){
        if(prohibitAfterUpdateTrigger){
            prohibitAfterUpdateTrigger=false;
            return true;
        }else{
            return prohibitAfterUpdateTrigger;
        }
    }
}