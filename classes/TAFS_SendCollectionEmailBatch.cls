/****************************************************************************************
* Created By      :  Pankaj Singh
* Created Date    :  08/05/2016
* Description     :  Batch class for sending collections email
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_SendCollectionEmailBatch implements Database.Batchable<sObject> {
    
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<Contact>
    * Description      :  This method is used to find all the debtor contacts to whom the emails need to be sent.
    **********************************************************************************************************************/
    global List<Contact> start(Database.BatchableContext BC) {
        //variable declaration
        List<Contact> contactlist = new List<Contact>();
        Set<Contact> setContacts = new Set<Contact>();
        List<TAFS_Invoice__c> invoiceList = new List<TAFS_Invoice__c>();
        List<TAFS_Invoice__c> invoiceListVer = new List<TAFS_Invoice__c>();
        set<Id> debtorIdList =  new set<Id>();
        set<Id> invoiceIdSet =  new set<Id>();
        //map<Id,String> debtorEmailMap =  new map<Id,String>();
        Map<Id,List<Contact>> mapContact = new Map<Id,List<Contact>>();
        list<TAFS_Debtor_Collection_Verification__c> dcList =  new list<TAFS_Debtor_Collection_Verification__c>();
        
        //forming dynamic query string
        String queryStr = 'SELECT ID,Name,TAFS_Balance__c,Debtor__c,TAFS_Client_Name__c,TAFS_PO_No__c,TAFS_Invoice_Batch__r.TAFS_Posted_Date__c ,CreatedDate , (SELECT ID,TAFS_Action__c,TAFS_Action_Date__c FROM Verificatons_Collections__r WHERE '+
                          'TAFS_Action__c IN (\'W/P\' ,\'C/B\')) FROM TAFS_Invoice__c WHERE (TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO :'+Label.TAFS_Collection_Batch_Label+' OR TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO : '+
                          Label.TAFS_Collection_Batch_Label_60Days+') AND TAFS_Balance__c >0 AND '+ 'Debtor__c!=null AND (Debtor__r.TAFS_Gallium_Paying_Agent__c=null OR Debtor__r.TAFS_Gallium_Paying_Agent__c=\'NULL\' OR Debtor__r.TAFS_Gallium_Paying_Agent__c=\'\') AND Debtor__r.TAFS_No_Collection_Email__c = false';
                       
        invoiceList = Database.query(queryStr); 
          
        for(TAFS_Invoice__c invoiceObj : invoiceList){
            invoiceIdSet.add(invoiceObj.Id);
            if(invoiceObj.Verificatons_Collections__r.size() == 0){
                debtorIdList.add(invoiceObj.Debtor__c);
            }                                
        }
        
        string queryStrVer = 'SELECT ID,Name,TAFS_Balance__c,Debtor__c,TAFS_PO_No__c,TAFS_Invoice_Batch__r.TAFS_Posted_Date__c ,CreatedDate , (SELECT ID,TAFS_Action__c,TAFS_Action_Date__c FROM '+
                             'Verificatons_Collections__r WHERE TAFS_Action_Date__c < TODAY AND TAFS_Action__c IN (\'W/P\' ,\'C/B\')) FROM TAFS_Invoice__c WHERE (TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO :'+
                              Label.TAFS_Collection_Batch_Label+' OR TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO : '+Label.TAFS_Collection_Batch_Label_60Days+') AND TAFS_Balance__c >0 AND Debtor__c!=null '+
                             'AND (Debtor__r.TAFS_Gallium_Paying_Agent__c=null OR Debtor__r.TAFS_Gallium_Paying_Agent__c=\'NULL\' OR Debtor__r.TAFS_Gallium_Paying_Agent__c=\'\') AND Debtor__r.TAFS_No_Collection_Email__c = false';
                                                                
        for(TAFS_Invoice__c invoiceObj : Database.query(queryStrVer)){
            invoiceIdSet.add(invoiceObj.Id);        
            if(invoiceObj.Verificatons_Collections__r.size()>0){
                debtorIdList.add(invoiceObj.Debtor__c);                
            }                                
        }
         
        if(!debtorIdList.isEmpty()){
            for(Contact con : [SELECT ID,Email,AccountId,TAFS_Cadence_Contact_Key__c FROM Contact WHERE AccountId IN:debtorIdList AND LastName = 'AP Payment Status-SFDC' ORDER BY CreatedDate DESC]){
                //contactlist.add(con);
                setContacts.add(con);
                if(mapContact.containsKey(con.AccountId)){
                    mapContact.get(con.AccountId).add(con);  
                }
                else{
                    List<Contact> lstContacts = new List<Contact>();
                    lstContacts.add(con);
                    mapContact.put(con.AccountId, lstContacts);
                } 
            }
        }
        for(TAFS_Invoice__c invObj : [SELECT ID,Name,TAFS_PO_No__c,TAFS_Balance__c,TAFS_Client_Name__c,Debtor__c FROM TAFS_Invoice__c WHERE ID IN:invoiceIdSet]){
            TAFS_Debtor_Collection_Verification__c dcObj = new TAFS_Debtor_Collection_Verification__c();
            dcObj.TAFS_Client_Name__c     = invObj.TAFS_Client_Name__c;
            dcObj.TAFS_Debtor__c          = invObj.Debtor__c;
            if(mapContact.containsKey(invObj.Debtor__c) && mapContact.get(invObj.Debtor__c).size()>0){
                dcObj.TAFS_Debtor_Email__c = mapContact.get(invObj.Debtor__c)[0].Email;
                dcObj.TAFS_ContactKey__c   = mapContact.get(invObj.Debtor__c)[0].TAFS_Cadence_Contact_Key__c;
            }
            //dcObj.TAFS_Debtor_Email__c    = debtorEmailMap.get(invObj.Debtor__c);
            dcObj.TAFS_Invoice__c         = invObj.Id;
            dcObj.TAFS_Invoice_No__c      = invObj.Name;
            dcObj.TAFS_Invoice_Balance__c = invObj.TAFS_Balance__c;
            dcObj.TAFS_PO_No__c           = invObj.TAFS_PO_No__c;
            dcObj.TAFS_Type__c            = 'Collection';
            dcList.add(dcObj);   
        }
        if(!dcList.isEmpty()){
            Database.insert(dcList);
        }
        contactlist.addAll(setContacts);    
        return contactlist;
    }
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<Contact>
    * Return Type      :  Void
    * Description      :  This method is used to send email to debtor contacts for invoices which is more than 46 days old
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<Contact> scope) {
    
        if(scope.size()>0){
        
            List<Messaging.SingleEmailMessage> messageList = new List<Messaging.SingleEmailMessage>();
            try{
                //getting the email template Id
                EmailTemplate template = [SELECT ID,Name, Body, subject FROM EmailTemplate WHERE DeveloperName = 'TAFS_Collection_Batch_Template'];
                //getting org wide email address id
                OrgWideEmailAddress emailAddress = [SELECT ID FROM OrgWideEmailAddress WHERE DisplayName = 'Debtor Collections Email' LIMIT 1];
                for(Contact conObj : scope){
                    //setting all the email parameters
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(conObj.Id);
                    mail.setWhatid(conObj.AccountId);
                   
                    mail.setTemplateId(template.Id);
                    mail.setSaveAsActivity(true);
                    mail.setOrgWideEmailAddressId(emailAddress.Id);
                    messageList.add(mail);                        
                }
                //sending the emails and storing the result
                if(!messageList.isEmpty()){
                    Messaging.SendEmailResult [] resultSet = Messaging.sendEmail(messageList);
                } 
   
            }catch(Exception excep){
                //Do Nothing
            }          
        }               
    }
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post the emails are sent.
    **********************************************************************************************************************/    
    global void finish(Database.BatchableContext BC) {
    
    }
}