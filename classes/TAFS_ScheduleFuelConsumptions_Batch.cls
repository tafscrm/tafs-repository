/* CLass Name   : TAFS_ScheduleFuelConsumptions_Batch
 * Description  : Validate Fuel Consumptions and create new Fuel Opportunities, if any are required
 * Created By   : Karthik Gulla
 * Created On   : 07-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla              07-Mar-2017              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleFuelConsumptions_Batch implements Schedulable {   
    global void execute(SchedulableContext SC) {
    	Id jobId = Database.executeBatch(new TAFS_Validate_FuelConsumptions_Batch());
        TAFS_Task_Batch_Execution__c cs = TAFS_Task_Batch_Execution__c.getInstance('Fuel Consumption Batch Time');
        if(jobId != null){
            cs.Job_Id__c = jobId;
            UPDATE cs;
        }
   }
}