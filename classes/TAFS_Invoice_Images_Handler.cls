/* CLass Name   : TAFS_Invoice_Images_Handler
 * Description  : Trigger methods for TAFS_Invoice_Images__c 
 * Created By   : Manoj M Vootla
 * Created On   : 17-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla              17-May-2015              Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_Invoice_Images_Handler {

/****************************************************************************************
* Method Name      :  updateInvoiceIdOnInvoiceImages
* Input Parameter  :  List of Invoice Images (List<TAFS_Invoice_Images__c>)
* Return Type      :  Void
* Description      :  This method populates Invoice Id based upon Invoice key
***************************************************************************************/
    public static void updateInvoiceIdOnInvoiceImages(List<TAFS_Invoice_Images__c> invoiceImagesList){
    
        List<String> invoiceNoList = new List<String>();
        Map<String,String> invoiceNoIdMap = new Map<String,String>();
        for(TAFS_Invoice_Images__c iImage : invoiceImagesList){
            if(iImage.TAFS_Invoice_key__c != null){
                invoiceNoList.add(string.valueof(Integer.valueof(iImage.TAFS_Invoice_key__c)));
            }
        }

        List<TAFS_Invoice__c> invoiceList = [SELECT Id,TAFS_Invoice_key__c,Name FROM TAFS_Invoice__c WHERE TAFS_Invoice_key__c IN: invoiceNoList];
        for(TAFS_Invoice__c invObj : invoiceList){
            invoiceNoIdMap.put(invObj.TAFS_Invoice_key__c,invObj.Id);
        }
        
        for(TAFS_Invoice_Images__c iImage : invoiceImagesList){
            if(invoiceNoIdMap.containsKey(string.valueof(Integer.valueof(iImage.TAFS_Invoice_key__c)))){
                iImage.TAFS_Invoice_Number__c = invoiceNoIdMap.get(string.valueof(Integer.valueof(iImage.TAFS_Invoice_key__c)));
            }
        } 
    }
}