/* Trigger Name : TAFS_DeleteInsurance
 * Description  : Class for deleting Insurance in Future method
 * Created By   : Raushan Anand
 * Created On   : 15-June-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand               15-June-2016            Initial version.
 *
 *****************************************************************************************/
public class TAFS_DeleteInsurance {
    @Future
    public static void deleteInsurance(List<Id> idList){
        List<TAFS_Insurance_Policies__c> invList = [SELECT Id from TAFS_Insurance_Policies__c where ID IN: idList];
        DELETE invList;
    }
    
}