global class Previous_MTD_Batch implements Database.batchable<sObject>{ 
    
    global Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator('SELECT Id, TAFS_Client_Number__c,TAFS_Month_To_Date_Factored_Volume_New__c,TAFS_Previous_Month_Factored_Volume__c FROM Account'); 
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope){
        List<Id> accountId = new List<Id>();
        for(Account acc : scope){
            accountId.add(acc.Id);
        }
        List<TAFS_Invoice_Batch__c> invBatchList = [SELECT Id,TAFS_Client_Id__c, TAFS_A_R_Amount__c,TAFS_Posted_Date__c FROM TAFS_Invoice_Batch__c WHERE TAFS_Client_Id__c IN : accountId AND TAFS_Posted_Date__c = LAST_MONTH];
        Map<Id,Double> clientFactorMap = new Map<Id,double>();
        for(TAFS_Invoice_Batch__c invBatchObj : invBatchList){
            if(invBatchObj.TAFS_Posted_Date__c != null && invBatchObj.TAFS_Posted_Date__c <= DateTime.now()){
                if(invBatchObj.TAFS_A_R_Amount__c != null){
                    if(clientFactorMap.containsKey(invBatchObj.TAFS_Client_Id__c)){
                        clientFactorMap.put(invBatchObj.TAFS_Client_Id__c,clientFactorMap.get(invBatchObj.TAFS_Client_Id__c) + invBatchObj.TAFS_A_R_Amount__c);
                    }
                    else{
                        clientFactorMap.put(invBatchObj.TAFS_Client_Id__c,invBatchObj.TAFS_A_R_Amount__c);
                    }
                }
            }
        }
        for(Account objAcc : scope){
            objAcc.TAFS_Previous_Month_Factored_Volume__c = objAcc.TAFS_Month_To_Date_Factored_Volume_New__c;
            if(clientFactorMap.containsKey(objAcc.Id)){
                objAcc.TAFS_Previous_Month_Factored_Volume__c = clientFactorMap.get(objAcc.Id);
            }
            else{
                objAcc.TAFS_Previous_Month_Factored_Volume__c = 0;
            }
        }
        UPDATE scope;
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}