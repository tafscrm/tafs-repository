//Class to update the task priority
global class Task_Batch_To_Delete implements Database.Batchable<sObject>{

    global List<Task> start(Database.BatchableContext BC){
        List<Task> taskList = [Select Id,Priority from Task where Priority='Normal' OR Priority='High'];
        return taskList;
    }
    global void execute(Database.BatchableContext BC, List<Task> scope){
        for(Task a : scope){
            if(a.Priority =='Normal')
               a.Priority = '1'; 
            else if(a.Priority =='High')
               a.Priority = '7'; 
		}
        Database.Update(scope);
    }
    global void finish(Database.BatchableContext BC){
        
    }

}