/* CLass Name   : Schedule_Reattempt_For_FC 
 * Description  : Reattempt FC Batch
 * Created By   : Raushan Anand
 * Created On   : 29-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                29-Aug-2016              Initial version.
 *
 *****************************************************************************************/
global with sharing class Schedule_Reattempt_For_FC implements Schedulable{

     global void execute(SchedulableContext sc) {
        TAFS_Reattempt_FC_Callout_Batch b = new TAFS_Reattempt_FC_Callout_Batch(); 
        Database.executebatch(b,1);
     }
}