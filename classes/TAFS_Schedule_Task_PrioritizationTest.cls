/* CLass Name   : TAFS_Schedule_Task_PrioritizationTest
 * Description  : Test class for TAFS_Schedule_Task_Prioritization
 * Created By   : Arpitha Sudhakar
 * Created On   : 14-Sept-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar            14-Sept-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_Schedule_Task_PrioritizationTest {

    static testmethod void test() {
       Test.startTest();
       TAFS_Task_Batch_Execution__c ct = new TAFS_Task_Batch_Execution__c(Name ='Last_Execution_Time',Job_Id__c='234678fghj',Timestamp__c = DateTime.now()-1);
       INSERT ct;
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
       lstAcct[0].TAFS_Batches_Blocked_Value__c = 'Negative';
       UPDATE lstAcct;
               
       TAFS_Schedule_Task_Prioritization c = new TAFS_Schedule_Task_Prioritization();
       String scheduleTime = '0 0 23 * * ?';
       /*UPSERT new TAFS_Task_Batch_Execution__c(
            Name = 'Last_Execution_Time',
            Timestamp__c = DateTime.Now());*/
       system.schedule('TAFS_ScheduleTaskPrioritizationBatchTest', scheduleTime, c);
       Test.stopTest();
    }
}