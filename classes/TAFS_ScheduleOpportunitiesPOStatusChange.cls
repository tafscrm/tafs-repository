/* CLass Name   : TAFS_ScheduleOpportunitiesPOStatusChange
 * Description  : Update the 'PO Status' to 'Canceled' if its pending for 2 or more working days
 * Created By   : Karthik Gulla
 * Created On   : 08-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla              08-Mar-2017              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleOpportunitiesPOStatusChange implements Schedulable {   
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_Opportunities_POStatus_Change_Batch(),50);
   }
}