/* CLass Name   : TAFS_RiskManagementRule_Trigger_Handler
 * Description  : Trigger methods for Risk Management Rule Object
 * Created By   : Karthik Gulla
 * Created On   : 07-DEC-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla             07-DEC-2016             Initial version.
 *
 *****************************************************************************************/
public class TAFS_RiskManagementRule_Trigger_Handler{
	/****************************************************************************************
	* Created By      :  Karthik Gulla
	* Create Date     :  07-DEC-2016
	* Description     :  Update Debtor Risk Management Rule Status to 'Enrolled' once rules is created
	* Modification Log:  Initial version.
	***************************************************************************************/
    public static void updateDebtorRiskManagementRuleStatus(List<TAFS_Risk_Management_Rule__c> lstRiskManagementRules){
    	List<Id> lstDebtorIds = new List<Id>();
    	for(TAFS_Risk_Management_Rule__c tafsRule:lstRiskManagementRules){
    		if(tafsRule.TAFS_Debtor__c != null)
    			lstDebtorIds.add(tafsRule.TAFS_Debtor__c);
    	}

    	List<Account> lstDebtors = new List<Account>();
    	for(Account acc:[SELECT Id, Debtor_RiskMgmt_Rule_Status__c FROM Account WHERE Id In :lstDebtorIds]){
    		acc.Debtor_RiskMgmt_Rule_Status__c = 'Enrolled';
    		lstDebtors.add(acc);
    	}

    	if(lstDebtors.size() > 0)
    		update lstDebtors;
    }
}