/* Class Name   : TAFS_Kanban_Task_Email_Handler
 * Description  : This class handles email coming from Kanban after updating Cards.
 * Created By   : Raushan Anand
 * Created On   : 29-Apr-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                29-Apr-2015              Initial version.
 *
 *****************************************************************************************/
global class TAFS_Kanban_Task_Email_Handler implements Messaging.InboundEmailHandler {
	
	 /* Method Name : handleInboundEmail
	 * Description : Captures Email Received on Email Service
	 * Return Type : Messaging.InboundEmailResult
	 * Input Parameter : Messaging.InboundEmail & Messaging.InboundEnvelope
	 */
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
		String emailBody = email.htmlBody.replaceAll('\\<.*?\\>', '');		
		Map<String,String> taskMap = getTaskIdAndAssigneeDetails(emailBody); 	
        System.Debug('###RA>>>'+taskMap);
        TAFS_Kanban_Integration_Service.getTaskDetails(taskMap);
        return result;
    }
    
	 /* Method Name : getTaskIdAndAssigneeDetails
	 * Description : Method returns Task Id and Assignee Name after parsing the receiving email
	 * Return Type : Map<String,String>
	 * Input Parameter : String
	 */
	 
    private Map<String,String> getTaskIdAndAssigneeDetails(String emailText){
        Map<String,String> taskMap = new Map<String,String>();
        
		Pattern taskIdPattern = Pattern.compile('Task ID (\\S+)\\s');
		Matcher taskIdMatcher = taskIdPattern.matcher(emailText);    
		if (taskIdMatcher.find()) {
		    taskMap.put('TaskId',taskIdMatcher.group(1));
		}
        Pattern assigneePattern = Pattern.compile('Assignee: (\\S+)\\s');
		Matcher assigneeMatcher = assigneePattern.matcher(emailText);    
		if (assigneeMatcher.find()) {
		    taskMap.put('Assignee',assigneeMatcher.group(1));
		}
        return taskMap;
    }
}