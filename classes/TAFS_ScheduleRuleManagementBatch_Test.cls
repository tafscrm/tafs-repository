/* CLass Name   : TAFS_ScheduleRuleManagementBatch_Test
 * Description  : Test class for TAFS_ScheduleRuleManagementBatch
 * Created By   : Karthik Gulla
 * Created On   : 28-Dec-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla          28-Dec-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_ScheduleRuleManagementBatch_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        DataUtilTest_TAFS.createTAFSBatchExecution('Rule Management Batch Time');
        List<Account> lstAcct = DataUtilTest_TAFS.createDebtors(1);
        List<TAFS_Invoice_Batch__c> batchLst = DataUtilTest_TAFS.createIvoiceBatches(1,lstAcct[0].id);
        System.assert(batchLst[0].Id != null);
        List<TAFS_Invoice__c> invLst = DataUtilTest_TAFS.createInvoices(2,batchLst[0].id);
        for(TAFS_Invoice__c tfInv:invLst){
            tfInv.Debtor__c = lstAcct[0].Id;
        }
        update invLst;

        Map<String,String> mapApplicableToAll = new Map<String,String>();
        mapApplicableToAll.put('Name','ApplicableToAll');
        mapApplicableToAll.put('Category','Applicable to All');
        mapApplicableToAll.put('SubCategory','Duplicate PO#');
        mapApplicableToAll.put('Debtor',lstAcct[0].Id);

        DataUtilTest_TAFS.createRiskManagementRule(mapApplicableToAll);

        Map<String,String> charsInPO = new Map<String,String>();
        charsInPO.put('Name','CharatersInPO#');
        charsInPO.put('Category','PO');
        charsInPO.put('SubCategory','Characters in PO#');
        charsInPO.put('Debtor',lstAcct[0].Id);
        charsInPO.put('Value','1;2');

        DataUtilTest_TAFS.createRiskManagementRule(charsInPO);

        Map<String,String> POAlphaNumStartsWith = new Map<String,String>();
        POAlphaNumStartsWith.put('Name','PO#AlphaNumericStartsWith');
        POAlphaNumStartsWith.put('Category','PO');
        POAlphaNumStartsWith.put('SubCategory','PO# Alpha-numeric Starts With');
        POAlphaNumStartsWith.put('Debtor',lstAcct[0].Id);
        POAlphaNumStartsWith.put('Value','1');

        DataUtilTest_TAFS.createRiskManagementRule(POAlphaNumStartsWith);

        Map<String,String> RangeCompWithPreviousPO = new Map<String,String>();
        RangeCompWithPreviousPO.put('Name','RangeComparisonWithPreviousPO');
        RangeCompWithPreviousPO.put('Category','PO');
        RangeCompWithPreviousPO.put('SubCategory','Range Comparison with Previous PO#');
        RangeCompWithPreviousPO.put('Debtor',lstAcct[0].Id);
        RangeCompWithPreviousPO.put('AcceptableMin','1');
        RangeCompWithPreviousPO.put('AcceptableMax','10');

        DataUtilTest_TAFS.createRiskManagementRule(RangeCompWithPreviousPO);
        
        /*Map<String,String> MaxInvAmtPercent = new Map<String,String>();
        MaxInvAmtPercent.put('Name','MaxInvoiceAmountPercent');
        MaxInvAmtPercent.put('Category','PO');
        MaxInvAmtPercent.put('SubCategory','Max Invoice Amount %');
        MaxInvAmtPercent.put('Debtor',lstAcct[0].Id);

        DataUtilTest_TAFS.createRiskManagementRule(MaxInvAmtPercent);*/
    } 

    /************************************************************************************
    * Method       :    testScheduleRiskManagementRuleBatch
    * Description  :    Test Method to test Risk Management Rules
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testScheduleRuleManagementBatch() {
        Test.startTest();
        TAFS_ScheduleRuleManagementBatch tafsRiskMgmtRuleBatch = new TAFS_ScheduleRuleManagementBatch();
        String scheduleTime = '0 0 23 * * ?';
        System.schedule('TAFS_ScheduleRiskManagementRuleBatchTest', scheduleTime, tafsRiskMgmtRuleBatch);
        Test.stopTest();
    }
}