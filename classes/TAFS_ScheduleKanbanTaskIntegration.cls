/* CLass Name   : TAFS_ScheduleKanbanTaskIntegration
 * Description  : Scheduler class to update Kanban Task everyday
 * Created By   : Raushan Anand
 * Created On   : 25-Oct-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand               25-Oct-2016            Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleKanbanTaskIntegration implements Schedulable {
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_KanbanTaskIntegrationBatch(),1);
        
   }
}