@isTest
private class TAFS_RoadsideSubscriptionDomainTest{

    /************************************************************************************
    * Method       :    testRSClientNumberUpdate
    * Description  :    Tests whether client id is populated properly on Roadside subscription or not
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testRSClientNumberUpdate() {
       
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
       
       List<TAFS_Roadside_Assistance__c> roadsideAssistanceList = DataUtilTest_TAFS.createRoadsideSubscriptions(2,lstAcct[0].Id);
       
       Test.startTest();
           insert roadsideAssistanceList;
       Test.stopTest();
       
       system.assertEquals(2,roadsideAssistanceList.size());
       system.assertEquals(roadsideAssistanceList[0].TAFS_Client_Number__c , lstAcct[0].TAFS_Client_Number__c);
    }
}