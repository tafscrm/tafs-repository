@isTest
private class TAFS_PreventDuplicateFuelCommitmentsTest{

    /************************************************************************************
    * Method       :    testInsertDuplicateFuelConsumptionRecord
    * Description  :    Scenario to cover the prevention of Duplicate fuel consumption record insertion
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testInsertDuplicateFuelConsumptionRecord() {
       
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
       
       List<TAFS_Fuel_Commitment__c> fuelCommitmentList = DataUtilTest_TAFS.createFuelCommitments(2,lstAcct[0].Id);
       
       Test.startTest();
       try{
           insert fuelCommitmentList[0];
           insert fuelCommitmentList[1];
          }
       catch(Exception e){}
       Test.stopTest();
       
       
    }
}