/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  24/03/2016
* Description     :  Controller class for the Client welcome/training page
* Modification Log:  Initial version.
***************************************************************************************/
public class TAFS_ClientWelcomePageController{

    public TAFS_Welcome_Client__c welcomeClientObj{get;set;}
    
    public boolean enableCall{get;set;}
    public boolean disableCall{get;set;}
    List<Event> eventObjList = new List<Event>();
    Contact conObj = new Contact();
    List<TAFS_Welcome_Client__c> welcomeClientList = new List<TAFS_Welcome_Client__c>();
    List<AccountTeamMember> accTeamMemberList =  new List<AccountTeamMember>();
    private DateTime callStartDateTime;

    public TAFS_ClientWelcomePageController(ApexPages.StandardController controller) {
               
        enableCall           = true;
        welcomeClientObj     = (TAFS_Welcome_Client__c)controller.getRecord();
        
        String eventId =  ApexPages.currentPage().getParameters().get('eventId'); 
            
        if( eventId!=null){
            eventObjList = new TAFS_SObjectSelector().selectEventByIds(new set<string>{eventId}); 
        }
        If(!eventObjList.isEmpty()){
        
             //querying the welcome client record based on the Client Id passed
             welcomeClientList = new TAFS_SObjectSelector().selectWelcomeClientByIds(new set<string>{eventObjList[0].WhatId});
             //querying the Acont Team Member record based on the Client Id and teh team member role passed
             accTeamMemberList = new TAFS_SObjectSelector().selectTeamMemberByAccountId(eventObjList[0].WhatId,'Sales Account Manager;Onboarding Specialist');
        }
        for(TAFS_Welcome_Client__c wcObj : welcomeClientList){
            if(wcObj.TAFS_Activity_Id__c.contains(eventObjList[0].Id)){
                welcomeClientObj = wcObj;
            }
        }
        if(!eventObjList.isEmpty()){
            conObj = [SELECT ID,Phone,MobilePhone,OtherPhone FROM Contact WHERE ID =: eventObjList[0].WhoId];
        }        
        if(!eventObjList.isEmpty()){
                   
            welcomeClientObj.TAFS_Additional_Client_Welcome_Contact__c     = eventObjList[0].TAFS_Additional_Client_Welcome_Contact__c;
            welcomeClientObj.TAFS_Onboarding_Specialist__c                 = eventObjList[0].TAFS_Onboarding_Specialist__c;
            welcomeClientObj.TAFS_Client_Name__c                           = eventObjList[0].whatId;
            if(eventObjList[0].Type=='Client Welcome')
                welcomeClientObj.TAFS_Client_Welcome_Contact__c            = eventObjList[0].whoId;
            if(eventObjList[0].Type=='Client Training')
                welcomeClientObj.TAFS_Client_Training_Contact__c           = eventObjList[0].whoId;            
            welcomeClientObj.TAFS_Scheduled_DateTime__c                    = eventObjList[0].StartDateTime;
            welcomeClientObj.TAFS_Contact_Mobile__c                        = conObj.MobilePhone;
            welcomeClientObj.TAFS_Contact_Phone__c                         = conObj.Phone;
            welcomeClientObj.TAFS_Contact_Other_Phone__c                   = conObj.OtherPhone;
            if(welcomeClientObj.TAFS_Called_No_Contact_Counter__c ==null)
                welcomeClientObj.TAFS_Called_No_Contact_Counter__c = 0;
        }
        if(!accTeamMemberList.isEmpty()){
             for(AccountTeamMember atm : accTeamMemberList ){
                 if(atm.TeamMemberRole == 'Sales Account Manager'){
                     welcomeClientObj.TAFS_Sales_Account_Manager__c = atm.UserId;
                 }
                 else if(atm.TeamMemberRole == 'Onboarding Specialist'){                 
                     welcomeClientObj.TAFS_Onboarding_Specialist__c = atm.UserId;
                 } 
             }
        }
    } 
    /************************************************************************************
    * Method       :    startCall
    * Description  :    Method invoked when start call button is pressed on Client welcome and client training page.
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public void startCall(){
        
        callStartDateTime = system.now();
        enableCall        = false;
        disableCall       = true;
    }   
    /************************************************************************************
    * Method       :    logCall
    * Description  :    Method invoked when Log call button is pressed on Client welcome and client training page.
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public PageReference logCall(){
    
        list<TAFS_EventsService.EventStub> requests = new list<TAFS_EventsService.EventStub>();
        
        TAFS_EventsService eventService = new TAFS_EventsService();
        
        //added by arpitha
        if(welcomeClientObj.TAFS_Notes__c == '' || welcomeClientObj.TAFS_Notes__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter notes before logging the call'));
            return null;
        }
        for(Event eventObj : eventObjList){
            //creating a list of DTO wrapper
            requests.add(new TAFS_EventsService.EventStub(welcomeClientObj, eventObj.whatId, eventObj , callStartDateTime));
        }
        if(!requests.isEmpty()){
            //calling the service class to process the data
            TAFS_EventsService.createwelcomeClientRecords(requests);
        }
        if(ApexPages.currentPage().getUrl().contains('retURL')){
            
            return (new PageReference(ApexPages.currentPage().getParameters().get('retURL')));
        }
        
        else if(!eventObjList.isEmpty()){
        
            return (new PageReference('/'+eventObjList[0].whatId));
        }
        else{
            return null;
       }        
    }
}