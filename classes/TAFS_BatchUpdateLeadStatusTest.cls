/* CLass Name   : TAFS_BatchUpdateLeadStatusTest
 * Description  : Test class for TAFS_BatchUpdateLeadStatus 
 * Created By   : Arpitha Sudhakar
 * Created On   : 21-June-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar                21-June-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_BatchUpdateLeadStatusTest {

    static testmethod void test() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       /*if(leadLst.size() != 0){
           DataUtilTest_TAFS.createTasksWhoId('Test task 2', 2, leadLst[0].Id);
       }*/
       
       DataUtilTest_TAFS.createQueueForLeadRemoval();
       
       
       List<Lead> leadList = [select id,name from Lead];
       
       Test.startTest();
       TAFS_BatchUpdateLeadStatus c = new TAFS_BatchUpdateLeadStatus();
       Database.executeBatch(c);
       Test.stopTest();
    }
    
    static testmethod void testpositive() {
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
       DataUtilTest_TAFS.createQueueForLeadRemoval();       
       
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        List<Lead> leadList = [select id,name from Lead];
        TAFS_BatchUpdateLeadStatus AU = new TAFS_BatchUpdateLeadStatus();
        //QL = AU.start(bc);
        
        /*Database.QueryLocatorIterator QIT =  QL.iterator();
        while (QIT.hasNext())
        {
            Account Acc = (Account)QIT.next();            
            System.debug(Acc);
            AcctList.add(Acc);
        }   */     
        
        AU.execute(BC, leadList);
        AU.finish(BC);        
    }
    
 /*
 * Method Name  : testBatchUpdateLeadStatus  
 * Description  : Test class for TAFS_BatchUpdateLeadStatus 
 * Created By   : Manoj Vootla
 * Created On   : 25-Aug-2016   
 */
    
    static testmethod void testBatchUpdateLeadStatus ()
     {
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='New',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_Lead_Sort_Order__c lsc1 = new TAFS_Lead_Sort_Order__c(Name='Actively Working',TAFS_Sort_Order__c ='1');
         insert lsc1;
         
         TAFS_Lead_Sort_Order__c lsc2 = new TAFS_Lead_Sort_Order__c(Name='Agreement Out',TAFS_Sort_Order__c ='1');
         insert lsc2;
         
         TAFS_Lead_Sort_Order__c lsc3 = new TAFS_Lead_Sort_Order__c(Name='Agreement Ready',TAFS_Sort_Order__c ='1');
         insert lsc3;
         
         TAFS_Lead_Sort_Order__c lsc4 = new TAFS_Lead_Sort_Order__c(Name='Application Out',TAFS_Sort_Order__c ='1');
         insert lsc4;
         
         TAFS_Lead_Sort_Order__c lsc5 = new TAFS_Lead_Sort_Order__c(Name='Awaiting Documentation',TAFS_Sort_Order__c ='1');
         insert lsc5;
         
         TAFS_Lead_Sort_Order__c lsc6 = new TAFS_Lead_Sort_Order__c(Name='No Contact',TAFS_Sort_Order__c ='1');
         insert lsc6;        
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100); 
         insert ls;
                                                                              
      List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
      list<Task> taskLst = DataUtilTest_TAFS.createTasksWhoId('LeadTask',1,leadLst[0].id);
      Test.startTest();
      TAFS_BatchUpdateLeadStatus b = new TAFS_BatchUpdateLeadStatus();
      Database.executeBatch(b);                                      
      Test.stopTest();
     }
    
}