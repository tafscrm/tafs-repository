/****************************************************************************************
* Created By      :  Naveenkanth B
* Create Date     :  07/06/2016
* Description     :  Scheduler for Batch Class to Update the Fields on Account
* Modification Log:  Initial version.
***************************************************************************************/

global class TAFS_SchedulerForAccountTeamMember implements Schedulable{
    
/************************************************************************************
* Method       :    execute
* Description  :    Method to create an instance of the Batch Class for the Scheduler.
* Parameter    :    SchedulableContext 
* Return Type  :    void
*************************************************************************************/
     global void execute(SchedulableContext sc) {
        TAFS_UpdateAccountTeamMember b = new TAFS_UpdateAccountTeamMember(); 
        database.executebatch(b);
     }
       
  }