/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  13/04/2016
* Description     :  Domain class for Roadside Assistance Claims
* Modification Log:  Initial version.
***************************************************************************************/
public with sharing class TAFS_RoadsideAssistanceClaimsDomain {

    private static boolean prohibitBeforeInsertTrigger = false;
    
    public void onBeforeInsert(List<TAFS_Roadside_Assistance_Claims__c> RAClaimsList){
    
        if(prohibitBeforeInsertTrigger)
        {
            return;
        }
        
        Map<String, String> clientNumerIdMap =  new Map<String, String>();
        
        //creating a map of client number and account ids
        for(Account acc : [SELECT Id,TAFS_Client_Number__c FROM ACCOUNT]){
           
            clientNumerIdMap.put(acc.TAFS_Client_Number__c, acc.Id);
        } 
        //Assigning the account id to the roadside claims record based on the client number
        for(TAFS_Roadside_Assistance_Claims__c rc : RAClaimsList){
            if(clientNumerIdMap.containsKey(rc.TAFS_Client_Number__c)){
                rc.TAFS_Client_ID__c = clientNumerIdMap.get(rc.TAFS_Client_Number__c);
            }           
        }
        prohibitBeforeInsertTrigger = true;
    
    }
    
}