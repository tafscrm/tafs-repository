/* Class Name   : TAFS_DocuSignUtility 
 * Description  : TUtility class for sending documents to contacts associated with leads using docusign
 * Created By   : Pankaj Singh
 * Created On   : 25-Jun-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj SIngh                22-Jun-2016             Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_DocuSignUtility {
    
    public string leadId{get;set;}
    private string receipientList = '';
    private decimal routingNo = 0;
    public General_Manager__c gm = General_Manager__c.getInstance('General Manager');
    
    public TAFS_DocuSignUtility(){
        leadId = ApexPages.currentPage().getParameters().get('Id');
    }
    public PageReference sendToDocuSign(){
         Lead lead;
                lead = [Select id,Email,TAFS_Application_Processing_Completed__c,(select Id,FirstName,LastName, Email from Contacts__r WHERE TAFS_Contact_Type__c = 'Owner'),Name,Owner.Name,Owner.Email from Lead where id=: leadId];
        
        string RC = '';string RSL='';string RSRO='';string RROS='';string CCRM='';string CCTM='';string CCNM='';string CRCL=''; string CRL='';string OCO='';string DST='';string LA='';string CEM='';string CES='';string STB='';string SSB='';string SES='';string SEM='';string SRS='';string SCS ='';string RES=''; 
        //DST='ab8269a7-474a-458d-8797-118ca95d8bc1'; 
        DST=''; 
        //Adding Notes & Attachments 
        LA='0'; 
        //Custom Recipient List 
        for(Contact con : lead.Contacts__r){
            routingNo = routingNo +1;
            receipientList = receipientList +'Email~'+con.Email+';FirstName~'+con.FirstName+';LastName~'+con.LastName+';Role~Signer'+routingNo+';RoutingOrder~1,';
        }
            // Added this section to add Josh as a signer for Agreements
             routingNo = routingNo +1;
               if(lead.TAFS_Application_Processing_Completed__c == True){
                   receipientList = receipientList +'Email~'+gm.Email__c+';FirstName~'+gm.First_Name__c+';LastName~'+gm.Last_Name__c+';Role~Signer'+routingNo+';RoutingOrder~2,';
               }
            
             system.debug('printing receipent list>>>>'+receipientList);
        CRL= receipientList + 'LoadDefaultContacts~0'; 
        //Custom Email Subject 
        CES='Please eSign Proposal -'+lead.Name; 
        //Custom Email Message 
        CEM= 'Envelope sent by '+ lead.Owner.Name+'(' + lead.Owner.Email +')'+ ' for ' +lead.Name; 
        // Show Email Subject (default in config) 
        SES = '1'; //Ex: '1' 
        // Show Email Message (default in config) 
        SEM = '1'; //Ex: '1' 
        // Show Tag Button (default in config) 
        STB = '1'; //Ex: '1' 
        // Show Chatter (default in config) 
        SCS = '0'; //Ex: '1' 
        OCO = ''; 
        PageReference pageRef = null;
        pageRef = new PageReference('/apex/dsfs__DocuSign_CreateEnvelope?DSEID=0&SourceID='+leadId+'&RC='+RC+'&RSL='+RSL+'&RSRO='+RSRO+'&RROS='+RROS+'&CCRM='+CCRM+'&CCTM='+CCTM+'&CRCL='+CRCL+'&CRL='+CRL+'&OCO='+OCO+'&DST='+DST+'&CCNM='+CCNM+'&LA='+LA+'&CEM='+CEM+'&CES='+CES+'&SRS='+SRS+'&STB='+STB+'&SSB='+SSB+'&SES='+SES+'&SEM='+SEM+'&SRS='+SRS+'&SCS='+SCS+'&RES='+RES);
        pageRef.setRedirect(true);    
        return pageRef;
   }
}