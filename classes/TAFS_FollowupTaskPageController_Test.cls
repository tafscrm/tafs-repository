@isTest
private class TAFS_FollowupTaskPageController_Test {

/************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
     
         list<Account> lstAcct = DataUtilTest_TAFS.createAccountsForAgreements(1); 
    } 
    /************************************************************************************
    * Method       :    testfollowUptaskCreation
    * Description  :    test the positive scenario of followup task creation
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testfollowUptaskCreation() {
        
       Account accObj = [select id from Account limit 1];
       PageReference pageRef = Page.TAFS_CreateFollowUpTask;
       Test.setCurrentPage(pageRef); 
       
       TAFS_Client_Interaction__c cIObj = new TAFS_Client_Interaction__c();
       
       cIObj.TAFS_Client_Name__c = accObj.Id;
       
       Test.startTest();
       
       TAFS_FollowupTaskPageController fwTaskInstance = new TAFS_FollowupTaskPageController(new ApexPages.StandardController(cIObj));
       fwTaskInstance.followUptask.Type='Follow Up';
       fwTaskInstance.followUptask.Subject = 'Call';
       fwTaskInstance.followUptask.ActivityDate = Date.Today();
       fwTaskInstance.followUptask.Description = 'test';
       fwTaskInstance.priorityVar= 'Normal';
       fwTaskInstance.statusVar= 'Open';
       
       fwTaskInstance.clientInteractionRec.TAFS_Assigned_To__c = UserInfo.getUserId();
       
       fwTaskInstance.getInformationFields();
       fwTaskInstance.getItemsDiscussedFields();
       fwTaskInstance.getTaskInfoFields();
       fwTaskInstance.getAdditionalInfoFields();
       fwTaskInstance.getReminderFields();
       fwTaskInstance.saveRecords();
       
       Test.stoptest();
       system.assert([SELECT ID FROM TASK].size()>0);       
    }  
    
    /************************************************************************************
    * Method       :    testfollowUptaskCreation
    * Description  :    test the positive scenario of followup task creation
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testfollowUptaskSaveNew() {
        
       Account accObj = [select id from Account limit 1];
       PageReference pageRef = Page.TAFS_CreateFollowUpTask;
       Test.setCurrentPage(pageRef); 
       
       TAFS_Client_Interaction__c cIObj = new TAFS_Client_Interaction__c();
       
       cIObj.TAFS_Client_Name__c = accObj.Id;
       
       Test.startTest();
       
       TAFS_FollowupTaskPageController fwTaskInstance = new TAFS_FollowupTaskPageController(new ApexPages.StandardController(cIObj));
       fwTaskInstance.followUptask.Type='Follow Up';
       fwTaskInstance.followUptask.Subject = 'Call';
       fwTaskInstance.followUptask.ActivityDate = Date.Today();
       fwTaskInstance.followUptask.Description = 'test';
       fwTaskInstance.priorityVar= 'Normal';
       fwTaskInstance.statusVar= 'Open';
       
       fwTaskInstance.clientInteractionRec.TAFS_Assigned_To__c = UserInfo.getUserId();
             
       fwTaskInstance.getInformationFields();
       fwTaskInstance.getItemsDiscussedFields();
       fwTaskInstance.getTaskInfoFields();
       fwTaskInstance.getAdditionalInfoFields();
       fwTaskInstance.getReminderFields();
       fwTaskInstance.getStatusValues();
       fwTaskInstance.getPriorityValues();
       fwTaskInstance.getNotesFields();
       fwTaskInstance.saveAndNew();
       
       Test.stoptest();
       system.assert([SELECT ID FROM TASK].size()>0);       
    }  
    /************************************************************************************
    * Method       :    testfollowUptaskFieldsNull
    * Description  :    test the negative scenario of followup task creation
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testfollowUptaskFieldsNull() {
        
       Account accObj = [select id from Account limit 1];
       PageReference pageRef = Page.TAFS_CreateFollowUpTask;
       Test.setCurrentPage(pageRef); 
       
       TAFS_Client_Interaction__c cIObj = new TAFS_Client_Interaction__c();
       
       cIObj.TAFS_Client_Name__c = accObj.Id;
       
       Test.startTest();
       
           TAFS_FollowupTaskPageController fwTaskInstance = new TAFS_FollowupTaskPageController(new ApexPages.StandardController(cIObj));
           fwTaskInstance.followUptask.Subject ='call'; 
           fwTaskInstance.followUptask.Type='Follow Up';
           fwTaskInstance.followUptask.Subject = 'Call';
           fwTaskInstance.followUptask.ActivityDate = Date.Today();
           fwTaskInstance.followUptask.Description = 'test';
           fwTaskInstance.followUptask.Priority = 'Normal';
           fwTaskInstance.followUptask.Status= 'Open';      
           fwTaskInstance.saveRecords();
       
       Test.stoptest();
       
       system.assert([SELECT ID FROM TASK].size()==0);    
       
    } 
    
    /************************************************************************************
    * Method       :    testfollowUptaskException
    * Description  :    test the exception scenario of followup task creation
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testfollowUptaskException() {
        
       Account accObj = [select id from Account limit 1];
       PageReference pageRef = Page.TAFS_CreateFollowUpTask;
       Test.setCurrentPage(pageRef); 
       
       TAFS_Client_Interaction__c cIObj = new TAFS_Client_Interaction__c();
       
       cIObj.TAFS_Client_Name__c = accObj.Id;
       
       Test.startTest();
       
       TAFS_FollowupTaskPageController fwTaskInstance = new TAFS_FollowupTaskPageController(new ApexPages.StandardController(cIObj));
       fwTaskInstance.followUptask.Subject ='call';       
       fwTaskInstance.saveAndNew();
       fwTaskInstance.saveRecords();
       
       Test.stoptest();
       
       system.assert([SELECT ID FROM TASK].size()==0);    
       
    }  

}