/* CLass Name   : TAFS_SchedulerForUpdateLeadStatusTest
 * Description  : Test class for TAFS_SchedulerForUpdateLeadStatus
 * Created By   : Arpitha Sudhakar
 * Created On   : 21-June-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar                21-June-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_SchedulerForUpdateLeadStatusTest {

    static testmethod void test() {
       Test.startTest();
       TAFS_SchedulerForUpdateLeadStatus c = new TAFS_SchedulerForUpdateLeadStatus();
       String scheduleTime = '0 0 23 * * ?';
       system.schedule('Test schedule', scheduleTime, c);
       Test.stopTest();
    }
}