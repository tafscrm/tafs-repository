/* CLass Name   : TAFS_Invoice_Batch_Trigger_Handler_test
 * Description  : Test class for Trigger methods of TAFS_Invoice_Batch_Trigger 
 * Created By   : Raushan Anand
 * Created On   : 26-Apr-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                31-Apr-2015              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_Schedule_MTD_Update_Test {

     @testsetup
    static void setUpInvoiceBatchData(){
        List<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        List<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);        
        DataUtilTest_TAFS.createIvoiceKeyMapping('Test1','Invoice Transactions','Status','0','Processed');
        DataUtilTest_TAFS.createIvoiceKeyMapping('Test2','Invoice Transactions','Type','0','Processed');  
    }
    
    static testmethod void testInvoiceBatchUpdate(){
    	Account acc = [SELECT Id,TAFS_Client_Number__c FROM Account limit 1];
    	List<TAFS_Invoice_Batch__c> invoiceBatchObjList = DataUtilTest_TAFS.createIvoiceBatches(1,acc.Id);
    	Test.startTest();
	    	String hour = String.valueOf(Datetime.now().hour());
			String min = String.valueOf(Datetime.now().minute());
			String ss = String.valueOf(Datetime.now().second()+5);
			String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
			Schedule_MTD_Update s = new Schedule_MTD_Update();
			System.schedule('Job Started At ' + String.valueOf(Datetime.now()), nextFireTime, s);
    	Test.stopTest();    	
    }
}