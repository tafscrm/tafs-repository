/****************************************************************************************
* Created By      :  Arpitha Sudhakar
* Create Date     :  06/06/2016
* Description     :  Batch class for lead ownership removal
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_BatchLeadOwnerRemoval implements Database.Batchable<sObject> {
    
    global List<Lead> start(Database.BatchableContext BC) {
        //variables
        List<Lead> leadListForLast24Hrs = new List<Lead>();
        List<Lead> leadListForLastNdays = new List<Lead>();
        Set<Id> leadSetIdsForLast24Hrs  = new Set<Id>();
        Set<Id> leadSetIdsForLastNdays  = new Set<Id>();
        List<Task> taskListFor24Hrs     = new List<Task>();
        List<Event> eventListFor24Hrs   = new List<Event>();
        List<Event> eventListForNdays   = new List<Event>();
        List<Task> taskListForNdays     = new List<Task>();
        Set<Id> allLeadIdsSet           = new Set<Id>();
        List<Lead> leadList             = new List<Lead>();
        Set<Id> groupSetId              = new Set<Id>();
        
        List<Lead> leadListForNoContact = new List<Lead>();
        Set<Id> leadSetIdsForNoContact  = new Set<Id>();
        List<Task> taskListForNoContact = new List<Task>();
        
        List<Group> groupLst = [select Id,name from Group where Type =: Label.TAFS_Queue_Value and (Name =: Label.TAFS_National_Accounts OR Name =: Label.TAFS_Direct_Sales)];
        for(Group groupObj : groupLst){
            groupSetId.add(groupObj.Id);
        }

        leadListForLast24Hrs = [SELECT Id,name,LastModifiedDate,TAFS_Lead_Type__c FROM Lead where OwnerId NOT IN :groupSetId AND 
                                (Status =: Label.TAFS_LeadStatus_New OR Status =: Label.TAFS_LeadStatus_No_Contact) AND LastModifiedDate <=: system.now().AddDays(-1)];
        
        for(Lead leadObj : leadListForLast24Hrs){
            leadSetIdsForLast24Hrs.add(leadObj.Id);
        }
        if(!leadSetIdsForLast24Hrs.isEmpty() || leadSetIdsForLast24Hrs != null){
            taskListFor24Hrs  = [SELECT Id,WhoId,CreatedDate FROM Task WHERE WhoId IN :leadSetIdsForLast24Hrs and CreatedDate >=: system.now().AddDays(-1) and CreatedDate <=: system.now()];
            eventListFor24Hrs = [SELECT Id,WhoId,CreatedDate FROM Event WHERE WhoId IN :leadSetIdsForLast24Hrs and CreatedDate >=: system.now().AddDays(-1) and CreatedDate <=: system.now()];
        }

        if(taskListFor24Hrs.size() != 0){
           for(Task taskObject : taskListFor24Hrs){
                leadSetIdsForLast24Hrs.remove(taskObject.WhoId); 
            }
        }
        if(eventListFor24Hrs.size() != 0){
            for(Event eventObject : eventListFor24Hrs){
                leadSetIdsForLast24Hrs.remove(eventObject.WhoId); 
            }
        }
        allLeadIdsSet.addAll(leadSetIdsForLast24Hrs);
        
        leadListForLastNdays = [SELECT Id,name,LastModifiedDate,TAFS_Lead_Type__c FROM Lead where OwnerId NOT IN :groupSetId  
                                AND (Status =: Label.TAFS_LeadStatus_Actively_Working OR Status =: Label.TAFS_LeadStatus_Awaiting_Documentation 
                                OR Status = :Label.TAFS_LeadStatus_Application_Out OR Status = : Label.TAFS_LeadStatus_Agreement_Ready OR Status = : Label.TAFS_LeadStatus_Agreement_Out)   
                                AND LastModifiedDate <: system.now().AddDays(Integer.valueOf(Label.TAFS_LeadOwner_Update_Days))];
        
        for(Lead leadObj : leadListForLastNdays){
            leadSetIdsForLastNdays.add(leadObj.Id);
        }
        if(!leadSetIdsForLastNdays.isEmpty() || leadSetIdsForLastNdays != null){
            taskListForNdays  = [SELECT Id,WhoId,CreatedDate FROM Task WHERE WhoId IN :leadSetIdsForLastNdays and CreatedDate >=: system.now().AddDays(Integer.valueOf(Label.TAFS_LeadOwner_Update_Days)) and CreatedDate <=: system.now()];
            eventListForNdays  = [SELECT Id,WhoId,CreatedDate FROM Event WHERE WhoId IN :leadSetIdsForLastNdays and CreatedDate >=: system.now().AddDays(Integer.valueOf(Label.TAFS_LeadOwner_Update_Days)) and CreatedDate <=: system.now()];
        }
        if(taskListForNdays.size() != 0){
            for(Task taskObject : taskListForNdays){
                leadSetIdsForLastNdays.remove(taskObject.WhoId); 
            }   
        }
        if(eventListForNdays.size() != 0){
            for(Event eventObject : eventListForNdays){
                leadSetIdsForLastNdays.remove(eventObject.WhoId); 
            }
        }
        allLeadIdsSet.addAll(leadSetIdsForLastNdays);
        
        //Start : Defect 165510 Lead Unassignment for 'No Contact'
        leadListForNoContact = [SELECT Id,name,TAFS_Lead_Type__c,Status FROM Lead where OwnerId NOT IN :groupSetId AND Status =: Label.TAFS_LeadStatus_No_Contact];
        
        for(Lead leadObj : leadListForNoContact){
                leadSetIdsForNoContact.add(leadObj.Id);
        }
        if(!leadSetIdsForNoContact.isEmpty() || leadSetIdsForNoContact != null){
            taskListForNoContact  = [SELECT Id,WhoId,CreatedDate,Status,Type FROM Task WHERE WhoId IN :leadSetIdsForNoContact AND Status =: 'Completed' AND Type =: 'Call No Contact'];
        }
        for(Task taskObj : taskListForNoContact){
            allLeadIdsSet.add(taskObj.WhoId);
        }
        //End : Defect 165510 Lead Unassignment for 'No Contact'
        
        if(!allLeadIdsSet.isEmpty() || allLeadIdsSet != null){
            leadList = [select id,name,TAFS_Lead_Type__c from lead where id IN :allLeadIdsSet];
        }
        return leadList;
    }
   
    global void execute(Database.BatchableContext BC, List<Lead> scope) {
        
        //variables
        Group grpObjectDirSales = [select Id,name from Group where Type =: Label.TAFS_Queue_Value and name =: Label.TAFS_Direct_Sales LIMIT 1];
        Group grpObjectNatAcc   = [select Id,name from Group where Type =: Label.TAFS_Queue_Value and name =: Label.TAFS_National_Accounts LIMIT 1];
        
        if(scope.size() > 0){
            for(Lead leadObject : scope)
            {
                if(leadObject.TAFS_Lead_Type__c == Label.TAFS_Direct_Sales){
                    if(grpObjectDirSales.id != null){
                        leadObject.OwnerId = grpObjectDirSales.Id;
                    }
                }
                else if(leadObject.TAFS_Lead_Type__c == Label.TAFS_National_Accounts){
                    if(grpObjectNatAcc.id != null){
                        leadObject.OwnerId = grpObjectNatAcc.Id;
                    }
                }            
            }
        }
        Database.update(scope);
    }   
    
    global void finish(Database.BatchableContext BC) {
        TAFS_BatchLeadStatusUpdateIfNoActivity leadStatusUpdateBatch = new TAFS_BatchLeadStatusUpdateIfNoActivity(); 
        database.executebatch(leadStatusUpdateBatch);
    }
}