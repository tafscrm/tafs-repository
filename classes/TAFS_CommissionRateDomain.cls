/****************************************************************************************
* Created By      :  Manoj Vootla
* Create Date     :  25/05/2016
* Description     :  Domain class for Commission Rate
* Modification Log:  Initial version.
***************************************************************************************/
public with sharing class TAFS_CommissionRateDomain {

    
    Map<Id, List<TAFS_Commission_Rate__c>> accountCRMap =  new Map<Id, List<TAFS_Commission_Rate__c>>();
    set<Id> accoundIdList = new set<Id>();
    
    public void onBeforeInsert(List<TAFS_Commission_Rate__c> commissionRatelist){

        //getting list of all the account ids associated with fuel discount records
        for(TAFS_Commission_Rate__c cr : commissionRatelist){
            accoundIdList.add(cr.TAFS_Ambassador_Name__c);
        }
        //getting all the existing fuel committments for the Accounts
        if(!accoundIdList.isEmpty())
        {
         for(Account accObj : [SELECT Id,(SELECT id,TAFS_Ambassador_Name__c,TAFS_Effective_Date__c,TAFS_Commission_Rate__c FROM Commission_Rates__r) FROM Account WHERE ID IN:accoundIdList])
         {
            accountCRMap.put(accObj.Id,accObj.Commission_Rates__r);
         }       
        }
        //logic to find the duplicate fuel committment record, if any
        for(TAFS_Commission_Rate__c cr : commissionRatelist){
            if(accountCRMap.containsKey(cr.TAFS_Ambassador_Name__c)){
                for(TAFS_Commission_Rate__c crOld : accountCRMap.get(cr.TAFS_Ambassador_Name__c)) {
                    if(crOld.TAFS_Effective_Date__c == cr.TAFS_Effective_Date__c)
                        cr.addError(Label.TAFS_Commission_Rate_Dup_Err_Msg);
                }              
            }
        }       
   
    }    
}