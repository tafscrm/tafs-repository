/* CLass Name : TAFS_Transaction_Name_Update_Batch
 * Description  : Update Month to date Factored Volume field on Account
 * Created By   : Raushan Anand
 * Created On   : 30-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                30-May-2015              Initial version.
 *
 *****************************************************************************************/
global class TAFS_Update_Month_To_Date_Batch implements Database.batchable<sObject>{ 
    
    global Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator('SELECT Id, TAFS_Client_Number__c,TAFS_Month_To_Date_Factored_Volume_New__c,TAFS_Previous_Month_Factored_Volume__c FROM Account'); 
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope){
        List<Id> accountId = new List<Id>();
        Integer currentMonth = DateTime.now().month();
        Integer currentYear = DateTime.now().year();
        for(Account acc : scope){
            accountId.add(acc.Id);
        }
        List<TAFS_Invoice_Batch__c> invBatchList = [SELECT Id,TAFS_Client_Id__c, TAFS_A_R_Amount__c,TAFS_Posted_Date__c FROM TAFS_Invoice_Batch__c WHERE TAFS_Client_Id__c IN : accountId AND TAFS_Posted_Date__c = THIS_MONTH AND TAFS_Status__c = 'Processed'];
        Map<Id,Double> clientFactorMap = new Map<Id,double>();
        for(TAFS_Invoice_Batch__c invBatchObj : invBatchList){
            if(invBatchObj.TAFS_Posted_Date__c != null && invBatchObj.TAFS_Posted_Date__c.month() == currentMonth && invBatchObj.TAFS_Posted_Date__c.year()==currentYear && invBatchObj.TAFS_Posted_Date__c <= DateTime.now()){
                if(invBatchObj.TAFS_A_R_Amount__c != null){
                    if(clientFactorMap.containsKey(invBatchObj.TAFS_Client_Id__c)){
                        clientFactorMap.put(invBatchObj.TAFS_Client_Id__c,clientFactorMap.get(invBatchObj.TAFS_Client_Id__c) + invBatchObj.TAFS_A_R_Amount__c);
                    }
                    else{
                        clientFactorMap.put(invBatchObj.TAFS_Client_Id__c,invBatchObj.TAFS_A_R_Amount__c);
                    }
                }
            }
        }
        for(Account objAcc : scope){
            objAcc.TAFS_Previous_Month_Factored_Volume__c = objAcc.TAFS_Month_To_Date_Factored_Volume_New__c;
            if(clientFactorMap.containsKey(objAcc.Id)){
                objAcc.TAFS_Month_To_Date_Factored_Volume_New__c = clientFactorMap.get(objAcc.Id);
            }
            else{
                objAcc.TAFS_Month_To_Date_Factored_Volume_New__c = 0;
            }
        }
        UPDATE scope;
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}