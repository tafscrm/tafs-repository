/****************************************************************************************
* Created By      :  Karthik Gulla
* Create Date     :  06/03/2017
* Description     :  Batch class to validate Fuel Consumptions
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_Validate_FuelConsumptions_Batch implements Database.Batchable<sObject> {
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<TAFS_Fuel_Consumptions__c>
    * Description      :  This method is used to pull the new Fuel Consumptions that are into the system
    **********************************************************************************************************************/
    global List<AggregateResult> start(Database.BatchableContext BC) {
        List<AggregateResult> lstFuelConsumptions = new List<AggregateResult>();
        TAFS_Task_Batch_Execution__c fuelConBatchTime = TAFS_Task_Batch_Execution__c.getInstance('Fuel Consumption Batch Time');
        for(AggregateResult tafsFuelCon: [SELECT TAFS_Client_ID__c ClientId, Max(TAFS_Transaction_Date__c) TransactionDate 
                                                    FROM TAFS_Fuel_Consumptions__c 
                                                    WHERE CreatedDate >:fuelConBatchTime.Timestamp__c 
                                                    AND TAFS_In_Network_Fuel_Stop__c = TRUE 
                                                    GROUP BY TAFS_Client_ID__c]){
            lstFuelConsumptions.add(tafsFuelCon);
        }
        return lstFuelConsumptions;
    }
    
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<TAFS_Fuel_Consumptions__c>
    * Return Type      :  Void
    * Description      :  This method is used to verify existing Open Fuel Opportunities and create if required.
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<AggregateResult> scope) {
        Map<String,Date> mapNewAccountFuelConsumptions = new Map<String,Date>();
        Map<String,Date> mapExistAccountFuelConsumptions = new Map<String,Date>();
        Map<Id,List<Opportunity>> mapAccountOpportunities = new Map<Id,List<Opportunity>>();
        Map<Id,List<AccountTeamMember>> mapAccountTeam = new Map<Id,List<AccountTeamMember>>();
        List<Opportunity> lstUpdateOpportunities = new List<Opportunity>();
        List<Opportunity> lstCreateOpportunities = new List<Opportunity>();
        List<String> lstAccountIds = new List<String>();

        Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Fuel').getRecordTypeId();
        TAFS_Task_Batch_Execution__c fuelConsBatchTime = TAFS_Task_Batch_Execution__c.getInstance('Fuel Consumption Batch Time');

        for(AggregateResult agResNewFuelCon:scope){
            lstAccountIds.add(String.valueOf(agResNewFuelCon.get('ClientId')));
            mapNewAccountFuelConsumptions.put(String.valueOf(agResNewFuelCon.get('ClientId')), Date.valueOf(agResNewFuelCon.get('TransactionDate')));
        }

        for(AggregateResult agRes:[SELECT MAX(TAFS_Transaction_Date__c) TransactionDate, TAFS_Client_ID__c ClientId
                                                            FROM TAFS_Fuel_Consumptions__c 
                                                            WHERE TAFS_Client_ID__c In :lstAccountIds
                                                            AND TAFS_In_Network_Fuel_Stop__c = TRUE 
                                                            AND CreatedDate <=:fuelConSBatchTime.Timestamp__c
                                                            GROUP BY TAFS_Client_ID__c]){
            mapExistAccountFuelConsumptions.put(String.valueOf(agRes.get('ClientId')), Date.valueOf(agRes.get('TransactionDate')));
        }

        for(Opportunity oppty:[SELECT Id, AccountId, Name FROM Opportunity WHERE AccountId In :lstAccountIds AND IsClosed = FALSE AND RecordTypeId = :recTypeId ORDER BY CreatedDate DESC]){
            if(mapAccountOpportunities.containsKey(oppty.AccountId)){
                mapAccountOpportunities.get(oppty.AccountId).add(oppty);
            }
            else{
                List<Opportunity> lstExistOpportunities = new List<Opportunity>();
                lstExistOpportunities.add(oppty);
                mapAccountOpportunities.put(oppty.AccountId,lstExistOpportunities);
            }
        }

        for(AccountTeamMember accTeamMember: [SELECT Id, AccountId, TeamMemberRole, UserId FROM AccountTeamMember 
                                                WHERE TeamMemberRole = 'Relationship Specialist'
                                                AND Account.TAFS_Company_Type__c = 'CARRIER'
                                                AND AccountId In :lstAccountIds ORDER BY CreatedDate DESC]){
            if(mapAccountTeam.containsKey(accTeamMember.AccountId)){
                mapAccountTeam.get(accTeamMember.AccountId).add(accTeamMember);
            }
            else{
                List<AccountTeamMember> lstAccTeam = new List<AccountTeamMember>();
                lstAccTeam.add(accTeamMember);
                mapAccountTeam.put(accTeamMember.AccountId,lstAccTeam);
            }
        }

        for(Account acc:[SELECT Id, Name FROM Account WHERE Id In :lstAccountIds AND TAFS_Company_Type__c = 'CARRIER']){
            if(mapNewAccountFuelConsumptions.get(acc.Id) != null){
                if(!mapExistAccountFuelConsumptions.containsKey(acc.Id) 
                    || (mapExistAccountFuelConsumptions.containsKey(acc.Id)
                        && mapNewAccountFuelConsumptions.containsKey(acc.Id)
                        && mapExistAccountFuelConsumptions.get(acc.Id) != null
                        && Math.abs((mapNewAccountFuelConsumptions.get(acc.Id)).daysBetween(mapExistAccountFuelConsumptions.get(acc.Id))) > 60)){
                    if(mapAccountOpportunities.containsKey(acc.Id)){
                        //Update the latest Open Fuel Opportunity to 'Closed Won'
                        mapAccountOpportunities.get(acc.Id)[0].StageName = 'Closed Won';
                        lstUpdateOpportunities.add(mapAccountOpportunities.get(acc.Id)[0]);
                    }
                    else{
                        //Create an Fuel Opportunity and mark it as Closed Won
                        Opportunity opp = new Opportunity();
                        opp.Name = 'Fuel - '+acc.Name;
                        opp.RecordTypeId = recTypeId;
                        opp.StageName = 'Closed Won';
                        if(mapAccountTeam.containsKey(acc.Id))
                            opp.OwnerId = mapAccountTeam.get(acc.Id)[0].UserId;
                        opp.CloseDate = System.today();
                        opp.AccountId = acc.Id;
                        lstCreateOpportunities.add(opp);
                    }
                }
            }
        }
        if(lstUpdateOpportunities.size() > 0)
            Database.update(lstUpdateOpportunities);
        if(lstCreateOpportunities.size() > 0)
            Database.insert(lstCreateOpportunities);
    }   
    
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post 'PO Status' Update
    **********************************************************************************************************************/  
    global void finish(Database.BatchableContext BC) {
        TAFS_Task_Batch_Execution__c cs = TAFS_Task_Batch_Execution__c.getInstance('Fuel Consumption Batch Time');
        if(cs.Job_Id__c != null){
            AsyncApexJob jobDetails = [Select Id, CompletedDate, JobType, Status from AsyncApexJob where Id=: cs.Job_Id__c];
            cs.Timestamp__c = jobDetails.CompletedDate;
            update cs;
        }
    }
}