/* Class Name   : TAFS_KanbanTaskIntegrationBatch_Test
 * Description  : This class performs unit tests of TAFS_Kanban_Integration_Handler
 * Created By   : Raushan Anand
 * Created On   : 2-Jan-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                2-Jan-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_KanbanTaskIntegrationBatch_Test {
	/* Method Name : testSetupData
     * Description : This method Insert/update the test data for all relevant object.
     * Return Type : void
     * Input Parameter : None
     */
    @testSetup 
    private static void testSetupData() {
        Test.StartTest();
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAccount = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
        TAFS_Invoice_Batch__c invoiceBatch = new TAFS_Invoice_Batch__c();
        invoiceBatch.Name = '123456';
        invoiceBatch.TAFS_Client_Number__c = '12345';
        invoiceBatch.TAFS_Client_ID__c=lstAccount[0].Id;
        ID rectypeid = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Kanban Task').getRecordTypeId();
        INSERT invoiceBatch;
        Task demoTask = new Task(RecordTypeId = rectypeid,OwnerId = UserInfo.getUserId(),Priority = 'Normal',Status='Open',WhatId=invoiceBatch.Id,TAFS_Kanban_Card_Id__c = '23456',TAFS_Blocked_State__c=true);
        INSERT demoTask;
        Test.StopTest();
    }
    private static testmethod void testScenario1(){
    	Test.StartTest();
        Map<String,String> responseHeader = new Map<String,String>();
        responseHeader.put('Content-Type', 'application/json');
        String responseBody = '{"numberoftasks": 1,    "page": 1,    "tasksperpage": 30,    "task":    [             {          "taskid": "282112",          "position": "234",          "type": "None",          "assignee": "Michaela R",          "title": "5046 - 501309",          "description": null,          "subtasks": "0",          "subtaskscomplete": "0",          "color": "#34a97b",          "priority": "Average",          "size": null,          "deadline": "31 Oct",          "extlink": "",          "tags": "",          "columnid": "archive_154",          "laneid": "157",          "blocked": 0,          "blockedreason": null,          "subtaskdetails": [],          "columnname": "Archive",          "lanename": "Standard EFS",          "createdorarchived": "2016-11-10 09:21:23",          "deadlineoriginalformat": "2016-10-31",          "columnpath": "Archive",          "leadtime": 0,          "loggedtime": 0,          "customfields":          [                         {                "fieldid": "24",                "name": "BS",                "type": "contributor",                "value": "Alex C",                "mandatory": false             },                         {                "fieldid": "26",                "name": "BAS",                "type": "contributor",                "value": "Heather G",                "mandatory": false             },                         {                "fieldid": "19",                "name": "Batch Number",                "type": "number",                "value": "123456",                "mandatory": true             },                         {                "fieldid": "23",                "name": "Client Number",                "type": "text",                "value": "12345",                "mandatory": true             }, 			{                "fieldid": "26",                "name": "VS",                "type": "contributor",                "value": "Heather G",                "mandatory": false             }, 			{                "fieldid": "26",                "name": "CS",                "type": "contributor",                "value": "Heather G",                "mandatory": false             }, 			{                "fieldid": "26",                "name": "FS",                "type": "contributor",                "value": "Heather G",                "mandatory": false             }          ],          "updatedat": "2016-11-10 09:21:23" 		}    ] }';
        Test.setMock(HttpCalloutMock.class, new TAFS_ReusableMockResponse_Test(200,'OK',responseBody,responseHeader));
        //Database.executeBatch(new TAFS_KanbanTaskIntegrationBatch(), 1);
        String scheduleTime = '0 0 23 * * ?';
        system.schedule('TAFS_ScheduleKanbanTaskTest', scheduleTime, new TAFS_ScheduleKanbanTaskIntegration());
        Test.StopTest();    
    }
}