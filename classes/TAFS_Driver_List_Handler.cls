/* CLass Name   : TAFS_Driver_List_Handler
 * Description  : Trigger methods for TAFS_Driver_List__c 
 * Created By   : Manoj M Vootla
 * Created On   : 27-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla              27-May-2015              Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_Driver_List_Handler {

/****************************************************************************************
* Method Name      :  updateClientIdOnDriverList
* Input Parameter  :  List of Driver Lists (List<TAFS_Driver_List__c>)
* Return Type      :  Void
* Description      :  This method populates CLient Id based upon Client key
***************************************************************************************/
    public static void updateClientIdOnDriverList(List<TAFS_Driver_List__c> driverListList){
    
        List<String> clientNoList = new List<String>();
        Map<String,String> clientNoIdMap = new Map<String,String>();
        for(TAFS_Driver_List__c dlist : driverListList){
            if(dlist.TAFS_Client_Number__c != null){
                clientNoList.add(dlist.TAFS_Client_Number__c);
            }
        }

        for(Account accObj : [SELECT Id,TAFS_Client_Number__c,Name FROM account WHERE TAFS_Client_Number__c IN: clientNoList]){
            clientNoIdMap.put(accObj.TAFS_Client_Number__c,accObj.Id);
        }
        for(TAFS_Driver_List__c dlist : driverListList){
            if(clientNoIdMap.containsKey(dlist.TAFS_Client_Number__c)){
                dlist.TAFS_Client_Name__c = clientNoIdMap.get(dlist.TAFS_Client_Number__c);
            }
        } 
    }
}