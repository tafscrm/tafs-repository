@isTest
private class TAFS_PreventDuplicateCommissionRateTest{

    /************************************************************************************
    * Method       :    testInsertDuplicateCommissionRateRecord
    * Description  :    Scenario to cover the prevention of Duplicate Commission Rate record insertion
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testInsertDuplicateCommissionRateRecord() {
       
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       //list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
       
       Recordtype rt = [select id,name from recordtype where name = 'Ambassador'];
       
       Account acc = new Account( 
                                    Name                                    = 'Test Account 001-',
                                    AccountNumber                           = '176455',
                                    TAFS_Client_Status__c                   = 'Client Created', 
                                    TAFS_Client_Credit_Check_Password__c    = 'test',
                                    TAFS_Lead_Source__c                     = 'test lead',                                    
                                    TAFS_Company_Type__c                    = 'Carrier',
                                    TAFS_Business_Type__c                   = 'test',
                                    TAFS_Currency_Type__c                   = 'USD',
                                    TAFS_UCC_Date__c                        =  Date.Today(),
                                    TAFS_Signed_Date__c                     =  Date.Today(),
                                    TAFS_Federal_Tax_ID__c                  = 'fed-001',
                                    TAFS_Default_Remittance_Method__c       = 'default',
                                    TAFS_Max_Invoice_Amount__c              =  200,
                                    Phone                                   = '9663066004',
                                    TAFS_Email__c                           = 'a@b.com',
                                    TAFS_Year_Started__c                    = 'test',
                                    TAFS_Office__c                          = 'US',
                                    TAFS_Client_Terms__c                    =  clientTermList[0].Id,
                                    BillingStreet                           = 'test',
                                    BillingState                            = 'test',
                                    BillingCity                             = 'test',
                                    BillingCountry                          = 'US',
                                    BillingPostalCode                       ='560100',
                                    TAFS_DOT__c                             = 'dot-001',
                                    TAFS_Client_Number__c                   = '12345',
                                    recordtypeid                            = rt.id
                                );
       insert acc;
       
       
       List<TAFS_Commission_Rate__c> commissionRateList = DataUtilTest_TAFS.createCommissionRates(2,acc.Id);

       
       Test.startTest();
       try{
       insert commissionRateList[0];
       insert commissionRateList[1];
       }
       catch(Exception e)
       {
        System.debug('The following exception has occurred: ' + e.getMessage());
       }
       Test.stopTest();
             
    }
}