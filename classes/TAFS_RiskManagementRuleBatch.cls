/* CLass Name   : TAFS_RiskManagementRuleBatch
 * Description  : Batch class to update Kanban Task everyday
 * Created By   : Pankaj Singh
 * Created On   : 25-Nov-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                25-Nov-2016            Initial version
 *
 *****************************************************************************************/
global class TAFS_RiskManagementRuleBatch implements Database.Batchable<TAFS_Invoice__c>, Database.Stateful {
   
    global map<Id,list<TAFS_Risk_Management_Rule__c>> debtorRuleMap;
    global map<Id,List<TAFS_Invoice__c>> debtorInvoicesMap ;
    global map<string,List<TAFS_Invoice__c>> invoiceAmountMap;
    global map<string,List<TAFS_Invoice__c>> duplicatePOMap;
    global list<TAFS_Risk_Management_Rule__c> applicableToAllRules;
    
    global TAFS_RiskManagementRuleBatch(){
    
        debtorRuleMap = new map<Id,list<TAFS_Risk_Management_Rule__c>>();
        debtorInvoicesMap = new map<Id,List<TAFS_Invoice__c>>();
        invoiceAmountMap = new map<string,List<TAFS_Invoice__c>>();
        duplicatePOMap = new map<string,List<TAFS_Invoice__c>>();
        applicableToAllRules = new list<TAFS_Risk_Management_Rule__c>();
        
    }
    
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<TAFS_Invoice__c>
    * Description      :  This method is used to find all the invoices created or updated in last 24 hours
    **********************************************************************************************************************/
    global list<TAFS_Invoice__c> start(Database.BatchableContext BC) 
    {  
       set<string> poNoSet = new set<string>();
       set<Id> debtorIds = new set<Id>();
       set<Id> clientIds = new set<Id>();      
       list<TAFS_Invoice__c> invoiceList = new list<TAFS_Invoice__c>();
       TAFS_Task_Batch_Execution__c cs1 = TAFS_Task_Batch_Execution__c.getInstance('Rule Management Batch Time');
       
       for(TAFS_Invoice__c invObj : [SELECT ID, TAFS_PO_No__c, TAFS_Client_Name__c,CreatedDate,TAFS_Balance__c,Name,TAFS_Invoice_Amount__c,Debtor__c ,TAFS_Client_Number__c,
                                     (SELECT ID,Description,CaseNumber,TAFS_Invoice__c,TAFS_PO_No__c FROM Cases__r) FROM TAFS_Invoice__c WHERE 
                                     TAFS_Purchased__c >=: cs1.Timestamp__c.addHours(-6) AND TAFS_Invoice_Batch__r.TAFS_Status__c!='void']){
                                     
               if(invObj.Cases__r.size()==0 && invObj.Debtor__c!=null){   
                   invoiceList.add(invObj);
                   poNoSet.add(invObj.TAFS_PO_No__c);
                   debtorIds.add(invObj.Debtor__c);
                   clientIds.add(invObj.TAFS_Client_Name__c);
               }                     
       }
       for(TAFS_Risk_Management_Rule__c rule : [SELECT ID,TAFS_Acceptable_Max__c,TAFS_Acceptable_Min__c,TAFS_Active__c,TAFS_Category__c,TAFS_Debtor__c, TAFS_Sub_Category__c,
                                                TAFS_Value__c FROM TAFS_Risk_Management_Rule__c WHERE TAFS_Active__c= true AND TAFS_Category__c = 'Applicable to All']){
                       
                       applicableToAllRules.add(rule);                                         
       }                         
                               
       for(TAFS_Risk_Management_Rule__c rmRule : [SELECT ID,TAFS_Acceptable_Max__c,TAFS_Acceptable_Min__c,TAFS_Active__c,TAFS_Category__c,TAFS_Debtor__c, 
                                                  TAFS_Sub_Category__c,TAFS_Value__c FROM TAFS_Risk_Management_Rule__c WHERE TAFS_Active__c= true AND TAFS_Debtor__c IN: debtorIds]){                                                  
                   
                   if(debtorRuleMap.containsKey(rmRule.TAFS_Debtor__c))
                        debtorRuleMap.get(rmRule.TAFS_Debtor__c).add(rmRule);
                   else{
                        List<TAFS_Risk_Management_Rule__c> lstRmRules = new List<TAFS_Risk_Management_Rule__c>();
                        lstRmRules.add(rmRule);
                        debtorRuleMap.put(rmRule.TAFS_Debtor__c,lstRmRules);
                   }                                           
       }
       string queryStr = 'SELECT ID, (SELECT ID, TAFS_Client_Name__c, TAFS_PO_No__c, TAFS_Balance__c,TAFS_Invoice_Amount__c,Debtor__c ,TAFS_Client_Number__c FROM Debtor__r WHERE '+  
                         'CreatedDate <= N_DAYS_AGO : '+ Label.TAFS_Risk_Management_Rule_Range_Comparison +' ORDER BY CreatedDate DESC LIMIT 1) FROM Account WHERE ID IN :debtorIds'; 
       /*string queryStr = 'SELECT ID, TAFS_Client_Name__c, TAFS_PO_No__c, TAFS_Balance__c,TAFS_Invoice_Amount__c,Debtor__c ,TAFS_Client_Number__c FROM TAFS_Invoice__c WHERE '+  
                         'Debtor__c IN :debtorIds AND CreatedDate <= N_DAYS_AGO : '+ Label.TAFS_Risk_Management_Rule_Range_Comparison +' ORDER BY CreatedDate DESC LIMIT 49999';*/
       //list<TAFS_Invoice__c> oldinvoicelist = Database.query(queryStr);
       for(Account debtorObj : Database.query(queryStr)){
            debtorInvoicesMap.put(debtorObj.Id, debtorObj.Debtor__r);  
       }
       string invAmountQuery = 'SELECT ID, TAFS_PO_No__c,CreatedDate, TAFS_Balance__c,TAFS_Invoice_Amount__c,Debtor__c ,TAFS_Client_Name__c FROM '+
                               'TAFS_Invoice__c WHERE Debtor__c IN :debtorIds AND TAFS_Client_Name__c IN:clientIds AND CreatedDate != Today AND CreatedDate = LAST_N_DAYS : '+ Label.TAFS_Risk_Management_Rule_Max_Invoice_Percent+ ' LIMIT 49999';
      
       for(TAFS_Invoice__c invRec : Database.query(invAmountQuery)){                                
                    
                        if(invoiceAmountMap.containsKey(string.valueOf(invRec.Debtor__c) + string.valueOf(invRec.TAFS_Client_Name__c)))
                            invoiceAmountMap.get(string.valueOf(invRec.Debtor__c) + string.valueOf(invRec.TAFS_Client_Name__c)).add(invRec);
                        else{
                            List<TAFS_Invoice__c> invList = new List<TAFS_Invoice__c>();
                            invList.add(invRec);
                            invoiceAmountMap.put(string.valueOf(invRec.Debtor__c) + string.valueOf(invRec.TAFS_Client_Name__c),invList);
                        }
       }
       for(TAFS_Invoice__c invRecObj : [SELECT ID, TAFS_Client_Name__c,TAFS_PO_No__c,CreatedDate, TAFS_Balance__c,TAFS_Invoice_Amount__c,Debtor__c ,TAFS_Client_Number__c 
                                        FROM TAFS_Invoice__c WHERE Debtor__c IN :debtorIds AND TAFS_PO_No__c IN : poNoSet]){
                                                            
                        if(duplicatePOMap.containsKey(invRecObj.Debtor__c + invRecObj.TAFS_PO_No__c)){
                            duplicatePOMap.get(invRecObj.Debtor__c + invRecObj.TAFS_PO_No__c).add(invRecObj);
                        }else{
                            List<TAFS_Invoice__c> invList = new List<TAFS_Invoice__c>();
                            invList.add(invRecObj);
                            duplicatePOMap.put(invRecObj.Debtor__c + invRecObj.TAFS_PO_No__c,invList);
                        } 
       }
       return invoiceList;       
    }
    
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<TAFS_Invoice__c>
    * Return Type      :  Void
    * Description      :  This method is used apply risk management rule to all the invoices and debtors
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext info, List<TAFS_Invoice__c> scope) {
        
        list<Case> caseList = new list<Case>();
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Flagged Invoices').getRecordTypeId(); 
        Id level1RiskMangementQueueId = [select Id from Group where Name =: Label.TAFS_Level_1_Risk_Management_Queue_Name and Type = 'Queue' limit 1].Id;       
        if(scope.size()>0){
            for(TAFS_Invoice__c invObj : scope){
                String descriptionstr = '';
                for(TAFS_Risk_Management_Rule__c allRule : applicableToAllRules){
                    if(allRule.TAFS_Sub_Category__c == 'Duplicate PO#'){
                        if(duplicatePOMap.containsKey(invObj.Debtor__c + invObj.TAFS_PO_No__c) && duplicatePOMap.get(invObj.Debtor__c + invObj.TAFS_PO_No__c).size()>1){
                            descriptionstr = descriptionstr +'\n'+ 'Category =' + allRule.TAFS_Category__c +' ; Sub-Category = '+ allRule.TAFS_Sub_Category__c + ' ; Value= '+
                                             allRule.TAFS_Value__c + '; Acceptable Min = '+ allRule.TAFS_Acceptable_Min__c + '; Acceptable Max =' + allRule.TAFS_Acceptable_Max__c;                                    
                        }        
                    }
                }  
                if(debtorRuleMap.containsKey(invObj.Debtor__c)){              
                    for(TAFS_Risk_Management_Rule__c rml : debtorRuleMap.get(invObj.Debtor__c)){
                        if(rml.TAFS_Category__c == 'PO'){
                            if(rml.TAFS_Sub_Category__c == 'Characters in PO#'){
                                if(rml.TAFS_Value__c!=null){
                                    set<Decimal> poSet = new set<Decimal>();                                    
                                    for(String val : rml.TAFS_Value__c.split(';')){
                                        poSet.add(Decimal.valueOf(val));
                                    }
                                    if(invObj.TAFS_PO_No__c!=null && !poSet.contains(invObj.TAFS_PO_No__c.length())){                                  
                                            descriptionstr = descriptionstr +'\n'+ 'Category =' + rml.TAFS_Category__c +' ; Sub-Category = '+ rml.TAFS_Sub_Category__c + ' ; Value= '+
                                                             rml.TAFS_Value__c + '; Acceptable Min = '+ rml.TAFS_Acceptable_Min__c + '; Acceptable Max =' + rml.TAFS_Acceptable_Max__c;                                    
                                           
                                    }
                                }
                            }
                            if(rml.TAFS_Sub_Category__c == 'PO# Format'){
                                if(invObj.TAFS_PO_No__c!=null){
                                    Pattern alphaNumPattern = Pattern.compile(rml.TAFS_Value__c);
                                    if(!alphaNumPattern.matcher(invObj.TAFS_PO_No__c).matches()){ 
                                         descriptionstr = descriptionstr + '\n'+ 'Category =' + rml.TAFS_Category__c +' ; Sub-Category = '+ rml.TAFS_Sub_Category__c + ' ; Value= '+
                                                          rml.TAFS_Value__c + '; Acceptable Min = '+ rml.TAFS_Acceptable_Min__c + '; Acceptable Max =' + rml.TAFS_Acceptable_Max__c;                                    
                                    }
                                }  
                            }
                            if(rml.TAFS_Sub_Category__c == 'Range Comparison with Previous PO#'){
                                if(invObj.TAFS_PO_No__c!=null && debtorInvoicesMap.containsKey(invObj.Debtor__c) && debtorInvoicesMap.get(invObj.Debtor__c).size()>0){                                    
                                    /*if(!(Decimal.valueOf(invObj.TAFS_PO_No__c.replaceAll('[a-z|A-Z|]', '')) - Decimal.valueOf(debtorInvoicesMap.get(invObj.Debtor__c)[0].TAFS_PO_No__c.replaceAll('[a-z|A-Z|]', '')) > rml.TAFS_Acceptable_Min__c) && 
                                    !((Decimal.valueOf(invObj.TAFS_PO_No__c.replaceAll('[a-z|A-Z|]', '')) - Decimal.valueOf(debtorInvoicesMap.get(invObj.Debtor__c)[0].TAFS_PO_No__c.replaceAll('[a-z|A-Z|]', ''))) < rml.TAFS_Acceptable_Max__c )) {*/
                                    if(!(Decimal.valueOf(invObj.TAFS_PO_No__c.replaceAll('[^\\d]', '')) - Decimal.valueOf(debtorInvoicesMap.get(invObj.Debtor__c)[0].TAFS_PO_No__c.replaceAll('[^\\d]', '')) > rml.TAFS_Acceptable_Min__c && 
                                    Decimal.valueOf(invObj.TAFS_PO_No__c.replaceAll('[^\\d]', '')) - Decimal.valueOf(debtorInvoicesMap.get(invObj.Debtor__c)[0].TAFS_PO_No__c.replaceAll('[^\\d]', '')) < rml.TAFS_Acceptable_Max__c) 
                                    //&& Decimal.valueOf(invObj.TAFS_PO_No__c.replaceAll('[^\\d]', '')) - Decimal.valueOf(debtorInvoicesMap.get(invObj.Debtor__c)[0].TAFS_PO_No__c.replaceAll('[^\\d]', '')) <> 0
                                    ) {                                        
                                         descriptionstr = descriptionstr + '\n'+ 'Category =' + rml.TAFS_Category__c +' ; Sub-Category = '+ rml.TAFS_Sub_Category__c + ' ; Value= '+
                                                          rml.TAFS_Value__c + '; Acceptable Min = '+ rml.TAFS_Acceptable_Min__c + '; Acceptable Max =' + rml.TAFS_Acceptable_Max__c;                                                                          
                                    }   
                                }
                            }                    
                        }
                        if(rml.TAFS_Category__c == 'Invoice Amount'){
                            if(rml.TAFS_Sub_Category__c == 'Max Invoice Amount %'){
                                Decimal amount = 0;
                                if(invoiceAmountMap.containsKey(string.valueOf(invObj.Debtor__c) + string.valueOf(invObj.TAFS_Client_Name__c))){
                                    for(TAFS_Invoice__c invObjRec : invoiceAmountMap.get(string.valueOf(invObj.Debtor__c) + string.valueOf(invObj.TAFS_Client_Name__c))){
                                        if(invObjRec.TAFS_Invoice_Amount__c!=null){
                                            amount = amount + invObjRec.TAFS_Invoice_Amount__c; 
                                        }    
                                    }
                                    if(amount>0){
                                        amount = amount/invoiceAmountMap.get(string.valueOf(invObj.Debtor__c) + string.valueOf(invObj.TAFS_Client_Name__c)).size();
                                        if(invObj.TAFS_Invoice_Amount__c > (Decimal.valueOf(rml.TAFS_Value__c)/100)*amount){
                                            descriptionstr = descriptionstr + '\n'+ 'Category =' + rml.TAFS_Category__c +' ; Sub-Category = '+ rml.TAFS_Sub_Category__c + ' ; Value= '+
                                                             rml.TAFS_Value__c + '; Acceptable Min = '+ rml.TAFS_Acceptable_Min__c + '; Acceptable Max =' + rml.TAFS_Acceptable_Max__c;                                 
                                        }
                                    }
                                }
                            }
                        }                        
                    }
                    
                    if(descriptionstr!=null && descriptionstr!=''){ 
            
                        Case caseObj             = new Case();
                        caseObj.Description      = descriptionstr;
                        caseObj.TAFS_Invoice__c  = invObj.Id;
                        caseObj.Subject          = 'Flagged Invoice : '+invObj.Name;
                        caseObj.Status           = 'New';
                        caseObj.AccountId        = invObj.Debtor__c;
                        caseObj.RecordTypeId     = recTypeId;
                        caseObj.OwnerId          = level1RiskMangementQueueId;
                        caseList.add(caseObj);
                    }               
                }
            }
            if(!caseList.isEmpty()){
                insert caseList;
            }        
        }               
    }
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post risk management rules have been applied.
    **********************************************************************************************************************/    
    global void finish(Database.BatchableContext info) {
    
        TAFS_Task_Batch_Execution__c cs = TAFS_Task_Batch_Execution__c.getInstance('Rule Management Batch Time');
        AsyncApexJob jobDetails = [Select Id,CompletedDate, JobType,Status from AsyncApexJob where Id=: cs.Job_Id__c];
        cs.Timestamp__c = jobDetails.CompletedDate;
        update cs;
    }
}