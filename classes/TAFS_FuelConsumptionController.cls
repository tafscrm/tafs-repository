/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  14/04/2016
* Description     :  Controller class for fuel consumtion inline vf page
* Modification Log:  Initial version.
***************************************************************************************/
public class TAFS_FuelConsumptionController{
    
    private ApexPages.StandardController controller {get; set;}
    public Account accountObj{get;set;}
    List<TAFS_Fuel_Consumptions__c> fcList =  new List<TAFS_Fuel_Consumptions__c>();
    List<Account> accList =  new List<Account>();
   
    
    public TAFS_FuelConsumptionController(ApexPages.StandardController controller){
        
        this.controller = controller;
        this.accountObj = (Account)controller.getRecord();
        if(accountObj.id!=null)
            accList = new TAFS_SObjectSelector().selectByAccountId(new set<string>{accountObj.Id});
        if(!accList.isEmpty())
            accountObj = accList[0];
       
    }
    /************************************************************************************
    * Method       :    updateAccount
    * Description  :    Method to call the Fuel consumtion service to update account.
    * Parameter    :    Nil    
    * Return Type  :    void
    *************************************************************************************/
    public void updateAccount(){
    
        list<TAFS_FuelConsumptionService.FCStub> requests = new list<TAFS_FuelConsumptionService.FCStub>();
        
        if(accountObj.Id !=null){
            requests.add(new TAFS_FuelConsumptionService.FCStub(accountObj));            
        } 
        if(!requests.isEmpty()){
           
            TAFS_FuelConsumptionService.updateAccount(requests);
        }   
    }
}