/**
 * @class Mock Callout TEST CLASS
 * @description Test methods for mock REST/SOAP callouts
 *
 * @author  Nick Worth
 * @created 10/28/2014
 * @version 1.0
 */

@isTest
public class MockCalloutTest implements HttpCalloutMock {

    private HttpResponse response;

    public MockCalloutTest(String body) {
        response = new HttpResponse();
        response.setBody(body);
        response.setStatusCode(200);
    }

    public HTTPResponse respond(HTTPRequest req) {
        return response;
    }

    public testmethod static void xmlTest() {
        String success = '<?xml version="1.0" encoding="UTF-8"?> <rsp stat="ok" version="1.0"> <api_key>6d800bfaab63274b1b908dacd8544936</api_key> </rsp>';        
  
        HttpCalloutMock mock = new MockCalloutTest(success);
        Test.setMock(HttpCalloutMock.class, mock);
        
        List<Lead> leadLst = DataUtilTest_TAFS.createleadsNoContact(1);
        List<Contact> conLst = DataUtilTest_TAFS.createContactsForLead(1,leadLst[0].id);
      User u = [select id,name,profile.name from user where profile.name != 'System Administrator' and profile.name != 'TAFS_Integration_User' and profile.name like '%TAFS_%' and isactive = true limit 1];
      Test.startTest();    
      leadLst[0].ownerid = u.id;
      leadLst[0].email = 'vmanoj@xyz.com';
      update leadLst[0]; 
      Pardot.createProspectsByLead(leadLst);
        //HttpResponse response = Pardot.httpCallout('http://testurl.com', success);
       //System.assertEquals(200, response.getStatusCode());        
        //System.assertEquals(response.getBody(), success );

    }

}