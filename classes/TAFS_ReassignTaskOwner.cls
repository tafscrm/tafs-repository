global without sharing class TAFS_ReassignTaskOwner {
    webservice static void reassignTask(String taskId){
        System.debug('Task Id'+ taskId);
        Task t = [Select Id, OwnerId From Task where Id =: taskId Limit 1];
        t.OwnerId = UserInfo.getUserId();
        System.debug('Task'+t);
        Update t;
        //return taskId + 'This is it' + UserInfo.getUserId();
    }
}