/* Class Name   : TAFS_VerCollectionComponentController
 * Description  : Populate Invoice in email being sent to Debtor Contact
 * Created By   : Pankaj Singh
 * Created On   : 9-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                9-Aug-2016              Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_VerCollectionComponentController{

    public List<TAFS_Invoice__c> invoiceList{set;} 
    public ID invDebtorId {get;set;}
    public Set<Id> invoiceIdSet = new Set<Id>();
    
    public List<TAFS_Invoice__c> getInvoiceList(){
    
        if(invDebtorId!=null){
            string queryStr = 'SELECT ID,TAFS_Balance__c ,Name, TAFS_Invoice_Amount__c,TAFS_PO_No__c, TAFS_Client_Name__r.Name ,Debtor__c  ,TAFS_Invoice_Batch__r.TAFS_Posted_Date__c ,CreatedDate , (SELECT ID,TAFS_Action__c,TAFS_Action_Date__c FROM '+ 
                              'Verificatons_Collections__r WHERE TAFS_Action__c IN (\'W/P\' ,\'C/B\')) FROM TAFS_Invoice__c WHERE (TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO :'+Label.TAFS_Collection_Batch_Label+' OR TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO :'+Label.TAFS_Collection_Batch_Label_60Days+
                              ') AND TAFS_Balance__c >0 AND Debtor__c =:invDebtorId';
                                                                                    
            for(TAFS_Invoice__c invObj : Database.query(queryStr)){
                if(invObj.Verificatons_Collections__r.size()==0){                   
                    invoiceIdSet.add(invObj.id);
                }
            }
            string queryStrVer = 'SELECT ID,Name,TAFS_Invoice_Amount__c,TAFS_Balance__c,TAFS_PO_No__c,Debtor__c,TAFS_Client_Name__r.Name,TAFS_Invoice_Batch__r.TAFS_Posted_Date__c ,CreatedDate , (SELECT ID,TAFS_Action__c,TAFS_Action_Date__c FROM '+
                                 'Verificatons_Collections__r WHERE TAFS_Action_Date__c < TODAY AND TAFS_Action__c IN (\'W/P\' ,\'C/B\')) FROM TAFS_Invoice__c WHERE (TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO :' + Label.TAFS_Collection_Batch_Label+' OR TAFS_Invoice_Batch__r.TAFS_Posted_Date__c = N_DAYS_AGO :'+Label.TAFS_Collection_Batch_Label_60Days+
                                 ') AND TAFS_Balance__c >0 AND Debtor__c =:invDebtorId';
                                            
            for(TAFS_Invoice__c invObjRec : Database.query(queryStrVer)){
                if(invObjRec.Verificatons_Collections__r.size()>0){                    
                    invoiceIdSet.add(invObjRec.id);
                }
            }            
            return ([Select Id,Name,TAFS_Client_Name__c,TAFS_PO_No__c,TAFS_Client_Name__r.Name,TAFS_Invoice_Amount__c FROM TAFS_Invoice__c WHERE ID IN : invoiceIdSet]);                          
        }
        else{
            return null;
        }    
    }
    public TAFS_VerCollectionComponentController(){
        
    }
}