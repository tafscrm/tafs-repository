/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  14/04/2016
* Description     :  Domain class for Fuel Discounts
* Modification Log:  Initial version.
***************************************************************************************/
public with sharing class TAFS_FuelDiscountDomain {

    private static boolean prohibitBeforeInsertTrigger = false;
    
    Map<Id, List<Fuel_Discount__c>> accountFDMap =  new Map<Id, List<Fuel_Discount__c>>();
    set<Id> accoundIdList = new set<Id>();
    
    public void onBeforeInsert(List<Fuel_Discount__c> fuelDiscountlist){
        
        if(prohibitBeforeInsertTrigger)
        {
            return;
        }
        //getting list of all the account ids associated with fuel discount records
        for(Fuel_Discount__c fc : fuelDiscountlist){
            accoundIdList.add(fc.TAFS_Client_ID__c);
        }
        //getting all the exiating fuel discounts for the Accounts
        for(Account accObj : [SELECT Id,(SELECT id,TAFS_Effective_Date_Gallon__c,TAFS_Effective_Date_Liter__c FROM Fuel_Discounts__r) FROM Account WHERE ID IN:accoundIdList])
        {
            accountFDMap.put(accObj.Id,accObj.Fuel_Discounts__r);
        }
        //logic to find the duplicate fuel discount record, if any
        for(Fuel_Discount__c fd : fuelDiscountlist){
            if(accountFDMap.containsKey(fd.TAFS_Client_ID__c)){
                for(Fuel_Discount__c fdOld : accountFDMap.get(fd.TAFS_Client_ID__c)) {
                    if(fdOld.TAFS_Effective_Date_Gallon__c == fd.TAFS_Effective_Date_Gallon__c || fdOld.TAFS_Effective_Date_Liter__c == fd.TAFS_Effective_Date_Liter__c)
                        fd.addError('You alreday have an existing fuel discount record with the same effective date. please change the effective date and try again');
                }              
            }
        }       
        prohibitBeforeInsertTrigger = true;    
    }    
}