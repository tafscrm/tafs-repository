@isTest
private class TAFS_UpdateAccountTeamMemberTest{

    /************************************************************************************
    * Method       :    testMethod1
    * Description  :    Test Class for the Batch Class TAFS_UpdateAccountTeamMember
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testMethod void testMethod1() {
       
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1); 
       list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(13,clientTermList[0].Id);
       list<AccountTeamMember> ATM = new list<AccountTeamMember>(); 
       AccountTeamMember ATM1 = new AccountTeamMember(AccountId=lstAcct[0].Id,UserId=UserInfo.getUserId(),TeamMemberRole='Relationship Specialist');      
       AccountTeamMember ATM2 = new AccountTeamMember(AccountId=lstAcct[1].Id,UserId=UserInfo.getUserId(),TeamMemberRole='Sales Account Manager');    
       AccountTeamMember ATM3 = new AccountTeamMember(AccountId=lstAcct[2].Id,UserId=UserInfo.getUserId(),TeamMemberRole='Onboarding Specialist');
       AccountTeamMember ATM4 = new AccountTeamMember(AccountId=lstAcct[3].Id,UserId=UserInfo.getUserId(),TeamMemberRole='Sales Account Manager – Referred');
       AccountTeamMember ATM5 = new AccountTeamMember(AccountId=lstAcct[4].Id,UserId=UserInfo.getUserId(),TeamMemberRole='Sales Account Manager – NA');
       AccountTeamMember ATM6 = new AccountTeamMember(AccountId=lstAcct[5].Id,UserId=UserInfo.getUserId(),TeamMemberRole='National Account Manager');
       AccountTeamMember ATM7 = new AccountTeamMember(AccountId=lstAcct[6].Id,UserId=UserInfo.getUserId(),TeamMemberRole='National Account Manager – Referred');
       AccountTeamMember ATM8 = new AccountTeamMember(AccountId=lstAcct[7].Id,UserId=UserInfo.getUserId(),TeamMemberRole='VP of Sales and Marketing');
       AccountTeamMember ATM9 = new AccountTeamMember(AccountId=lstAcct[8].Id,UserId=UserInfo.getUserId(),TeamMemberRole='VP of Sales and Marketing – Referred');
       AccountTeamMember ATM10 = new AccountTeamMember(AccountId=lstAcct[9].Id,UserId=UserInfo.getUserId(),TeamMemberRole='VP of Sales and Marketing – NA');
       AccountTeamMember ATM11 = new AccountTeamMember(AccountId=lstAcct[10].Id,UserId=UserInfo.getUserId(),TeamMemberRole='VP of Sales and Marketing – Retained');
       AccountTeamMember ATM12 = new AccountTeamMember(AccountId=lstAcct[11].Id,UserId=UserInfo.getUserId(),TeamMemberRole='VP of Sales and Marketing – NA Referred');
       AccountTeamMember ATM13 = new AccountTeamMember(AccountId=lstAcct[12].Id,UserId=UserInfo.getUserId(),TeamMemberRole='Underwriter');
       
       ATM.add(ATM1);
       ATM.add(ATM2);
       ATM.add(ATM3);
       ATM.add(ATM4);
       ATM.add(ATM5);
       ATM.add(ATM6);
       ATM.add(ATM7);
       ATM.add(ATM8);
       ATM.add(ATM9);
       ATM.add(ATM10);
       ATM.add(ATM11);
       ATM.add(ATM12);
       ATM.add(ATM13);
       
       insert ATM;
       
       update(lstAcct);
      
       
       Test.startTest();          
          TAFS_UpdateAccountTeamMember obj  = new TAFS_UpdateAccountTeamMember();
          DataBase.executeBatch(obj);
       Test.stopTest();
       
       system.assertEquals(ATM13.TeamMemberRole,'Underwriter');
    }
}