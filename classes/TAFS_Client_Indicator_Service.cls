/* CLass Name   : TAFS_Client_Indicator_Service
 * Description  : Class to update indicator fields on Client
 * Created By   : Raushan Anand
 * Created On   : 16-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              16-Aug-2016              Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_Client_Indicator_Service{
    public Static String getBatchProcessedDetails(Id accId){
        String result;
        List<TAFS_Invoice_Batch__c> batchList = [SELECT Id FROM TAFS_Invoice_Batch__c WHERE TAFS_Client_ID__c=:accId AND TAFS_Blocked_State__c=: TRUE AND (TAFS_Status__c != 'Void' OR TAFS_Status__c != 'Processed')];
        result=batchList.isEmpty()?'Neutral':'Negative';
        return result;
    }
    public Static String getRoadsideAssistanceClaims(Id accId){
        String result;
        List<TAFS_Roadside_Assistance_Claims__c> claimsList = [SELECT Id FROM TAFS_Roadside_Assistance_Claims__c WHERE TAFS_Client_ID__c=:accId AND TAFS_Date_of_Claim__c = LAST_N_DAYS:30];
        result=claimsList.isEmpty()?'Neutral':'Negative';
        return result;
    }
    public Static String getFactoredVolumevsAverage(Id accId){
        String result;
        Double totalFactoredVolume =0,newFactoredVolume=0,percentageVolume=0;
        
        for(TAFS_Invoice_Batch__c objBatch : [Select Id, TAFS_A_R_Amount__c, TAFS_Posted_Date__c FROM TAFS_Invoice_Batch__c WHERE TAFS_Client_ID__c=: accId AND TAFS_Posted_Date__c = LAST_N_DAYS : 74]){
            if(objBatch.TAFS_Posted_Date__c < DateTime.now()-14 && objBatch.TAFS_Posted_Date__c >= DateTime.now()-74 && objBatch.TAFS_A_R_Amount__c != null){
                totalFactoredVolume += objBatch.TAFS_A_R_Amount__c;
            }
            else if(objBatch.TAFS_Posted_Date__c >= DateTime.now()-14 && objBatch.TAFS_A_R_Amount__c != null){
                newFactoredVolume += objBatch.TAFS_A_R_Amount__c;
            }
        }
        if(totalFactoredVolume !=0 && newFactoredVolume != 0){
            totalFactoredVolume = totalFactoredVolume/4.28;
            percentageVolume = (newFactoredVolume-totalFactoredVolume)/totalFactoredVolume;
            if(percentageVolume >=0.2)
                result = 'Positive';
            else if(percentageVolume >= -0.2 && percentageVolume < 0.2)
                result = 'Neutral';
            else if(percentageVolume < -0.2 && percentageVolume >= -0.5)
                result = 'Warning';
            else if(percentageVolume < -0.5)
                result = 'Negative';
            else
                result = 'Neutral';
        }
        else
            result = 'Neutral';
        return result;
    }
    public Static String getFuelVolumevsAverage(Id accId){
        String result;
        Double totalFuelVolume =0,netFuelVolume=0,percentageVolume=0;
        for(TAFS_Fuel_Consumptions__c objFuelCon : [Select Id, TAFS_Total_Gallons__c, TAFS_Transaction_Date__c FROM TAFS_Fuel_Consumptions__c WHERE TAFS_Client_ID__c=: accId AND TAFS_Transaction_Date__c = LAST_N_DAYS : 74]){
            if(objFuelCon.TAFS_Transaction_Date__c < DateTime.now()-14 && objFuelCon.TAFS_Transaction_Date__c >= DateTime.now()-74 && objFuelCon.TAFS_Total_Gallons__c != null){
                totalFuelVolume += objFuelCon.TAFS_Total_Gallons__c;
            }
            else if(objFuelCon.TAFS_Transaction_Date__c >= DateTime.now()-14 && objFuelCon.TAFS_Total_Gallons__c != null){
                netFuelVolume += objFuelCon.TAFS_Total_Gallons__c;
            }
        }
        if(totalFuelVolume !=0 && netFuelVolume != 0){
            totalFuelVolume = totalFuelVolume/4.28;
            percentageVolume = (netFuelVolume-totalFuelVolume)/totalFuelVolume;
            if(percentageVolume >=0.2)
                result = 'Positive';
            else if(percentageVolume >= -0.2 && percentageVolume < 0.2)
                result = 'Neutral';
            else if(percentageVolume < -0.2 && percentageVolume >= -0.5)
                result = 'Warning';
            else if(percentageVolume < -0.5)
                result = 'Negative';
            else
                result = 'Neutral';
        }
        else
            result = 'Neutral';
        return result;
    }
    public Static String getOverallClientHealth(Account objAccount){
        String result;
        if(objAccount != null){
            if((objAccount.TAFS_Factored_Volume_vs_Average__c == 'Positive' || objAccount.TAFS_Factored_Volume_vs_Average__c == 'Neutral')
            && (objAccount.TAFS_Fuel_Volume_vs_Average__c == 'Positive' || objAccount.TAFS_Fuel_Volume_vs_Average__c == 'Neutral')
            && (objAccount.TAFS_Batches_Blocked_Value__c == 'Positive'  || objAccount.TAFS_Batches_Blocked_Value__c == 'Neutral')
            && (objAccount.TAFS_Recent_Roadside_Claims__c == 'Positive' || objAccount.TAFS_Recent_Roadside_Claims__c == 'Neutral')
            && (objAccount.Client_Status_Health_Value__c == 'Positive'  || objAccount.Client_Status_Health_Value__c == 'Neutral')
            && (objAccount.TAFS_Fuel_vs_Commitment_Value__c == 'Positive' || objAccount.TAFS_Fuel_vs_Commitment_Value__c == 'Neutral' )
            && (objAccount.Web_Activity_Value__c == 'Positive' || objAccount.Web_Activity_Value__c == 'Neutral')
            && (objAccount.Notice_of_Termination_Value__c == 'Positive' ||  objAccount.Notice_of_Termination_Value__c == 'Neutral')){
                   result='Positive';
               }
            else if((objAccount.TAFS_Factored_Volume_vs_Average__c == 'Warning' || objAccount.TAFS_Factored_Volume_vs_Average__c == 'Negative') || 
                    (objAccount.TAFS_Fuel_Volume_vs_Average__c == 'Warning' ||  objAccount.TAFS_Fuel_Volume_vs_Average__c == 'Negative')||
                    (objAccount.TAFS_Batches_Blocked_Value__c == 'Warning' ||  objAccount.TAFS_Batches_Blocked_Value__c == 'Negative')||
                    (objAccount.TAFS_Recent_Roadside_Claims__c == 'Warning' ||  objAccount.TAFS_Recent_Roadside_Claims__c == 'Negative')||
                    (objAccount.Client_Status_Health_Value__c == 'Warning' ||  objAccount.Client_Status_Health_Value__c == 'Negative')||
                    (objAccount.TAFS_Fuel_vs_Commitment__c == 'Warning' ||  objAccount.TAFS_Fuel_vs_Commitment__c == 'Negative')||
                    (objAccount.Web_Activity_Value__c == 'Warning' ||  objAccount.Web_Activity_Value__c == 'Negative')||
                    (objAccount.Notice_of_Termination_Value__c == 'Warning' ||  objAccount.Notice_of_Termination_Value__c == 'Negative')){
                        result='Negative';
                    }
            else{
                result = 'Neutral';
            }
            
        }
        else
            result = 'Neutral';
            return result;
    }
    
    public Static String getFuelvsCommitmentDetails(Account objAccount){
        String result;
        Double netValue=0;
        if(Date.Today().Day() >= 10){
            if(objAccount.TAFS_Difference_Usage_vs_Committed__c != null && objAccount.TAFS_Prorated_Committment_MTD__c != null && objAccount.TAFS_Difference_Usage_vs_Committed__c != 0 &&  objAccount.TAFS_Prorated_Committment_MTD__c != 0){
                netValue = objAccount.TAFS_Difference_Usage_vs_Committed__c / objAccount.TAFS_Prorated_Committment_MTD__c;
                if(netValue >=0.2)
                    result ='Positive';
                else if(netValue >= -0.05 && netValue <0.2)
                    result ='Neutral';
                else if(netValue < -0.05 && netValue > -0.2)
                    result='Warning';
                else
                    result='Negative';
                        
            }
            else{
                result = 'Neutral';
            }
        }
        return result;
    }
    public Static String getNoticeOfTerminationDetails(Account objAccount){
        String result;
        if(objAccount.TAFS_Notice_Of_Termination_Count__c > 0)
            result = 'Negative';
        else
            result = 'Neutral';
        return result;
    }
    public Static String getWebActivityDetails(Account objAccount){
        String result;
        if(objAccount.TAFS_Last_Website_Login_Date__c != null){
            integer daysBetween = objAccount.TAFS_Last_Website_Login_Date__c.Date().daysBetween(Date.Today());
            System.Debug('###RA-->daysBetween'+daysBetween);
            if(daysBetween <= 7)
                result = 'Positive';
            else if (daysBetween > 7 && daysBetween <=30)
                result ='Warning';
            else
                result ='Negative';
        }
        else{
            result ='Negative';
        }
        
        return result;
    }
    public Static String getClientStatusHealthDetails(Account objAccount){
        String result;
        
        if(objAccount.TAFS_Client_Status__c == 'Active - In Review' )
            result = 'Warning';
        else if(objAccount.TAFS_Client_Status__c == 'Freeze - No Insurance' || objAccount.TAFS_Client_Status__c == 'Freeze - No Authority' || objAccount.TAFS_Client_Status__c == 'Freeze - Out of Service' || objAccount.TAFS_Client_Status__c == 'Terminated')
            result = 'Negative';
        else
            result = 'Neutral';
            
        return result;
    }
}