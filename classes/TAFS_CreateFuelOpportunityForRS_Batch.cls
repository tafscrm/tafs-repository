/****************************************************************************************
* Created By      :  Karthik Gulla
* Create Date     :  16/02/2017
* Description     :  Batch class to Create a Fuel Opportunity when a Relationship specialist
                     is added to Client
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_CreateFuelOpportunityForRS_Batch implements Database.Batchable<sObject> {
    /**********************************************************************************************************************
    * Method Name      :  start
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  List<AccountTeamMember>
    * Description      :  This method is used to find the relationship specialists added to Client
    **********************************************************************************************************************/
    global List<AccountTeamMember> start(Database.BatchableContext BC) {
        String accTeamMemberQueryStr = 'SELECT Id, AccountId, Account.Name, TeamMemberRole, UserId FROM AccountTeamMember WHERE Account.TAFS_Company_Type__c = \'CARRIER\' AND TeamMemberRole = \'Relationship Specialist\' AND (CreatedDate = YESTERDAY OR CreatedDate = TODAY)';
        return Database.query(accTeamMemberQueryStr);
    }
    
    /**********************************************************************************************************************
    * Method Name      :  execute
    * Input Parameter  :  Database.BatchableContext BC, List<AccountTeamMember>
    * Return Type      :  Void
    * Description      :  This method is create an Fuel Opportunity for Relationship Specialist
    **********************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<AccountTeamMember> scope) {
        List<Opportunity> lstOpportunities = new List<Opportunity>();
        List<Id> lstAccountIds = new List<Id>();
        Map<Id,List<Opportunity>> mapAccountOpportunities = new Map<Id,List<Opportunity>>();
        Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Fuel').getRecordTypeId();

        for(AccountTeamMember accTeamMember:scope){
            lstAccountIds.add(accTeamMember.AccountId);
        }

        for(Opportunity oppty:[SELECT Id, AccountId, Name FROM Opportunity WHERE AccountId In :lstAccountIds AND IsClosed = FALSE AND RecordTypeId = :recTypeId ORDER BY CreatedDate Desc]){
            if(mapAccountOpportunities.containsKey(oppty.AccountId)){
                mapAccountOpportunities.get(oppty.AccountId).add(oppty);
            }
            else{
                List<Opportunity> lstExistOpportunities = new List<Opportunity>();
                lstExistOpportunities.add(oppty);
                mapAccountOpportunities.put(oppty.AccountId,lstExistOpportunities);
            }
        }

        List<Opportunity> lstExistOpenOpportunities = new List<Opportunity>();
        for(AccountTeamMember accTeamMember:scope){
            if(mapAccountOpportunities.containsKey(accTeamMember.AccountId) && mapAccountOpportunities.get(accTeamMember.AccountId).size() > 0){
                lstExistOpenOpportunities = mapAccountOpportunities.get(accTeamMember.AccountId);    
                lstExistOpenOpportunities[0].OwnerId = accTeamMember.UserId;
            }
            else{
                Opportunity opp = new Opportunity();
                opp.Name = 'Fuel - '+accTeamMember.Account.Name;
                opp.RecordTypeId = recTypeId;
                opp.StageName = 'Qualification';
                opp.OwnerId = accTeamMember.UserId;
                opp.CloseDate = System.today().addDays(30);
                opp.AccountId = accTeamMember.AccountId;
                lstOpportunities.add(opp);
            }
        }

        if(lstOpportunities.size() > 0)
            Database.Insert(lstOpportunities);
        if(lstExistOpenOpportunities.size() > 0)
            Database.Update(lstExistOpenOpportunities);
    }   
    
    /**********************************************************************************************************************
    * Method Name      :  finish
    * Input Parameter  :  Database.BatchableContext BC
    * Return Type      :  void
    * Description      :  This method is used to perform any activities post creation of Opportunities
    **********************************************************************************************************************/  
    global void finish(Database.BatchableContext BC) {
        System.debug('### Fuel Opportunities created successfully ###');
    }
}