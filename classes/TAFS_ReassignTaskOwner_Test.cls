/* CLass Name   : TAFS_ReassignTaskOwner_Test
 * Description  : Test class for Task Assignment
 * Created By   : Raushan Anand
 * Created On   : 22-Dec-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand            22-Dec-2016              Initial version.
 *
 *****************************************************************************************/

@isTest
public class TAFS_ReassignTaskOwner_Test {
    @testSetup
    static void testSetupData() {
        ID rectypeid = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Kanban Task').getRecordTypeId();
        Task demoTask = new Task(RecordTypeId = rectypeid,OwnerId = UserInfo.getUserId(),Priority = 'Normal',Status='Open',TAFS_Kanban_Card_Id__c = '23456',TAFS_Blocked_State__c=true);
        INSERT demoTask;
    }
    private static testmethod void testReassignOwner(){
        Task t = [Select Id FROM Task where TAFS_Kanban_Card_Id__c ='23456' LIMIT 1];
        TAFS_ReassignTaskOwner.reassignTask(t.Id);
    }
}