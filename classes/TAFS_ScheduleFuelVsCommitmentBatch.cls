/* CLass Name   : TAFS_ScheduleFuelVsCommitmentBatch 
 * Description  : Update Fuel Vs Commitment Values
 * Created By   : Raushan Anand
 * Created On   : 29-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                29-Aug-2016              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleFuelVsCommitmentBatch implements Schedulable {
     global void execute(SchedulableContext SC) {
         Database.executeBatch(new TAFS_Update_Fuel_Vs_Commitment_Batch(),200);
   }
}