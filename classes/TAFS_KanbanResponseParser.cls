public class TAFS_KanbanResponseParser {
    
    public static integer numberOftasksVar= 0;
    public static List<WrapResponse> responseParse(HTTPResponse res){
        List<WrapResponse> wrapList = new List<WrapResponse>();
        TaskWrapper tWrap = (TaskWrapper) System.JSON.deserialize(res.getBody(), TaskWrapper.class);
        numberOftasksVar = (tWrap.numberoftasks/30)+1;
        System.debug('tWrap'+tWrap);
        if(tWrap != null){
                for(TAFS_KanbanResponseParser.Task taskObj : tWrap.task){
                    if(!taskObj.customfields.isEmpty()){
                        WrapResponse wrapR = new WrapResponse();
                        for(cls_customfields custF : taskObj.customfields){                            
                            if(custF.name == 'Batch Number'){
                                wrapR.batchNumber = custF.value;
                            }
                            if(custF.name == 'Client Number'){
                                wrapR.clientNumber = custF.value;
                            }
                            if(custF.name == 'BS'){
                                wrapR.bs = custF.value;
                            }
                            if(custF.name == 'BAS'){
                                wrapR.bas = custF.value;
                            }
                            if(custF.name == 'FS'){
                                wrapR.fs = custF.value;
                            }
                            if(custF.name == 'VS'){
                                wrapR.vs = custF.value;
                            }
                            if(custF.name == 'CS'){
                                wrapR.cs = custF.value;
                            }  
                        }
                        wrapR.color = taskObj.color;
                        wrapR.tags = taskObj.tags;
                        wrapR.lanename = taskObj.lanename;
                        System.debug('wrapR'+ wrapR);
                        wrapList.add(wrapR);
                    }
                }
            }
        return wrapList;
    }
    public class TaskWrapper{
        public Integer numberoftasks;   
        public Integer page;    
        public Integer tasksperpage;    
        public Task[] task;
    }
    public class Task {
        public String taskid;   
        public String position; 
        public String type; 
        public String assignee; 
        public String title;
        public String description;
        public String subtasks; 
        public String subtaskscomplete; 
        public String color;    
        public String priority; 
        public String size;
        public String deadline; 
        public String extlink;  
        public String tags;
        public String columnid; 
        public String laneid;
        public String blocked;
        public String blockedreason;
        public cls_subtaskDetails[] subtaskdetails;
        public String columnname;   
        public String lanename;
        public String createdorarchived;    
        public String deadlineoriginalformat;   
        public String columnpath;   
        public Integer leadtime;    
        public Integer loggedtime;
        public cls_customfields[] customfields;
        public String updatedat;
    }
    
    
    public class cls_customfields {
        public String fieldid;  
        public String name; 
        public String type; 
        public String value;
        public boolean mandatory;
    }
    public class cls_subtaskDetails {
        public String subtaskid;    
        public String completiondate;   
        public String assignee;
        public String title;    
        public String taskparent;
        public String attachments;
    }
    public class WrapResponse{
        public String batchNumber {get;set;}
        public String clientNumber{get;set;}
        public String bs{get;set;}
        public String bas{get;set;}
        public String fs{get;set;}
        public String vs{get;set;}
        public String cs{get;set;}
        public String color{get;set;}
        public String tags{get;set;}
        public String lanename{get;set;}
    }
}