/* CLass Name   : TAFS_VerCollectionComponentControlTest
 * Description  : Test class for TAFS_VerCollectionComponentController
 * Created By   : Arpitha Sudhakar
 * Created On   : 21-June-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar                21-June-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TAFS_VerCollectionComponentControlTest {

    static testmethod void test() {
       List<TAFS_Client_Terms__c> clientTermlst = DataUtilTest_TAFS.createClientTerms(1);
       List<Account> accLst = DataUtilTest_TAFS.createAccounts(1,clientTermlst[0].Id);
       List<TAFS_Invoice_Batch__c> batchLst = DataUtilTest_TAFS.createIvoiceBatches(1,accLst[0].id);
       system.assert(batchLst[0].Id != null);
       List<TAFS_Invoice__c> invLst = DataUtilTest_TAFS.createInvoices(1,batchLst[0].id);
       Test.startTest();
       
       TAFS_VerCollectionComponentController test1 = new TAFS_VerCollectionComponentController();
       test1.invDebtorId = invLst[0].Id;
       test1.invoiceList = invLst;
       system.assert(invLst[0].Id != null);
       ApexPages.currentPage().getParameters().put('invDebtorId', invLst[0].Id);
       test1.getInvoiceList();
       
       Test.stopTest();
    }
    
    static testmethod void testNegative() {
       List<TAFS_Client_Terms__c> clientTermlst = DataUtilTest_TAFS.createClientTerms(1);
       List<Account> accLst = DataUtilTest_TAFS.createAccounts(1,clientTermlst[0].Id);
       List<TAFS_Invoice_Batch__c> batchLst = DataUtilTest_TAFS.createIvoiceBatches(1,accLst[0].id);
       system.assert(batchLst[0].Id != null);
       List<TAFS_Invoice__c> invLst = DataUtilTest_TAFS.createInvoices(1,batchLst[0].id);
       Test.startTest();
       
       TAFS_VerCollectionComponentController test1 = new TAFS_VerCollectionComponentController();
       system.assert(invLst[0].Id != null);
       ApexPages.currentPage().getParameters().put('invDebtorId', invLst[0].Id);
       test1.getInvoiceList();
       
       Test.stopTest();
    }
}