/* CLass Name   : TAFS_ScheduleChatterFeedRemoval_Test
 * Description  : Test class for TAFS_ScheduleChatterFeedRemoval
 * Created By   : Karthik Gulla
 * Created On   : 10-Nov-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla          10-Nov-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_ScheduleChatterFeedRemoval_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
    	list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
    	DataUtilTest_TAFS.createChatterFeeds(1,lstAcct[0].Id);
    } 

    /************************************************************************************
    * Method       : 	testScheduleChatterFeedRemoval
    * Description  :    Test Method to test all chatter feeds are deleted
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testScheduleChatterFeedRemoval() {
        Test.startTest();
        TAFS_ScheduleChatterFeedRemoval tafsChatterFeed = new TAFS_ScheduleChatterFeedRemoval();
        String scheduleTime = '0 0 23 * * ?';
        System.schedule('TAFS_ScheduleUpdateDebtorVolumeTest', scheduleTime, tafsChatterFeed);
        Test.stopTest();
    }
}