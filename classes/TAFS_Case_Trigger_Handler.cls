/* Class Name   : TAFS_Case_Trigger_Handler
 * Description  : This class handles the trigger operations on Case
 * Created By   : Karthik Gulla
 * Created On   : 28-Nov-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla               28-Nov-2016              Initial version.
 *
 *****************************************************************************************/
 public with sharing class TAFS_Case_Trigger_Handler {
 
    /* Method Name : validateCaseCommentsBeforeClose
     * Description : This method validates whether the case comments are available before the Case is closed
     * Return Type : void
     * Input Parameter : List of Case - List<Case> 
     */
     public static void validateCaseCommentsBeforeClose(List<Case> lstCases){
     
     }
 }