@isTest
private class TAFS_ClientWelcomePageControllerTest{

/************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
     
         list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
         list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
         list<Contact> contactList =  DataUtilTest_TAFS.createContacts(1,lstAcct[0].Id);
         list<Event> eventList = DataUtilTest_TAFS.createEvents(1,contactList[0].Id,lstAcct[0].Id);    
    }    
    
    public static testmethod void testClientWelcome() {
        
       Event event = [select id,StartDateTime,EndDateTime,WhoId ,WhatId, OwnerId from Event limit 1];
       PageReference pageRef = Page.TAFS_ExecuteClientWelcomePage;
       Test.setCurrentPage(pageRef); 
       ApexPages.currentPage().getParameters().put('eventId', event.id);
       
       TAFS_Welcome_Client__c cw = new TAFS_Welcome_Client__c();
       
       cw.TAFS_Conference_EFS_Account_Management__c = true;
       cw.TAFS_Confirm_client_can_log_into_Sight__c = true;
       cw.TAFS_Download_Mobile_app__c               = true;
       cw.TAFS_Driver_Version__c= true;
       cw.TAFS_Emergency_Roadside_Assistance__c= true;
       cw.TAFS_Give_6_digit_account_number__c= true;
       cw.TAFS_Gold_Tire_Discount_Program__c= true;
       cw.TAFS_Go_through_Invoice_Search__c= true;
       cw.TAFS_submit_credit_check_on_new_debtor__c= true;
       cw.TAFS_Owner_Version__c= true;
       cw.TAFS_Refer_a_Friend__c= true;
       cw.TAFS_Review_detail_tab__c= true;
       cw.TAFS_Review_look_up_for__c= true;
       cw.TAFS_Review_main_page__c= true;
       cw.TAFS_Review_procedures__c= true;
       cw.TAFS_Review_reports__c= true;
       cw.TAFS_Review_requirements_paper_work__c = true;
       cw.TAFS_Review_required_paper_work__c = true;
       cw.TAFS_Review_transaction_limit_defaults__c = true;
       cw.TAFS_Review_transaction_tab__c = true;
       cw.TAFS_Set_up_ACH_option__c = true;
       cw.TAFS_Set_up_additional_functionality__c = true;
       cw.TAFS_Show_fast_aging_with_balances__c = true;
       cw.TAFS_Show_reporting__c = true;
       cw.TAFS_Fuel_Rebate__c = true;
       cw.TAFS_Validate_EFS_phone_no_with_client__c = true;
       cw.TAFS_Walk_through_EFS_money_codes__c = true;
       cw.TAFS_Notes__c = 'test';
       
       Test.startTest();
       
       TAFS_ClientWelcomePageController cwInstance = new TAFS_ClientWelcomePageController(new ApexPages.StandardController(cw));
       cwInstance.startCall();
       cwInstance.logCall();
       
       Test.stoptest();
       
       system.assertEquals(cw.TAFS_Status__c ,'Closed');
       system.assert(cw.Id != null);
       
    }
    
    public static testmethod void testClientWelcomeTwo() {
        
       Event event = [select id,StartDateTime,EndDateTime,WhoId ,WhatId, OwnerId from Event limit 1];
       PageReference pageRef = Page.TAFS_ExecuteClientWelcomePage;
       Test.setCurrentPage(pageRef); 
       ApexPages.currentPage().getParameters().put('eventId', event.id);
       
       TAFS_Welcome_Client__c cw = new TAFS_Welcome_Client__c();
       
       cw.TAFS_Conference_EFS_Account_Management__c = true;
       cw.TAFS_Confirm_client_can_log_into_Sight__c = true;
       cw.TAFS_Download_Mobile_app__c               = true;
       cw.TAFS_Driver_Version__c= true;
       cw.TAFS_Emergency_Roadside_Assistance__c= true;
       cw.TAFS_Give_6_digit_account_number__c= true;
       cw.TAFS_Gold_Tire_Discount_Program__c= true;
       cw.TAFS_Go_through_Invoice_Search__c= true;
       cw.TAFS_submit_credit_check_on_new_debtor__c= true;
       cw.TAFS_Owner_Version__c= true;
       cw.TAFS_Refer_a_Friend__c= true;
       cw.TAFS_Review_detail_tab__c= true;
       cw.TAFS_Review_look_up_for__c= true;
       cw.TAFS_Review_main_page__c= true;
       cw.TAFS_Review_procedures__c= true;
       cw.TAFS_Review_reports__c= true;
       cw.TAFS_Review_requirements_paper_work__c = true;
       cw.TAFS_Review_required_paper_work__c = true;
       cw.TAFS_Review_transaction_limit_defaults__c = true;
       cw.TAFS_Review_transaction_tab__c = true;
       cw.TAFS_Set_up_ACH_option__c = true;
       cw.TAFS_Set_up_additional_functionality__c = true;
       cw.TAFS_Show_fast_aging_with_balances__c = true;
       cw.TAFS_Show_reporting__c = true;
       cw.TAFS_Fuel_Rebate__c = true;
       cw.TAFS_Validate_EFS_phone_no_with_client__c = true;
       cw.TAFS_Walk_through_EFS_money_codes__c = true;
       
       Test.startTest();
       
       TAFS_ClientWelcomePageController cwInstance = new TAFS_ClientWelcomePageController(new ApexPages.StandardController(cw));
       cwInstance.startCall();
       cwInstance.logCall();
       
       Test.stoptest();       
       
    }
    
    public static testmethod void testClientWelcomeNegative() {
        
       Event event = [select id,StartDateTime,EndDateTime,WhoId ,WhatId, OwnerId from Event limit 1];
       Account accoutRec = [select id from Account limit 1];
       
       PageReference pageRef = Page.TAFS_ExecuteClientWelcomePage;
       Test.setCurrentPage(pageRef);
        
       ApexPages.currentPage().getParameters().put('eventId', event.id);
       
       TAFS_Welcome_Client__c cw = new TAFS_Welcome_Client__c();
       cw.TAFS_Notes__c = 'test';
       
       TAFS_ClientWelcomePageController cwInstance = new TAFS_ClientWelcomePageController(new ApexPages.StandardController(cw));
       
       Test.startTest();
       
           TAFS_SObjectSelector objectSelector = new TAFS_SObjectSelector();
           objectSelector.selectTeamMemberByAccountId(accoutRec.Id,'Sales Account Manager');
           cwInstance.startCall();
           cwInstance.logCall();
       
       Test.stopTest();
       
       system.assertNotEquals(cw.TAFS_Status__c,'Closed');
       system.assert(cw.Id != null);
              
    }
    
    

}