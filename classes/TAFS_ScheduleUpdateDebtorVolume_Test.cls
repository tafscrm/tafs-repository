/* CLass Name   : TAFS_ScheduleUpdateDebtorVolume_Test
 * Description  : Test class for TAFS_ScheduleUpdateDebtorVolume
 * Created By   : Karthik Gulla
 * Created On   : 09-Dec-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla          09-Dec-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_ScheduleUpdateDebtorVolume_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAcct = DataUtilTest_TAFS.createDebtors(1);
        List<TAFS_Invoice_Batch__c> lstInvBatches = DataUtilTest_TAFS.createIvoiceBatches(1,lstAcct[0].Id);
        DataUtilTest_TAFS.createInvoices(1, lstInvBatches[0].Id);
    } 

    /************************************************************************************
    * Method       :    testScheduleUpdateDebtorVolume
    * Description  :    Test Method to Update Debtor Volume (Debtor A/R Balance) on Debtor
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testScheduleUpdateDebtorVolume() {
        Test.startTest();
        TAFS_ScheduleUpdateDebtorVolume tafsUpdateDebtVol = new TAFS_ScheduleUpdateDebtorVolume();
        String scheduleTime = '0 0 23 * * ?';
        System.schedule('TAFS_ScheduleUpdateDebtorVolumeTest', scheduleTime, tafsUpdateDebtVol);
        Test.stopTest();
    }
}