/* Class Name   : TAFS_Opportunity_Trigger_Handler
 * Description  : This class handles the trigger operations on Opportunity
 * Created By   : Karthik Gulla
 * Created On   : 04-April-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla                04-April-2017           Initial version.
 *
 *****************************************************************************************/
public class TAFS_Opportunity_Trigger_Handler{
	/************************************************************************************
    * Method       :    triggerEmailwithDocusignAttachment
    * Description  :    Method to trigger an email with attachments from Docusign status
    					records of Opportunity
    * Parameter    :    List<Opportunity>    
    * Return Type  :    void
    *************************************************************************************/
    public static void triggerEmailwithDocusignAttachment(List<Opportunity> opportunityList, Map<Id,Opportunity> mapExistOpportunities){
    	List<Id> lstOpportunityIds = new List<Id>();
        Map<Id,Opportunity> mapOpportunities;
    	List<Id> lstDocusignIds = new List<Id>();
    	List<Messaging.SingleEmailMessage> mailsToBeSent = new List<Messaging.SingleEmailMessage>();
    	Map<Id,List<DocusignStatusWrapper>> mapDocusignRecords = new Map<Id,List<DocusignStatusWrapper>>();
    	Map<Id,List<Messaging.EmailFileAttachment>> mapOppAttachments = new Map<Id,List<Messaging.EmailFileAttachment>>();
        Map<Id,Id> mapOpportunityDocSign = new Map<Id,Id>();
        Map<Id,List<Attachment>> mapTempAttachments = new Map<Id,List<Attachment>>();

    	Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Tire Program').getRecordTypeId();

    	for(Opportunity opp:opportunityList){
    		if((opp.StageName == 'PO Redeemed' && opp.StageName != mapExistOpportunities.get(opp.Id).StageName)
    			&& opp.RecordTypeId == recTypeId
    			&& opp.TAFS_Agreement_Type__c == 'Tax Exempt'){
    			lstOpportunityIds.add(opp.Id);
    		}
    	}

        mapOpportunities = new Map<Id,Opportunity>([SELECT Id, AccountId, Account.Name, TAFS_PO_Number__c, TAFS_PO_Status_Change_Date__c,
                                                    TAFS_PO_Redemption_Receipt__c, TAFS_PO_Redemption_Name__c
                                                    FROM Opportunity WHERE Id In :lstOpportunityIds]);
        
    	for(dsfs__DocuSign_Status__c docusignStatusRec:[SELECT Id, dsfs__Opportunity__c, dsfs__Completed_Date_Time__c FROM dsfs__DocuSign_Status__c
                    									WHERE dsfs__Opportunity__c In :lstOpportunityIds 
                                                        AND dsfs__Envelope_Status__c = 'Completed' 
                                                        ORDER BY dsfs__Completed_Date_Time__c DESC]){
            DocusignStatusWrapper docSignWrapper = new DocusignStatusWrapper(new dsfs__DocuSign_Status__c(Id = docusignStatusRec.Id,
                                                                                        dsfs__Opportunity__c = docusignStatusRec.dsfs__Opportunity__c,
                                                                                        dsfs__Completed_Date_Time__c = docusignStatusRec.dsfs__Completed_Date_Time__c));
    		if(mapDocusignRecords.containsKey(docusignStatusRec.dsfs__Opportunity__c)){
    			mapDocusignRecords.get(docusignStatusRec.dsfs__Opportunity__c).add(docSignWrapper);
    		}
    		else{
    			List<DocusignStatusWrapper> lstDocSignStatusRecords = new List<DocusignStatusWrapper>();
    			lstDocSignStatusRecords.add(docSignWrapper);
    			mapDocusignRecords.put(docusignStatusRec.dsfs__Opportunity__c, lstDocSignStatusRecords);
    		}
    	}
       
    	for(Id oppId:mapDocusignRecords.keySet()){
    		mapDocusignRecords.get(oppId).sort();
    		lstDocusignIds.add(mapDocusignRecords.get(oppId)[0].docSignStatusWrapper.Id);
            mapOpportunityDocSign.put(mapDocusignRecords.get(oppId)[0].docSignStatusWrapper.Id, oppId);
    	}

    	for(Attachment attmt:[SELECT Id, Name, Body, ParentId, ContentType FROM Attachment WHERE ParentId In :lstDocusignIds ORDER BY LastModifiedDate DESC]){
            if(!attmt.Name.toLowerCase().contains('namepdf')){
                if(mapTempAttachments.containsKey(attmt.ParentId)){
                    mapTempAttachments.get(attmt.ParentId).add(attmt);
                }
                else{
                    List<Attachment> lstatmts = new List<Attachment>();
                    lstatmts.add(attmt);
                    mapTempAttachments.put(attmt.ParentId,lstatmts);
                }
            }
    	}
        for(Id atmtParentId:mapTempAttachments.keySet()){
            Messaging.EmailFileAttachment msgAttachment = new Messaging.EmailFileAttachment();
            msgAttachment.setBody(mapTempAttachments.get(atmtParentId)[0].Body);
            msgAttachment.setContentType(mapTempAttachments.get(atmtParentId)[0].ContentType);
            msgAttachment.setFileName(mapTempAttachments.get(atmtParentId)[0].Name);
            mapOppAttachments.put(atmtParentId,new List<Messaging.EmailFileAttachment>{msgAttachment});
        }
        
    	for(Id attmtId:mapOppAttachments.keySet()){
    		Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
    		emailMessage.setToAddresses(new List<String>{Label.TAFS_Opportunity_PO_Redeemed_ToEmailAddress});
    		emailMessage.setSubject('TAFS Tax Exemption Form for '+mapOpportunities.get(mapOpportunityDocSign.get(attmtId)).Account.Name);
            emailMessage.setHtmlBody(formEmailBody(mapOpportunities.get(mapOpportunityDocSign.get(attmtId))));
    		emailMessage.setFileAttachments(mapOppAttachments.get(attmtId));
    		mailsToBeSent.add(emailMessage);
    	}
        if(mailsToBeSent.size()>0)
    	   Messaging.sendEmail(mailsToBeSent);
    }

    /************************************************************************************
    * Method       :    formEmailBody
    * Description  :    Method to form an email Body 
    * Parameter    :    Opportunity oppty   
    * Return Type  :    String
    *************************************************************************************/
    public static String formEmailBody(Opportunity oppty){
        String body = 'Please find the signed Tax Exemption Form for '+oppty.Account.Name+' attached.<br/><br/>';
        String P0Number = oppty.TAFS_PO_Number__c != null ? oppty.TAFS_PO_Number__c : '';
        String POStatusChangeDate = '';
        if(oppty.TAFS_PO_Status_Change_Date__c != null){
            DateTime dTime = DateTime.newInstance(oppty.TAFS_PO_Status_Change_Date__c.Year(),oppty.TAFS_PO_Status_Change_Date__c.Month(),oppty.TAFS_PO_Status_Change_Date__c.day());
            POStatusChangeDate = dTime.format('MM-dd-YYYY');
        }
        String PORedeemptionReceipt = oppty.TAFS_PO_Redemption_Receipt__c != null ? oppty.TAFS_PO_Redemption_Receipt__c : '';
        String PORedeemedBy = oppty.TAFS_PO_Redemption_Name__c != null ? oppty.TAFS_PO_Redemption_Name__c : '';

        body += '<table><thead>Purchase Order Information</thead>';
        body += '<tr><td>PO#: '+P0Number+'</td></tr>';
        body += '<tr><td>Redeemed Date: '+POStatusChangeDate+'</td></tr>';
        body += '<tr><td>PO Redemption Receipt: '+PORedeemptionReceipt+'</td></tr>';
        body += '<tr><td>PO Redeemed By: '+PORedeemedBy+'</td></tr></table>';
        return body;
    }

    /************************************************************************************
    * Class        :    DocusignStatusWrapper
    * Description  :    Wrapper class implementing Comparable to sort out docusign status 
                        records
    *************************************************************************************/
    public class DocusignStatusWrapper implements Comparable {
    	public dsfs__DocuSign_Status__c docSignStatusWrapper;
	    
	    // Constructor
	    public DocusignStatusWrapper(dsfs__DocuSign_Status__c docSignStatWrapper) {
	        docSignStatusWrapper = docSignStatWrapper;
	    }
	    
	    // Compare Docusign Status Records based on the DocusignStatus Completed Date.
	    public Integer compareTo(Object compareTo) {
	        // Cast argument to FuelConsumptionWrapper
	        DocusignStatusWrapper compareToDocSignStat = (DocusignStatusWrapper)compareTo;
	        
	        // The return value of 0 indicates that both elements are equal.
	        Integer returnValue = 0;
	        if (docSignStatusWrapper.dsfs__Completed_Date_Time__c > compareToDocSignStat.docSignStatusWrapper.dsfs__Completed_Date_Time__c) {
	            // Set return value to a negative value.
	            returnValue = -1;
	        } else if (docSignStatusWrapper.dsfs__Completed_Date_Time__c < compareToDocSignStat.docSignStatusWrapper.dsfs__Completed_Date_Time__c) {
	            // Set return value to a positive value.
	            returnValue = 1;
	        }
	        return returnValue;       
	    }
	}
}