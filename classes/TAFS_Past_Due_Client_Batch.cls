/* Class Name   : TAFS_Past_Due_Client_Batch
 * Description  : Batch to run everyday and create task based upon Cliennt Interaction
 * Created By   : Raushan Anand
 * Created On   : 9-Sep-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand               9-Sep-2016               Initial version.
 *
 *****************************************************************************************/
global class TAFS_Past_Due_Client_Batch implements Database.Batchable<sObject>{

    global List<Account> start(Database.BatchableContext BC){
        List<Account> accList = [Select Id,TAFS_Relationship_Specialist_UserId__c FROM Account WHERE RecordType.Name = 'Client' AND TAFS_Client_Status__c != 'Terminated'];
        return accList;
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        List<Task> taskList = new List<Task>();
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true];
        Datetime nextStart = BusinessHours.nextStartDate(bh.id, DateTime.now());
        Id recordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Client Follow-up').getRecordTypeId();
        Map<Id,Task> accountTaskMap = new Map<Id,Task>();
        Set<Id> clientSet = new Set<Id>();
        for(TAFS_Client_Interaction__c objIL:[Select Id,TAFS_Client_Name__c FROM TAFS_Client_Interaction__c WHERE TAFS_Client_Name__c IN: scope and TAFS_Client_Name__r.TAFS_Last_Successful_Check_In_Date__c = N_DAYS_AGO:10]){
        
            if(String.IsnotBlank(objIL.TAFS_Client_Name__c))
                clientSet.add(objIL.TAFS_Client_Name__c);
        }
        List<Task> existingTask = [Select Id, Subject,Priority,WhatId,TAFS_Followup_Reason__c,What.Type,ActivityDate,OwnerId,Status FROM Task where WhatId IN: scope AND RecordType.Name = 'Client Follow-up' AND Status='Open'];
        for(Task objTask : existingTask){
            accountTaskMap.put(objTask.WhatId,objTask);
        }
        for(Account accObj : scope){ 

            if(clientSet.contains(accObj.Id)){
                System.Debug('Test 1' + accountTaskMap);
                if(accountTaskMap.containsKey(accObj.Id)){
                    System.Debug('Test 2'+ accObj.Id);
                    Task objTask = accountTaskMap.get(accObj.Id);
                    if(objTask.WhatId == accObj.Id){
                        objTask.ActivityDate = nextStart.date();
                        if(objTask.TAFS_Followup_Reason__c != null && (!objTask.TAFS_Followup_Reason__c.contains('No Client Interaction in the last 10 days')))
                            objTask.TAFS_Followup_Reason__c += ';No Client Interaction in the last 10 days';
                        taskList.add(objTask);
                    }
                }
                
                else{
                    Task t = new Task();
                    t.Subject = 'Past Due Client';
                    t.RecordTypeId = recordTypeId;
                    t.Priority = '1';
                    t.WhatId = accObj.Id;
                    t.TAFS_Followup_Reason__c = 'No Client Interaction in the last 10 days';
                    t.ActivityDate = nextStart.date();
                    if(STring.IsNotBlank(accObj.TAFS_Relationship_Specialist_UserId__c))
                        t.OwnerId = accObj.TAFS_Relationship_Specialist_UserId__c;
                    System.debug('objId====>>'+ t);
                    taskList.add(t);
                }
            }
            
        }
        if(!taskList.isEmpty()){
            Database.Upsert(taskList);
        }
    }
    global void finish(Database.BatchableContext BC){
        //if(!Test.isRunningTest()){
            //CronTrigger  ct = [SELECT Id,Cronjobdetail.name, PreviousFireTime,nextfiretime FROM CronTrigger where Cronjobdetail.name = 'TAFS_ScheduleTaskPrioritizationBatch'];
            //System.debug('ct--->'+ct);           
            TAFS_Task_Batch_Execution__c cs = TAFS_Task_Batch_Execution__c.getInstance('Last_Execution_Time');
            AsyncApexJob jobDetails = [Select Id,CompletedDate, JobType,Status from AsyncApexJob where Id=: cs.Job_Id__c];
            cs.Timestamp__c = jobDetails.CompletedDate;
            update cs;   
        //}
    }

}