@isTest
private with sharing class TAFS_TaskTriggerHandler_TEST {

    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
     
         list<Lead> leadList                          = DataUtilTest_TAFS.createleads(1);
         //list<Task> tasklist                          = DataUtilTest_TAFS.createAccountsForAgreements(1);
         //list<Contact> lstcontact                   = DataUtilTest_TAFS.createContactsForLead(1,leadList[0].Id);
         //list<TAFS_Agreement__c> createAgreements   = DataUtilTest_TAFS.createAgreements(1,accList[0].Id);
    } 
    private static list<Task> createTasks(integer total, Id leadId) 
    {
        list<Task> taskList = new list<Task>();
        for(integer i = 0; i < total; i++)
        {
            taskList.add(new Task( Subject              = 'Test',
                                   OwnerId              = UserInfo.getUserId(),
                                   WhoId                = leadId,
                                   status               = 'Completed',
                                   Type                 = 'Call No Contact'
                                 ));
        }
        return taskList;
    }   
    private static testmethod void testStatusNew(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='New',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_Lead_Sort_Order__c lsc1 = new TAFS_Lead_Sort_Order__c(Name='No Contact',TAFS_Sort_Order__c ='1');
         insert lsc1;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
     
         list<Task> taskList = createTasks(1,leadObj.Id); 
         
         Test.startTest();
             insert taskList;
         Test.stopTest();
         system.assert([SELECT ID FROm Task].size()>0);       
                 
     }
     
     private static testmethod void testActivWork(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='Actively Working',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'Actively Working';
         update leadObj;
         
         list<Task> taskList = createTasks(1,leadObj.Id); 
         
         Test.startTest();
             insert taskList;
         Test.stopTest(); 
         system.assert([SELECT ID FROm Task].size()>0);            
                 
     }
     
     private static testmethod void testAgreeOut(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='Agreement Out',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'Agreement Out';
         update leadObj;
         
         list<Task> taskList = createTasks(1,leadObj.Id); 
         
         Test.startTest();
             insert taskList;
         Test.stopTest();   
         system.assert([SELECT ID FROm Task].size()>0);           
                 
     }
     
     private static testmethod void testAgreeReady(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='Agreement Ready',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'Agreement Ready';
         update leadObj;
         
         list<Task> taskList = createTasks(1,leadObj.Id); 
         
         Test.startTest();
             insert taskList;
         Test.stopTest(); 
         system.assert([SELECT ID FROm Task].size()>0);           
                 
     }
     
     private static testmethod void testApplicationOut(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='Application Out',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'Application Out';
         update leadObj;
         
         list<Task> taskList = createTasks(1,leadObj.Id); 
         
         Test.startTest();
             insert taskList;
         Test.stopTest();
         system.assert([SELECT ID FROm Task].size()>0);            
                 
     }
     
     private static testmethod void testAwaitDoc(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='Awaiting Documentation',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'Awaiting Documentation';
         update leadObj;
         
         list<Task> taskList = createTasks(1,leadObj.Id); 
         
         Test.startTest();
             insert taskList;
         Test.stopTest(); 
         system.assert([SELECT ID FROm Task].size()>0);           
                 
     }
     
     private static testmethod void testNoContact(){
     
         TAFS_Lead_Sort_Order__c lsc = new TAFS_Lead_Sort_Order__c(Name='No Contact',TAFS_Sort_Order__c ='1');
         insert lsc;
         
         TAFS_LeadSortCustomSetting__c ls = new TAFS_LeadSortCustomSetting__c(TAFS_Reference_Date_Field_Future__c = system.now()+100,
                                                                              TAFS_Reference_Date_Field_Past__c = system.now()-100);
         insert ls;
         
         Lead leadObj = [SELECT ID FROM Lead LIMIT 1];
         leadObj.Status = 'No Contact';
         update leadObj;
         
         list<Task> taskList = createTasks(1,leadObj.Id); 
         
         Test.startTest();
             insert taskList;
         Test.stopTest(); 
         system.assert([SELECT ID FROm Task].size()>0);           
                 
     }
 }