/* CLass Name   : TAFS_Invoice_Trigger_Handler
 * Description  : Trigger methods for TAFS_Invoice_Trigger 
 * Created By   : Raushan Anand
 * Created On   : 26-Apr-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                26-Apr-2015              Initial version.
 *
 *****************************************************************************************/
public class TAFS_Invoice_Trigger_Handler {
    
/****************************************************************************************
* Method Name      :  updateStatusTypeOnInvoice
* Input Parameter  :  List of Invoice (List<TAFS_Invoice__c>)
* Return Type      :  Void
* Description      :  This method converts the Type and Status value from Custom Settings.
                      Custom settings stores Key Value pair for each object.
***************************************************************************************/
    
    public static void updateStatusTypeOnInvoice(List<TAFS_Invoice__c> invoiceList){
        
        Map<String,String> typeKeyVal = new Map<String,String>();
        Map<String,String> statusKeyVal = new Map<String,String>();
        Map<String,String> inDisputeKeyVal = new Map<String,String>();
        List<String> clientNumberList = new List<String>();
        Map<String, String> clientNumberIdMap = new Map<String,String>();
        
        List<TAFS_Invoic_Key_Value_Mapping__c> keyValList = TAFS_Invoic_Key_Value_Mapping__c.getall().values();
        for(TAFS_Invoic_Key_Value_Mapping__c obj : keyValList){
            if(obj.TAFS_Object_Name__c == 'Invoice' && obj.TAFS_Field_Name__c == 'Invoice Delivery Method'){
                typeKeyVal.put(obj.TAFS_Cadence_Key__c,obj.TAFS_SFDC_Value__c);
            }
            else if(obj.TAFS_Object_Name__c == 'Invoice' && obj.TAFS_Field_Name__c == 'Invoice Delivery Status'){
                statusKeyVal.put(obj.TAFS_Cadence_Key__c,obj.TAFS_SFDC_Value__c);
            } 
            else if(obj.TAFS_Object_Name__c == 'Invoice' && obj.TAFS_Field_Name__c == 'In Dispute'){
                inDisputeKeyVal.put(obj.TAFS_Cadence_Key__c,obj.TAFS_SFDC_Value__c);
            }   
        }
        for(TAFS_Invoice__c invoiceObj : invoiceList){
            if(invoiceObj.TAFS_Client_Number__c != null){
                clientNumberList.add(invoiceObj.TAFS_Client_Number__c);
            }
            if(invoiceObj.TAFS_Invoice_Delivery_Method__c != null){
                if(typeKeyVal.containsKey(invoiceObj.TAFS_Invoice_Delivery_Method__c)){
                    invoiceObj.TAFS_Invoice_Delivery_Method__c = typeKeyVal.get(invoiceObj.TAFS_Invoice_Delivery_Method__c);
                }
                /*else{
                    invoiceObj.TAFS_Invoice_Delivery_Method__c = '';
                }*/
            }
            if(invoiceObj.TAFS_Invoice_Delivery_Status__c != null){
                if(statusKeyVal.containsKey(invoiceObj.TAFS_Invoice_Delivery_Status__c)){
                    invoiceObj.TAFS_Invoice_Delivery_Status__c = statusKeyVal.get(invoiceObj.TAFS_Invoice_Delivery_Status__c);
                }
                /*else{
                    invoiceObj.TAFS_Invoice_Delivery_Status__c = '';
                }*/
            }
            if(invoiceObj.TAFS_In_Dispute__c != null){
                if(inDisputeKeyVal.containsKey(invoiceObj.TAFS_In_Dispute__c)){
                    invoiceObj.TAFS_In_Dispute__c = inDisputeKeyVal.get(invoiceObj.TAFS_In_Dispute__c);
                }
            }
        }
        if(!clientNumberList.isEmpty()){
            List<Account> accList = [SELECT Id, TAFS_Client_Number__c FROM Account WHERE TAFS_Client_Number__c IN: clientNumberList];
            for(Account acc : accList){
                clientNumberIdMap.put(acc.TAFS_Client_Number__c,acc.Id);
            }
        }
         for(TAFS_Invoice__c invoiceObj : invoiceList){
            if(invoiceObj.TAFS_Client_Number__c != null){
                if(clientNumberIdMap.containsKey(invoiceObj.TAFS_Client_Number__c)){
                    if(Trigger.IsInsert)
                        invoiceObj.TAFS_Client_Name__c=clientNumberIdMap.get(invoiceObj.TAFS_Client_Number__c);
                }
            }
        }
    }
    public static void updateBatchIdOnInvoice(List<TAFS_Invoice__c> invoiceList){
        List<String> batchNoList = new List<String>();
        Map<String,String> batchNoIdMap = new Map<String,String>();
        for(TAFS_Invoice__c invoiceObj : invoiceList){
            if(invoiceObj.TAFS_Batch_No__c != null){
                batchNoList.add(invoiceObj.TAFS_Batch_No__c);
            }
        }
        List<TAFS_Invoice_Batch__c> invoiceBatchList = [SELECT Id,Name,TAFS_Client_Number__c FROM TAFS_Invoice_Batch__c WHERE Name IN: batchNoList];
        for(TAFS_Invoice_Batch__c invObj : invoiceBatchList){
            batchNoIdMap.put(invObj.Name + invObj.TAFS_Client_Number__c,invObj.Id);
            system.debug('printing unique key>>>'+invObj.Name + invObj.TAFS_Client_Number__c);
        }
        for(TAFS_Invoice__c invoiceObj : invoiceList){
            if(batchNoIdMap.containsKey(invoiceObj.TAFS_Batch_No__c + invoiceObj.TAFS_Client_Number__c)){
                invoiceObj.TAFS_Invoice_Batch__c = batchNoIdMap.get(invoiceObj.TAFS_Batch_No__c + invoiceObj.TAFS_Client_Number__c);
            }
        }
    }
    /****************************************************************************************
    * Method Name      :  updateDebtorFirstAssociationWithClient
    * Input Parameter  :  List of Invoice (List<TAFS_Invoice__c>)
    * Return Type      :  Void
    * Description      :  This method updates the Debtor first association with Client checkbox on Invoice
    ***************************************************************************************/
    public static void updateDebtorFirstAssociationWithClient(List<TAFS_Invoice__c> invoiceList){
        Set<String> debtorClientSet = new Set<String>();
        Set<Id> accountSet = new Set<Id>();
        Set<Id> debtorSet = new Set<Id>();
        for(TAFS_Invoice__c invoiceObj : invoiceList){
            if(invoiceObj.Debtor__c!=null && invoiceObj.TAFS_Client_Name__c!=null){
                invoiceObj.TAFS_Client_Debtor_Map__c = invoiceObj.Debtor__c + '_'+invoiceObj.TAFS_Client_Name__c;
            }
            if(String.IsNotBlank(invoiceObj.TAFS_Client_Debtor_Map__c)){
                debtorClientSet.add(invoiceObj.TAFS_Client_Debtor_Map__c);
                accountSet.add(invoiceObj.TAFS_Client_Name__c);
                debtorSet.add(invoiceObj.Debtor__c);
            }
        }
        Set<String> existingDebtorClientSet = new Set<String>();
        System.Debug('##ERA1'+ invoiceList[0].TAFS_Client_Debtor_Map__c);
        for(TAFS_Invoice__c invObject : [SELECT Id,TAFS_Client_Debtor_Map__c FROM TAFS_Invoice__c WHERE  TAFS_Client_Debtor_Map__c IN : debtorClientSet AND TAFS_Client_Name__c IN: accountSet AND Debtor__c IN: debtorSet AND TAFS_Debtor_first_association_withClient__c=true]){
            existingDebtorClientSet.add(invObject.TAFS_Client_Debtor_Map__c);
        }
        System.Debug('##ERA2'+ existingDebtorClientSet);
        for(TAFS_Invoice__c invoiceObj : invoiceList){
            System.Debug('##ERA3'+ existingDebtorClientSet.contains(invoiceObj.TAFS_Client_Debtor_Map__c));
            if(!existingDebtorClientSet.contains(invoiceObj.TAFS_Client_Debtor_Map__c)){
                invoiceObj.TAFS_Debtor_first_association_withClient__c = true;
            }
        }
    }
}