/* CLass Name   : TAFS_Schedule_Task_Prioritization
 * Description  : Update Task Priority daily
 * Created By   : Raushan Anand
 * Created On   : 12-Sep-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                12-Sep-2015              Initial version.
 *
 *****************************************************************************************/
global class TAFS_Schedule_Task_Prioritization implements Schedulable {
    global void execute(SchedulableContext SC) {
        //Database.executeBatch(new TAFS_Task_Prioritization_Batch(),100);
        Id jobId = Database.executeBatch(new TAFS_Client_Indicator_Population_Batch(),10);
        TAFS_Task_Batch_Execution__c cs = TAFS_Task_Batch_Execution__c.getInstance('Last_Execution_Time');
        if(jobId != null){
        	cs.Job_Id__c = jobId;
            UPDATE cs;
        }
   }
}