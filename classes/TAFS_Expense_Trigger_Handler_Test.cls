/* CLass Name   : TAFS_Expense_Trigger_Handler_Test
 * Description  : Test class for TAFS_PopulateInvoiceBatchOnExpense trigger, TAFS_ExpenseHandler
 * Created By   : Karthik Gulla
 * Created On   : 07-Nov-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla          14-Nov-2016              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_Expense_Trigger_Handler_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
        list<TAFS_Invoice_Batch__c> lstInvoiceBatches = DataUtilTest_TAFS.createIvoiceBatches(1,lstAcct[0].Id);
        list<TAFS_Expense__c> lstExpenses = DataUtilTest_TAFS.createExpenses(1,lstInvoiceBatches[0].Id,lstInvoiceBatches[0].TAFS_External_Id__c);
    } 

    /************************************************************************************
    * Method       :    testExpenseHandler
    * Description  :    Test Method to test whether the invoicebatchId is updated 
						on expense record
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testExpenseHandler() { 
        //Start Test
        Test.startTest();

        //Stop Test
        Test.stopTest();
        list<TAFS_Invoice_Batch__c> lstInvBatches = [SELECT Id,TAFS_External_Id__c FROM TAFS_Invoice_Batch__c];
        list<TAFS_Expense__c> lstExpenses = [SELECT Id, TAFS_Invoice_Batch__c, TAFS_BatchNbr_ClientNbr__c FROM TAFS_Expense__c];

        System.assertEquals(lstInvBatches[0].Id, lstExpenses[0].TAFS_Invoice_Batch__c);
    }
}