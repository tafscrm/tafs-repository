/* Class Name   : TAFS_Error_Logger
 * Description  : Class to log error in Error Log object
 * Created By   : Raushan Anand
 * Created On   : 16-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                16-May-2015              Initial version.
 *
 *****************************************************************************************/

public with sharing class TAFS_Error_Logger {
    
    public static void insertErrorLog(HTTPResponse errResponse,HTTPRequest errRequest){
        try{
            TAFS_Error_Log__c errLog = new TAFS_Error_Log__c();
            errLog.TAFS_Status__c= errResponse.getStatus();
            errLog.TAFS_Status_Code__c= String.Valueof(errResponse.getStatusCode());
            errLog.TAFS_Request__c= String.ValueOf(errRequest);
            errLog.TAFS_Response_Body__c= errResponse.getBody();
            INSERT errLog;
        }
        catch(Exception ex){
            System.Debug('Exception : -->'+ ex);
        }
    }
    public static void insertErrorLogForFC(HTTPResponse errResponse,HTTPRequest errRequest){
        try{
            TAFS_Error_Log__c errLog = new TAFS_Error_Log__c();
            errLog.TAFS_Status__c= errResponse.getStatus();
            errLog.TAFS_Status_Code__c= String.Valueof(errResponse.getStatusCode());
            errLog.TAFS_Request__c= String.ValueOf(errRequest);
            errLog.TAFS_Response_Body__c= errResponse.getBody();
            errLog.TAFS_Endpoint__c = errRequest.getEndpoint();
            errLog.TAFS_Type__c = 'Freight Connect';
            INSERT errLog;
        }
        catch(Exception ex){
            System.Debug('Exception : -->'+ ex);
        }
    }
    
    public static void insertErrorLogForPN(HTTPResponse errResponse,HTTPRequest errRequest){
        try{
            TAFS_Error_Log__c errLog = new TAFS_Error_Log__c();
            errLog.TAFS_Status__c= errResponse.getStatus();
            errLog.TAFS_Status_Code__c= String.Valueof(errResponse.getStatusCode());
            errLog.TAFS_Request__c= String.ValueOf(errRequest);
            errLog.TAFS_Response_Body__c= errResponse.getBody();
            errLog.TAFS_Endpoint__c = errRequest.getEndpoint();
            errLog.TAFS_Type__c = 'Push Notification';
            INSERT errLog;
        }
        catch(Exception ex){
            System.Debug('Exception : -->'+ ex);
        }
    }

}