/* Class Name   : TAFS_Opportunity_DocuSignUtility 
 * Description  : Utility class for sending documents to contacts associated with Opportunities using docusign
 * Created By   : Karthik Gulla
 * Created On   : 02-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla                02-Mar-2017             Initial version.
 *  * Karthik Gulla                11-Apr-2017             Business Loan Docusign Process
 *
 *****************************************************************************************/
public with sharing class TAFS_Opportunity_DocuSignUtility {
    public String opportunityId { get;set; }
    public String CRL = '';    // Custom Recipient List
    public String RC = '';     // To pull documents from Object Related List - Related Content
    public String LA = '0';    // Load Attachments
    public String CES = '';    // Custom Email Subject 
    public String CEM = '';    // Custom Email Message
    public TAFS_Opportunity_DocuSignUtility(){
        opportunityId = ApexPages.currentPage().getParameters().get('Id');
    }

    public PageReference sendDocumentsToDocuSign(){
        Opportunity opp = [SELECT Id, TAFS_Signer__r.FirstName, TAFS_Signer__r.LastName, TAFS_Signer__r.Email, Account.Name, RecordType.Name FROM Opportunity WHERE Id = :opportunityId];
        Map<String,String> mapDocusignValues = new Map<String,String>();
        if(opp.RecordType.Name == 'Tire Program'){ 
            List<AccountTeamMember> lstAccTeam = new List<AccountTeamMember>();
            for(AccountTeamMember accTeam:[SELECT Id, User.Name, User.Email FROM AccountTeamMember 
                                            WHERE AccountId = :opp.AccountId 
                                            AND TeamMemberRole = 'Relationship Specialist' 
                                            ORDER BY CreatedDate DESC]){
                lstAccTeam.add(accTeam);
            }

            CRL = 'Email~'+opp.TAFS_Signer__r.Email+';FirstName~'+opp.TAFS_Signer__r.FirstName+';LastName~'+opp.TAFS_Signer__r.LastName;
            mapDocusignValues.put('CRL', CRL);

            //Custom Email Subject 
            CES = 'Please eSign TAFS Elite Tire Club quote for - '+opp.Account.Name;
            mapDocusignValues.put('CES', CES);

            //Custom Email Message 
            if(lstAccTeam.size() > 0)
                CEM= 'Sent by ('+lstAccTeam[0].User.Name+','+lstAccTeam[0].User.Email+'), for ('+opp.Account.Name+')'; 
            mapDocusignValues.put('CEM', CEM);
            mapDocusignValues.put('LA', LA);
        }
        else if(opp.RecordType.Name == 'Business Loan'){
            CRL = 'Email~'+opp.TAFS_Signer__r.Email+';FirstName~'+opp.TAFS_Signer__r.FirstName+';LastName~'+opp.TAFS_Signer__r.LastName+',';
            List<General_Manager__c> lstGeneralManagerDetails = General_Manager__c.getAll().values();
            //routingNo = routingNo +1;
            for(General_Manager__c genManager:lstGeneralManagerDetails){
                if(genManager.Name == 'General Manager'){
                    CRL = CRL+'Email~'+genManager.Email__c+';FirstName~'+genManager.First_Name__c+';LastName~'+genManager.Last_Name__c;  
                }
            }
            mapDocusignValues.put('CRL', CRL);
            System.debug('### CRL ###'+CRL);

            //Custom Email Subject 
            CES = 'Please eSign Business Loan Application for - '+opp.Account.Name;
            mapDocusignValues.put('CES', CES);
            mapDocusignValues.put('LA', LA);
            mapDocusignValues.put('CEM', CEM);
        }
        return triggerDocusign(opportunityId, mapDocusignValues);
    }

    public static PageReference triggerDocusign(Id oppId, Map<String,String> mapDocsignVals){
        PageReference pageRef = null;
        pageRef = new PageReference('/apex/dsfs__DocuSign_CreateEnvelope?SourceID='+oppId+'&CRL='+mapDocsignVals.get('CRL')+'&LA='+mapDocsignVals.get('LA')+'&CES='+mapDocsignVals.get('CES')+'&CEM='+mapDocsignVals.get('CEM'));
        pageRef.setRedirect(true);
        return pageRef;
    }
}