/* CLass Name   : TAFS_ScheduleValidateDebtorInvBal_Test
 * Description  : Test class for TAFS_Schedule_ValidateDebtorInvBal_Batch
 * Created By   : Karthik Gulla
 * Created On   : 16-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla               16-Mar-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_ScheduleValidateDebtorInvBal_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(2,clientTermList[0].Id);
        List<Account> lstDebtors = DataUtilTest_TAFS.createDebtors(1);
        List<TAFS_Invoice_Batch__c> lstInvBatches = DataUtilTest_TAFS.createIvoiceBatches(1,lstAcct[0].Id);
        List<TAFS_Invoice__c> invLst = DataUtilTest_TAFS.createInvoices(2,lstInvBatches[0].id);
        for(TAFS_Invoice__c tfInv:invLst){
            tfInv.Debtor__c = lstDebtors[0].Id;
            tfInv.TAFS_Client_Name__c = lstAcct[0].Id;
        }
        update invLst;
    } 

    /************************************************************************************
    * Method       :    testScheduleInvoiceSummaryLoad
    * Description  :    Test Method to test the load of Invoice summary with monthly Calculations
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testScheduleValidateDebtorInvBal() {
        Test.startTest();
        TAFS_Schedule_ValidateDebtorInvBal_Batch debtorInvBalBatch = new TAFS_Schedule_ValidateDebtorInvBal_Batch();
        String scheduleTime = '0 0 23 * * ?';
        System.schedule('TAFS_Schedule_ValidateDebtorInvBal_Batch_Test', scheduleTime, debtorInvBalBatch);
        Test.stopTest();
    }
}