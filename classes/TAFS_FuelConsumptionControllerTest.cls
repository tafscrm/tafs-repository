@isTest
private class TAFS_FuelConsumptionControllerTest{

/************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
     
         list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
         list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
         list<TAFS_Fuel_Consumptions__c> fcList =  DataUtilTest_TAFS.createFuelConsumptions(1,lstAcct[0].Id);
         fcList[0].TAFS_Transaction_Date__c = Date.today().addMonths(-1);
         update fcList[0];
         list<Fuel_Discount__c> fdList = DataUtilTest_TAFS.createFuelDiscounts(1,lstAcct[0].Id);
         insert fdList;
         fdList[0].TAFS_Effective_Date_Liter__c = Date.Today() - 1;
         fdList[0].TAFS_Effective_Date_Gallon__c = Date.Today() - 1;
         update fdList[0];   
    }    
    /************************************************************************************
    * Method       :    testClientWelcome
    * Description  :    Test Method to test client welcome
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testFuelConsumptionPage() {
        
       Account accRec = [select id,TAFS_In_Network_Usage_MTD__c,TAFS_Fuel_Savings_Previous_Month__c,TAFS_Approximate_Savings_Roadside__c,TAFS_Prorated_Committment_MTD__c from Account limit 1];
       List<TAFS_Fuel_Commitment__c> fuelCommitmentList = DataUtilTest_TAFS.createFuelCommitments(2,accRec.Id);
       Test.startTest();
           fuelCommitmentList[0].TAFS_Effective_Date__c = Date.today() - 1;
           insert fuelCommitmentList[0];
           PageReference pageRef = Page.TAFS_FuelConsumptionPage;
           Test.setCurrentPage(pageRef); 
              
           TAFS_FuelConsumptionController fcInstance = new TAFS_FuelConsumptionController(new ApexPages.StandardController(accRec));
           fcInstance.updateAccount();
           
       Test.stopTest();
       
       system.assert(accRec.TAFS_In_Network_Usage_MTD__c!=null);
    }
}