/****************************************************************************************
* Created By      :  Karthik Gulla
* Create Date     :  09/01/2017
* Description     :  Batch class to delete if any Orphaned Contacts available after Lead Conversion process
* Modification Log:  Initial version.
***************************************************************************************/
global class TAFS_BatchDeleteOrphanedContacts implements Database.Batchable<sObject> {
    
    global List<Lead> start(Database.BatchableContext BC) {
        List<Lead> lstLeads = new List<Lead>();
        for(Lead convLead:[SELECT Id, ConvertedContactId, ConvertedAccountId FROM Lead WHERE ConvertedContactId != null 
                                                                    AND IsConverted = true]){
            lstLeads.add(convLead);
        }
        return lstLeads;
    }
   
    global void execute(Database.BatchableContext BC, List<Lead> scope) {
        List<Id> lstContactIds = new List<Id>();
        List<Task> lstTasks = new List<Task>();
        Map<Id,Id> mapConvertedAccCon = new Map<Id,Id>();

        for(Lead conLead:scope){
            mapConvertedAccCon.put(conLead.ConvertedContactId, conLead.ConvertedAccountId);
        }

        for(Contact con:[SELECT Id, Name FROM Contact WHERE Id In :mapConvertedAccCon.keyset() AND AccountId = null]){
            lstContactIds.add(con.Id);
        }

        for(Task t: [SELECT Id, WhoId, WhatId FROM Task WHERE WhoId In :lstContactIds]){
            t.WhatId = mapConvertedAccCon.get(t.WhoId);
            t.WhoId = null;
            lstTasks.add(t);
        }
        if(lstTasks.size() > 0)
            update lstTasks;
               
        List<Contact> lstContacts = [SELECT Id, Name FROM Contact WHERE Id In :lstContactIds]; 
        if(lstContacts.size() > 0)
            delete lstContacts;
    }   
    
    global void finish(Database.BatchableContext BC) {
    
    }
}