/* CLass Name   : TAFS_ScheduleCurrentBusinessDaysBatch 
 * Description  : Update Current Business Days on Account
 * Created By   : Pankaj Singh
 * Created On   : 09-Sep-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               09-Sep-2016              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleCurrentBusinessDaysBatch implements Schedulable {
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_UpdateCurrentBusinessDaysBatch(),200);
   }
}