/* CLass Name   : TAFS_ScheduleChatterFeedsRemoval 
 * Description  : Remove Chatter feeds on all objects that are created 31 days or before
 * Created By   : Karthik Gulla
 * Created On   : 10-Nov-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla              11-Nov-2016              Initial version.
 *
 *****************************************************************************************/
global class TAFS_ScheduleChatterFeedRemoval implements Schedulable {	
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TAFS_ChatterFeedRemoval_Batch());
   }
}