/* Class Name   : TAFS_Kanban_Integration_Handler
 * Description  : This class handles the integration Callouts and response from KANBAN.
 * Created By   : Raushan Anand
 * Created On   : 2-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                2-May-2015              Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_Kanban_Integration_Handler {
    
    /* Method Name : responseParser
     * Description : This method Insert/update the task from Kanban in SFDC from the Response received.
     * Return Type : void
     * Input Parameter : Json Response - (HttpResponse) 
     */
     
    public static void responseParser(HttpResponse response){
        String responseBody = response.getBody();
        responseBody=responseBody.replace('"date":', '"cdate":');
        TaskWrapper tWrap = (TaskWrapper) System.JSON.deserialize(responseBody, TaskWrapper.class);
        
        List<Task> objTaskList = new List<Task>();
        List<Task> updateTaskList = new List<Task>();        
        String userId;
        String batchId;
        String batchNumber;
        String clientNumber;
        String commentonCard = '';
        List<String> kanbanComments = new List<String>();
        Id taskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Kanban Task').getRecordTypeId();
        
        if(tWrap != null){
            //Batch Id custom Field value population
            if(!tWrap.customfields.isEmpty()){
                for(cls_customfields custField : tWrap.customfields){
                    if(custField.name == 'Batch Number'){
                        batchNumber = custField.value;
                    }
                    if(custField.name == 'Client Number'){
                        clientNumber = custField.value;
                    }
                }
                List<TAFS_Invoice_Batch__c> invoiceBatch = [SELECT Id, Name FROM TAFS_Invoice_Batch__c WHERE Name =: batchNumber AND TAFS_Client_Number__c =: clientNumber LIMIT 1];
                if(!invoiceBatch.isEmpty())
                    batchId = invoiceBatch[0].Id;
                }
            //Comments population
            if(!tWrap.comments.isEmpty()){
                for(cls_comments objComment : tWrap.comments){
                    if(objComment.text != null){
                        commentonCard += 'Author : ' + objComment.author + '\n'+'Date : '+ objComment.cDate+'\n'+ 'Comments : '+ objComment.text + '\n\n';
                        System.Debug('###RA>>commentonCard>>>>'+commentonCard);
                    }
                }
            }
            //Assigned to Population
            if(tWrap.assignee != null){
                List<User> objUserList = [SELECT Id, Name FROM User WHERE TAFS_Kanban_Id__c =: tWrap.assignee LIMIT 1];
                if(!objUserList.isEmpty()){
                    userId= objUserList[0].Id;
                }
            }
            
            if(tWrap.taskid != null){
                objTaskList = [SELECT Id, OwnerId,Subject,Description,Priority,ActivityDate,WhatId,TAFS_Blocked_Reason__c,TAFS_Is_Future_Context__c,
                               TAFS_Blocked_State__c,TAFS_Kanban_Column__c,TAFS_Kanban_Swimlane__c,TAFS_New_Comment__c,TAFS_Kanban_Card_Id__c,TAFS_Batch_Number__c
                               FROM Task WHERE TAFS_Kanban_Card_Id__c =: tWrap.taskId];
                
                if(objTaskList.isEmpty()){
                    Task objTask = new Task();
                    objTask.RecordTypeId = taskRecordTypeId;
                    objTask.TAFS_Is_Future_Context__c = false;
                    objTask.Subject = 'Kanbanize Task: '+tWrap.Title;
                    if(String.IsNotBlank(tWrap.description))
                        objTask.TAFS_Description__c = tWrap.description.left(254);
                    objTask.ActivityDate = Date.today();
                    objTask.TAFS_Blocked_Reason__c = tWrap.blockedreason;
                    
                    if(tWrap.blocked == '1')
                        objTask.TAFS_Blocked_State__c = true;
                    else
                        objTask.TAFS_Blocked_State__c = false;
                        
                    objTask.TAFS_Kanban_Column__c = tWrap.columnname;
                    objTask.TAFS_Kanban_Swimlane__c = tWrap.lanename;
                    objTask.TAFS_Kanban_Card_Id__c = tWrap.taskId;
                    
                    if(tWrap.blockedreason =='Blocked - RS Input Required'){
                        objTask.Status ='Open';
                        objTask.Priority = '7';
                    }
                    else{
                        objTask.Status ='Completed';
                        objTask.Priority = '1';
                    }   
                    
                    if(userId != null)
                        objTask.OwnerId = userId;
                    if(batchId != null)
                        objTask.WhatId = batchId;
                    if(batchNumber != null)
                        objTask.TAFS_Batch_Number__c = batchNumber;
                    
                    if(clientNumber != null)
                        objTask.TAFS_Client_Number__c = clientNumber;
                    
                    if(commentonCard != null){
                        objTask.description = commentonCard;
                    }
                    updateTaskList.add(objTask);
                }
                else{
                    //Update existing Task Record.
                    Task objTask = objTaskList.get(0);
                    objTask.TAFS_Is_Future_Context__c = false;
                    //objTask.Subject = tWrap.Title;
                    //objTask.ActivityDate = Date.today();
                    objTask.TAFS_Blocked_Reason__c = tWrap.blockedreason;
                    if(String.IsNotBlank(tWrap.description))
                        objTask.TAFS_Description__c = tWrap.description.left(254);
                    
                    if(tWrap.blocked == '1'){
                        objTask.TAFS_Blocked_State__c = true;
                        if(tWrap.blockedreason =='Blocked - RS Input Required'){
                            objTask.Status ='Open';
                            objTask.Priority = '7';
                        }
                        else{
                            objTask.Status ='Completed';
                            objTask.Priority = '1';
                        }   
                    
                    }
                    else{
                        objTask.TAFS_Blocked_State__c = false;
                        objTask.Status ='Completed';
                    }   
                    objTask.TAFS_Kanban_Column__c = tWrap.columnname;
                    objTask.TAFS_Kanban_Swimlane__c = tWrap.lanename;
                    objTask.TAFS_Kanban_Card_Id__c = tWrap.taskId;
                    
                    
                    if(userId != null)
                        objTask.OwnerId = userId;
                    if(batchId != null)                    
                        objTask.WhatId = batchId;
                    if(batchNumber != null)
                        objTask.TAFS_Batch_Number__c = batchNumber;
                    
                    if(clientNumber != null)
                        objTask.TAFS_Client_Number__c = clientNumber;
                    
                    if(commentonCard != null){
                        objTask.description = commentonCard;
                    }
                    updateTaskList.add(objTask);
                }
                if(!updateTaskList.isEmpty()){
                    UPSERT updateTaskList;
                }
            }          
        }

    }

    /* Class Name : TaskWrapper
     * Description : Wrapper class to desirialize response received from Kanban
     */

    public class TaskWrapper {
        public String taskid;   //22844
        public String boardid;  //29
        public String title;    //In Progress Task for GG
        public String description;  //Testing Email Integration in SFDC V1.1
        public String type; //None
        public String assignee; //Gaurav
        public String subtasks; //0
        public String subtaskscomplete; //0
        public String color;    //#34a97b
        public String priority; //Average
        public String deadline; //16 Feb
        public String deadlineoriginalformat;   //2017-02-16
        public String extlink;  //www.gg.com
        public String tags; //
        public Integer leadtime;    //20
        public String blocked;  //1
        public String blockedreason;    //Waiting on third party
        public String columnname;   //Done
        public String lanename; //Default Swimlane
        public String columnid; //done_316
        public String laneid;   //317
        public String position; //0
        public String columnpath;   //Done
        public Integer loggedtime;  //0
        public cls_customfields[] customfields;
        public cls_comments[] comments;
    
    }  
    
    /* Class Name : cls_customfields
     * Description : Wrapper class to capture repetative custom fields values associated with Card
     */
 
    class cls_customfields {
        public String fieldid;  //58
        public String name; //Client ID
        public String type; //text
        public String value;    //11211
        public boolean mandatory;
    }
    
    /* Class Name : cls_comments
     * Description : Wrapper class to capture repetative comments associated with Card
     */
    
    class cls_comments {
        public String author;   //Raushan
        public String event;    //Comment added
        public String text;     //C1
        public String cdate;    //2016-05-02 13:41:51
        public String taskid;   //22844
    }
}