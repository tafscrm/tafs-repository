/* CLass Name   : TAFS_Verificatons_Collections_Handler
 * Description  : Trigger methods for TAFS_Verificatons_Collections__c 
 * Created By   : Raushan Anand
 * Created On   : 27-Apr-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                27-Apr-2015              Initial version.
 *
 *****************************************************************************************/
public class TAFS_Verificatons_Collections_Handler {

/****************************************************************************************
* Method Name      :  updateBatchIdOnInvoice
* Input Parameter  :  List of Verifications and Collections (List<TAFS_Verificatons_Collections__c>)
* Return Type      :  Void
* Description      :  This method populates Invoice Id based upon Invoice No.
***************************************************************************************/
    public static void updateInvoiceIdOnVerification(List<TAFS_Verificatons_Collections__c> verificationList){
    
        Map<String,String> actionKeyVal = new Map<String,String>();
        Map<String,String> typeKeyVal = new Map<String,String>();
        List<String> invoiceNoList = new List<String>();
        Map<String,String> invoiceNoIdMap = new Map<String,String>();
        for(TAFS_Verificatons_Collections__c verObj : verificationList){
            if(verObj.TAFS_Invoice_key__c != null){
                invoiceNoList.add(verObj.TAFS_Invoice_key__c);
            }
        }
        List<TAFS_Invoic_Key_Value_Mapping__c> keyValList = TAFS_Invoic_Key_Value_Mapping__c.getall().values();
        
        for(TAFS_Invoic_Key_Value_Mapping__c obj : keyValList){
            if(obj.TAFS_Object_Name__c == 'Verification Collection' && obj.TAFS_Field_Name__c == 'Action'){
                actionKeyVal.put(obj.TAFS_Cadence_Key__c,obj.TAFS_SFDC_Value__c);
            }
            if(obj.TAFS_Object_Name__c == 'Verification Collection' && obj.TAFS_Field_Name__c == 'Type'){
                typeKeyVal.put(obj.TAFS_Cadence_Key__c,obj.TAFS_SFDC_Value__c);
            }
        }
        List<TAFS_Invoice__c> invoiceList = [SELECT Id,TAFS_Invoice_key__c,Name FROM TAFS_Invoice__c WHERE TAFS_Invoice_key__c IN: invoiceNoList];
        for(TAFS_Invoice__c invObj : invoiceList){
            invoiceNoIdMap.put(invObj.TAFS_Invoice_key__c,invObj.Id);
        }
        for(TAFS_Verificatons_Collections__c verObj : verificationList){
            if(invoiceNoIdMap.containsKey(verObj.TAFS_Invoice_key__c)){
                verObj.TAFS_Invoice__c = invoiceNoIdMap.get(verObj.TAFS_Invoice_key__c);
            }
            if(verObj.TAFS_Action__c!= null){
                if(actionKeyVal.containsKey(verObj.TAFS_Action__c)){
                    verObj.TAFS_Action__c = actionKeyVal.get(verObj.TAFS_Action__c);
                }
                if(typeKeyVal.containsKey(verObj.TAFS_Type__c)){
                    verObj.TAFS_Type__c = typeKeyVal.get(verObj.TAFS_Type__c);
                }
            }
        }
    }
}