/* CLass Name   : TAFS_ScheduleFuelConsumptions_Test
 * Description  : Test class for TAFS_ScheduleFuelConsumptions_Batch
 * Created By   : Karthik Gulla
 * Created On   : 07-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla          07-Mar-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TAFS_ScheduleFuelConsumptions_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        DataUtilTest_TAFS.createTAFSBatchExecution('Fuel Consumption Batch Time');
        list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
        list<Account> lstAcct = DataUtilTest_TAFS.createAccounts(2,clientTermList[0].Id);
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[0].Id, UserInfo.getUserId(), 'Relationship Specialist');
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[0].Id, UserInfo.getUserId(), 'Relationship Specialist');
        DataUtilTest_TAFS.createAccountTeamMember(lstAcct[1].Id, UserInfo.getUserId(), 'Relationship Specialist');
        DataUtilTest_TAFS.createOpportunities(1, lstAcct[1].Id,'Fuel');
        DataUtilTest_TAFS.createFuelConsumptions(1, lstAcct[0].Id);
        DataUtilTest_TAFS.createFuelConsumptions(1, lstAcct[1].Id);
    } 

    /************************************************************************************
    * Method       :    testScheduleFuelConsumptions
    * Description  :    Test Method to test the validation of Fuel Consumptions and create
                        fuel opportunities if any are required
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testScheduleFuelConsumptions() {
        Test.startTest();
        TAFS_ScheduleFuelConsumptions_Batch tafsFuelConsumptions = new TAFS_ScheduleFuelConsumptions_Batch();
        String scheduleTime = '0 0 23 * * ?';
        System.schedule('TAFS_ScheduleFuelConsumptions_Batch', scheduleTime, tafsFuelConsumptions);
        Test.stopTest();
    }
}