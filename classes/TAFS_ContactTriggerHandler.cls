/* CLass Name   : TAFS_ContactTriggerHandler
 * Description  : Trigger methods for Contact
 * Created By   : Arpitha Sudhakar
 * Created On   : 12-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar            12-Aug-2016              Initial version.
 *  * Anupama Ajith               17-Aug-2016              Added code to dynamically assign Dousign Signer Role number to the General Manager
                                                           Added code to extend the trigger to Client contacts
 *****************************************************************************************/

public with sharing class TAFS_ContactTriggerHandler{
    
    public static void contactInsert(List<Contact> contactList){
        Set<Id> leadSetIds =  new Set<Id>();
        
        // Added for extending the trigger to Client contacts
        Set<Id> clientSetIds =  new Set<Id>();
        
        List<Contact> contacts = [select id,TAFS_Contact_Type__c,TAFS_Lead__r.id,Account.id, Account.name from Contact where id in :contactList];
        
        for(Contact contactObj : contacts){
            // Getting Leads Ids for new Lead contacts
                if(String.IsNotBlank(contactObj.TAFS_Lead__c) && (contactObj.Account.name == 'TAFS Inc.' || contactObj.Account.name == '' || contactObj.Account.name == null)){
                    leadSetIds.add(contactObj.TAFS_Lead__c);
                    system.debug(contactObj+',1 lead id: '+contactObj.TAFS_Lead__c);     
                }
                
                // Getting Client Ids for new Client contacts
                if(contactObj.Account!=null && (contactObj.Account.name != 'TAFS Inc.' || contactObj.Account.name != '')){
                    clientSetIds.add(contactObj.Account.id);
                }
         }
       
        //system.debug(contactList[0].id+', lead id: '+contactList[0].TAFS_Lead__c);
        // update the number of owner contacts in Leads       
        List<Lead> leadList = [select Id,TAFS_Number_of_Contacts__c,TAFS_General_Manager_tag__c,(SELECT Id,TAFS_Contact_Type__c,Name FROM Contacts__r) FROM Lead where Id IN :leadSetIds AND isConverted = False];
        for(Lead leadObj:leadList){
            leadObj.TAFS_Number_of_Contacts__c = leadObj.Contacts__r.size();
            Integer docusignRoutingNo = 1;
            //Dynamically assign last signer role to GM based on the number of owner contacts
            system.debug('printing contact type????'+leadObj.Contacts__r);
            for(Contact con : leadObj.Contacts__r){
                system.debug('printing contact type????'+con.TAFS_Contact_Type__c);
                if(con.TAFS_Contact_Type__c == 'Owner'){
                    docusignRoutingNo = docusignRoutingNo +1;
                }            
            }
            system.debug('printing docusing routing no>>>>'+docusignRoutingNo);
            leadObj.TAFS_General_Manager_tag__c = 's'+docusignRoutingNo;
        }
        Database.update(leadList);
        
        // update the number of owner contacts in Account   
        List<Account> clientList = [select Id,TAFS_Number_of_Owner_Contacts__c,TAFS_General_Manager_tag__c,(SELECT Id,name FROM Contacts WHERE TAFS_Contact_Type__c = 'Owner') FROM Account where Id IN :clientSetIds];
        for(Account clientObj : clientList){
           clientObj.TAFS_Number_of_Owner_Contacts__c = clientObj.Contacts.size();
            
            //Dynamically assign last signer role to GM based on the number of owner contacts
            Integer docusignRoutingNo = clientObj.Contacts.size() + 1;
            clientObj.TAFS_General_Manager_tag__c = 's'+docusignRoutingNo;
         }
         try{
            Database.update(clientList);
        }
        catch(Exception ex){
            for(Contact clientObj : contactList){
                clientObj.addError(Label.TAFS_Contact_Validation_Message);
            }
        }
        
        
    }
    public static void updateContactFromFC(List<Contact> contactList){
        List<String> referenceIdList = new List<String>();
        List<String> sfIdList = new List<String>();
        List<Lead> leadList = new List<Lead>();
        //List<Account> clientList = new List<Account>();
        Map<String,Id> leadMap = new Map<String,Id>();
        //Map<String,Id> clientMap = new Map<String,Id>();
        for(Contact conObj : contactList){
            if(String.isNotBlank(conObj.TAFS_Lead_Reference_ID__c))
                referenceIdList.add(conObj.TAFS_Lead_Reference_ID__c);
        }
        //List<Contact> conList = [SELECT Id from Contact WHERE TAFS_Lead_Reference_ID__c IN : referenceIdList AND RecordType.Name = 'Master' AND createdBy.Name =: Label.TAFS_Integration_User];
        User userObj = [SELECT id,Name,Profile.Name FROM User WHERE id=:UserInfo.getuserid()];
        leadList = [Select Id, TAFS_Reference_ID__c FROM Lead WHERE TAFS_Reference_ID__c IN : referenceIdList AND IsConverted =: false];
        
        for(Lead l : leadList){
            leadMap.put(l.TAFS_Reference_ID__c, l.Id);
        }
        /*clientList = [Select Id, TAFS_Reference_ID__c FROM Account WHERE TAFS_Reference_ID__c IN : referenceIdList];
        for(Account acc : clientList){
            clientMap.put(acc.TAFS_Reference_Id__c, acc.Id);
        }*/
        
        /*if(conList.size()>0){
            DELETE conList;
        } */
        for(Contact con : contactList){
            /*if(clientMap.containsKey(con.TAFS_Lead_Reference_ID__c)){
                con.AccountId = clientMap.get(con.TAFS_Lead_Reference_ID__c);
            }
            else */
            if(leadMap.containsKey(con.TAFS_Lead_Reference_ID__c)){
                con.TAFS_Lead__c = leadMap.get(con.TAFS_Lead_Reference_ID__c);
                if(userObj.Profile.Name == Label.TAFS_Integration_User && con.TAFS_Lead__c !=null && con.AccountId == null)
                
                    con.AccountId = Label.TAFS_Inc_Client_Id;
            }
        }
    }
    /*
    @Future
    public static void deleteContact(List<Id> idList){
        List<Contact> conList = [SELECT Id from Contact where ID IN: idList];
        DELETE conList;
    }*/
}