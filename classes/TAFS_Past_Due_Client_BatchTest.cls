/* CLass Name   : TAFS_Past_Due_Client_BatchTest
 * Description  : Test class for TAFS_Past_Due_Client_Batch
 * Created By   : Arpitha Sudhakar
 * Created On   : 14-Sept-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * --------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar            14-Sept-2016              Initial version.
 *
 *****************************************************************************************/
 
@isTest
public class TAFS_Past_Due_Client_BatchTest {

    /*static testmethod void test() {
        Database.BatchableContext BC;
       list<TAFS_Client_Terms__c> clientTermList = DataUtilTest_TAFS.createClientTerms(1);
       list<Account> accList = DataUtilTest_TAFS.createAccounts(1,clientTermList[0].Id);
       AccountHistory accHist = new AccountHistory(AccountId =accList[0].Id ,field = 'TAFS_Factored_Volume_vs_Average__c');
       insert accHist;
       List<AccountHistory> accH = new List<AccountHistory>();
       accH.add(accHist);
       DataUtilTest_TAFS.createTasks('Test task 1', 1);
       
       Test.startTest();
       TAFS_Past_Due_Client_Batch AU = new TAFS_Past_Due_Client_Batch();
       Database.executeBatch(AU);
       Test.stopTest();
       
       try{
            AU.execute(BC, accList);
        }catch(Exception ex){}
        AU.finish(BC);
    }*/
}