@isTest
private with sharing class TAFS_DocusignUtility_TEST {

	/************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
     
         list<Lead> leadList 						= DataUtilTest_TAFS.createleads(1);
         list<Account> accList						= DataUtilTest_TAFS.createAccountsForAgreements(1);
         list<Contact> lstcontact   				= DataUtilTest_TAFS.createContactsForLead(1,leadList[0].Id);
         list<TAFS_Agreement__c> createAgreements	= DataUtilTest_TAFS.createAgreements(1,accList[0].Id);
    } 
    private static testmethod void testLeadDocumentSend() {
    	
    	Lead leadObj = [SELECT ID FROM LEAD LIMIT 1];    	
    	Test.startTest();
	    	PageReference pageRef = Page.TAFS_SendDocuSignEmailPage;
	        Test.setCurrentPage(pageRef);
	        ApexPages.currentPage().getParameters().put('Id', leadObj.id);
	        TAFS_DocuSignUtility newInstance = new TAFS_DocuSignUtility ();
	    	newInstance.sendToDocuSign();
	    Test.stopTest();
    } 
    private static testmethod void testAgrrementDocumentSend() {
        
        TAFS_Agreement__c agreementObj = [SELECT ID FROM TAFS_Agreement__c LIMIT 1];
        General_Manager__c gm = new General_Manager__c(Name = 'General Manager',Email__c='jgoode@tafs.com',First_Name__c='Josh',Last_Name__c='Goode');
        insert gm;
                
        Test.startTest();
            PageReference pageRef = Page.TAFS_SendDocuSignRenegotiationEmailPage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id', agreementObj.id);
            TAFS_DocuSignRenegotiationUtility  newInstance = new TAFS_DocuSignRenegotiationUtility  ();
            newInstance.sendToDocuSign();
        Test.stopTest();
    }    
}