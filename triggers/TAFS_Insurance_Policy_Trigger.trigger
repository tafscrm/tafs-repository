/* Trigger Name : TAFS_Insurance_Policy_Trigger
 * Description  : Trigger on Insurance Policy
 * Created By   : Raushan Anand
 * Created On   : 15-June-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand               15-June-2016            Initial version.
 *
 *****************************************************************************************/
trigger TAFS_Insurance_Policy_Trigger on TAFS_Insurance_Policies__c (before insert,after insert) {
    List<String> referenceIdList = new List<String>();
    List<String> sfIdList = new List<String>();
    List<Lead> leadList = new List<Lead>();
    List<Account> clientList = new List<Account>();
    Map<String,Id> leadMap = new Map<String,Id>();
    Map<String,Id> clientMap = new Map<String,Id>();
    if(Trigger.IsInsert && Trigger.IsBefore){ 
        for(TAFS_Insurance_Policies__c  inv : Trigger.New){
            if(String.isNotBlank(inv.TAFS_Reference_ID__c))
                referenceIdList.add(inv.TAFS_Reference_ID__c);
        }
        List<TAFS_Insurance_Policies__c> insuranceList = [SELECT Id from TAFS_Insurance_Policies__c WHERE TAFS_Reference_ID__c IN : referenceIdList];
        leadList = [Select Id, TAFS_Reference_ID__c FROM Lead WHERE TAFS_Reference_ID__c IN : referenceIdList AND IsConverted =: false];
        for(Lead l : leadList){
            leadMap.put(l.TAFS_Reference_ID__c, l.Id);
        }
        clientList = [Select Id, TAFS_Reference_ID__c FROM Account WHERE TAFS_Reference_ID__c IN : referenceIdList];
        for(Account acc : clientList){
            clientMap.put(acc.TAFS_Reference_Id__c, acc.Id);
        }
        
        if(insuranceList.size()>0){
            DELETE insuranceList;
        } 
        for(TAFS_Insurance_Policies__c  inv : Trigger.New){
            if(clientMap.containsKey(inv.TAFS_Reference_ID__c)){
                inv.TAFS_Client__c = clientMap.get(inv.TAFS_Reference_ID__c);
            }
            else if(leadMap.containsKey(inv.TAFS_Reference_ID__c)){
                inv.TAFS_Lead__c = leadMap.get(inv.TAFS_Reference_ID__c);
            }
        }
    }
    if(Trigger.IsInsert && Trigger.IsAfter){
        List<ID> idList = new List<ID>(); 
        for(TAFS_Insurance_Policies__c  inv : Trigger.New){
        	if((inv.TAFS_Effective_Date__c == null) && String.isBlank(inv.TAFS_Insurer__c) && String.isBlank(inv.TAFS_Type__c)){
                   idList.add(inv.Id);
               }
        }
        System.debug('###RA'+idList);
        if(!idList.isEmpty())
        	TAFS_DeleteInsurance.deleteInsurance(idList);
    }
}