/* Trigger Name : TAFS_Driver_List_Trigger
 * Description  : Trigger on Driver List
 * Created By   : Manoj M Vootla
 * Created On   : 27-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla                27-May-2015              Initial version.
 *
 *****************************************************************************************/
trigger TAFS_Driver_List_Trigger on TAFS_Driver_List__c(before insert) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
      TAFS_Driver_List_Handler.updateClientIdOnDriverList(Trigger.New);
        }
        if(Trigger.isUpdate){
            
        }
    }
}