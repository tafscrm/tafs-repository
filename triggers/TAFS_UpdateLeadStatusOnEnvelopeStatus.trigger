trigger TAFS_UpdateLeadStatusOnEnvelopeStatus on dsfs__DocuSign_Status__c (after insert, after update)
{

    // get a set of all completed docusign statuses with Leads
    Set<Id> LeadId = new Set<Id>();
    Set<Id> agreementId = new Set<Id>();
    Map<Id,dsfs__DocuSign_Status__c> LeadDocuSignStatus = new Map<Id,dsfs__DocuSign_Status__c>();
    Map<Id,dsfs__DocuSign_Status__c> AgreementDocuSignStatus = new Map<Id,dsfs__DocuSign_Status__c>();
    for(dsfs__DocuSign_Status__c  status : Trigger.new) {
        if (status.dsfs__Lead__c != null && String.isblank(status.TAFS_Agreement__c) && (status.dsfs__Envelope_Status__c== 'Completed' || status.dsfs__Envelope_Status__c== 'Delivered' || status.dsfs__Envelope_Status__c== 'Sent')) {  
           LeadId.add(status.dsfs__Lead__c);
           LeadDocuSignStatus.put(status.dsfs__Lead__c,status);
       }     
       else if(status.TAFS_Agreement__c != null && (status.dsfs__Envelope_Status__c== 'Completed' || status.dsfs__Envelope_Status__c== 'Delivered' || status.dsfs__Envelope_Status__c== 'Sent')) {  
           agreementId.add(status.TAFS_Agreement__c);
           AgreementDocuSignStatus.put(status.TAFS_Agreement__c,status);                                                                   
       }
    }

    // retrieve these Leads and Agreements
    List<Lead> leads= [SELECT Id, Status, isConverted FROM Lead WHERE Id IN :LeadId and isConverted = false];
    List<TAFS_Agreement__C> agreements= [SELECT Id, TAFS_Status__c FROM TAFS_Agreement__c WHERE Id IN :agreementId];

    // update these Leads
    if(!leads.isEmpty() && leads!=null){
        for(Lead l : leads) {
          dsfs__DocuSign_Status__c dsstatus = LeadDocuSignStatus.get(l.id);
          if((dsstatus.dsfs__Envelope_Status__c== 'Sent' || dsstatus.dsfs__Envelope_Status__c== 'Delivered') && dsstatus.Document_Name__c.contains('Application')){
             l.Status = 'Application Out';
          }
          else if(dsstatus.dsfs__Envelope_Status__c== 'Completed' && dsstatus.Document_Name__c.contains('Application')){
             l.Status = 'Application In';
          }
          else if((dsstatus.dsfs__Envelope_Status__c== 'Sent' || dsstatus.dsfs__Envelope_Status__c== 'Delivered') && dsstatus.Document_Name__c.contains('Agreement')){
             l.Status = 'Agreement Out';
          }
          else if(dsstatus.dsfs__Envelope_Status__c== 'Completed' && dsstatus.Document_Name__c.contains('Agreement')){
             l.Status = 'Agreement In';
          }  
        }
     }
     if(!leads.isEmpty() && leads!=null)
         update leads;
     // update these Agreements
    if(!agreements.isEmpty() && agreements!=null){
        for(TAFS_Agreement__c a : agreements) {
          dsfs__DocuSign_Status__c dsstatus = AgreementDocuSignStatus.get(a.id);
           if((dsstatus.dsfs__Envelope_Status__c== 'Sent' || dsstatus.dsfs__Envelope_Status__c== 'Delivered') && dsstatus.Document_Name__c.contains('Agreement')){
             a.TAFS_Status__c = 'Agreement Out';
          }
          else if(dsstatus.dsfs__Envelope_Status__c== 'Completed' && dsstatus.Document_Name__c.contains('Agreement')){
             a.TAFS_Status__c = 'Agreement In';
          }  
        }
     }
     if(!agreements.isEmpty() && agreements!=null)
         update agreements;
}