/****************************************************************************************
* Created By      :  Arpitha Sudhakar
* Create Date     :  19/08/2016
* Description     :  After insert on Account to enter default value for gallon/liter 
* Modification Log:  Initial version.
***************************************************************************************/
trigger TAFS_UpdateLiterGallonDefaultValue on Account (after insert) {
    TAFS_UpdateLiterGallonDefaultValue.onAfterInsert(trigger.new); 
}