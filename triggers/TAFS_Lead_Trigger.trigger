/* Trigger Name : TAFS_Lead_Trigger
 * Description  : Trigger on Lead
 * Created By   : Manoj M Vootla
 * Created On   : 7-June-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla                7-May-2016              Initial version.
 *
 *****************************************************************************************/
 
trigger TAFS_Lead_Trigger on Lead (after update,before update,before insert, after insert) {
	List<Lead> lstLeads = new List<Lead>();

    if(Trigger.IsBefore){
        TAFS_LeadTriggerHandler.formatphonenum(Trigger.New);
        
        if(Trigger.IsUpdate){
            TAFS_LeadTriggerHandler.validateFCBeforeUpdate(Trigger.New,Trigger.oldMap);
            TAFS_LeadTriggerHandler.updateLeadStatuswhileLeadUpdate(Trigger.NewMap,Trigger.oldMap);
            List<String> referenceList = new List<String>();
            for(Lead objLead : Trigger.New){
                if(objLead.IsConverted)
                    lstLeads.add(objLead);

                if(objLead.IsConverted && String.IsnotBlank(objLead.TAFS_Reference_ID__c)){
                    referenceList.add(objLead.TAFS_Reference_ID__c);
                }
            }
            if(!referenceList.isEmpty()){
                TAFS_LeadTriggerHandler.freightConnectUpdate(referenceList);
            }
            if(lstLeads.size()>0)
                TAFS_LeadTriggerHandler.customizeLeadConversion(lstLeads);
        }
        if(Trigger.IsInsert){
            TAFS_LeadTriggerHandler.updateLeadStatusToIncorrectInfo(Trigger.New);
            TAFS_LeadTriggerHandler.validateFCBeforeInsert(Trigger.New);
        }
    }
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            TAFS_LeadTriggerHandler.updateLeadObjectionOnCon(trigger.New);
        }
        if(Trigger.isUpdate){
            //Associate Insurance Policies to Account
            //TAFS_LeadTriggerHandler.associateInsurancePolicyToAccount(Trigger.New);
            TAFS_LeadTriggerHandler.updateLeadObjectionOnCon(trigger.New);
            //Preventing Lead Assignment based on status and owner
            Profile p = [Select id,name from Profile where Id =: userinfo.getProfileid()];
            String pname = p.name;
            if(pname != 'System Administrator' && pname != 'TAFS_Integration_User')
            TAFS_LeadTriggerHandler.preventLeadAssignment(trigger.new);
            
            //Pardot - Create Prospect
            TAFS_LeadTriggerHandler.afterUpdate();
            
            //Post Lead Conversion Operations
            List<Lead> convertedLead = new List<Lead>();
            List<Id> convertedLeadIDs = new List<ID>();
            for(Lead objLead:Trigger.new)
            {
             if(objLead.IsConverted && objLead.status == Label.TAFS_LeadStatus_Qualified && Trigger.oldMap.get(objLead.Id).status != Label.TAFS_LeadStatus_Qualified)
              convertedLead.add(objLead);
              convertedLeadIDs.add(objLead.Id);
            }          
            
            if(!convertedLead.isEmpty()){            
                TAFS_LeadTriggerHandler.postLeadConversionOperations(convertedLead);
                //Agreement Object - lead conversion
                TAFS_LeadTriggerHandler.mapLeadtoContractAfterConversion(convertedLeadIDs);
            }
            
        }
    }
}