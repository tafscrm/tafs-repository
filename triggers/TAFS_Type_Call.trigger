/* Trigger Name : TAFS_Type_Call
 * Description  : Trigger to Update Lead Status
 * Created By   : Piyush Gupta
 * Created On   : 9-Jun-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Piyush Gupta                9-Jun-2016              Initial version.
 *
 *****************************************************************************************/



trigger TAFS_Type_Call on Task (after insert) 
{
    ID uid;
    list<lead> leadlist1= new list<lead>();
    list<lead> leadlist2= new list<lead>();

    for(Task taskobj1: trigger.new)
    {
        if(taskobj1.whoId!=null){
            if(taskobj1.type == 'Call' && taskobj1.whoId.getSObjectType().getDescribe().getName()=='Lead' )
            {
                uid=taskobj1.whoId;
                leadlist1= [select status,TAFS_Lead_Type__c from lead where id=:uid];
                for(lead leadobj1 : leadlist1)
                {
                    if(leadobj1.status=='New' || leadobj1.status=='No Contact' )
                    {
                        leadobj1.status='Actively Working';
                        
                        leadlist2.add(leadobj1);
                    }
                }    
            }
        }   
    }
    Database.update(leadlist2);
}