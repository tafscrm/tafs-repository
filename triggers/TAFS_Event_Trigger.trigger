/* Trigger Name : TAFS_Event_Trigger
 * Description  : Trigger to update Lead sort order
 * Created By   : Pankaj Singh
 * Created On   : 8-jun-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               3-May-2015              Initial version.
 *
 *****************************************************************************************/
trigger TAFS_Event_Trigger on Event (after insert) {

    TAFS_Event_Trigger_Handler.sortLeadStatus(trigger.new);
}