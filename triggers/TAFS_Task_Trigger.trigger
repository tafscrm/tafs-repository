/* Trigger Name : TAFS_Task_Trigger
 * Description  : Trigger to update Task
 * Created By   : Raushan Anand
 * Created On   : 3-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                3-May-2015              Initial version.
 *  * Manoj Vootla                 4-May-2016              SMS Alerts to Lead Contacts
 *
 *****************************************************************************************/
trigger TAFS_Task_Trigger on Task (before insert, before update,after insert, after update,before delete) {
    
    List<String> cardIdList = new List<String>();
    Map<String,String> cardIdCommentMap = new Map<String,String>();   
        if(Trigger.isBefore){  
            if(TAFS_Check_Recursive.runOnce()){          
                Profile p = [Select id,name from Profile where Id =: userinfo.getProfileid()];
                String pname = p.name;
                if(Trigger.isUpdate){
                    String kanbanUserId;
                    List<User> userList= [SELECT Id,TAFS_Kanban_Id__c FROM User WHERE Id =: UserInfo.getUserId()];
                    if(!userList.isEmpty()){
                        kanbanUserId= userList[0].TAFS_Kanban_Id__c;
                    }
                    ID rectypeid = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Kanban Task').getRecordTypeId();
                    Map<Id,Task> oldTaskMap = Trigger.oldMap;
                    
                    for(Task objTask : Trigger.New){
                        if((!objTask.TAFS_Blocked_State__c) && String.isBlank(objTask.TAFS_New_Comment__c) && objTask.RecordTypeId == rectypeid && objTask.TAFS_Is_Future_Context__c
                            && pname != 'System Administrator' && pname != 'TAFS_Integration_User'){
                            objTask.addError(Label.TAFS_Error_Message_for_Comments);
                        }
                        if(objTask.TAFS_Blocked_Reason__c != null && (!objTask.TAFS_Blocked_State__c) && objTask.TAFS_Blocked_Reason__c != 'Blocked - RS Input Required' && objTask.RecordTypeId == rectypeid
                            && pname != 'System Administrator' && pname != 'TAFS_Integration_User'){
                            objTask.addError(Label.TAFS_Blocked_Reason_Error_Msg);
                        }
                        Task oldTAsk = oldTaskMap.get(objTask.Id);//|| (objTask.OwnerId != oldTAsk.OwnerId)
                        if(objTask.TAFS_Is_Future_Context__c && rectypeid == objTask.RecordTypeId){
                            if(((objTask.Priority != oldTAsk.Priority) || (objTask.Status != oldTAsk.Status)) && objTask.RecordTypeId == rectypeid && pname != 'System Administrator' && pname != 'TAFS_Integration_User')
                                objTask.addError(Label.TAFS_Restrict_Edit_on_Task_Error_Msg);
                            if(objTask.TAFS_New_Comment__c != null){
                                string author = kanbanUserId != null? kanbanUserId : UserInfo.getName();
                                String newComment = '\n\n Author : '+ author +'\nDate : '+ Date.Today()+'\nComments : '+objTask.TAFS_New_Comment__c;
                                if(String.IsBlank(objTask.Description))
                                    objTask.Description = newComment;
                                else
                                    objTask.Description += newComment;
                                
                                cardIdCommentMap.put(objTask.TAFS_Kanban_Card_Id__c,objTask.TAFS_New_Comment__c);
                                objTask.TAFS_New_Comment__c='';
                            }
                            if((!objTask.TAFS_Blocked_State__c) && oldTAsk.TAFS_Blocked_State__c && rectypeid == objTask.RecordTypeId){
                                
                                objTask.Status='Completed';
                                objTask.TAFS_Blocked_Reason__c='';
                                cardIdList.add(objTask.TAFS_Kanban_Card_Id__c);
                            }
                        }
                        else{
                            objTask.TAFS_Is_Future_Context__c=true;
                        }
                    }
                    if(!cardIdList.isEmpty()){
                        TAFS_Task_Trigger_Handler.updateCardsonKanban(cardIdList);
                    }
                    if(!cardIdCommentMap.isEmpty()){
                        TAFS_Task_Trigger_Handler.addComments(cardIdCommentMap);
                    }
                }
                if(Trigger.isInsert){
                    for(Task objTask : Trigger.New){
                        objTask.TAFS_Is_Future_Context__c=true;
                    }                       
                }
                if(Trigger.IsDelete){
                    if(pname != 'System Administrator' && pname != 'TAFS_Integration_User'){
                        for(Task taskObj : Trigger.Old){
                            taskObj.addError('You are not allowed to delete a task. Please go ahead and mark the task as Completed');
                        }
                    }
                }
            } 
        }       
        if(Trigger.isAfter){ 
           if(TAFS_Check_Recursive.runAfter()){                   
               if(Trigger.isInsert)
               {   
                   //Sending SMS Alerts to Lead Contacts                    
                   TAFS_Task_Trigger_Handler.sendSMSToLeadContacts(Trigger.New);
                   TAFS_Task_Trigger_Handler.updateLastAttemptedContactDateOnAccount(Trigger.new);
               }                    
               if(Trigger.isUpdate || Trigger.isInsert){
                        TAFS_Task_Trigger_Handler.UpdateFieldsOnInvoiceBatch(Trigger.New);
                        TAFS_Task_Trigger_Handler.sortLeadStatus(trigger.new);
                        TAFS_Task_Trigger_Handler.leadUnassignWhenTaskComplete(trigger.new);
                        TAFS_Task_Trigger_Handler.updateLeadStatusToIncorrectInfo(trigger.new);
                }
            }          
       }        
                
}