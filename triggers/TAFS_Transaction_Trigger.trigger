/* Trigger Name : TAFS_Transaction_Trigger
 * Description  : Trigger to update Status and Type
 * Created By   : Raushan Anand
 * Created On   : 26-Apr-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                26-Apr-2015              Initial version.
 *
 *****************************************************************************************/
trigger TAFS_Transaction_Trigger on TAFS_Invoice_Transaction__c (before insert,before update) {
    if(Trigger.isBefore){
            TAFS_Transaction_Trigger_Handler.updateStatusTypeOnInvoice(Trigger.New);
            TAFS_Transaction_Trigger_Handler.updateInvoiceIdOnTransaction(Trigger.New);
      
    }
}