/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  13/04/2016
* Description     :  trigger to populate the client Id based on the client number.
* Modification Log:  Initial version.
***************************************************************************************/
trigger TAFS_UpdateUsageClientIdOnInsert on TAFS_Roadside_Assistance__c (before insert) {

    new TAFS_RoadsideSubscriptionDomain().onBeforeInsert(trigger.new);
}