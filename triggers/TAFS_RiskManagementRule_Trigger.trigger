/* Trigger Name : TAFS_RiskManagementRule_Trigger
 * Description  : Trigger on Risk Management Rule
 * Created By   : Karthik Gulla
 * Created On   : 7-DEC-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla                7-DEC-2016              Initial version.
 *
 *****************************************************************************************/
trigger TAFS_RiskManagementRule_Trigger on TAFS_Risk_Management_Rule__c (After Insert) {
	if(Trigger.isAfter){
        if(Trigger.isInsert){
        	//updates 'Debtor Risk Management Rule Status' on Debtor to 'Enrolled' , once rule is created
        	TAFS_RiskManagementRule_Trigger_Handler.updateDebtorRiskManagementRuleStatus(Trigger.New);
        }
    }
}