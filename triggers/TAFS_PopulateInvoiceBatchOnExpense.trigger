/* CLass Name   : TAFS_PopulateInvoiceBatchOnExpense
 * Description  : Trigger for expense object
 * Created By   : Pankaj Singh
 * Created On   : 28-Oct-2015 
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                28-Oct-2015              Initial version.
 *
 *****************************************************************************************/
trigger TAFS_PopulateInvoiceBatchOnExpense on TAFS_Expense__c (before insert) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            TAFS_ExpenseHandler.populateInvoiceBatchonExpense(trigger.new);
        }
    }
}