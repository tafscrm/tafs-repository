/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  14/04/2016
* Description     :  trigger to prevent duplicate fuel discount records with same effective date.
* Modification Log:  Initial version.
***************************************************************************************/
trigger TAFS_PreventDuplicateFuelDiscounts on Fuel_Discount__c (before insert) {

    new TAFS_FuelDiscountDomain().onBeforeInsert(trigger.new);   

}