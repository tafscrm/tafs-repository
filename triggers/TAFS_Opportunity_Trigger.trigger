/* Trigger Name : TAFS_Opportunity_Trigger
 * Description  : Trigger on Opportunity
 * Created By   : Karthik Gulla
 * Created On   : 04-April-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla           	   04-April-2017           Initial version.
 *
 *****************************************************************************************/
trigger TAFS_Opportunity_Trigger on Opportunity (Before Update) {
    if(Trigger.IsBefore){
        if(Trigger.IsUpdate){
            TAFS_Opportunity_Trigger_Handler.triggerEmailwithDocusignAttachment(Trigger.New, Trigger.OldMap);
        }
    }
}