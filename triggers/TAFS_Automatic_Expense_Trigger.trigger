/* Trigger Name : TAFS_Automatic_Expense_Trigger
 * Description  : Trigger on Automatic Expense
 * Created By   : Manoj M Vootla
 * Created On   : 27-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla                27-May-2015              Initial version.
 *
 *****************************************************************************************/
trigger TAFS_Automatic_Expense_Trigger on TAFS_Automatic_Expenses__c(before insert) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
      TAFS_Automatic_Expenses_Handler.updateClientIdOnAutomaticExpense(Trigger.New);
        }
        if(Trigger.isUpdate){
            
        }
    }
}