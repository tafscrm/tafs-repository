/* Trigger Name : TAFS_Verificatons_Collections_Trigger
 * Description  : Trigger on Verificatons & Collections
 * Created By   : Raushan Anand
 * Created On   : 27-Apr-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                27-Apr-2015              Initial version.
 *
 *****************************************************************************************/
trigger TAFS_Verificatons_Collections_Trigger on TAFS_Verificatons_Collections__c (before insert, before update) {
    if(Trigger.isBefore){
        
            TAFS_Verificatons_Collections_Handler.updateInvoiceIdOnVerification(Trigger.New);        
    }
}