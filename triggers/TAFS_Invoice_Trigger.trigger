/* Trigger Name : TAFS_Invoice_Batch_Trigger
 * Description  : Trigger to update Status and Type
 * Created By   : Raushan Anand
 * Created On   : 26-Apr-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                26-Apr-2015              Initial version.
 *
 *****************************************************************************************/
trigger TAFS_Invoice_Trigger on TAFS_Invoice__c (before insert,before update) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            TAFS_Invoice_Trigger_Handler.updateStatusTypeOnInvoice(Trigger.New);
            TAFS_Invoice_Trigger_Handler.updateBatchIdOnInvoice(Trigger.New);
            TAFS_Invoice_Trigger_Handler.updateDebtorFirstAssociationWithClient(Trigger.New);
        }
        if(Trigger.isUpdate){
            TAFS_Invoice_Trigger_Handler.updateStatusTypeOnInvoice(Trigger.New);
            TAFS_Invoice_Trigger_Handler.updateDebtorFirstAssociationWithClient(Trigger.New);
        }
    }
}