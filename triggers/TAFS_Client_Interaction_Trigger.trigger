/* Trigger Name : TAFS_Client_Interaction_Trigger
 * Description  : Trigger on Client Interaction
 * Created By   : Manoj M Vootla
 * Created On   : 30-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla                30-May-2015              Initial version.
 *  * Karthik Gulla                 22-Dec-2016              made invocation of
 *  *                                                        updateLastAttemptedContactDateOnClient 
 *  *                                                        to Before Insert
 *****************************************************************************************/
trigger TAFS_Client_Interaction_Trigger on TAFS_Client_Interaction__c (Before Insert) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
          TAFS_Client_Interaction_Handler.updateLastAttemptedContactDateOnClient(Trigger.New);
        }
    }
}