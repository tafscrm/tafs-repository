/* Trigger Name : TAFS_Contact_Trigger
 * Description  : Trigger on Contact
 * Created By   : Arpitha Sudhakar
 * Created On   : 12-Aug-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Arpitha Sudhakar            12-Aug-2016              Initial version.
 *
 *****************************************************************************************/
trigger TAFS_Contact_Trigger on Contact(after insert, after update, after delete,before Insert){
    if(Trigger.IsBefore){
        if(Trigger.isInsert){
            TAFS_ContactTriggerHandler.updateContactFromFC(Trigger.new);
        }
    }
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            TAFS_ContactTriggerHandler.contactInsert(Trigger.new);
            /*List<ID> idList = new List<ID>(); 
            for(Contact  con : Trigger.New){
                if(String.isBlank(con.FirstName) && String.isBlank(con.Phone) && String.isBlank(con.Email)){
                   idList.add(con.Id);
                }
            }
            System.debug('###RA'+idList);
            if(!idList.isEmpty())
                TAFS_ContactTriggerHandler.deleteContact(idList);*/
        }
        if(Trigger.isDelete){
            TAFS_ContactTriggerHandler.contactInsert(Trigger.old);
        }
        if(Trigger.isUpdate){
            if(TAFS_Check_Recursive.runContactAfterUpdate()){
                TAFS_ContactTriggerHandler.contactInsert(Trigger.new);
            }
        }
    }
}