/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  14/04/2016
* Description     :  trigger to prevent duplicate fuel commitment records with same effective date.
* Modification Log:  Initial version.
***************************************************************************************/
trigger TAFS_PreventDuplicateFuelCommitments on TAFS_Fuel_Commitment__c (before insert) {

 new TAFS_FuelCommitmentDomain().onBeforeInsert(trigger.new);

}