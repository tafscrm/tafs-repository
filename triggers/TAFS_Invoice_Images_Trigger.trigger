/* Trigger Name : TAFS_Invoice_Images_Trigger
 * Description  : Trigger on Invoice Images
 * Created By   : Manoj M Vootla
 * Created On   : 17-May-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla                17-May-2015              Initial version.
 *
 *****************************************************************************************/
trigger TAFS_Invoice_Images_Trigger on TAFS_Invoice_Images__c (before insert) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
      TAFS_Invoice_Images_Handler.updateInvoiceIdOnInvoiceImages(Trigger.New);
        }
        if(Trigger.isUpdate){
            
        }
    }
}