/* Trigger Name  : TAFS_UpdateInvoiceBatchOnIncomingSMS
 * Description  : Trigger on Incoming SMS
 * Created By   : Manoj M Vootla
 * Created On   : 28-July-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla               28-July-2016           Initial version.
 *
 *****************************************************************************************/
trigger TAFS_UpdateInvoiceBatchOnIncomingSMS on smagicinteract__Incoming_SMS__c (before insert) {
 
 if(Trigger.isInsert)
   {
    if(Trigger.isBefore)
    {
     List<Id> ibConList = new List<Id>();
     List<Id> contIdList = new List<Id>();
     Map<Id,Id> ibConBatchMap = new Map<Id,Id>();
     Map<Id,String> ibConRSEmailMap = new Map<Id,String>();
     Map<Id,String> contLeadEmailMap = new Map<Id,String>();
     Map<Id,String> contRSEmailMap = new Map<Id,String>();
     for(smagicinteract__Incoming_SMS__c incSMS : Trigger.new)
     { 
      if(incSMS.TAFS_Invoice_Batch_Contact_Relation__c != null)
      ibConList.add(incSMS.TAFS_Invoice_Batch_Contact_Relation__c);
      if(incSMS.smagicinteract__Contact__c != null)
      contIdList.add(incSMS.smagicinteract__Contact__c);
     }
     
     for(TAFS_Invoice_Batch_Contact_Relation__c ibCon :[select id,name,TAFS_Invoice_Batch__c,TAFS_Relationship_Specialist_Email__c from TAFS_Invoice_Batch_Contact_Relation__c where id in: ibConList ])
     {
      ibConBatchMap.put(ibCon.id,ibCon.TAFS_Invoice_Batch__c);
      ibConRSEmailMap.put(ibCon.id,ibCon.TAFS_Relationship_Specialist_Email__c);
     }
     
     for(Contact c : [select id,name,TAFS_Lead_Owner_Email__c,TAFS_Relationship_Specialist_Email__C from contact where id in : contIdList])
     {
      contLeadEmailMap.put(c.id,c.TAFS_Lead_Owner_Email__c);
      contRSEmailMap.put(c.id,c.TAFS_Relationship_Specialist_Email__c);
     }
     
     for(smagicinteract__Incoming_SMS__c incSMS : Trigger.new)
     {
      if(incSMS.TAFS_Invoice_Batch_Contact_Relation__c != null)
        {
         incSMS.TAFS_Invoice_Batch__c = ibConBatchMap.get(incSMS.TAFS_Invoice_Batch_Contact_Relation__c);
         incSMS.TAFS_Relationship_Specialist_Email__c = ibConRSEmailMap.get(incSMS.TAFS_Invoice_Batch_Contact_Relation__c);
        }
      if(incSMS.smagicinteract__Contact__c != null)
         incSMS.TAFS_Lead_Owner_Email__c = contLeadEmailMap.get(incSMS.smagicinteract__Contact__c); 
         incSMS.TAFS_Relationship_Specialist_Email__c = contRSEmailMap.get(incSMS.smagicinteract__Contact__c); 
     }
     
     
    }
   }
}