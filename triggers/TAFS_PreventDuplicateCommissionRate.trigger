/****************************************************************************************
* Created By      :  Manoj Vootla
* Create Date     :  26/05/2016
* Description     :  trigger to prevent duplicate Commission Rate records with same effective date.
* Modification Log:  Initial version.
***************************************************************************************/
trigger TAFS_PreventDuplicateCommissionRate on TAFS_Commission_Rate__c (before insert) {

    new TAFS_CommissionRateDomain().onBeforeInsert(trigger.new);
}