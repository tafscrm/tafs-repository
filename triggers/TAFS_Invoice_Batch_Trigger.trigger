/* Trigger Name : TAFS_Invoice_Batch_Trigger
 * Description  : Trigger to update Status and Type
 * Created By   : Raushan Anand
 * Created On   : 26-Apr-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand                26-Apr-2015              Initial version.
 *  * Manoj Vootla                 29-Jul-2016              SMS Notications
 *****************************************************************************************/
trigger TAFS_Invoice_Batch_Trigger on TAFS_Invoice_Batch__c (before insert,before update,after insert,after update) {
        
    if(Trigger.isBefore){
        if(Trigger.isInsert || Trigger.isUpdate){
            for(TAFS_Invoice_Batch__c batchObj : trigger.New){
                if(batchObj.TAFS_Status__c!=null && (batchObj.TAFS_Status__c.equalsIgnoreCase('Processed') || batchObj.TAFS_Status__c.equalsIgnoreCase('void'))){
                    batchObj.TAFS_Blocked_State__c = false;
                    batchObj.TAFS_Blocked_Reason__c = '';
                }
            }
            TAFS_Invoice_Batch_Trigger_Handler.updateStatusTypeOnInvoice(Trigger.New);
        }
    }
    if(Trigger.IsAfter){
        if(Trigger.isInsert || Trigger.isUpdate){
           
            TAFS_Invoice_Batch_Trigger_Handler.updateMonthToDate(Trigger.New);
            TAFS_Invoice_Batch_Trigger_Handler.associateKanbanTask(Trigger.New);
            
        }
        if(Trigger.isInsert){
        
            set<Id> invoiceBatchidSet = new set<Id>();
            TAFS_Invoice_Batch_SMS_Trigger_Helper.createInvoiceBatchContactRecords(Trigger.new);
            for(TAFS_Invoice_Batch__c invoiceBatchObj : trigger.new){
                invoiceBatchidSet.add(invoiceBatchObj.id);
            }
            if(!invoiceBatchidSet.isEmpty()) {           
                TAFS_PushNotificationHandler.onBatchInsert(invoiceBatchidSet); 
            }                      
        }
        if(Trigger.isUpdate){
            
            List<TAFS_Invoice_Batch__c> updatedIBList = new List<TAFS_Invoice_Batch__c>();
            set<Id> invoiceBatchidSet = new set<Id>();
            for(TAFS_Invoice_Batch__c ib : Trigger.new)
            {
                if((Trigger.oldmap.get(ib.id).TAFS_Blocked_State__c != ib.TAFS_Blocked_State__c && ib.TAFS_Blocked_State__c) || 
                (Trigger.oldmap.get(ib.id).TAFS_Status__c != ib.TAFS_Status__c && ib.TAFS_Status__c == Label.TAFS_Pick_list_Value_For_Invoice_Batch_Processed) || 
                (Trigger.oldmap.get(ib.id).TAFS_Paid_to_Client__c != ib.TAFS_Paid_to_Client__c && ib.TAFS_Paid_to_Client__c >0) ||
                (Trigger.oldmap.get(ib.id).TAFS_Blocked_Reason__c != ib.TAFS_Blocked_Reason__c && ib.TAFS_Blocked_Reason__c == Label.TAFS_Blocked_Batch_RSInput))
                {
                    invoiceBatchidSet.add(ib.Id);
                }    
                if(Trigger.oldmap.get(ib.id).TAFS_Blocked_State__c != ib.TAFS_Blocked_State__c || Trigger.oldmap.get(ib.id).TAFS_Blocked_Reason__c != ib.TAFS_Blocked_Reason__c || Trigger.oldmap.get(ib.id).TAFS_Status__c != ib.TAFS_Status__c
                || Trigger.oldmap.get(ib.id).TAFS_Paid_to_Client__c != ib.TAFS_Paid_to_Client__c)
                {
                    updatedIBList.add(ib);
                }
           }
           if(!invoiceBatchidSet.isEmpty()){
               
               TAFS_PushNotificationHandler.onBatchUpdate(invoiceBatchidSet);
           }
           if(!updatedIBList.isEmpty()){
               TAFS_Invoice_Batch_SMS_Trigger_Helper.createInvoiceBatchContactRecords(updatedIBList);
           }
        }
    }

}