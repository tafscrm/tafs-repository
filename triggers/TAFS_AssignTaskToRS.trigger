/* Trigger Name : TAFS_Lead_Trigger
 * Description  : Trigger on Lead
 * Created By   : Pankaj Singh
 * Created On   : 17-June-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               17-June-2016              Initial version.
 *
 *****************************************************************************************/
trigger TAFS_AssignTaskToRS on Account (before update) {
    TAFS_AccountTriggerHandler.assignTask(trigger.new , trigger.oldmap);
}