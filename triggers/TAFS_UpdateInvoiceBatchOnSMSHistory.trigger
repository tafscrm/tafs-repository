/* Trigger Name  : TAFS_UpdateInvoiceBatchOnSMSHistory
 * Description  : Trigger on SMS History
 * Created By   : Manoj M Vootla
 * Created On   : 28-July-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Manoj M Vootla               28-July-2016           Initial version.
 *
 *****************************************************************************************/
trigger TAFS_UpdateInvoiceBatchOnSMSHistory on smagicinteract__smsMagic__c (before insert) {
  
  if(Trigger.isInsert)
   {
    if(Trigger.isBefore)
    {
     List<Id> ibConList = new List<Id>();
     Map<Id,Id> ibConBatchMap = new Map<Id,Id>();
     
     for(smagicinteract__smsMagic__c smsHist : Trigger.new)
     {
      if(smsHist.TAFS_Invoice_Batch_Contact_Relation__c != null)
      ibConList.add(smsHist.TAFS_Invoice_Batch_Contact_Relation__c);
     }
     
     for(TAFS_Invoice_Batch_Contact_Relation__c ibCon :[select id,name,TAFS_Invoice_Batch__c from TAFS_Invoice_Batch_Contact_Relation__c where id in: ibConList ])
     {
      ibConBatchMap.put(ibCon.id,ibCon.TAFS_Invoice_Batch__c);
     }
     
     for(smagicinteract__smsMagic__c smsHist : Trigger.new)
     {
      if(smsHist.TAFS_Invoice_Batch_Contact_Relation__c != null)
       smsHist.TAFS_Invoice_Batch__c = ibConBatchMap.get(smsHist.TAFS_Invoice_Batch_Contact_Relation__c);
     }
     
    }
   }
}