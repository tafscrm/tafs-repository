<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-Campaign</defaultLandingTab>
    <tab>standard-Campaign</tab>
    <tab>standard-Lead</tab>
    <tab>TAFS_Competitor__c</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>TAFS_Invoice_Batch__c</tab>
    <tab>TAFS_Invoice__c</tab>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
</CustomApplication>
