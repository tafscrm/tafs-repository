<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TAFS_Set_Send_SMS_To_False_On_Contact</fullName>
        <field>TAFS_Send_SMS_Leads__c</field>
        <literalValue>0</literalValue>
        <name>Set Send SMS (Leads) to False on Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Update_SMS_Phone_On_Contact</fullName>
        <field>TAFS_SMS_Phone__c</field>
        <formula>IF( NOT( ISBLANK( MobilePhone ) ) ,&apos;1&apos;+SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(MobilePhone , &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;) , &apos;1&apos;+SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(Phone , &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;))</formula>
        <name>Update SMS Phone on Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Trigger_Context</fullName>
        <field>TAFS_Is_Lead_Trigger_Context__c</field>
        <literalValue>0</literalValue>
        <name>Update Lead Trigger Context</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TAFS Modify Trigger Context</fullName>
        <actions>
            <name>Update_Lead_Trigger_Context</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.TAFS_Is_Lead_Trigger_Context__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Lead_SMS_Agreement_Out_Canada</fullName>
        <actions>
            <name>SMS_Notification_Lead_Agreement_Out_Canada</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT( ISBLANK( TAFS_Lead__c ) ) , ISPICKVAL(TAFS_Lead__r.Status,&apos;Agreement Out&apos;) , TAFS_Send_SMS_Leads__c , OR( TAFS_Lead__r.State = &apos;AB&apos;,TAFS_Lead__r.State = &apos;BC&apos;,TAFS_Lead__r.State = &apos;MB&apos;,TAFS_Lead__r.State = &apos;NB&apos;,TAFS_Lead__r.State = &apos;NL&apos;,TAFS_Lead__r.State = &apos;NT&apos;,TAFS_Lead__r.State = &apos;NS&apos;,TAFS_Lead__r.State = &apos;NU&apos;,TAFS_Lead__r.State = &apos;ON&apos;,TAFS_Lead__r.State = &apos;PE&apos;,TAFS_Lead__r.State = &apos;QC&apos;,TAFS_Lead__r.State = &apos;SK&apos;,TAFS_Lead__r.State = &apos;YT&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Lead_SMS_Agreement_Out_USA</fullName>
        <actions>
            <name>SMS_Notification_Lead_Agreement_Out_USA</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT( ISBLANK( TAFS_Lead__c ) ) , ISPICKVAL(TAFS_Lead__r.Status,&apos;Agreement Out&apos;) , TAFS_Send_SMS_Leads__c , OR( AND(TAFS_Lead__r.State &lt;&gt; &apos;AB&apos;,TAFS_Lead__r.State &lt;&gt; &apos;BC&apos;,TAFS_Lead__r.State &lt;&gt; &apos;MB&apos;,TAFS_Lead__r.State &lt;&gt; &apos;NB&apos;,TAFS_Lead__r.State &lt;&gt; &apos;NL&apos;,TAFS_Lead__r.State &lt;&gt; &apos;NT&apos;,TAFS_Lead__r.State &lt;&gt; &apos;NS&apos;,TAFS_Lead__r.State &lt;&gt; &apos;NU&apos;,TAFS_Lead__r.State &lt;&gt; &apos;ON&apos;,TAFS_Lead__r.State &lt;&gt; &apos;PE&apos;,TAFS_Lead__r.State &lt;&gt; &apos;QC&apos;,TAFS_Lead__r.State &lt;&gt; &apos;SK&apos;,TAFS_Lead__r.State &lt;&gt; &apos;YT&apos;) , ISBLANK(TAFS_Lead__r.State)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Lead_SMS_Application_Out_Canada</fullName>
        <actions>
            <name>SMS_Notification_Lead_Application_Out_Canada</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT( ISBLANK( TAFS_Lead__c ) ) , ISPICKVAL(TAFS_Lead__r.Status,&apos;Application Out&apos;) , TAFS_Send_SMS_Leads__c , OR( TAFS_Lead__r.State = &apos;AB&apos;,TAFS_Lead__r.State = &apos;BC&apos;,TAFS_Lead__r.State = &apos;MB&apos;,TAFS_Lead__r.State = &apos;NB&apos;,TAFS_Lead__r.State = &apos;NL&apos;,TAFS_Lead__r.State = &apos;NT&apos;,TAFS_Lead__r.State = &apos;NS&apos;,TAFS_Lead__r.State = &apos;NU&apos;,TAFS_Lead__r.State = &apos;ON&apos;,TAFS_Lead__r.State = &apos;PE&apos;,TAFS_Lead__r.State = &apos;QC&apos;,TAFS_Lead__r.State = &apos;SK&apos;,TAFS_Lead__r.State = &apos;YT&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Lead_SMS_Application_Out_USA</fullName>
        <actions>
            <name>SMS_Notification_Lead_Application_Out_USA</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT( ISBLANK( TAFS_Lead__c ) ) , ISPICKVAL(TAFS_Lead__r.Status,&apos;Application Out&apos;) , TAFS_Send_SMS_Leads__c , OR( AND(TAFS_Lead__r.State &lt;&gt; &apos;AB&apos;,TAFS_Lead__r.State &lt;&gt; &apos;BC&apos;,TAFS_Lead__r.State &lt;&gt; &apos;MB&apos;,TAFS_Lead__r.State &lt;&gt; &apos;NB&apos;,TAFS_Lead__r.State &lt;&gt; &apos;NL&apos;,TAFS_Lead__r.State &lt;&gt; &apos;NT&apos;,TAFS_Lead__r.State &lt;&gt; &apos;NS&apos;,TAFS_Lead__r.State &lt;&gt; &apos;NU&apos;,TAFS_Lead__r.State &lt;&gt; &apos;ON&apos;,TAFS_Lead__r.State &lt;&gt; &apos;PE&apos;,TAFS_Lead__r.State &lt;&gt; &apos;QC&apos;,TAFS_Lead__r.State &lt;&gt; &apos;SK&apos;,TAFS_Lead__r.State &lt;&gt; &apos;YT&apos;) , ISBLANK(TAFS_Lead__r.State)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Set_Send_SMS_To_False_On_Contact</fullName>
        <actions>
            <name>TAFS_Set_Send_SMS_To_False_On_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.TAFS_Send_SMS_Leads__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Set the Send SMS (Leads) field to false on Contact, as soon as it is made true.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Update_SMS_Phone_On_Contact</fullName>
        <actions>
            <name>TAFS_Update_SMS_Phone_On_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( RecordType.Name  &lt;&gt; &apos;Debtor&apos; , OR( ISCHANGED( MobilePhone ) , ISCHANGED( Phone ), ISNEW()  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>SMS_Notification_Lead_Agreement_Out_Canada</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Contact-fae6</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>1</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
    <tasks>
        <fullName>SMS_Notification_Lead_Agreement_Out_USA</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Contact-ac64</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>1</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
    <tasks>
        <fullName>SMS_Notification_Lead_Application_Out_Canada</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Contact-e761</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>1</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
    <tasks>
        <fullName>SMS_Notification_Lead_Application_Out_USA</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Contact-a6d5</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>1</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
</Workflow>
