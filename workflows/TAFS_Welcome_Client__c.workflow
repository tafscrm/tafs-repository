<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Called_No_Contact_Counter</fullName>
        <field>TAFS_Called_No_Contact_Counter__c</field>
        <formula>TAFS_Called_No_Contact_Counter__c +1</formula>
        <name>Update Called No Contact Counter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TAFS_UpdateCallNoContactCounter</fullName>
        <actions>
            <name>Update_Called_No_Contact_Counter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(ISNEW(),AND(TAFS_Activate_ATM_option__c = false,TAFS_Activate_EFS_cards__c=false,TAFS_Conference_EFS_Account_Management__c=false,TAFS_Confirm_client_can_log_into_Sight__c=false,TAFS_Download_Mobile_app__c=false,TAFS_Driver_Version__c=false,TAFS_Emergency_Roadside_Assistance__c=false, TAFS_Give_6_digit_account_number__c=false,TAFS_Go_through_Invoice_Search__c=false,TAFS_submit_credit_check_on_new_debtor__c=false,TAFS_Owner_Version__c=false,TAFS_Paper_Submission_Review_Completed__c=false,TAFS_Refer_a_Friend__c=false, TAFS_Review_detail_tab__c=false,TAFS_Review_look_up_for__c=false,TAFS_Review_main_page__c=false,TAFS_Review_procedures__c=false,TAFS_Review_reports__c=false,TAFS_Review_required_paper_work__c=false,TAFS_Review_requirements_paper_work__c=false, TAFS_Review_transaction_limit_defaults__c=false,TAFS_Review_transaction_tab__c=false,TAFS_Set_up_ACH_option__c=false ,TAFS_Set_up_additional_functionality__c=false,TAFS_Show_fast_aging_with_balances__c=false,TAFS_Show_reporting__c=false,TAFS_Fuel_Rebate__c =false, TAFS_Validate_EFS_phone_no_with_client__c =false,TAFS_Validate_EFS_phone_no_with_client__c =false,TAFS_Walk_through_EFS_money_codes__c=false )),true, AND(NOT(ISNEW()),NOT(OR(ISCHANGED(TAFS_Activate_ATM_option__c),ISCHANGED(TAFS_Activate_EFS_cards__c), ISCHANGED(TAFS_Conference_EFS_Account_Management__c),ISCHANGED(TAFS_Confirm_client_can_log_into_Sight__c),ISCHANGED(TAFS_Download_Mobile_app__c),ISCHANGED(TAFS_Driver_Version__c),ISCHANGED(TAFS_Emergency_Roadside_Assistance__c), ISCHANGED(TAFS_Give_6_digit_account_number__c),ISCHANGED(TAFS_Gold_Tire_Discount_Program__c),ISCHANGED(TAFS_Go_through_Invoice_Search__c),ISCHANGED(TAFS_submit_credit_check_on_new_debtor__c),ISCHANGED(TAFS_Owner_Version__c), ISCHANGED(TAFS_Paper_Submission_Review_Completed__c),ISCHANGED(TAFS_Refer_a_Friend__c),ISCHANGED(TAFS_Review_detail_tab__c),ISCHANGED(TAFS_Review_look_up_for__c),ISCHANGED(TAFS_Review_main_page__c),ISCHANGED(TAFS_Review_procedures__c), ISCHANGED(TAFS_Review_reports__c),ISCHANGED(TAFS_Review_required_paper_work__c),ISCHANGED(TAFS_Review_requirements_paper_work__c),ISCHANGED(TAFS_Review_transaction_limit_defaults__c),ISCHANGED(TAFS_Review_transaction_tab__c),ISCHANGED(TAFS_Set_up_ACH_option__c ), ISCHANGED(TAFS_Set_up_additional_functionality__c),ISCHANGED(TAFS_Show_fast_aging_with_balances__c),ISCHANGED(TAFS_Show_reporting__c),ISCHANGED(TAFS_Fuel_Rebate__c ), ISCHANGED(TAFS_Validate_EFS_phone_no_with_client__c ), ISCHANGED(TAFS_Validate_EFS_phone_no_with_client__c ),ISCHANGED( TAFS_Walk_through_EFS_money_codes__c )))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
