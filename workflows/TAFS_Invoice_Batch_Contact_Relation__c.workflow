<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>TAFS_SMS_Invoice_Batch_Blocked</fullName>
        <actions>
            <name>SMS_Notification_Blocked</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( INCLUDES( TAFS_Preferred_Communication_Channel__c , &quot;Text&quot;) , TAFS_Blocked_State__c , TAFS_Blocked_Reason__c = &quot;Blocked - RS Input Required&quot; ,ISPICKVAL(Contact__r.Account.TAFS_Office__c, &apos;USA&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_SMS_Invoice_Batch_Blocked_Canada</fullName>
        <actions>
            <name>SMS_Notification_Blocked_Canada</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( INCLUDES( TAFS_Preferred_Communication_Channel__c , &quot;Text&quot;) , TAFS_Blocked_State__c , TAFS_Blocked_Reason__c = &quot;Blocked - RS Input Required&quot; ,ISPICKVAL(Contact__r.Account.TAFS_Office__c, &apos;CANADA&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_SMS_Invoice_Batch_Created</fullName>
        <actions>
            <name>SMS_Notification_Created</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( INCLUDES( TAFS_Preferred_Communication_Channel__c , &quot;Text&quot;), TAFS_Invoice_Batch_Created__c ,  ISPICKVAL(Contact__r.Account.TAFS_Office__c, &apos;USA&apos;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_SMS_Invoice_Batch_Created_Canada</fullName>
        <actions>
            <name>SMS_Notification_Created_Canada</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( INCLUDES( TAFS_Preferred_Communication_Channel__c , &quot;Text&quot;), TAFS_Invoice_Batch_Created__c ,  ISPICKVAL(Contact__r.Account.TAFS_Office__c, &apos;CANADA&apos;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_SMS_Invoice_Batch_Funded</fullName>
        <actions>
            <name>SMS_Notification_Funded</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( INCLUDES( TAFS_Preferred_Communication_Channel__c , &quot;Text&quot;),TAFS_Invoice_Batch_Status__c = &quot;Processed&quot;, TAFS_Paid_to_Client__c  &gt; 0,  OR(ISCHANGED(TAFS_Paid_to_Client__c),ISCHANGED( TAFS_Invoice_Batch_Status__c ), ISNEW() ),ISPICKVAL(Contact__r.Account.TAFS_Office__c, &apos;USA&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_SMS_Invoice_Batch_Funded_Canada</fullName>
        <actions>
            <name>SMS_Notification_Funded_Canada</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( INCLUDES( TAFS_Preferred_Communication_Channel__c , &quot;Text&quot;),TAFS_Invoice_Batch_Status__c = &quot;Processed&quot;, TAFS_Paid_to_Client__c  &gt; 0,  OR(ISCHANGED(TAFS_Paid_to_Client__c),ISCHANGED( TAFS_Invoice_Batch_Status__c ), ISNEW() ),ISPICKVAL(Contact__r.Account.TAFS_Office__c, &apos;CANADA&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>SMS_Notification_Blocked</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-TAFS_Invoice_Batch_Contact_Relation__c-0f89</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>1</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
    <tasks>
        <fullName>SMS_Notification_Blocked_Canada</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-TAFS_Invoice_Batch_Contact_Relation__c-22a4</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>1</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
    <tasks>
        <fullName>SMS_Notification_Created</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-TAFS_Invoice_Batch_Contact_Relation__c-f2a3</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>1</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
    <tasks>
        <fullName>SMS_Notification_Created_Canada</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-TAFS_Invoice_Batch_Contact_Relation__c-a26c</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>1</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
    <tasks>
        <fullName>SMS_Notification_Funded</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-TAFS_Invoice_Batch_Contact_Relation__c-d35b</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>1</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
    <tasks>
        <fullName>SMS_Notification_Funded_Canada</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-TAFS_Invoice_Batch_Contact_Relation__c-c392</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>1</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
</Workflow>
