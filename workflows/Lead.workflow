<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Direct_Sales_CFO_Approval</fullName>
        <description>Direct Sales - CFO Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Approval</template>
    </alerts>
    <alerts>
        <fullName>Direct_Sales_CFO_Rejection</fullName>
        <description>Direct Sales - CFO Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Director_of_Sales</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Sales_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Vice_President_and_General_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Direct_Sales_Director_of_Sales_Approval</fullName>
        <description>Direct Sales - Director of Sales Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Approval</template>
    </alerts>
    <alerts>
        <fullName>Direct_Sales_Director_of_Sales_Rejection</fullName>
        <description>Direct Sales - Director of Sales Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Sales_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Direct_Sales_General_Manager_Approval</fullName>
        <description>Direct Sales - General Manager Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Approval</template>
    </alerts>
    <alerts>
        <fullName>Direct_Sales_General_Manager_Rejection</fullName>
        <description>Direct Sales - General Manager Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Director_of_Sales</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Sales_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Rejection</template>
    </alerts>
    <alerts>
        <fullName>National_Account_VP_of_Sales_Approval</fullName>
        <description>National Account - VP of Sales Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Approval</template>
    </alerts>
    <alerts>
        <fullName>National_Account_VP_of_Sales_Rejection</fullName>
        <description>National Account - VP of Sales Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>National_Account_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Rejection</template>
    </alerts>
    <alerts>
        <fullName>National_Accounts_CFO_Approval</fullName>
        <description>National Accounts - CFO Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Approval</template>
    </alerts>
    <alerts>
        <fullName>National_Accounts_CFO_Rejection</fullName>
        <description>National Accounts - CFO Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>National_Account_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Vice_President_and_General_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Vice_President_of_Sales_and_Marketing</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Rejection</template>
    </alerts>
    <alerts>
        <fullName>National_Accounts_General_Manager_Approval</fullName>
        <description>National Accounts - General Manager Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Approval</template>
    </alerts>
    <alerts>
        <fullName>National_Accounts_General_Manager_Rejection</fullName>
        <description>National Accounts - General Manager Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>National_Account_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Vice_President_of_Sales_and_Marketing</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Rejection</template>
    </alerts>
    <alerts>
        <fullName>National_Manager_Special_Pricing_Approval</fullName>
        <description>National Manager - Special Pricing Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Approval</template>
    </alerts>
    <alerts>
        <fullName>National_Manager_Special_Pricing_Rejection</fullName>
        <description>National Manager - Special Pricing Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Sales_Manager_Special_Pricing_Approval</fullName>
        <description>Sales Manager - Special Pricing Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Approval</template>
    </alerts>
    <alerts>
        <fullName>Sales_Manager_Special_Pricing_Rejection</fullName>
        <description>Sales Manager - Special Pricing Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Special_Pricing_Approval</fullName>
        <description>Special Pricing Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Director_of_Credit_and_Underwriting</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Underwriter</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Approval</template>
    </alerts>
    <alerts>
        <fullName>Special_Pricing_request_submission_National_Accounts</fullName>
        <description>Special Pricing request submission- National Accounts</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Request</template>
    </alerts>
    <alerts>
        <fullName>TAFS_Send_Email_Alert_Lead_Agreement_Ready</fullName>
        <description>Send Email Alert On Lead Agreement Ready</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TAFS_Lead_Agreement_Ready_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>TAFS_Send_Email_Alert_Lead_Application_Denied</fullName>
        <description>Send Email Alert On Lead Application Denied</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TAFS_Lead_Application_Denied_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Application_Processing_Completion_Update</fullName>
        <field>TAFS_Application_Processing_Completed__c</field>
        <literalValue>1</literalValue>
        <name>Application Processing Completion Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status_Approval</fullName>
        <field>Status</field>
        <literalValue>Pricing Approved</literalValue>
        <name>Lead Status Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status_Rejection</fullName>
        <description>Rejection of Special pricing request</description>
        <field>Status</field>
        <literalValue>Pricing Denied</literalValue>
        <name>Lead Status Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status_Update</fullName>
        <description>Update the Lead status when special pricing is approved</description>
        <field>Status</field>
        <literalValue>Pricing Approved</literalValue>
        <name>Lead Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status_Update_UW</fullName>
        <description>The Lead status needs to be updated when the Underwriter modifies the Rate, Annual Yield % or Estimated Monthly Volume for a Lead with an approved Special Pricing Rate</description>
        <field>Status</field>
        <literalValue>Pending Price Approval</literalValue>
        <name>Lead Status Update UW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status_Update_for_denied_approvals</fullName>
        <description>Update Lead status to Pending Pricing Approval for previously denied requests</description>
        <field>Status</field>
        <literalValue>Pending Price Approval</literalValue>
        <name>Lead Status Update-for denied approvals</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status_Update_for_previously_denied</fullName>
        <description>Updating the Lead status for re-sending denied approval requests</description>
        <field>Status</field>
        <literalValue>Pending Price Approval</literalValue>
        <name>Lead Status Update-for previously denied</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submit_for_Special_Pricing_Approval</fullName>
        <description>When the Rate is not 5%, leads should be submitted for special pricing rate approval</description>
        <field>Status</field>
        <literalValue>Pricing Negotiation</literalValue>
        <name>Submit for Special Pricing Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_ClearNotEligibleReason</fullName>
        <field>TAFS_Not_Eligible_Reason__c</field>
        <name>TAFS_ClearNotEligibleReason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Not_eligible_reason_cleared</fullName>
        <field>TAFS_Not_Eligible_Reason__c</field>
        <name>TAFS_Not_eligible_reason_cleared</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Status_new_update</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>TAFS_Status_new_update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_UpdateLeadOwnerToDirectSales</fullName>
        <field>OwnerId</field>
        <lookupValue>Direct_Sales_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TAFS_UpdateLeadOwnerToDirectSales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_UpdateLeadOwnerToNA</fullName>
        <field>OwnerId</field>
        <lookupValue>National_Accounts_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TAFS_UpdateLeadOwnerToNA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_UpdateLeadStatusToNE</fullName>
        <field>Status</field>
        <literalValue>Not Eligible</literalValue>
        <name>TAFS_UpdateLeadStatusToNE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_UpdateLeadStatusToNew</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>TAFS_UpdateLeadStatusToNew</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_UpdateLeadStatusToNotEligible</fullName>
        <field>Status</field>
        <literalValue>Not Eligible</literalValue>
        <name>TAFS_UpdateLeadStatusToNotEligible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_UpdateNEReason</fullName>
        <field>TAFS_Not_Eligible_Reason__c</field>
        <literalValue>No Authority</literalValue>
        <name>TAFS_UpdateNEReason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_UpdateNotEligibleReason</fullName>
        <field>TAFS_Not_Eligible_Reason__c</field>
        <literalValue>No Authority</literalValue>
        <name>TAFS_UpdateNotEligibleReason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Update_Company_Type_On_Lead</fullName>
        <field>TAFS_Company_Type__c</field>
        <literalValue>BROKER</literalValue>
        <name>Update Company Type On Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Update_Company_Type_to_Carrier</fullName>
        <field>TAFS_Company_Type__c</field>
        <literalValue>CARRIER</literalValue>
        <name>Update Company Type to Carrier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Update_Email_Dup_with_Email</fullName>
        <field>TAFS_Email_Dup__c</field>
        <formula>Email</formula>
        <name>Update Email Dup with Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Update_LeadOwner_To_DirectSalesQueu</fullName>
        <field>OwnerId</field>
        <lookupValue>Direct_Sales_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TAFS_Update_LeadOwner_To_DirectSalesQueu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Update_LeadOwner_To_NationAccQueue</fullName>
        <field>OwnerId</field>
        <lookupValue>National_Accounts_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TAFS_Update_LeadOwner_To_NationAccQueue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Agreement_In_field</fullName>
        <description>Update Agreement In field with date and time when lead status is changed to Agreement In f</description>
        <field>TAFS_Agreement_In__c</field>
        <formula>NOW()</formula>
        <name>Update Agreement In field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Agreement_Out_field</fullName>
        <description>Update Agreement Out field with date and time when lead status is changed to Agreement Out</description>
        <field>TAFS_Agreement_Out__c</field>
        <formula>NOW()</formula>
        <name>Update Agreement Out field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Agreement_Ready_field_with_date_a</fullName>
        <description>Update Agreement Ready with date and time when lead status is changed to Agreement Ready</description>
        <field>TAFS_Agreement_Ready__c</field>
        <formula>NOW()</formula>
        <name>Update Agreement Ready field with date a</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Application_Approved_field</fullName>
        <description>Update Application Approved field with date and time when lead status is changed to Application Approved</description>
        <field>TAFS_Application_Approved__c</field>
        <formula>NOW()</formula>
        <name>Update Application Approved field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Application_In_field</fullName>
        <description>Update Application In field with date and time when lead status is changed to Application In</description>
        <field>TAFS_Application_In__c</field>
        <formula>NOW()</formula>
        <name>Update Application In field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Application_Out_field</fullName>
        <description>Update Application Outfield with date and time when lead status is changed to Application Out</description>
        <field>TAFS_Application_Out__c</field>
        <formula>NOW()</formula>
        <name>Update Application Out field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Qualified_field_with_date_and_tim</fullName>
        <description>Update Qualified with date and time when lead status is changed to Qualified</description>
        <field>TAFS_Qualified__c</field>
        <formula>NOW()</formula>
        <name>Update Qualified field with date and tim</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Application Processing Completion Update</fullName>
        <actions>
            <name>Application_Processing_Completion_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Application Approved,Pricing Approved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Special Pricing Rate should be Approved</fullName>
        <actions>
            <name>Submit_for_Special_Pricing_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Application Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Rate__c</field>
            <operation>notEqual</operation>
            <value>5</value>
        </criteriaItems>
        <description>When the Rate is not 5%, it should be submitted for approval</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS Send Email Notification On Agreement Ready</fullName>
        <actions>
            <name>TAFS_Send_Email_Alert_Lead_Agreement_Ready</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Rule to send an email notification to lead owners when the lead status changes to agreement ready.</description>
        <formula>AND(     OR(ISBLANK(Owner:Queue.Id),ISNULL(Owner:Queue.Id)),     ISPICKVAL( Status , &quot;Agreement Ready&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS Send Email Notification On Application Denied</fullName>
        <actions>
            <name>TAFS_Send_Email_Alert_Lead_Application_Denied</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Rule to send an email notification to lead owners when the lead status changes to agreement ready.</description>
        <formula>AND(     OR(ISBLANK(Owner:Queue.Id),ISNULL(Owner:Queue.Id)),     ISPICKVAL( Status , &quot;Application Denied&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS Update Agreement In field with date and time</fullName>
        <actions>
            <name>Update_Agreement_In_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Agreement In</value>
        </criteriaItems>
        <description>workflow for Updating Agreement In field with  date and time when lead status is changed to Agreement In</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS Update Agreement Out field with date and time</fullName>
        <actions>
            <name>Update_Agreement_Out_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Agreement Out</value>
        </criteriaItems>
        <description>workflow for Updating Agreement Out field with  date and time when lead status is changed to Agreement Out</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS Update Agreement Ready field with date and time</fullName>
        <actions>
            <name>Update_Agreement_Ready_field_with_date_a</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Agreement Ready</value>
        </criteriaItems>
        <description>workflow for Updating Agreement Ready field with date and time when lead status is changed to Agreement Ready</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS Update Application Approved</fullName>
        <actions>
            <name>Update_Application_Approved_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Application Approved</value>
        </criteriaItems>
        <description>workflow for Updating Application Approved field with  date and time when lead status is changed to Application Approved</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS Update Application In field with date and time</fullName>
        <actions>
            <name>Update_Application_In_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Application In</value>
        </criteriaItems>
        <description>workflow for Updating Application In field with  date and time when lead status is changed to Application In</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS Update Application Out field with date and time</fullName>
        <actions>
            <name>Update_Application_Out_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Application Out</value>
        </criteriaItems>
        <description>workflow for Updating Application Out field with  date and time when lead status is changed to Application Out</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS Update Qualified field with date and time</fullName>
        <actions>
            <name>Update_Qualified_field_with_date_and_tim</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <description>workflow for Updating Qualified field with  date and time when lead status is changed to Qualified</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Assign_LeadOwner_To_DirectSalesQueue</fullName>
        <actions>
            <name>TAFS_Update_LeadOwner_To_DirectSalesQueu</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.TAFS_Lead_Type__c</field>
            <operation>equals</operation>
            <value>Direct Sales</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Assign_LeadOwner_To_NationalAccountsQueue</fullName>
        <actions>
            <name>TAFS_Update_LeadOwner_To_NationAccQueue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.TAFS_Lead_Type__c</field>
            <operation>equals</operation>
            <value>National Accounts</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Company_Type_On_Authority_Status</fullName>
        <actions>
            <name>TAFS_Update_Company_Type_On_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Lead.TAFS_Authority_Status_Broker__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Broker_Application_Pending__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>This rule evaluates the Autority Status (Broker) and Broker Application Pending field to populate Company Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Company_Type_To_Carrier</fullName>
        <actions>
            <name>TAFS_Update_Company_Type_to_Carrier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.TAFS_Authority_Status_Broker__c</field>
            <operation>notEqual</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Broker_Application_Pending__c</field>
            <operation>notEqual</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Authority_Status_Broker__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Broker_Application_Pending__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule evaluates the Autority Status (Broker) and Broker Application Pending field to populate Company Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Lead_AuthorityStatus%28Broker%29</fullName>
        <active>true</active>
        <formula>AND( TEXT(TAFS_Company_Type__c) =&quot;Broker&quot;, ISCHANGED( TAFS_Authority_Status_Broker__c ),  ISPICKVAL( PRIORVALUE( TAFS_Authority_Status_Broker__c ),&quot;Active&quot; ), ISPICKVAL( TAFS_Authority_Status_Broker__c , &quot;Inactive&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Nurture_Not_Interested_Reassignment_DS</fullName>
        <actions>
            <name>TAFS_Update_LeadOwner_To_DirectSalesQueu</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Nurture - Not Interested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Not Eligible</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Lead_Type__c</field>
            <operation>equals</operation>
            <value>Direct Sales</value>
        </criteriaItems>
        <description>When a lead is moved by a user to &quot;Nurture - Not Interested&quot; or &quot;Not Eligible&quot; status, the lead owner should be removed and assigned to DS queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Nurture_Not_Interested_Reassignment_NA</fullName>
        <actions>
            <name>TAFS_Update_LeadOwner_To_NationAccQueue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Nurture - Not Interested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Not Eligible</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Lead_Type__c</field>
            <operation>equals</operation>
            <value>National Accounts</value>
        </criteriaItems>
        <description>When a lead is moved by a user to &quot;Nurture - Not Interested&quot; or &quot;Not Eligible&quot; status, the lead owner should be removed and assigned to NA queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_UpdateLeadOwnerToNAForNELeads</fullName>
        <actions>
            <name>TAFS_UpdateLeadOwnerToNA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TAFS_UpdateLeadStatusToNE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TAFS_UpdateNEReason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>New,No Contact</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Authority_Status_Common__c</field>
            <operation>equals</operation>
            <value>None,Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Authority_Status_Contract__c</field>
            <operation>equals</operation>
            <value>None,Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Authority_Status_Broker__c</field>
            <operation>equals</operation>
            <value>None,Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Common_Application_Pending__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Broker_Application_Pending__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Contract_Application_Pending__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Lead_Type__c</field>
            <operation>equals</operation>
            <value>National Accounts</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_UpdateLeadOwnerforNonEligibleLeads</fullName>
        <actions>
            <name>TAFS_UpdateLeadOwnerToDirectSales</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TAFS_UpdateLeadStatusToNotEligible</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TAFS_UpdateNotEligibleReason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>New,No Contact</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Authority_Status_Common__c</field>
            <operation>equals</operation>
            <value>None,Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Authority_Status_Contract__c</field>
            <operation>equals</operation>
            <value>None,Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Authority_Status_Broker__c</field>
            <operation>equals</operation>
            <value>None,Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Common_Application_Pending__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Broker_Application_Pending__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Contract_Application_Pending__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Lead_Type__c</field>
            <operation>equals</operation>
            <value>Direct Sales</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_UpdateLeadStatusToNew</fullName>
        <actions>
            <name>TAFS_ClearNotEligibleReason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TAFS_UpdateLeadStatusToNew</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Not Eligible</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Not_Eligible_Reason__c</field>
            <operation>equals</operation>
            <value>No Authority</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Authority_Status_Contract__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Authority_Status_Broker__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Authority_Status_Common__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Update_Lead_Email</fullName>
        <actions>
            <name>TAFS_Update_Email_Dup_with_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_phone_email</fullName>
        <actions>
            <name>TAFS_Status_new_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (( TEXT( Status) = &quot;Incorrect Info&quot;),OR( ISCHANGED( Phone ), ISCHANGED( Email )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_status_update</fullName>
        <actions>
            <name>TAFS_Not_eligible_reason_cleared</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TAFS_Status_new_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND( 3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Not Eligible</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Not_Eligible_Reason__c</field>
            <operation>equals</operation>
            <value>No Authority</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Authority_Status_Contract__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Authority_Status_Broker__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TAFS_Authority_Status_Common__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Updates to Approved Special Pricing</fullName>
        <actions>
            <name>Lead_Status_Update_UW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule updates the Lead Status if the Rate related fields are updated by an Underwriter post Special pricing Approval</description>
        <formula>AND(TAFS_Rate__c &lt;&gt; 5 , ISPICKVAL(Status,&quot;Pricing Approved&quot;) , OR($Profile.Name = &apos;TAFS_Director_of_Credit_And_Underwriting&apos;, $Profile.Name = &apos;TAFS_Credit_And_Underwriting_Analyst&apos;,  $Profile.Name = &apos;TAFS_Audit_And_Underwriting_Analyst&apos;,  $Profile.Name = &apos;TAFS_Underwriter&apos;),  OR(ISCHANGED( TAFS_Rate__c ),  ISCHANGED( TAFS_Estimated_Monthly_Volume__c ),  ISCHANGED( TAFS_Fuel_Commitment__c ),  ISCHANGED( TAFS_Hard_Reserves__c ),  ISCHANGED( TAFS_Turn_Days__c ),  ISCHANGED( TAFS_Net_Fuel_Rebate__c ),  ISCHANGED( TAFS_Reserve__c ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
