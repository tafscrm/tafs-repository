<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CFO_Special_Pricing_Approval_for_Agreement_Renegotiation</fullName>
        <description>CFO - Special Pricing Approval for Agreement Renegotiation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Renegotiation_Approval</template>
    </alerts>
    <alerts>
        <fullName>CFO_Special_Pricing_Rejection_for_Agreement_Renegotiation</fullName>
        <description>CFO - Special Pricing Rejection for Agreement Renegotiation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Client_Relationship_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Client_Relationship_Supervisor</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Vice_President_and_General_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Renegotiation_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Client_Relationship_Manager_Special_Pricing_Approval_for_Agreement_Renegotiation</fullName>
        <description>Client Relationship Manager - Special Pricing Approval for Agreement Renegotiation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Renegotiation_Approval</template>
    </alerts>
    <alerts>
        <fullName>Client_Relationship_Manager_Special_Pricing_Rejection_for_Agreement_Renegotiatio</fullName>
        <description>Client Relationship Manager - Special Pricing Rejection for Agreement Renegotiation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Client_Relationship_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Client_Relationship_Supervisor</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Renegotiation_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Client_Relationship_Supervisor_Special_Pricing_Approval_for_Agreement_Renegotiat</fullName>
        <description>Client Relationship Supervisor - Special Pricing Approval for Agreement Renegotiation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Renegotiation_Approval</template>
    </alerts>
    <alerts>
        <fullName>Client_Relationship_Supervisor_Special_Pricing_Rejection_for_Agreement_Renegotia</fullName>
        <description>Client Relationship Supervisor - Special Pricing Rejection for Agreement Renegotiation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Renegotiation_Rejection</template>
    </alerts>
    <alerts>
        <fullName>General_Manager_Special_Pricing_Approval_for_Agreement_Renegotiation</fullName>
        <description>General Manager - Special Pricing Approval for Agreement Renegotiation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Renegotiation_Approval</template>
    </alerts>
    <alerts>
        <fullName>General_Manager_Special_Pricing_Rejection_for_Agreement_Renegotiation</fullName>
        <description>General Manager - Special Pricing Rejection for Agreement Renegotiation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Client_Relationship_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Client_Relationship_Supervisor</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Renegotiation_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Special_Pricing_Approval_for_Renegotiated_Agreements</fullName>
        <description>Special Pricing Approval for Renegotiated Agreements</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Director_of_Credit_and_Underwriting</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Underwriter</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Special_Pricing_Renegotiation_Approval</template>
    </alerts>
    <fieldUpdates>
        <fullName>Agreement_Status_Approval</fullName>
        <field>TAFS_Status__c</field>
        <literalValue>Pricing Approved</literalValue>
        <name>Agreement Status Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Agreement_Status_Rejection</fullName>
        <field>TAFS_Status__c</field>
        <literalValue>Pricing Denied</literalValue>
        <name>Agreement Status Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Agreement_Status_Update_UW</fullName>
        <description>The Agreement status needs to be updated when the Underwriter modifies the Rate, Annual Yield % or Estimated Monthly Volume for a Lead with an approved Special Pricing Rate</description>
        <field>TAFS_Status__c</field>
        <literalValue>Pending Pricing Approval</literalValue>
        <name>Agreement Status Update UW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Agreement_Status_for_denied_approvals</fullName>
        <description>pdate Agreement status to Pending Pricing Approval for previously denied requests</description>
        <field>TAFS_Status__c</field>
        <literalValue>Pending Pricing Approval</literalValue>
        <name>Agreement Status-for denied approvals</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submit_for_Renegotiated_Pricing_Approval</fullName>
        <description>When the Rate is not 5%, Agreements should be submitted for special pricing rate approval</description>
        <field>TAFS_Status__c</field>
        <literalValue>Pending Pricing Approval</literalValue>
        <name>Submit for Renegotiated Pricing Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Special Pricing Renegotiated Rate should be Approved</fullName>
        <actions>
            <name>Submit_for_Renegotiated_Pricing_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TAFS_Agreement__c.TAFS_Status__c</field>
            <operation>equals</operation>
            <value>Pricing Negotiation</value>
        </criteriaItems>
        <criteriaItems>
            <field>TAFS_Agreement__c.TAFS_Rate__c</field>
            <operation>notEqual</operation>
            <value>5</value>
        </criteriaItems>
        <description>When the Rate is not 5%, it should be submitted for approval</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Updates to Approved Special Pricing Agreements</fullName>
        <actions>
            <name>Agreement_Status_Update_UW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule updates the Agreement Status if the Rate related fields are updated by an Underwriter post Special pricing Approval</description>
        <formula>AND(TAFS_Rate__c &lt;&gt; 5 , ISPICKVAL( TAFS_Status__c ,&quot;Pricing Approved&quot;) , OR($Profile.Name = &apos;TAFS_Director_of_Credit_And_Underwriting&apos;, $Profile.Name = &apos;TAFS_Credit_And_Underwriting_Analyst&apos;, $Profile.Name = &apos;TAFS_Audit_And_Underwriting_Analyst&apos;, $Profile.Name = &apos;TAFS_Underwriter&apos;), OR(ISCHANGED( TAFS_Rate__c ), ISCHANGED( TAFS_Estimated_Monthly_Volume__c ), ISCHANGED( TAFS_Fuel_Commitment__c ), ISCHANGED( TAFS_Hard_Reserves__c ), ISCHANGED( TAFS_Turn_Days__c ), ISCHANGED( TAFS_Net_Fuel_Rebate__c ), ISCHANGED( TAFS_Reserve__c ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
