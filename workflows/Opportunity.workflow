<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">

    <fieldUpdates>
        <fullName>TAFS_POStatus_LastUpdateDate_FieldUpdate</fullName>
        <description>This field Update is used to update &apos;PO Status Last Update&apos; whenever a &apos;PO Status&apos; is changed</description>
        <field>TAFS_PO_Status_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>PO Status Last Update Date Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TAFS_TireProgram_Opportunity_PO_Status_LastUpdate_Date</fullName>
        <actions>
            <name>TAFS_POStatus_LastUpdateDate_FieldUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is used to date at which the PO status is last updated</description>
        <formula>ISCHANGED( TAFS_PO_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
