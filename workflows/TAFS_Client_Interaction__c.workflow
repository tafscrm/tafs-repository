<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TAFS_Update_Last_Success_Check_In_Date</fullName>
        <field>TAFS_Last_Successful_Check_In_Date__c</field>
        <formula>TODAY()</formula>
        <name>TAFS Update Last Success Check In Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>TAFS_Client_Name__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>TAFS Update Last Success Check In Date</fullName>
        <actions>
            <name>TAFS_Update_Last_Success_Check_In_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4</booleanFilter>
        <criteriaItems>
            <field>TAFS_Client_Interaction__c.TAFS_Tools_Discussed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>TAFS_Client_Interaction__c.TAFS_Find_Out_More_Discussed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>TAFS_Client_Interaction__c.TAFS_Account_Reviewed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>TAFS_Client_Interaction__c.TAFS_Sales_Initiative_Discussed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Given a new Client Interaction is created, when any item is checked from the fields  (Tools Discussed, Account Reviewed, Find Out More Discussed, Sales Initiative Discussed) then update the Account Field &quot;Last Client Check In&quot; to today&apos;s date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
