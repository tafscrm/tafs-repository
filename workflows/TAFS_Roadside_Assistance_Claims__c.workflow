<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TAFS_Update_Approximate_Savings_Field</fullName>
        <field>TAFS_Approximate_Savings__c</field>
        <formula>IF(TAFS_Service_Type__c = &quot;Fuel/Fluids&quot; ,$Setup.TAFS_RA_Approximate_Savings__c.TAFS_Fuel_Fluids_Savings__c , IF(TAFS_Service_Type__c = &quot;Jump Start&quot; ,$Setup.TAFS_RA_Approximate_Savings__c.TAFS_Jump_Start_Savings__c,IF(TAFS_Service_Type__c = &quot;Lockout&quot; ,$Setup.TAFS_RA_Approximate_Savings__c.TAFS_Lockout_Savings__c,IF(TAFS_Service_Type__c= &quot;Mobile Mechanic&quot; ,$Setup.TAFS_RA_Approximate_Savings__c.TAFS_Mobile_Mechanic_savings__c,IF(TAFS_Service_Type__c= &quot;Tire Change&quot; ,$Setup.TAFS_RA_Approximate_Savings__c.TAFS_Tire_Change_Savings__c,IF(TAFS_Service_Type__c= &quot;Towing&quot; ,$Setup.TAFS_RA_Approximate_Savings__c.TAFS_Towing_savings__c,IF(TAFS_Service_Type__c= &quot;Winching&quot; ,$Setup.TAFS_RA_Approximate_Savings__c.Tafs_Winching_savings__c,IF(TAFS_Service_Type__c= &quot;Tire Replacement&quot; ,$Setup.TAFS_RA_Approximate_Savings__c.TAFS_Tire_Replacement_Savings__c,NUll))))))))</formula>
        <name>Update Approximate Savings Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unique_field_on_Roadside_Claims</fullName>
        <field>Vehicle_ServiceType_And_Date_Combination__c</field>
        <formula>TAFS_Vehicle__c+TAFS_Service_Type__c+TEXT( TAFS_Date_of_Claim__c )</formula>
        <name>Update Unique field on Roadside Claims</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TAFS_UpdateApproximateSavingsBasedOnServiceType</fullName>
        <actions>
            <name>TAFS_Update_Approximate_Savings_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TAFS_Roadside_Assistance_Claims__c.TAFS_Service_Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Unique field on Roadside Claims</fullName>
        <actions>
            <name>Update_Unique_field_on_Roadside_Claims</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TAFS_Roadside_Assistance_Claims__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
