<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>TAFS_Send_Email_Alert_On_Client_Setup</fullName>
        <description>Send Email Alert On Client Setup</description>
        <protected>false</protected>
        <recipients>
            <field>TAFS_Sales_Account_Manager_Lookup__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TAFS_Client_Setup_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>TAFS_Client_Status_To_ActiveGoodStanding</fullName>
        <description>Update client status to Active - Good Standing</description>
        <field>TAFS_Client_Status__c</field>
        <literalValue>Active - Good Standing</literalValue>
        <name>Client Status To Active - Good Standing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Client_Status_To_FreezeNoAuthority</fullName>
        <description>Update client status to Freeze - No Authority</description>
        <field>TAFS_Client_Status__c</field>
        <literalValue>Freeze - No Authority</literalValue>
        <name>Client Status To Freeze - No Authority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Client_Status_To_FreezeNoInsurance</fullName>
        <description>Update the client status to Freeze - No Insurance</description>
        <field>TAFS_Client_Status__c</field>
        <literalValue>Freeze - No Insurance</literalValue>
        <name>Client Status To Freeze - No Insurance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Update</fullName>
        <field>TAFS_Client_Status__c</field>
        <literalValue>RS Assignment</literalValue>
        <name>TAFS Update Client Status To RS Assignme</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Update_Awaiting_First_Funding_Date</fullName>
        <field>TAFS_Awaiting_First_Funding_Date__c</field>
        <formula>TODAY()</formula>
        <name>TAFS Update Awaiting First Funding Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Update_Client_Welcome_Field</fullName>
        <field>TAFS_Client_Welcome_Date__c</field>
        <formula>TODAY()</formula>
        <name>TAFS Update Client Welcome Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TAFS_Update_NoBuy_To_Checked</fullName>
        <description>Update NoBuy To Checked</description>
        <field>TAFS_No_Buy__c</field>
        <literalValue>1</literalValue>
        <name>Update NoBuy To Checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Funded_Checkbox</fullName>
        <field>TAFS_Funded__c</field>
        <literalValue>1</literalValue>
        <name>Update Funded Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Client Welcome Date</fullName>
        <actions>
            <name>TAFS_Update_Client_Welcome_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.TAFS_Client_Status__c</field>
            <operation>equals</operation>
            <value>Client Welcome</value>
        </criteriaItems>
        <description>Populates &quot;Client Welcome Date&quot; field with today&apos;s date when an Account is edited to Client Status = &quot;Client Welcome&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS Awaiting First Funding Date</fullName>
        <actions>
            <name>TAFS_Update_Awaiting_First_Funding_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.TAFS_Client_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting First Funding</value>
        </criteriaItems>
        <description>Populates &quot;Awaiting First Funding Date&quot; field with today&apos;s date when an Account is edited to Client Status = &quot;Awaiting First Funding&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS First Funding Date Created Updated</fullName>
        <actions>
            <name>TAFS_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Funded_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK(TAFS_First_Funding_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Client_Authority_Status_Broker_Active</fullName>
        <actions>
            <name>TAFS_Client_Status_To_ActiveGoodStanding</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Given Company Type = Broker, When Authority Status (Broker) goes from Inactive to Active, then Client Status = Active - Good Standing</description>
        <formula>AND( ISPICKVAL( TAFS_Company_Type__c ,&apos;BROKER&apos;) ,  ISPICKVAL(  TAFS_Authority_Status_Broker__c ,&apos;Active&apos;), ISPICKVAL(PRIORVALUE(TAFS_Authority_Status_Broker__c),&apos;Inactive&apos;),ISPICKVAL(TAFS_Client_Status__c,&apos;Freeze - No Authority&apos;)   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Client_Authority_Status_Broker_Inactive</fullName>
        <actions>
            <name>TAFS_Client_Status_To_FreezeNoAuthority</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Given Company Type = Broker, When Authority Status (Broker) goes from Active to Inactive, then Client Status = Freeze - No Authority</description>
        <formula>AND( ISPICKVAL( TAFS_Company_Type__c ,&apos;BROKER&apos;) ,  ISPICKVAL( TAFS_Authority_Status_Broker__c ,&apos;Inactive&apos;), ISPICKVAL(PRIORVALUE(TAFS_Authority_Status_Broker__c),&apos;Active&apos;),NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;Terminated&apos;)), NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;Write Off Balance&apos;)), NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;Client Created&apos;)), NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;Client Welcome&apos;)), NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;Awaiting First Funding&apos;)), NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;RS Assignment&apos;))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Client_Authority_Status_Carrirer_Active</fullName>
        <actions>
            <name>TAFS_Client_Status_To_ActiveGoodStanding</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Given Company Type = Carrier, When Authority Status (Contract) OR Authority Status (Common) goes from Inactive to Active, then Client Status = Active - Good Standing</description>
        <formula>AND( ISPICKVAL( TAFS_Company_Type__c ,&apos;CARRIER&apos;) ,  OR(  AND(ISPICKVAL( TAFS_Authority_Status_Contract__c ,&apos;Active&apos;), ISPICKVAL(PRIORVALUE(TAFS_Authority_Status_Contract__c),&apos;Inactive&apos;) ),  AND(ISPICKVAL( TAFS_Authority_Status_Common__c ,&apos;Active&apos;),  ISPICKVAL(PRIORVALUE(TAFS_Authority_Status_Common__c ),&apos;Inactive&apos;))), ISPICKVAL(TAFS_Client_Status__c,&apos;Freeze - No Authority&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Client_Authority_Status_Carrirer_Inactive</fullName>
        <actions>
            <name>TAFS_Client_Status_To_FreezeNoAuthority</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Given Company Type = Carrier, When Authority Status (Contract) OR Authority Status (Common) goes from Active to Inactive, then Client Status = Freeze - No Authority</description>
        <formula>AND( ISPICKVAL( TAFS_Company_Type__c ,&apos;CARRIER&apos;) ,   OR( AND(ISPICKVAL( TAFS_Authority_Status_Contract__c ,&apos;Inactive&apos;), ISPICKVAL(PRIORVALUE(TAFS_Authority_Status_Contract__c),&apos;Active&apos;) ), AND(ISPICKVAL( TAFS_Authority_Status_Common__c ,&apos;Inactive&apos;),  ISPICKVAL(PRIORVALUE(TAFS_Authority_Status_Common__c ),&apos;Active&apos;))), NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;Terminated&apos;)),  NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;Write Off Balance&apos;)),  NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;Client Created&apos;)),  NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;Client Welcome&apos;)),  NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;Awaiting First Funding&apos;)),  NOT(ISPICKVAL( TAFS_Client_Status__c , &apos;RS Assignment&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_NoBuy_Should_Be_Checked</fullName>
        <actions>
            <name>TAFS_Update_NoBuy_To_Checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Client Status changed and new status contains Client Status = Freeze - No Insurance, Freeze - No Authority, Freeze - Out of Service then &apos;No Buy&apos; should be checked</description>
        <formula>IF(OR(ISPICKVAL(TAFS_Client_Status__c,&apos;Freeze - No Authority&apos;),  ISPICKVAL(TAFS_Client_Status__c,&apos;Freeze - No Insurance&apos;),  ISPICKVAL(TAFS_Client_Status__c,&apos;Freeze - Out of Service&apos;)), true,false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Send_Email_Notification_On_Client_Setup</fullName>
        <actions>
            <name>TAFS_Send_Email_Alert_On_Client_Setup</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(OR(PRIORVALUE( TAFS_Reference_Id__c)= &quot;&quot;,PRIORVALUE( TAFS_Reference_Id__c)= null),OR(TAFS_Reference_Id__c!=null,TAFS_Reference_Id__c!=&quot;&quot;)),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Update_Client_Status_To_Active_Good_Standing</fullName>
        <actions>
            <name>TAFS_Client_Status_To_ActiveGoodStanding</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.TAFS_Client_Status__c</field>
            <operation>equals</operation>
            <value>Freeze - No Insurance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.TAFS_BIPD_Insurance_Covered__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Update the client status to Active - Good Standing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TAFS_Update_Client_Stauts_To_Freeze_No_Insurance</fullName>
        <actions>
            <name>TAFS_Client_Status_To_FreezeNoInsurance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.TAFS_Company_Type__c</field>
            <operation>equals</operation>
            <value>CARRIER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.TAFS_Client_Status__c</field>
            <operation>notEqual</operation>
            <value>Freeze - No Authority,Freeze - Out of Service,Terminated,Write Off Balance,Client Created,Client Welcome,Awaiting First Funding,RS Assignment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.TAFS_BIPD_Insurance_Required__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.TAFS_BIPD_Insurance_Covered__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <description>Update the client status to Freeze - No Insurance</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
